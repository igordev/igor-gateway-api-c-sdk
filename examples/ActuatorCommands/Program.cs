﻿using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Apis.Actuators;
using Igor.Gateway.Dtos.Actuators;

namespace ActuatorCommands
{
    internal class Program
    {
        private static async Task Main(string[] args)
        {
            var actuatorService = new ActuatorService("[your API key here]");

            await SendRawDataCommand(actuatorService);

            await SendStringCommand(actuatorService);

            await SendPercentageCommand(actuatorService);

            await SendBinaryCommand(actuatorService);

            await SendRangeCommand(actuatorService);

            await SendDiscreteCommand(actuatorService);

            await SendCustomCommand(actuatorService);
        }

        // Use the NativeActuatorCommandParametersDto to send raw byte data
        // to your device
        private static async Task SendRawDataCommand(IActuatorService actuatorService)
        {
            // Passthrough16 max value length is 1000
            var myData = new byte[] { 1, 2, 3, 4, 10, 25, 100, 150, 250, 255 };

            var commandParameters = new NativeActuatorCommandParametersDto
            {
                CommandType = "Passthrough16",
                Value = myData
            };

            // "commandCollectionName" is an optional parameter, however for better performance
            // you should specify it when executing. If your device is connected
            // to an Igor node via RS-485 or USB, the command collection name is typically
            // "Serial1" or "Usb1". If you are unsure, you can navigate to the actuator
            // detail page in your Igor Gateway. The command collection name is in the header
            // for the supported commands in the format: "Commands for XXXXXX", where XXXXXX
            // is Serial 1 or Usb 1 in this example. Be sure to remove spaces when providing
            // it to the API.
            await actuatorService.CommandNative(1, commandParameters, "Usb1");
        }

        // If your device supports native string commands, you can use the
        // CommandString method as a simple way to send string commands to your device.
        //
        //  At the time of writing, the supported encoding types for strings are:
        // - ASCII
        // - Unicode
        // - UTF8 (default)
        private static async Task SendStringCommand(IActuatorService actuatorService)
        {
            await actuatorService.CommandString(1, "my string data", ActuatorStringEncoding.Unicode, "Serial1");
        }

        // Send percentage values. Valid range is 0 - 10000
        private static async Task SendPercentageCommand(IActuatorService actuatorService)
        {
            await actuatorService.CommandPercentage(1, 1000);
        }

        // Send binary values.
        //
        // Binary values are either 1 or 0. The CommandBinary
        // helper method requires you pass 'true' or 'false'. true = 1, false = 0
        private static async Task SendBinaryCommand(IActuatorService actuatorService)
        {
            await actuatorService.CommandBinary(1, true);
        }

        // Send range values.
        //
        // Valid values for ranged commands are relative to the device being controlled
        // and the range of values it provided the Igor Gateway upon discovery.
        //
        // In this example, we're sending the value of 100. As long as the min/max
        // values for the range command include the value 100, the API would accept this
        // command.
        private static async Task SendRangeCommand(IActuatorService actuatorService)
        {
            await actuatorService.CommandRange(1, 100);
        }

        // Send discrete values.
        //
        // Valid values for discrete commands are relative to the device being controlled
        // and the discrete values it provided the Igor Gateway upon discovery.
        //
        // In this example, we're sending the value of "Down". The valid values for 
        // a discrete command could be anything from a numeric value to a string.
        private static async Task SendDiscreteCommand(IActuatorService actuatorService)
        {
            await actuatorService.CommandDiscrete(1, "Down");
        }

        // Send custom values to custom commands.
        //
        // The Igor Gateway allows actuators to be created with completely custom
        // commands that do not need to be of type Binary, Percentage, Ranged, Discrete,
        // Passthrough8, or Passthrough16.
        //
        // In this example, we're calling a custom command with a value that command
        // supports.
        private static async Task SendCustomCommand(IActuatorService actuatorService)
        {
            var commandParameters = new NativeActuatorCommandParametersDto
            {
                CommandType = "channel-control",
                Value = "up"
            };

            await actuatorService.CommandNative(1, commandParameters, "MyCustomCommandCollection");
        }
    }
}
