﻿using System;
using Igor.Gateway.Dtos.WebHooks;
using Microsoft.AspNetCore.Mvc;

namespace WebHookListener.Controllers
{
    // http://localhost:5000/api/receiver

    [Route("api/[controller]")]
    [ApiController]
    public class ReceiverController : ControllerBase
    {
        [HttpPost]
        public void Post([FromBody] WebHookEventDto dto)
        {
            if (!(dto?.Notifications?.Count > 0)) return;

            foreach (var notification in dto.Notifications)
            {
                if (notification.Action == "Space State Changed")
                    Console.WriteLine($"Space Changed Event - State: {notification.Value} ");
            }
        }
    }
}
