IF /I "%APPVEYOR_REPO_BRANCH%"=="master" (
    call dotnet pack
) ELSE IF /I "%APPVEYOR_REPO_BRANCH%"=="develop" (
    call dotnet pack --version-suffix "prerelease%APPVEYOR_BUILD_NUMBER%"
) ELSE (
    call dotnet pack --version-suffix "prerelease%APPVEYOR_REPO_BRANCH%%APPVEYOR_BUILD_NUMBER%"
)