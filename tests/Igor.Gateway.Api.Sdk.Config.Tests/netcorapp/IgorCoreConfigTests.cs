﻿#if NETCOREAPP2_0
using Microsoft.Extensions.Configuration;
using Xunit;

namespace Igor.Gateway.Api.Sdk.Config.Tests.netcorapp
{
    public class IgorCoreConfigTests
    {
        private readonly IConfigurationRoot _configuration;
        private readonly IgorCoreConfig _sdkConfig;

        public IgorCoreConfigTests()
        {
            _configuration = new ConfigurationBuilder()
                .AddInMemoryCollection()
                .Build();

            _sdkConfig = new IgorCoreConfig(_configuration);
        }

        [Fact]
        public void TryGetStringShouldReturnConfiguredString()
        {
            const string expected = "ApiAddress";

            _configuration[ConfigConstants.ApiAddressIndex] = expected;

            var actual = _sdkConfig.ApiAddress;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void ApiKeyShouldReturnConfiguredString()
        {
            const string expected = "ApiKey";

            _configuration[ConfigConstants.ApiKeyIndex] = expected;

            var actual = _sdkConfig.ApiKey;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void NotConfiguredValuesShouldReturnNull()
        {
            const string expected = "";

            _configuration[ConfigConstants.ApiKeyIndex] = expected;
            _configuration[ConfigConstants.ApiAddressIndex] = expected;

            var keyActual = _sdkConfig.ApiKey;
            var addressActual = _sdkConfig.ApiAddress;

            Assert.Equal(expected, keyActual);
            Assert.Equal(expected, addressActual);
        }
    }
}
#endif