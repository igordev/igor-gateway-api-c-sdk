﻿#if NET461
using System;
using System.Configuration;
using Xunit;

namespace Igor.Gateway.Api.Sdk.Config.Tests.net461
{
    public class IgorNetConfigTests : IDisposable
    {
        private readonly IgorNetConfig _sdkConfig;

        public IgorNetConfigTests()
        {
            _sdkConfig = new IgorNetConfig();
        }

        [Fact]
        public void ApiAddressShouldReturnConfiguredString()
        {
            const string expected = "ApiAddress";

            SetSettings(ConfigConstants.ApiAddressIndex, expected);

            var actual = _sdkConfig.ApiAddress;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void ApiKeyShouldReturnConfiguredString()
        {
            const string expected = "ApiKey";

            SetSettings(ConfigConstants.ApiKeyIndex, expected);

            var actual = _sdkConfig.ApiKey;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void NotConfiguredValuesShouldReturnNull()
        {
            const string expected = "";

            SetSettings(ConfigConstants.ApiKeyIndex, expected);
            SetSettings(ConfigConstants.ApiAddressIndex, expected);

            var keyActual = _sdkConfig.ApiKey;
            var addressActual = _sdkConfig.ApiAddress;

            Assert.Equal(expected, keyActual);
            Assert.Equal(expected, addressActual);
        }

        public void Dispose()
        {
            ResetConfig();
        }

        private void SetSettings(string key, string value)
        {
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.AppSettings.Settings.Add(key, value);
            config.Save(ConfigurationSaveMode.Full);
            ConfigurationManager.RefreshSection("appSettings");
        }

        private void ResetConfig()
        {
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.ConnectionStrings.ConnectionStrings.Clear();
            config.AppSettings.Settings.Clear();
            config.Save(ConfigurationSaveMode.Full);
            ConfigurationManager.RefreshSection("appSettings");
        }
    }
}
#endif