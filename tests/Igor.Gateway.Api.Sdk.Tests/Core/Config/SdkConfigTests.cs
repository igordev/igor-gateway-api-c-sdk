﻿using Igor.Gateway.Api.Sdk.Core.Config;
using Xunit;

namespace Igor.Gateway.Api.Sdk.Tests.Core.Config
{
    public class SdkConfigTests
    {
        [Fact]
        public void TestShouldSetApiAddressToLocalhost()
        {
            const string expected = "http://localhost";

            var config = new SdkConfig();

            Assert.Equal((string)expected, (string)config.ApiAddress);
        }

        [Fact]
        public void TestShouldSetApiAddressToValue()
        {
            const string expected = "ApiAddress";

            var config = new SdkConfig();

            config.ApiAddress = expected;

            Assert.Equal((string)expected, (string)config.ApiAddress);
        }
    }
}
