﻿using Igor.Gateway.Api.Sdk.Core.Http.Security;
using Igor.Gateway.Api.Sdk.Core.Serialization;
using Xunit;

namespace Igor.Gateway.Api.Sdk.Tests.Core.Http.Security
{
    public class JwtHeaderTests
    {
        [Fact]
        public void TestShouldSerializeHeaderWithCorrectPropertyNamesAndValues()
        {
            var expected = "{\"typ\":\"jwt\",\"alg\":\"HS256\"}";

            var serializer = new JsonSerializer();

            var header = new JwtHeader();

            var actual = serializer.Serialize(header);

            Assert.Equal(expected, actual);
        }
    }
}
