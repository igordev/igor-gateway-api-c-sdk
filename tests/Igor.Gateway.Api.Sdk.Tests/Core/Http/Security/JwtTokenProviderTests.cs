﻿using Igor.Gateway.Api.Sdk.Core.Http.Security;
using Moq;
using Xunit;

namespace Igor.Gateway.Api.Sdk.Tests.Core.Http.Security
{
    public class JwtTokenProviderTests
    {
        [Fact]
        public void TestGetTokenShouldReturnValidJwt()
        {
            const string expectedHeader = "eyJ0eXAiOiJqd3QiLCJhbGciOiJIUzI1NiJ9";
            const string expectedPayload = "eyJpYXQiOiIyMDAxLTAyLTAzVDA0OjA1OjA2IiwianRpIjowfQ";
            const string expectedSignature = "Il5kMD7QaxPM39AbjI84_qIwSCFQh4dHq1oDCf76LLk";
            const string expected = "eyJ0eXAiOiJqd3QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOiIyMDAxLTAyLTAzVDA0OjA1OjA2IiwianRpIjowfQ.Il5kMD7QaxPM39AbjI84_qIwSCFQh4dHq1oDCf76LLk";

            var headerProviderMock = new Mock<IJwtHeaderProvider>();
            var payloadProviderMock = new Mock<IJwtPayloadProvider>();
            var signatureProviderMock = new Mock<IJwtSignatureProvider>();

            headerProviderMock
                .Setup(p => p.GetEncodedHeader())
                .Returns(expectedHeader);

            payloadProviderMock
                .Setup(p => p.GetEncodedPayload())
                .Returns(expectedPayload);

            signatureProviderMock
                .Setup(p => p.Sign(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(expectedSignature);

            var provider = new JwtTokenProvider(
                headerProviderMock.Object,
                payloadProviderMock.Object,
                signatureProviderMock.Object
                );

            var actual = provider.GetToken("key");

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TestShouldReturnNull()
        {
            var headerProviderMock = new Mock<IJwtHeaderProvider>();
            var payloadProviderMock = new Mock<IJwtPayloadProvider>();
            var signatureProviderMock = new Mock<IJwtSignatureProvider>();

            var provider = new JwtTokenProvider(
                headerProviderMock.Object,
                payloadProviderMock.Object,
                signatureProviderMock.Object
            );

            var actual = provider.GetToken("");

            Assert.Null(actual);
        }
    }
}
