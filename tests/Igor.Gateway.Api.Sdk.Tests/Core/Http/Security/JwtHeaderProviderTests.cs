﻿using Igor.Gateway.Api.Sdk.Core.Http.Security;
using Igor.Gateway.Api.Sdk.Core.Serialization;
using Xunit;

namespace Igor.Gateway.Api.Sdk.Tests.Core.Http.Security
{
    public class JwtHeaderProviderTests
    {
        [Fact]
        public void TestGetHeaderShouldGenerateValidEncodedString()
        {
            const string expected = "eyJ0eXAiOiJqd3QiLCJhbGciOiJIUzI1NiJ9";

            var provider = new JwtHeaderProvider(new JsonSerializer());

            var actual = provider.GetEncodedHeader();

            Assert.Equal(expected, actual);
        }
    }
}
