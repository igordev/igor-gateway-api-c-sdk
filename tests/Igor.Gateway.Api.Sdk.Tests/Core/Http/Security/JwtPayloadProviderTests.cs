﻿using System;
using Igor.Gateway.Api.Sdk.Core;
using Igor.Gateway.Api.Sdk.Core.Http.Security;
using Igor.Gateway.Api.Sdk.Core.Serialization;
using Moq;
using Xunit;

namespace Igor.Gateway.Api.Sdk.Tests.Core.Http.Security
{
    public class JwtPayloadProviderTests
    {
        [Fact]
        public void TestGetPayloadShouldGetValidEncodedString()
        {
            var dateTimeMock = new Mock<IDateTimeWrapper>();
            var guidMock = new Mock<IGuidWrapper>();
            
            var utcDateTime = new DateTime(2001, 2, 3, 4, 5, 6);
            var guid = new Guid("00000000-0000-0000-0000-000000000001");

            const string expected = "eyJpYXQiOjk4MTE3MzEwNiwianRpIjowfQ";

            dateTimeMock
                .Setup(p => p.GetUtcNow())
                .Returns(utcDateTime);

            guidMock
                .Setup(p => p.NewGuid())
                .Returns(guid);

            var provider = new JwtPayloadProvider(new JsonSerializer(), dateTimeMock.Object, guidMock.Object);

            var actual = provider.GetEncodedPayload();

            Assert.Equal(expected, actual);
        }
    }
}
