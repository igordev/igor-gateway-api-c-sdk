﻿using System;
using Igor.Gateway.Api.Sdk.Core;
using Igor.Gateway.Api.Sdk.Core.Http.Security;
using Igor.Gateway.Api.Sdk.Core.Serialization;
using Moq;
using Xunit;

namespace Igor.Gateway.Api.Sdk.Tests.Core.Http.Security
{
    public class JwtPayloadTests
    {
        [Fact]
        public void TestShouldSerializePayloadWithCorrectPropertyNames()
        {
            var dateTimeMock = new Mock<IDateTimeWrapper>();
            var guidMock = new Mock<IGuidWrapper>();

            var utcDateTime = new DateTime(2001, 2, 3, 4, 5, 6);
            var guid = new Guid("12345678-4321-1234-6789-123456789ABC");

            const string expected = "{\"iat\":981173106,\"jti\":1311747200790058616}";

            var serializer = new JsonSerializer();

            dateTimeMock
                .Setup(p => p.GetUtcNow())
                .Returns(utcDateTime);

            guidMock
                .Setup(p => p.NewGuid())
                .Returns(guid);

            var payload = new JwtPayload(dateTimeMock.Object, guidMock.Object);

            var actual = serializer.Serialize(payload);

            Assert.Equal(expected, actual);
        }
    }
}
