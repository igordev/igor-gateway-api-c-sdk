﻿using Igor.Gateway.Api.Sdk.Core.Http.Security;
using Xunit;

namespace Igor.Gateway.Api.Sdk.Tests.Core.Http.Security
{
    public class JwtSignatureProviderTests
    {
        [Fact]
        public void TestSignShouldReturnValidEncodedSignature()
        {
            const string headerPayload = "header.payload";
            const string apiKey = "key";
            const string expected = "Il5kMD7QaxPM39AbjI84_qIwSCFQh4dHq1oDCf76LLk";

            var provider = new JwtSignatureProvider();

            var actual = provider.Sign(headerPayload, apiKey);

            Assert.Equal(expected, actual);
        }
    }
}
