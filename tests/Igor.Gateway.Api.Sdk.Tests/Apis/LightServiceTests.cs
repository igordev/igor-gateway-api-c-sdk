﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Apis.Lights;
using Igor.Gateway.Dtos.Events;
using Igor.Gateway.Dtos.Lighting;
using Igor.Gateway.Dtos.Lights;
using Igor.Gateway.Dtos.Tags;
using Igor.Sdk.Common.Models;
using Xunit;

namespace Igor.Gateway.Api.Sdk.Tests.Apis
{
    public class LightServiceTests : ApiServiceFixture<LightService, ILightService>
    {
        [Fact]
        public async Task ShouldGetAllLights()
        {
            SetupGetResultList<LightDto>("/lights");

            var actual = await Service.GetAll();
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldGetLight()
        {
            var dto = new LightDto {Id = 56};
            SetupGet("/lights/56", dto);

            var actual = await Service.Get(56);
            Assert.Equal(56, actual.Id);
            AssertUsedBearerToken();
        }

        [Fact]
        public async Task ShouldCreateLight()
        {
            var light = new CreateLightDto {ExternalId = Guid.NewGuid().ToString()};
            SetupPost("/lights", 99);

            var actual = await Service.Create(light);
            Assert.Equal(light.ExternalId, GetRequestContent<CreateLightDto>().ExternalId);
            Assert.Equal(99, actual);
        }

        [Fact]
        public async Task ShouldDeleteLight()
        {
            SetupDelete("/lights/45");

            await Service.Delete(45);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }

        [Fact]
        public async Task ShouldUpdateLight()
        {
            var dto = new UpdateLightDto {Name = Guid.NewGuid().ToString()};
            SetupPut("/lights/12");

            await Service.Update(12, dto);
            Assert.Equal(dto.Name, GetRequestContent<UpdateLightDto>().Name);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
        }

        [Fact]
        public async Task ShouldGetEventsForLight()
        {
            SetupGetResultList<EventDto>("/lights/79/events");

            var actual = await Service.GetEvents(79);
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldTurnOnLight()
        {
            SetupPost("/lights/12/turnon");

            await Service.TurnOn(12);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Fact]
        public async Task ShouldTurnOffLight()
        {
            SetupPost("/lights/17/turnoff");

            await Service.TurnOff(17);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Fact]
        public async Task ShouldSetLightingForLight()
        {
            var lighting = new LightingDto {Duration = 52345};
            SetupPost("/lights/65/lighting");

            await Service.SetLighting(65, lighting);
            Assert.Equal(lighting.Duration, GetRequestContent<LightingDto>().Duration);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Fact]
        public async Task ShouldSetLightingForAllLights()
        {
            var dtos = new[] {new BatchLightingDto(), new BatchLightingDto(), new BatchLightingDto()};
            SetupPost("/lights/lighting");

            await Service.SetLighting(dtos);
            Assert.Equal(3, GetRequestContent<ResultList<BatchLightingDto>>().List.Count);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Fact]
        public async Task ShouldAddTag()
        {
            SetupPut("/lights/4/tags");
            
            await Service.AddTag(4, new AddTagsDto {Names = new[] {"test"}});
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
            Assert.Equal("test", GetRequestContent<AddTagsDto>().Names.First());
        }

        [Fact]
        public async Task ShouldRemoveTag()
        {
            SetupDelete("/lights/4/tags/test");

            await Service.RemoveTag(4, "test");
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }
    }
}
