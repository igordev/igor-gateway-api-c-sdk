﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Apis.SpaceGroups;
using Igor.Gateway.Dtos.SpaceGroups;
using Igor.Gateway.Dtos.Spaces;
using Igor.Gateway.Dtos.Tags;
using Igor.Sdk.Common.Queries;
using Xunit;

namespace Igor.Gateway.Api.Sdk.Tests.Apis
{
    public class SpaceGroupServiceTests : ApiServiceFixture<SpaceGroupService, ISpaceGroupService>
    {
        [Fact]
        public async Task ShouldCreateSpaceGroup()
        {
            var dto = new CreateSpaceGroupDto {Name = Guid.NewGuid().ToString()};
            SetupPost("/spacegroups", 111);

            var actual = await Service.Create(dto);
            Assert.Equal(dto.Name, GetRequestContent<CreateSpaceGroupDto>().Name);
            Assert.Equal(111, actual);
        }

        [Fact]
        public async Task TestGetShouldCallGetAsyncToGetEndpoint()
        {
            var dto = new SpaceGroupDto { Name = Guid.NewGuid().ToString() };
            SetupGet("/spacegroups/90", dto);

            var actual = await Service.Get(90);
            Assert.Equal(dto.Name, actual.Name);
            AssertUsedBearerToken();
        }

        [Fact]
        public async Task ShouldDeleteSpaceGroup()
        {
            SetupDelete("/spacegroups/12");

            await Service.Delete(12);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }

        [Fact]
        public async Task ShouldUpdateSpaceGroup()
        {
            var dto = new UpdateSpaceGroupDto { Name = Guid.NewGuid().ToString() };
            SetupPut("/spacegroups/41");

            await Service.Update(41, dto);
            Assert.Equal(dto.Name, GetRequestContent<UpdateSpaceGroupDto>().Name);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
        }

        [Fact]
        public async Task ShouldSearchForSpaceGroups()
        {
            SetupGetPaginatedList<SpaceGroupsSearchResultsDto>("/spacegroups");

            var actual = await Service.Search();
            AssertPaginatedListDto(3, actual);
        }

        [Fact]
        public async Task ShouldSearchSpaceGroupsUsingSortDirection()
        {
            SetupGetPaginatedList<SpaceGroupsSearchResultsDto>("/spacegroups?sortDir=desc");

            var actual = await Service.Search(new SpaceGroupsSearchParameters { SortDirection = QuerySortDirection.Descending });
            AssertPaginatedListDto(3, actual);
        }

        [Fact]
        public async Task ShouldSearchSpaceGroupsUsingPageSizeAndPageNumber()
        {
            SetupGetPaginatedList<SpaceGroupsSearchResultsDto>("/spacegroups?page=3&pageSize=54");

            var actual = await Service.Search(new SpaceGroupsSearchParameters { Page = 3, PageSize = 54 });
            AssertPaginatedListDto(3, actual);
        }

        [Fact]
        public async Task ShouldGetSpaces()
        {
            SetupGetResultList<SpaceDto>("/spacegroups/99/spaces");

            var actual = await Service.GetSpaces(99);
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldAddSpace()
        {
            SetupPut("/spacegroups/4/spaces/33");

            await Service.AddSpace(4, 33);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
        }

        [Fact]
        public async Task ShouldRemoveSpace()
        {
            SetupDelete("/spacegroups/4/spaces/21");

            await Service.RemoveSpace(4, 21);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }

        [Fact]
        public async Task ShouldAddTag()
        {
            SetupPut("/spacegroups/4/tags");
            
            await Service.AddTag(4, new AddTagsDto {Names = new[] {"test"}});
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
            Assert.Equal("test", GetRequestContent<AddTagsDto>().Names.First());
        }

        [Fact]
        public async Task ShouldRemoveTag()
        {
            SetupDelete("/spacegroups/4/tags/test");

            await Service.RemoveTag(4, "test");
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }
    }
}
