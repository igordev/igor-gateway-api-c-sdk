﻿using System;
using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Apis.Policies;
using Igor.Gateway.Api.Sdk.Apis.SpaceTypes;
using Igor.Gateway.Dtos.Policies;
using Igor.Gateway.Dtos.SpaceTypes;
using Xunit;

namespace Igor.Gateway.Api.Sdk.Tests.Apis
{
    public class SpaceTypeServiceTests : ApiServiceFixture<SpaceTypeService, ISpaceTypeService>
    {
        [Fact]
        public async Task ShouldGetAllSpaceTypes()
        {
            SetupGetResultList<SpaceTypeDto>("/spacetypes");

            var actual = await Service.GetAll();
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldGetSpaceType()
        {
            var dto = new SpaceTypeDto {Name = Guid.NewGuid().ToString()};
            SetupGet("/spacetypes/43", dto);

            var actual = await Service.Get(43);
            Assert.Equal(dto.Name, actual.Name);
            AssertUsedBearerToken();
        }

        [Fact]
        public async Task ShouldGetPolicyForSpaceType()
        {
            var dto = new PolicyDto {Name = Guid.NewGuid().ToString()};
            SetupGet("/spacetypes/98/policy", dto);

            var actual = await Service.GetPolicy(98);
            Assert.Equal(dto.Name, actual.Name);
            AssertUsedBearerToken();
        }
    }
}
