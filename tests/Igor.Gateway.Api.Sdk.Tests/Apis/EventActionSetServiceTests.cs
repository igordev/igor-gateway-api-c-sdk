﻿using System.Net.Http;
using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Apis.EventActionSets;
using Igor.Gateway.Dtos.EventActionSets;
using Xunit;

namespace Igor.Gateway.Api.Sdk.Tests.Apis
{
    public class EventActionSetServiceTests : ApiServiceFixture<EventActionSetService, IEventActionSetService>
    {
        [Fact]
        public async Task ShouldGetAllEventActionSets()
        {
            SetupGetResultList<EventActionSetDto>("/eventactionsets");

            var actual = await Service.GetAll();
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldGetEventActionSet()
        {
            var dto = new EventActionSetDto { Id = 34 };
            SetupGet("/eventactionsets/34", dto);

            var actual = await Service.Get(34);
            Assert.Equal(34, actual.Id);
            AssertUsedBearerToken();
        }

        [Fact]
        public async Task ShouldGetEventActionSetsOfDevice()
        {
            SetupGetResultList<EventActionSetOfEntityDto>("/eventactionsets/drycontact/6");

            var actual = await Service.GetEventActionSets(EntityType.DryContact, 6);
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldCreateEventActionSet()
        {
            var dto = new CreateEventActionSetDto();
            SetupPost("/eventactionsets", 33);

            var actual = await Service.Create(dto);
            Assert.Equal(33, actual);
        }

        [Fact]
        public async Task ShouldUpdateEventActionSet()
        {
            var dto = new UpdateEventActionSetDto { ActionSetId = 44};
            SetupPut("/eventactionsets/12");

            await Service.Update(12, dto);
            Assert.Equal(dto.ActionSetId, GetRequestContent<UpdateEventActionSetDto>().ActionSetId);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
        }

        [Fact]
        public async Task ShouldDeleteActionSet()
        {
            SetupDelete("/eventactionsets/71");

            await Service.Delete(71);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }
    }
}
