﻿using System.Net.Http;
using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Apis.Health;
using Xunit;

namespace Igor.Gateway.Api.Sdk.Tests.Apis
{
    public class HealthServiceTests : ApiServiceFixture<HealthService, IHealthService>
    {
        [Fact]
        public async Task ShouldPingHealth()
        {
            SetupGet<object>("/health/ping");

            await Service.Ping();
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Get));
        }
    }
}
