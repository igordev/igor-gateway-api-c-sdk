﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Apis.Tags;
using Igor.Gateway.Dtos.Tags;
using Igor.Sdk.Common.Queries;
using Xunit;

namespace Igor.Gateway.Api.Sdk.Tests.Apis
{
    public class TagServiceTests : ApiServiceFixture<TagService, ITagService>
    {
        [Fact]
        public async Task ShouldCreateTag()
        {
            var dto = new CreateTagDto { Name = Guid.NewGuid().ToString() };
            SetupPost("/tags", 111);

            var actual = await Service.Create(dto);
            Assert.Equal(dto.Name, GetRequestContent<CreateTagDto>().Name);
            Assert.Equal(111, actual);
        }

        [Fact]
        public async Task TestGetShouldCallGetAsyncToGetEndpoint()
        {
            var dto = new TagDto { Name = Guid.NewGuid().ToString() };
            SetupGet("/tags/90", dto);

            var actual = await Service.Get(90);
            Assert.Equal(dto.Name, actual.Name);
            AssertUsedBearerToken();
        }

        [Fact]
        public async Task ShouldDeleteTag()
        {
            SetupDelete("/tags/12");

            await Service.Delete(12);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }

        [Fact]
        public async Task ShouldUpdateTag()
        {
            var dto = new UpdateTagDto { Name = Guid.NewGuid().ToString() };
            SetupPut("/tags/41");

            await Service.Update(41, dto);
            Assert.Equal(dto.Name, GetRequestContent<UpdateTagDto>().Name);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
        }

        [Fact]
        public async Task ShouldSearchForTags()
        {
            SetupGetPaginatedList<TagsSearchResultsDto>("/tags");

            var actual = await Service.Search();
            AssertPaginatedListDto(3, actual);
        }

        [Fact]
        public async Task ShouldSearchTagsUsingSortDirection()
        {
            SetupGetPaginatedList<TagsSearchResultsDto>("/tags?sortDir=desc");

            var actual = await Service.Search(new TagsSearchParameters { SortDirection = QuerySortDirection.Descending });
            AssertPaginatedListDto(3, actual);
        }

        [Fact]
        public async Task ShouldSearchTagsUsingPageSizeAndPageNumber()
        {
            SetupGetPaginatedList<TagsSearchResultsDto>("/tags?page=3&pageSize=54");

            var actual = await Service.Search(new TagsSearchParameters { Page = 3, PageSize = 54 });
            AssertPaginatedListDto(3, actual);
        }
    }
}
