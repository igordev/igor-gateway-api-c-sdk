﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Apis.Schedules;
using Igor.Gateway.Dtos.ActionSets;
using Igor.Gateway.Dtos.Events;
using Igor.Gateway.Dtos.Schedules;
using Igor.Sdk.Common.Queries;
using Xunit;

namespace Igor.Gateway.Api.Sdk.Tests.Apis
{
    public class ScheduleServiceTests : ApiServiceFixture<ScheduleService, IScheduleService>
    {

        [Fact]
        public async Task ShouldGetAllSchedules()
        {
            SetupGetResultList<ScheduleDto>("/schedules");

            var actual = await Service.GetAll();
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldGetSchedule()
        {
            var dto = new ScheduleDto {CronExpression = Guid.NewGuid().ToString()};
            SetupGet("/schedules/54", dto);

            var actual = await Service.Get(54);
            Assert.Equal(dto.CronExpression, actual.CronExpression);
            AssertUsedBearerToken();
        }

        [Fact]
        public async Task ShouldCreateSchedule()
        {
            var dto = new CreateScheduleDto {CronExpression = Guid.NewGuid().ToString()};
            SetupPost("/schedules", 9);

            var actual = await Service.Create(dto);
            Assert.Equal(dto.CronExpression, GetRequestContent<CreateScheduleDto>().CronExpression);
            Assert.Equal(9, actual);
        }

        [Fact]
        public async Task ShouldDeleteSchedule()
        {
            SetupDelete("/schedules/98");

            await Service.Delete(98);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }

        [Fact]
        public async Task ShouldUpdateSchedule()
        {
            var dto = new UpdateScheduleDto {CronExpression = Guid.NewGuid().ToString()};
            SetupPut("/schedules/75");

            await Service.Update(75, dto);
            Assert.Equal(dto.CronExpression, GetRequestContent<UpdateScheduleDto>().CronExpression);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
        }

        [Fact]
        public async Task ShouldGetEventsForSchedule()
        {
            SetupGetResultList<EventDto>("/schedules/76/events");

            var actual = await Service.GetEvents(76);
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldDeleteActionSetFromSchedul()
        {
            SetupDelete("/schedules/98/actionset");

            await Service.RemoveActionSet(98);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }

        [Fact]
        public async Task ShouldGetActionSetForSchedul()
        {
            var dto = new ActionSetDto {Name = Guid.NewGuid().ToString()};
            SetupGet("/schedules/32/actionset", dto);

            var actual = await Service.GetActionSet(32);
            Assert.Equal(dto.Name, actual.Name);
            AssertUsedBearerToken();
        }

        [Fact]
        public async Task ShouldUpdateActionSetForSchedule()
        {
            var dto = new UpdateScheduleActionSetDto {Id = 76};
            SetupPut("/schedules/43/actionset");

            await Service.SetActionSet(43, dto);
            Assert.Equal(dto.Id, GetRequestContent<UpdateScheduleActionSetDto>().Id);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
        }

        [Fact]
        public async Task ShouldSearchForSchedules()
        {
            SetupGetPaginatedList<ScheduleDto>("/schedules/search");

            var actual = await Service.Search();
            AssertPaginatedListDto(3, actual);
        }

        [Fact]
        public async Task ShouldSearchActionSetsUsingSortDirection()
        {
            SetupGetPaginatedList<ScheduleDto>("/schedules/search?sortDir=desc");

            var actual = await Service.Search(new SchedulesSearchParameters { SortDirection = QuerySortDirection.Descending });
            AssertPaginatedListDto(3, actual);
        }

        [Fact]
        public async Task ShouldSearchSpacesUsingPageSizeAndPageNumber()
        {
            SetupGetPaginatedList<ScheduleDto>("/schedules/search?page=3&pageSize=54");

            var actual = await Service.Search(new SchedulesSearchParameters { Page = 3, PageSize = 54 });
            AssertPaginatedListDto(3, actual);
        }
    }
}
