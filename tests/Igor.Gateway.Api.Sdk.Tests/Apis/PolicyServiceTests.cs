﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Apis.Policies;
using Igor.Gateway.Dtos.Policies;
using Xunit;

namespace Igor.Gateway.Api.Sdk.Tests.Apis
{
    public class PolicyServiceTests : ApiServiceFixture<PolicyService, IPolicyService>
    {
        [Fact]
        public async Task ShouldGetAllPolicies()
        {
            SetupGetResultList<PolicyDto>("/policies");

            var actual = await Service.GetAll();
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldGetPolicy()
        {
            var dto = new PolicyDto {Name = Guid.NewGuid().ToString()};
            SetupGet("/policies/123", dto);

            var actual = await Service.Get(123);
            Assert.Equal(dto.Name, actual.Name);
            AssertUsedBearerToken();
        }

        [Fact]
        public async Task ShouldUpdatePolicy()
        {
            var dto = new UpdatePolicyDto {MaximumLightLevel = 23};
            SetupPut("/policies/34");

            await Service.Update(34, dto);
            Assert.Equal(23, GetRequestContent<UpdatePolicyDto>().MaximumLightLevel);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
        }
    }
}
