﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Apis.DeviceNodes;
using Igor.Gateway.Dtos.DeviceNodes;
using Igor.Gateway.Dtos.Devices;
using Igor.Gateway.Dtos.NetworkNodes;
using Xunit;

namespace Igor.Gateway.Api.Sdk.Tests.Apis
{
    public class DeviceNodeServiceTests : ApiServiceFixture<DeviceNodeService, IDeviceNodeService>
    {
        [Fact]
        public async Task ShouldGetAllDeviceNodes()
        {
            SetupGetResultList<DeviceNodeDto>("/devicenodes");

            var actual = await Service.GetAll();
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldGetDeviceNode()
        {
            var dto = new DeviceNodeDto {ExternalId = Guid.NewGuid().ToString()};
            SetupGet("/devicenodes/45", dto);

            var actual = await Service.Get(45);
            Assert.Equal(dto.ExternalId, actual.ExternalId);
            AssertUsedBearerToken();
        }

        [Fact]
        public async Task ShouldCreateDeviceNode()
        {
            var dto = new CreateDeviceNodeDto { ExternalId = Guid.NewGuid().ToString() };
            SetupPost("/devicenodes", 15);

            var actual = await Service.Create(dto);
            Assert.Equal(dto.ExternalId, GetRequestContent<CreateDeviceNodeDto>().ExternalId);
            Assert.Equal(15, actual);
        }

        [Fact]
        public async Task ShouldDeleteDeviceNode()
        {
            SetupDelete("/devicenodes/65");

            await Service.Delete(65);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }

        [Fact]
        public async Task ShouldUpdateDeviceNode()
        {
            var dto = new UpdateDeviceNodeDto {Name = Guid.NewGuid().ToString()};
            SetupPut("/devicenodes/12");

            await Service.Update(12, dto);
            Assert.Equal(dto.Name, GetRequestContent<UpdateDeviceNodeDto>().Name);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
        }

        [Fact]
        public async Task ShouldDeleteAllDeviceNodes()
        {
            SetupDelete("/devicenodes/deleteall");

            await Service.DeleteAll();
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }

        [Fact]
        public async Task ShouldGetNetworkNodeForDeviceNode()
        {
            var dto = new NetworkNodeDto {ExternalId = Guid.NewGuid().ToString()};
            SetupGet("/devicenodes/11/networknode", dto);

            var actual = await Service.GetNetworkNode(11);
            Assert.Equal(dto.ExternalId, actual.ExternalId);
            AssertUsedBearerToken();
        }

        [Fact]
        public async Task ShouldBringDeviceNodeOnline()
        {
            SetupPost("/devicenodes/11/bringonline");

            await Service.BringOnline(11);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Fact]
        public async Task ShouldTakeDeviceNodeOffline()
        {
            SetupPost("/devicenodes/11/takeoffline");

            await Service.TakeOffline(11);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Fact]
        public async Task ShouldGetDevicesAttachedToDeviceNode()
        {
            SetupGetResultList<DeviceDto>("/devicenodes/4/devices");

            var actual = await Service.GetDevices(4);
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldGetEvents()
        {
            SetupGetResultList<DeviceDto>("/devicenodes/4/events");

            var actual = await Service.GetEvents(4);
            AssertListDto(3, actual);
        }
    }
}
