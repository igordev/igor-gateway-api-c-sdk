﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Igor.Exceptions;
using Igor.Gateway.Api.Sdk.Apis.Spaces;
using Igor.Gateway.Dtos.Devices;
using Igor.Gateway.Dtos.Events;
using Igor.Gateway.Dtos.Lighting;
using Igor.Gateway.Dtos.Lights;
using Igor.Gateway.Dtos.LightSensors;
using Igor.Gateway.Dtos.MotionSensors;
using Igor.Gateway.Dtos.Policies;
using Igor.Gateway.Dtos.SpaceGroups;
using Igor.Gateway.Dtos.Spaces;
using Igor.Gateway.Dtos.SpaceTypes;
using Igor.Gateway.Dtos.Tags;
using Igor.Gateway.Dtos.TemperatureSensors;
using Igor.Gateway.Dtos.Zones;
using Igor.Sdk.Common.Models;
using Igor.Sdk.Common.Queries;
using Xunit;
using Xunit.Sdk;

namespace Igor.Gateway.Api.Sdk.Tests.Apis
{
    public class SpaceServiceTests : ApiServiceFixture<SpaceService, ISpaceService>
    {
        [Fact]
        public async Task ShouldCreateSpace()
        {
            var dto = new CreateSpaceDto {Name = Guid.NewGuid().ToString()};
            SetupPost("/spaces", 99);

            var actual = await Service.Create(dto);
            Assert.Equal(dto.Name, GetRequestContent<CreateSpaceDto>().Name);
            Assert.Equal(99, actual);
        }

        [Fact]
        public async Task TestGetShouldCallGetAsyncToGetEndpoint()
        {
            var dto = new SpaceDto {Name = Guid.NewGuid().ToString()};
            SetupGet("/spaces/90", dto);

            var actual = await Service.Get(90);
            Assert.Equal(dto.Name, actual.Name);
            AssertUsedBearerToken();
        }

        [Fact]
        public async Task ShouldDeleteSpace()
        {
            SetupDelete("/spaces/12");

            await Service.Delete(12);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }

        [Fact]
        public async Task ShouldUpdateSpace()
        {
            var dto = new UpdateSpaceDto {Name = Guid.NewGuid().ToString()};
            SetupPut("/spaces/41");

            await Service.Update(41, dto);
            Assert.Equal(dto.Name, GetRequestContent<UpdateSpaceDto>().Name);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
        }

        [Fact]
        public async Task ShouldGetAllSpaces()
        {
            SetupGetResultList<SpaceDto>("/spaces");

            var actual = await Service.GetAll();
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldGetAllDevicesForSpace()
        {
            SetupGetResultList<DeviceDto>("/spaces/96/devices");

            var actual = await Service.GetDevices(96);
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldAddDeviceToSpace()
        {
            var dto = new AddDeviceToSpaceDto {DeviceId = 123};
            SetupPut("/spaces/4/devices");

            await Service.AddDevice(4, dto);
            Assert.Equal(dto.DeviceId, GetRequestContent<AddDeviceToSpaceDto>().DeviceId);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
        }

        [Fact]
        public async Task ShouldGetEventsForSpace()
        {
            SetupGetResultList<EventDto>("/spaces/8/events/all");

            var actual = await Service.GetAllEvents(8);
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldGetRecentEventsForSpace()
        {
            SetupGetResultList<EventDto>("/spaces/54/events");

            var actual = await Service.GetRecentEvents(54);
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldTurnOnSpace()
        {
            SetupPost("/spaces/91/turnon");

            await Service.TurnOn(91);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Fact]
        public async Task ShouldTurnOffSpace()
        {
            SetupPost("/spaces/87/turnoff");

            await Service.TurnOff(87);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Fact]
        public async Task ShouldSetLightingForSpace()
        {
            var dto = new LightingDto {Duration = 9};
            SetupPost("/spaces/43/lighting");

            await Service.SetLighting(43, dto);
            Assert.Equal(dto.Duration, GetRequestContent<LightingDto>().Duration);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Fact]
        public async Task ShouldSetLightingForAllSpaces()
        {
            var dtos = new[] { new BatchLightingDto(), new BatchLightingDto(), new BatchLightingDto() };
            SetupPost("/spaces/lighting");

            await Service.SetLighting(dtos);
            Assert.Equal(3, GetRequestContent<ResultList<BatchLightingDto>>().List.Count);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Fact]
        public async Task ShouldSynchronizeSpaces()
        {
            SetupPost("/spaces/synchronize");

            await Service.Synchronize();
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Fact]
        public async Task ShouldGetActivePolicies()
        {
            SetupGetResultList<ActivePolicyValueDto>("/spaces/8/activepolicy");

            var actual = await Service.GetActivePolicies(8);
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldGetZonesForSpace()
        {
            SetupGetResultList<ZoneDto>("/spaces/12/zones");

            var actual = await Service.GetZones(12);
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldCreateZoneForSpace()
        {
            var dto = new CreateZoneDto {Name = Guid.NewGuid().ToString()};
            SetupPost("/spaces/78/zones", 99);

            var actual = await Service.CreateZone(78, dto);
            Assert.Equal(dto.Name, GetRequestContent<CreateZoneDto>().Name);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
            Assert.Equal(99, actual);
        }

        [Fact]
        public async Task ShouldBeginZoneConfigurationForSpace()
        {
            SetupPost("/spaces/92/zones/config/enter");

            await Service.BeginZoneConfiguration(92);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Fact]
        public async Task ShouldEndZoneConfigurationForSpace()
        {
            SetupPost("/spaces/51/zones/config/exit");

            await Service.EndZoneConfiguration(51);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Fact]
        public async Task ShouldSaveZoneConfiguration()
        {
            SetupPost("/spaces/12/zones/config/save");

            await Service.SaveZoneConfiguration(12);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Fact]
        public async Task ShouldDeleteDryContactFromSpace()
        {
            SetupDelete("/spaces/4/drycontacts/21");

            await Service.RemoveDevice(4, 21, DeviceType.DryContact);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }

        [Fact]
        public async Task ShouldDeleteSensorFromSpace()
        {
            SetupDelete("/spaces/4/sensors/21");

            await Service.RemoveDevice(4, 21, DeviceType.Sensor);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }

        [Fact]
        public async Task ShouldDeleteLightFromSpace()
        {
            SetupDelete("/spaces/34/lights/1");

            await Service.RemoveDevice(34, 1, DeviceType.Light);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }

        [Fact]
        public async Task ShouldDeleteMotionSensorFromSpace()
        {
            SetupDelete("/spaces/4/motionsensors/21");

            await Service.RemoveDevice(4, 21, DeviceType.MotionSensor);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }

        [Fact]
        public async Task ShouldDeleteLightSensorFromSpace()
        {
            SetupDelete("/spaces/5/lightsensors/3");

            await Service.RemoveDevice(5, 3, DeviceType.LightSensor);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }

        [Fact]
        public async Task ShouldDeleteTemperatureSensorFromSpace()
        {
            SetupDelete("/spaces/8/temperaturesensors/7");

            await Service.RemoveDevice(8, 7, DeviceType.TemperatureSensor);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }

        [Fact]
        public async Task ShouldDeleteRelayFromSpace()
        {
            SetupDelete("/spaces/10/relays/12");

            await Service.RemoveDevice(10, 12, DeviceType.Relay);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }

        [Fact]
        public async Task ShouldDeleteWallControlFromSpace()
        {
            SetupDelete("/spaces/4/wallcontrols/21");

            await Service.RemoveDevice(4, 21, DeviceType.WallControl);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }

        [Fact]
        public async Task ShouldGetLightsForSpace()
        {
            SetupGetResultList<LightDto>("/spaces/12/lights");

            var actual = await Service.GetLights(12);
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldGetMotionSensorsForSpace()
        {
            SetupGetResultList<MotionSensorDto>("/spaces/98/motionsensors");

            var actual = await Service.GetMotionSensors(98);
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldGetLightSensors()
        {
            SetupGetResultList<LightSensorDto>("/spaces/13/lightsensors");

            var actual = await Service.GetLightSensors(13);
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldGetTemperatureSensors()
        {
            SetupGetResultList<TemperatureSensorDto>("/spaces/7/temperaturesensors");

            var actual = await Service.GetTemperatureSensors(7);
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldGetSpaceGroupsForSpace()
        {
            SetupGetResultList<SpaceGroupDto>("/spaces/15/spacegroups");

            var actual = await Service.GetSpaceGroups(15);
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldDeletePolicyFromSpace()
        {
            SetupDelete("/spaces/89/policy");

            await Service.DeletePolicy(89);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }

        [Fact]
        public async Task ShoulGetPolicyForSpace()
        {
            var dto = new PolicyDto {Name = Guid.NewGuid().ToString()};
            SetupGet("/spaces/87/policy", dto);

            var actual = await Service.GetPolicy(87);
            Assert.Equal(dto.Name, actual.Name);
            AssertUsedBearerToken();
        }

        [Fact]
        public async Task ShouldCreatePolicyForSpace()
        {
            var dto = new CreatePolicyDto {MaximumLightLevel = 54};
            SetupPost("/spaces/44/policy", $"https://localhost/admin/spaces/44/policy");

            var actual = await Service.CreatePolicy(44, dto);
            Assert.Equal(dto.MaximumLightLevel, GetRequestContent<CreatePolicyDto>().MaximumLightLevel);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
            Assert.Equal(44, actual);
        }

        [Fact]
        public async Task ShouldUpdatePolicyForSpace()
        {
            var dto = new UpdatePolicyDto { MaximumLightLevel = 54 };
            SetupPut("/spaces/44/policy");

            await Service.UpdatePolicy(44, dto);
            Assert.Equal(dto.MaximumLightLevel, GetRequestContent<CreatePolicyDto>().MaximumLightLevel);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
        }

        [Fact]
        public async Task ShouldGetSpaceTypeForSpace()
        {
            var dto = new SpaceTypeDto {Name = Guid.NewGuid().ToString()};
            SetupGet("/spaces/98/spacetype", dto);

            var actual = await Service.GetSpaceType(98);
            Assert.Equal(dto.Name, actual.Name);
            AssertUsedBearerToken();
        }

        [Fact]
        public async Task ShouldUpdateSpaceType()
        {
            var dto = new SpaceTypeDto {Name = Guid.NewGuid().ToString()};
            SetupPut("/spaces/9/spacetype");

            await Service.SetSpaceType(9, dto);
            Assert.Equal(dto.Name, GetRequestContent<SpaceTypeDto>().Name);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
        }

        [Fact]
        public async Task ShouldGetVacancyTimerForSpace()
        {
            var dto = new SpaceTimerDto {DateTimeExpires = new DateTime(2015, 04, 30)};
            SetupGet("/spaces/1/timer", dto);

            var actual = await Service.GetVacancyTimer(1);
            Assert.Equal(dto.DateTimeExpires, actual.DateTimeExpires);
            AssertUsedBearerToken();
        }

        [Fact]
        public async Task ShouldSearchForSpaces()
        {
            SetupGetPaginatedList<SpacesSearchResultsDto>("/spaces/search");

            var actual = await Service.Search();
            AssertPaginatedListDto(3, actual);
        }

        [Fact]
        public async Task ShouldSearchSpacesUsingSortDirection()
        {
            SetupGetPaginatedList<SpacesSearchResultsDto>("/spaces/search?sortDir=desc");

            var actual = await Service.Search(new SpacesSearchParameters { SortDirection = QuerySortDirection.Descending });
            AssertPaginatedListDto(3, actual);
        }

        [Fact]
        public async Task ShouldSearchSpacesUsingPageSizeAndPageNumber()
        {
            SetupGetPaginatedList<SpacesSearchResultsDto>("/spaces/search?page=3&pageSize=54");

            var actual = await Service.Search(new SpacesSearchParameters { Page = 3, PageSize = 54 });
            AssertPaginatedListDto(3, actual);
        }

        [Fact]
        public async Task ShouldResumeAutomationsForSpace()
        {
            SetupDelete("/spaces/89/pausedautomations");

            await Service.ResumeAutomations(89);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }

        [Fact]
        public async Task ShouldGetAutomationsForSpace()
        {
            var dto = new PausedSpaceAutomationDto { ResumeType = SpaceAutomationResumeType.Expired };
            SetupGet("/spaces/87/pausedautomations", dto);

            var actual = await Service.GetPausedAutomations(87);
            Assert.Equal(dto.ResumeType, actual.ResumeType);
            AssertUsedBearerToken();
        }

        [Fact]
        public async Task ShouldPauseAutomationsForSpace()
        {
            var dto = new PausedSpaceAutomationDto { ResumeType = SpaceAutomationResumeType.Expired };
            SetupPost("/spaces/44/pausedautomations", "https://localhost/spaces/44/pausedautomations");

            var actual = await Service.PauseAutomations(44, dto);
            Assert.Equal(dto.ResumeType, GetRequestContent<PausedSpaceAutomationDto>().ResumeType);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
            Assert.Equal(44, actual);
        }

        [Fact]
        public async Task ShouldCoverAllDeviceTypesForDeleteEndpoint()
        {
            foreach (var deviceType in Enum.GetValues(typeof(DeviceType)))
            {
                if (deviceType.Equals(DeviceType.DeviceNode) || deviceType.Equals(DeviceType.NetworkNode))
                {
                    var exception = await Assert.ThrowsAsync<IgorInvalidOperationException>(async () =>
                        await Service.RemoveDevice(4, 21, (DeviceType)deviceType));

                    Assert.StartsWith("Delete not setup for ", exception.Message);
                }
                else
                {
                    var exception = await Assert.ThrowsAsync<Exception>(async () =>
                        await Service.RemoveDevice(4, 21, (DeviceType) deviceType));

                    Assert.StartsWith("No request has been setup for ", exception.Message);
                }
            }
        }

        [Fact]
        public async Task ShouldAddTag()
        {
            SetupPut("/spaces/4/tags");
            
            await Service.AddTag(4, new AddTagsDto {Names = new[] {"test"}});
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
            Assert.Equal("test", GetRequestContent<AddTagsDto>().Names.First());
        }

        [Fact]
        public async Task ShouldRemoveTag()
        {
            SetupDelete("/spaces/4/tags/test");

            await Service.RemoveTag(4, "test");
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }

        [Fact]
        public async Task ShouldCreateSpaceEvent()
        {
            var dto = new CreateSpaceEventDto() { Name = "bob", Value = "{test}"};
            SetupPost("/spaces/1/events");
    
            await Service.CreateSpaceEvent(1, dto);
            Assert.Equal(dto.Name, GetRequestContent<CreateSpaceEventDto>().Name);
            Assert.Equal(dto.Value, GetRequestContent<CreateSpaceEventDto>().Value);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Fact]
        public async Task ShouldCreateEventForSpace()
        {
            var dto = new CreateSpaceEventActionSetDto { EventType = SpaceEventType.Vacant };
            SetupPost("/spaces/1/events");

            await Service.CreateEvent(1, dto);
            Assert.Equal(SpaceEventType.Vacant, GetRequestContent<CreateSpaceEventActionSetDto>().EventType);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Fact]
        public async Task ShouldGetSpaceMacAddresses()
        {
            var dto = new SpaceMacAddressDto() { SpaceId = 1 };
            SetupGetResultList<SpaceMacAddressDto>("/spaces/1/macaddresses");

            var result = await Service.GetSpaceMacAddresses(dto.SpaceId);

            AssertListDto(3, result);
        }
    }
}
