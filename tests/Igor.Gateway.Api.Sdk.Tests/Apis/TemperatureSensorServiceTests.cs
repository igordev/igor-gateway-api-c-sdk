﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Apis.TemperatureSensors;
using Igor.Gateway.Dtos.Events;
using Igor.Gateway.Dtos.Tags;
using Igor.Gateway.Dtos.TemperatureSensors;
using Xunit;

namespace Igor.Gateway.Api.Sdk.Tests.Apis
{
    public class TemperatureSensorServiceTests : ApiServiceFixture<TemperatureSensorService, ITemperatureSensorService>
    {
        [Fact]
        public async Task ShouldGetAllTemperatureSensors()
        {
            SetupGetResultList<TemperatureSensorDto>("/temperaturesensors");

            var actual = await Service.GetAll();
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldGetTemperatureSensor()
        {
            var dto = new TemperatureSensorDto {ExternalId = Guid.NewGuid().ToString()};
            SetupGet("/temperaturesensors/65", dto);

            var actual = await Service.Get(65);
            Assert.Equal(dto.ExternalId, actual.ExternalId);
            AssertUsedBearerToken();
        }

        [Fact]
        public async Task ShouldCreateTemperatureSensor()
        {
            var dto = new CreateTemperatureSensorDto {ExternalId = Guid.NewGuid().ToString()};
            SetupPost("/temperaturesensors", 33);

            var actual = await Service.Create(dto);
            Assert.Equal(dto.ExternalId, GetRequestContent<CreateTemperatureSensorDto>().ExternalId);
            Assert.Equal(33, actual);
        }

        [Fact]
        public async Task ShouldDeleteTemperatureSensor()
        {
            SetupDelete("/temperaturesensors/43");

            await Service.Delete(43);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }

        [Fact]
        public async Task ShouldUpdateTemperatureSensor()
        {
            var dto = new UpdateTemperatureSensorDto {Name = Guid.NewGuid().ToString()};
            SetupPut("/temperaturesensors/12");

            await Service.Update(12, dto);
            Assert.Equal(dto.Name, GetRequestContent<UpdateTemperatureSensorDto>().Name);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
        }

        [Fact]
        public async Task ShouldGetEventsForTemperatureSensor()
        {
            SetupGetResultList<EventDto>("/temperaturesensors/44/events");

            var actual = await Service.GetEvents(44);
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldCreateEventForTemperatureSensor()
        {
            var dto = new CreateTemperatureSensorEventDto {Temperature = 44.3};
            SetupPost("/temperaturesensors/23/events");

            await Service.CreateEvent(23, dto);
            Assert.Equal(dto.Temperature, GetRequestContent<CreateTemperatureSensorEventDto>().Temperature);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Fact]
        public async Task ShouldAddTag()
        {
            SetupPut("/temperaturesensors/4/tags");
            
            await Service.AddTag(4, new AddTagsDto {Names = new[] {"test"}});
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
            Assert.Equal("test", GetRequestContent<AddTagsDto>().Names.First());
        }

        [Fact]
        public async Task ShouldRemoveTag()
        {
            SetupDelete("/temperaturesensors/4/tags/test");

            await Service.RemoveTag(4, "test");
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }
    }
}
