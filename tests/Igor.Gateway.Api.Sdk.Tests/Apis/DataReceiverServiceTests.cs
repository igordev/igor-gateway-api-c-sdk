﻿using System.Net.Http;
using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Apis.Data;
using Igor.Gateway.Dtos.DataReceivers;
using Igor.Sdk.Common.Queries;
using Xunit;

namespace Igor.Gateway.Api.Sdk.Tests.Apis
{
    public class DataReceiverServiceTests : ApiServiceFixture<DataReceiverService, IDataReceiverService>
    {
        [Fact]
        public async Task ShouldCreateDataReceiver()
        {
            var dto = new CreateDataReceiverDto {Address = "test"};
            SetupPost("/data/receivers", 123);

            var actual = await Service.Create(dto);

            Assert.Equal("test", dto.Address);
        }

        [Fact]
        public async Task TestGetShouldCallGetAsyncToGetEndpoint()
        {
            var dto = new DataReceiverDto { Address = "test" };
            SetupGet("/data/receivers/123", dto);

            var actual = await Service.Get(123);
            Assert.Equal(dto.Address, actual.Address);
            AssertUsedBearerToken();
        }

        [Fact]
        public async Task ShouldSearchForDataReceivers()
        {
            SetupGetPaginatedList<DataReceiversSearchResultsDto>("/data/receivers");

            var actual = await Service.Search();
            AssertPaginatedListDto(3, actual);
        }

        [Fact]
        public async Task ShouldSearchSpaceGroupsUsingSortDirection()
        {
            SetupGetPaginatedList<DataReceiversSearchResultsDto>("/data/receivers?sortDir=desc");

            var actual = await Service.Search(new DataReceiversSearchParameters { SortDirection = QuerySortDirection.Descending });
            AssertPaginatedListDto(3, actual);
        }

        [Fact]
        public async Task ShouldSearchSpaceGroupsUsingPageSizeAndPageNumber()
        {
            SetupGetPaginatedList<DataReceiversSearchResultsDto>("/data/receivers?page=3&pageSize=54");

            var actual = await Service.Search(new DataReceiversSearchParameters { Page = 3, PageSize = 54 });
            AssertPaginatedListDto(3, actual);
        }

        [Fact]
        public async Task ShouldDeleteDataReceiver()
        {
            SetupDelete("/data/receivers/12");

            await Service.Delete(12);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }

        [Fact]
        public async Task UpdateShouldCallUpdateEndpoint()
        {
            var dto = new UpdateDataReceiverDto {Address = "old"};
            SetupPut("/data/receivers/123");

            await Service.Update(123, dto);
            Assert.Equal(dto.Address, GetRequestContent<UpdateDataReceiverDto>().Address);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
        }
    }
}
