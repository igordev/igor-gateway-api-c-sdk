﻿using System.Net.Http;
using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Apis.Connectors.Mqtt;
using Igor.Gateway.Dtos.Connectors.Mqtt;
using Igor.Sdk.Common.Models;
using Igor.Sdk.Common.Queries;
using Xunit;

namespace Igor.Gateway.Api.Sdk.Tests.Apis
{
    public class MqttClientServiceTests : ApiServiceFixture<MqttClientService, IMqttClientService>
    {
        [Fact]
        public async Task ShouldSearchForMqttClients()
        {
            SetupGetPaginatedList<MqttClientsSearchResultDto>("/connectors/mqtt/clients");

            var actual = await Service.Search();
            AssertPaginatedListDto(3, actual);
        }

        [Fact]
        public async Task ShouldSearchSpaceGroupsUsingSortDirection()
        {
            SetupGetPaginatedList<MqttClientsSearchResultDto>("/connectors/mqtt/clients?sortDir=desc");

            var actual = await Service.Search(new MqttClientsSearchParameters { SortDirection = QuerySortDirection.Descending });
            AssertPaginatedListDto(3, actual);
        }

        [Fact]
        public async Task ShouldSearchSpaceGroupsUsingPageSizeAndPageNumber()
        {
            SetupGetPaginatedList<MqttClientsSearchResultDto>("/connectors/mqtt/clients?page=3&pageSize=54");

            var actual = await Service.Search(new MqttClientsSearchParameters { Page = 3, PageSize = 54 });
            AssertPaginatedListDto(3, actual);
        }

        [Fact]
        public async Task ShouldGetMqttClient()
        {
            var dto = new MqttClientDto { Name = "test" };
            SetupGet("/connectors/mqtt/clients/12", dto);

            var actual = await Service.Get(12);
            Assert.Equal(dto.Name, actual.Name);
            AssertUsedBearerToken();
        }

        [Fact]
        public async Task ShouldCreateMqttClient()
        {
            var dto = new CreateMqttClientDto { Name = "test" };
            SetupPost("/connectors/mqtt/clients", 1);

            var actual = await Service.Create(dto);
            Assert.Equal(dto.Name, GetRequestContent<CreateMqttClientDto>().Name);
            Assert.Equal(1, actual);
        }

        [Fact]
        public async Task ShouldDeleteMqttClient()
        {
            SetupDelete("/connectors/mqtt/clients/9");

            await Service.Delete(9);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }

        [Fact]
        public async Task ShouldDeleteMqttClientCredentials()
        {
            SetupDelete("/connectors/mqtt/clients/9/credentials");

            await Service.DeleteCredentials(9); 
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }

        [Fact]
        public async Task ShouldUpdateMqttClient()
        {
            var dto = new UpdateMqttClientDto { Name = "test" };
            SetupPut("/connectors/mqtt/clients/12");

            await Service.Update(12, dto);
            Assert.Equal(dto.Name, GetRequestContent<UpdateMqttClientDto>().Name);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
        }

        [Fact]
        public async Task ShouldTestMqttClient()
        {
            SetupPost("/connectors/mqtt/clients/9/test");

            await Service.Test(9);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Fact]
        public async Task ShouldGetSupportedEvents()
        {
            var dtos = new ResultList<string>(new[] { "one", "two", "three" });
            SetupGet("/connectors/mqtt/clients/supported-events", dtos);

            var actual = await Service.GetSupportedEvents();
            AssertListDto(3, actual);
        }
    }
}
