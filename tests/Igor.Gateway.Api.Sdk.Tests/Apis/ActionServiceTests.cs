﻿using System.Net.Http;
using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Apis;
using Igor.Gateway.Api.Sdk.Apis.Actions;
using Igor.Gateway.Dtos.Actions;
using Igor.Gateway.Dtos.Events;
using Xunit;

namespace Igor.Gateway.Api.Sdk.Tests.Apis
{
    public class ActionServiceTests : ApiServiceFixture<ActionService, IActionService>
    {
        [Fact]
        public async Task ShouldGetAllActionsForActionSet()
        {
            SetupGetResultList<ActionDto>("/actionsets/65/actions");
            
            var actual = await Service.GetAll(65);

            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldGetActionForActionSet()
        {
            var dto = new ActionDto {Id = 51};
            SetupGet("/actionsets/8/actions/51", dto);

            var actual = await Service.Get(8, 51);
            Assert.Equal(51, actual.Id);
            AssertUsedBearerToken();
        }

        [Fact]
        public async Task ShouldCreateActionForActionSet()
        {
            var dto = new CreateActionDto<ActionParametersDto> {Type = ActionType.SetLighting};
            SetupPost("/actionsets/45/actions", 5);

            var actual = await Service.Create(45, dto);
            Assert.Equal(ActionType.SetLighting, GetRequestContent<CreateActionDto<ActionParametersDto>>().Type);
            Assert.Equal(5, actual);
        }

        [Fact]
        public async Task ShouldDeleteActionFromActionSet()
        {
            SetupDelete("/actionsets/5/actions/12");

            await Service.Delete(5, 12);

            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }

        [Fact]
        public async Task ShouldUpdateActionOnActionSet()
        {
            var dto = new UpdateActionDto<ActionParametersDto> {Type = ActionType.SetLighting};
            SetupPut("/actionsets/4/actions/13");

            await Service.Update(4, 13, dto);
            Assert.Equal(ActionType.SetLighting, GetRequestContent<UpdateActionDto<ActionParametersDto>>().Type);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
        }

        [Fact]
        public async Task ShouldExecuteActionOnActionSet()
        {
            SetupPost("/actionsets/9/actions/2/execute");

            await Service.Execute(9, 2);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Fact]
        public async Task ShouldGetEventsForAction()
        {
            SetupGetResultList<EventDto>("/actionsets/12/actions/34/events");

            var actual = await Service.GetEvents(12, 34);
            AssertListDto(3, actual);
        }
    }
}
