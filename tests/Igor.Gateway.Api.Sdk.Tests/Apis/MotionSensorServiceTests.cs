﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Apis.MotionSensors;
using Igor.Gateway.Dtos.Events;
using Igor.Gateway.Dtos.MotionSensors;
using Igor.Gateway.Dtos.Tags;
using Xunit;

namespace Igor.Gateway.Api.Sdk.Tests.Apis
{
    public class MotionSensorServiceTests : ApiServiceFixture<MotionSensorService, IMotionSensorService>
    {
        [Fact]
        public async Task ShouldGetAllMotionSensors()
        {
            SetupGetResultList<MotionSensorDto>("/motionsensors");

            var actual = await Service.GetAll();
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldGetMotionSensor()
        {
            var dto = new MotionSensorDto {ExternalId = Guid.NewGuid().ToString()};
            SetupGet("/motionsensors/21", dto);

            var actual = await Service.Get(21);
            Assert.Equal(dto.ExternalId, actual.ExternalId);
            AssertUsedBearerToken();
        }

        [Fact]
        public async Task ShouldCreateMotionSensor()
        {
            var dto = new CreateMotionSensorDto {ExternalId = Guid.NewGuid().ToString()};
            SetupPost("/motionsensors", 22);

            var actual = await Service.Create(dto);
            Assert.Equal(dto.ExternalId, GetRequestContent<CreateMotionSensorDto>().ExternalId);
            Assert.Equal(22, actual);
        }

        [Fact]
        public async Task ShouldDeleteMotionSensor()
        {
            SetupDelete("/motionsensors/55");

            await Service.Delete(55);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }

        [Fact]
        public async Task ShouldUpdateMotionSensor()
        {
            var dto = new UpdateMotionSensorDto {Name = Guid.NewGuid().ToString()};
            SetupPut("/motionsensors/65");

            await Service.Update(65, dto);
            Assert.Equal(dto.Name, GetRequestContent<UpdateMotionSensorDto>().Name);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
        }

        [Fact]
        public async Task ShouldGetEventsForMotionSensor()
        {
            SetupGetResultList<EventDto>("/motionsensors/75/events");

            var actual = await Service.GetEvents(75);
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldCreateEventForMotionSensor()
        {
            var dto = new CreateMotionSensorEventDto {State = MotionSensorState.Occupancy};
            SetupPost("/motionsensors/54/events");

            await Service.CreateEvent(54, dto);
            Assert.Equal(MotionSensorState.Occupancy, GetRequestContent<CreateMotionSensorEventDto>().State);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Fact]
        public async Task ShouldAddTag()
        {
            SetupPut("/motionsensors/4/tags");
            
            await Service.AddTag(4, new AddTagsDto {Names = new[] {"test"}});
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
            Assert.Equal("test", GetRequestContent<AddTagsDto>().Names.First());
        }

        [Fact]
        public async Task ShouldRemoveTag()
        {
            SetupDelete("/motionsensors/4/tags/test");

            await Service.RemoveTag(4, "test");
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }
    }
}
