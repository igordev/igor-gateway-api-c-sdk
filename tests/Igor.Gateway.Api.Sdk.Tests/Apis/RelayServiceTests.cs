﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Apis.Relays;
using Igor.Gateway.Dtos.Events;
using Igor.Gateway.Dtos.Relays;
using Igor.Gateway.Dtos.Tags;
using Xunit;

namespace Igor.Gateway.Api.Sdk.Tests.Apis
{
    public class RelayServiceTests : ApiServiceFixture<RelayService, IRelayService>
    {
        [Fact]
        public async Task ShouldGetAllRelays()
        {
            SetupGetResultList<RelayDto>("/relays");

            var actual = await Service.GetAll();
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldGetRelay()
        {
            var expected = new RelayDto {Id = 65};
            SetupGet("/relays/65", expected);

            var actual = await Service.Get(65);
            Assert.Equal(65, actual.Id);
            AssertUsedBearerToken();
        }

        [Fact]
        public async Task ShouldCreateRelay()
        {
            var dto = new CreateRelayDto {ExternalId = Guid.NewGuid().ToString()};
            SetupPost("/relays", 1);

            var actual = await Service.Create(dto);
            Assert.Equal(dto.ExternalId, GetRequestContent<CreateRelayDto>().ExternalId);
            Assert.Equal(1, actual);
        }

        [Fact]
        public async Task ShouldDeleteRelay()
        {
            SetupDelete("/relays/76");

            await Service.Delete(76);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }

        [Fact]
        public async Task ShouldUpdateRelay()
        {
            var dto = new UpdateRelayDto {Name = Guid.NewGuid().ToString()};
            SetupPut("/relays/45");

            await Service.Update(45, dto);
            Assert.Equal(dto.Name, GetRequestContent<UpdateRelayDto>().Name);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
        }

        [Fact]
        public async Task ShouldGetEventsForRelay()
        {
            SetupGetResultList<EventDto>("/relays/65/events");

            var actual = await Service.GetEvents(65);
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldOpenRelay()
        {
            SetupPost("/relays/78/open");

            await Service.Open(78);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Fact]
        public async Task ShouldCloseRelay()
        {
            SetupPost("/relays/78/close");

            await Service.Close(78);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Fact]
        public async Task ShouldAddTag()
        {
            SetupPut("/relays/4/tags");
            
            await Service.AddTag(4, new AddTagsDto {Names = new[] {"test"}});
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
            Assert.Equal("test", GetRequestContent<AddTagsDto>().Names.First());
        }

        [Fact]
        public async Task ShouldRemoveTag()
        {
            SetupDelete("/relays/4/tags/test");

            await Service.RemoveTag(4, "test");
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }
    }
}
