﻿using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Apis.Dashboards;
using Igor.Gateway.Dtos.Dashboards;
using Xunit;

namespace Igor.Gateway.Api.Sdk.Tests.Apis
{
    public class DashboardServiceTests : ApiServiceFixture<DashboardService, IDashboardService>
    {
        [Fact]
        public async Task ShouldGetDeviceStatusByDeviceType()
        {
            SetupGetResultList<DashboardDeviceStatusByDeviceTypeDto>("/dashboards/devicestatusbydevicetype");

            var actual = await Service.GetDeviceStatusByDeviceType();
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldGetDeviceStatusByNetworkSwitch()
        {
            SetupGetResultList<DashboardDeviceStatusByNetworkSwitchDto>("/dashboards/devicestatusbynetworkswitch");

            var actual = await Service.GetDeviceStatusByNetworkSwitch();
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldGetDeviceStatusBySpace()
        {
            SetupGetResultList<DashboardDeviceStatusBySpaceDto>("/dashboards/devicestatusbyspace");

            var actual = await Service.GetDeviceStatusBySpace();
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldGetDeviceActivityByHour()
        {
            SetupGetResultList<DashboardDeviceActivityByHourDto>("/dashboards/deviceactivitybyhour");

            var actual = await Service.GetDeviceActivityByHour();
            AssertListDto(3, actual);
        }
    }
}
