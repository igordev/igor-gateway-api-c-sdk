﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Apis.WebHooks;
using Igor.Gateway.Dtos.WebHooks;
using Igor.Sdk.Common.Models;
using Xunit;

namespace Igor.Gateway.Api.Sdk.Tests.Apis
{
    public class WebHookServiceTests : ApiServiceFixture<WebHookService, IWebHookService>
    {
        [Fact]
        public async Task ShouldGetAllWebhooksForApplicationKey()
        {
            SetupGetResultList<WebHookDto>("/applicationkeys/44/webhooks");

            var actual = await Service.GetAll(44);
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldGetWebHookForApplicationKey()
        {
            var dto = new WebHookDto {ApplicationKey = Guid.NewGuid().ToString()};
            SetupGet("/applicationkeys/99/webhooks/12", dto);

            var actual = await Service.Get(99, 12);
            Assert.Equal(dto.ApplicationKey, actual.ApplicationKey);
            AssertUsedBearerToken();
        }

        [Fact]
        public async Task ShouldCreateWebHookForApplicationKey()
        {
            var dto = new CreateWebHookDto {CallbackUrl = Guid.NewGuid().ToString()};
            SetupPost("/applicationkeys/84/webhooks", 1);

            var actual = await Service.Create(84, dto);
            Assert.Equal(dto.CallbackUrl, GetRequestContent<CreateWebHookDto>().CallbackUrl);
            Assert.Equal(1, actual);
        }

        [Fact]
        public async Task ShouldDeleteWebHook()
        {
            SetupDelete("/applicationkeys/33/webhooks/9");

            await Service.Delete(33, 9);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }

        [Fact]
        public async Task ShouldUpdateWebhook()
        {
            var dto = new UpdateWebHookDto {CallbackUrl = Guid.NewGuid().ToString()};
            SetupPut("/applicationkeys/34/webhooks/12");

            await Service.Update(34, 12, dto);
            Assert.Equal(dto.CallbackUrl, GetRequestContent<UpdateWebHookDto>().CallbackUrl);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
        }

        [Fact]
        public async Task ShouldGetSupportedEvents()
        {
            var dtos = new ResultList<string>(new[] {"one", "two", "three"});
            SetupGet("/webhooks/supported-events", dtos);

            var actual = await Service.GetSupportedEvents();
            AssertListDto(3, actual);
        }
    }
}
