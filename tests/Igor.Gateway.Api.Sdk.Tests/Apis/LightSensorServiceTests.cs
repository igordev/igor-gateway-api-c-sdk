﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Apis.LightSensors;
using Igor.Gateway.Dtos.Events;
using Igor.Gateway.Dtos.LightSensors;
using Igor.Gateway.Dtos.Tags;
using Xunit;

namespace Igor.Gateway.Api.Sdk.Tests.Apis
{
    public class LightSensorServiceTests : ApiServiceFixture<LightSensorService, ILightSensorService>
    {
        [Fact]
        public async Task ShouldGetAllLightSensors()
        {
            SetupGetResultList<LightSensorDto>("/lightsensors");

            var actual = await Service.GetAll();
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldGetLightSensor()
        {
            var dto = new LightSensorDto {ExternalId = Guid.NewGuid().ToString()};
            SetupGet("/lightsensors/9", dto);

            var actual = await Service.Get(9);
            Assert.Equal(dto.ExternalId, actual.ExternalId);
            AssertUsedBearerToken();
        }

        [Fact]
        public async Task ShouldCreateLightSensor()
        {
            var dto = new CreateLightSensorDto {ExternalId = Guid.NewGuid().ToString()};
            SetupPost("/lightsensors", 334);

            var actual = await Service.Create(dto);
            Assert.Equal(dto.ExternalId, GetRequestContent<CreateLightSensorDto>().ExternalId);
            Assert.Equal(334, actual);
        }

        [Fact]
        public async Task ShouldDeleteLightSensor()
        {
            SetupDelete("/lightsensors/12");

            await Service.Delete(12);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }

        [Fact]
        public async Task ShouldUpdateLightSensor()
        {
            var dto = new UpdateLightSensorDto {Name = Guid.NewGuid().ToString()};
            SetupPut("/lightsensors/44");

            await Service.Update(44, dto);
            Assert.Equal(dto.Name, GetRequestContent<UpdateLightSensorDto>().Name);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
        }

        [Fact]
        public async Task ShouldGetEventsForLightSensor()
        {
            SetupGetResultList<EventDto>("/lightsensors/51/events");

            var actual = await Service.GetEvents(51);
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldCreateEventForLightSensor()
        {
            var dto = new CreateLightSensorEventDto {SensorLevel = 4423};
            SetupPost("/lightsensors/32/events");

            await Service.CreateEvent(32, dto);
            Assert.Equal(4423, GetRequestContent<CreateLightSensorEventDto>().SensorLevel);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Fact]
        public async Task ShouldAddTag()
        {
            SetupPut("/lightsensors/4/tags");
            
            await Service.AddTag(4, new AddTagsDto {Names = new[] {"test"}});
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
            Assert.Equal("test", GetRequestContent<AddTagsDto>().Names.First());
        }

        [Fact]
        public async Task ShouldRemoveTag()
        {
            SetupDelete("/lightsensors/4/tags/test");

            await Service.RemoveTag(4, "test");
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }
    }
}
