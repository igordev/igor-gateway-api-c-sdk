﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Apis.ActionSets;
using Igor.Gateway.Dtos.ActionSets;
using Igor.Gateway.Dtos.Events;
using Igor.Sdk.Common.Queries;
using Xunit;

namespace Igor.Gateway.Api.Sdk.Tests.Apis
{
    public class ActionSetServiceTests : ApiServiceFixture<ActionSetService, IActionSetService>
    {
        [Fact]
        public async Task ShouldGetAllActionSets()
        {
            SetupGetResultList<ActionSetDto>("/actionsets");

            var actual = await Service.GetAll();
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldGetActionSet()
        {
            var dto = new ActionSetDto {Id = 34};
            SetupGet("/actionsets/34", dto);

            var actual = await Service.Get(34);
            Assert.Equal(34, actual.Id);
            AssertUsedBearerToken();
        }

        [Fact]
        public async Task ShouldCreateActionSet()
        {
            var dto = new CreateActionSetDto {Name = Guid.NewGuid().ToString()};
            SetupPost("/actionsets", 33);

            var actual = await Service.Create(dto);
            Assert.Equal(dto.Name, GetRequestContent<CreateActionSetDto>().Name);
            Assert.Equal(33, actual);
        }

        [Fact]
        public async Task ShouldDeleteActionSet()
        {
            SetupDelete("/actionsets/71");

            await Service.Delete(71);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }

        [Fact]
        public async Task ShouldUpdateActionSet()
        {
            var dto = new UpdateActionSetDto {Name = Guid.NewGuid().ToString()};
            SetupPut("/actionsets/12");

            await Service.Update(12, dto);
            Assert.Equal(dto.Name, GetRequestContent<UpdateActionSetDto>().Name);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
        }

        [Fact]
        public async Task ShouldExecuteActionSet()
        {
            SetupPost("/actionsets/56/execute");

            await Service.Execute(56);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Fact]
        public async Task ShouldGetEventsForActionSet()
        {
            SetupGetResultList<EventDto>("/actionsets/88/events");

            var actual = await Service.GetEvents(88);
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldCancelActionSet()
        {
            var dto = new CancelOptionsDto {CancelType = CancelType.Immediate};
            SetupPost("/actionsets/99/cancel");

            await Service.Cancel(99, dto);

            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Fact]
        public async Task ShouldSearchForActionSets()
        {
            SetupGetPaginatedList<ActionSetsSearchResultsDto>("/actionsets/search");

            var actual = await Service.Search();
            AssertPaginatedListDto(3, actual);
        }

        [Fact]
        public async Task ShouldSearchActionSetsUsingSortDirection()
        {
            SetupGetPaginatedList<ActionSetsSearchResultsDto>("/actionsets/search?sortDir=desc");

            var actual = await Service.Search(new ActionSetsSearchParameters
                {SortDirection = QuerySortDirection.Descending});
            AssertPaginatedListDto(3, actual);
        }

        [Fact]
        public async Task ShouldSearchSpacesUsingPageSizeAndPageNumber()
        {
            SetupGetPaginatedList<ActionSetsSearchResultsDto>("/actionsets/search?page=3&pageSize=54");

            var actual = await Service.Search(new ActionSetsSearchParameters {Page = 3, PageSize = 54});
            AssertPaginatedListDto(3, actual);
        }
    }
}