﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Apis.DryContacts;
using Igor.Gateway.Dtos.DryContacts;
using Igor.Gateway.Dtos.Events;
using Igor.Gateway.Dtos.Tags;
using Xunit;

namespace Igor.Gateway.Api.Sdk.Tests.Apis
{
    public class DryContactServiceTests : ApiServiceFixture<DryContactService, IDryContactService>
    {
        [Fact]
        public async Task ShouldGetAllDryContacts()
        {
            SetupGetResultList<DryContactDto>("/drycontacts");

            var actual = await Service.GetAll();
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldGetDryContact()
        {
            var dto = new DryContactDto { ExternalId = Guid.NewGuid().ToString() };
            SetupGet("/drycontacts/21", dto);

            var actual = await Service.Get(21);
            Assert.Equal(dto.ExternalId, actual.ExternalId);
            AssertUsedBearerToken();
        }

        [Fact]
        public async Task ShouldCreateDryContact()
        {
            var dto = new CreateDryContactDto { ExternalId = Guid.NewGuid().ToString() };
            SetupPost("/drycontacts", 22);

            var actual = await Service.Create(dto);
            Assert.Equal(dto.ExternalId, GetRequestContent<CreateDryContactDto>().ExternalId);
            Assert.Equal(22, actual);
        }

        [Fact]
        public async Task ShouldDeleteDryContact()
        {
            SetupDelete("/drycontacts/55");

            await Service.Delete(55);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }

        [Fact]
        public async Task ShouldUpdateDryContact()
        {
            var dto = new UpdateDryContactDto { Name = Guid.NewGuid().ToString() };
            SetupPut("/drycontacts/65");

            await Service.Update(65, dto);
            Assert.Equal(dto.Name, GetRequestContent<UpdateDryContactDto>().Name);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
        }

        [Fact]
        public async Task ShouldGetEventsForDryContact()
        {
            SetupGetResultList<EventDto>("/drycontacts/75/events");

            var actual = await Service.GetEvents(75);
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldCreateEventForDryContact()
        {
            var dto = new CreateDryContactEventDto { EventType = DryContactEventType.Closed };
            SetupPost("/drycontacts/54/events");

            await Service.CreateEvent(54, dto);
            Assert.Equal(DryContactEventType.Closed, GetRequestContent<CreateDryContactEventDto>().EventType);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Fact]
        public async Task ShouldAddTag()
        {
            SetupPut("/drycontacts/4/tags");
            
            await Service.AddTag(4, new AddTagsDto {Names = new[] {"test"}});
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
            Assert.Equal("test", GetRequestContent<AddTagsDto>().Names.First());
        }

        [Fact]
        public async Task ShouldRemoveTag()
        {
            SetupDelete("/drycontacts/4/tags/test");

            await Service.RemoveTag(4, "test");
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }
    }
}
