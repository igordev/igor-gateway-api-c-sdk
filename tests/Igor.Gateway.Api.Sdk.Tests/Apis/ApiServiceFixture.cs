﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using Igor.Gateway.Api.Sdk.Core.Config;
using Igor.Gateway.Api.Sdk.Core.Http.Security;
using Igor.Gateway.Api.Sdk.Core.Serialization;
using Igor.Gateway.Api.Sdk.Tests.Fakes.Https;
using Igor.Sdk.Common;
using Igor.Sdk.Common.Models;
using Xunit;

namespace Igor.Gateway.Api.Sdk.Tests.Apis
{
    public class ApiServiceFixture<TService, TInterface>
    {
        protected SdkConfig SdkConfig { get; }
        protected FakeHttpClientService HttpClientService { get; }
        protected TInterface Service { get; }

        public ApiServiceFixture()
        {
            SdkConfig = new SdkConfig {ApiAddress = "http://localhost", ApiKey = "key"};
            HttpClientService = new FakeHttpClientService();

            Service = (TInterface) Activator.CreateInstance(
                typeof(TService),
                HttpClientService,
                new JsonSerializer(),
                SdkConfig,
                new JwtTokenProvider(),
                null);
        }

        protected void SetupGetResultList<T>(string path, int count = 3)
            where T : new()
        {
            var dtos = Enumerable.Range(0, count).Select(i => new T()).ToArray();
            HttpClientService.SetupGet($"{SdkConfig.ApiAddress}/api{path}", dtos.ToResultList());
        }

        protected void SetupGetPaginatedList<T>(string path, int count = 3) 
            where T : new()
        {
            var dtos = Enumerable.Range(0, count).Select(i => new T()).ToArray();
            HttpClientService.SetupGet($"{SdkConfig.ApiAddress}/api{path}", dtos.ToPaginatedList());
        }

        protected HttpResponseMessage SetupGet<T>(string path, T dto = null)
            where T : class, new()
        {
            return HttpClientService.SetupGet($"{SdkConfig.ApiAddress}/api{path}", dto ?? new T());
        }

        protected HttpResponseMessage SetupPost(string path, int? createdId = null)
        {
            return createdId != null
                ? HttpClientService.SetupPost($"{SdkConfig.ApiAddress}/api{path}",
                    locationHeader: new Uri($"https://localhost/admin{path}/{createdId}"))
                : HttpClientService.SetupPost($"{SdkConfig.ApiAddress}/api{path}");
        }

        protected HttpResponseMessage SetupPost(string path, string location)
        {
            return HttpClientService.SetupPost($"{SdkConfig.ApiAddress}/api{path}",
                locationHeader: new Uri(location));
        }

        protected HttpResponseMessage SetupPostEventFile(string path, string fileType)
        {
            return HttpClientService.SetupPost($"{SdkConfig.ApiAddress}/api{path}/{fileType}",
                status: HttpStatusCode.Created);
        }

        protected HttpResponseMessage SetupPut(string path)
        {
            return HttpClientService.SetupPut($"{SdkConfig.ApiAddress}/api{path}");
        }

        protected HttpResponseMessage SetupDelete(string path)
        {
            return HttpClientService.SetupDelete($"{SdkConfig.ApiAddress}/api{path}");
        }

        protected void AssertListDto<T>(int expectedCount, ResultList<T> listDto)
        {
            Assert.Equal(expectedCount, listDto.List.Count);
            AssertUsedBearerToken();
        }

        protected void AssertPaginatedListDto<T>(int expectedCount, IPaginatedList<T> listDto)
        {
            Assert.Equal(expectedCount, listDto.List.Count);
            AssertUsedBearerToken();
        }

        protected void AssertSameResponse(HttpResponseMessage expected, HttpResponseMessage actual)
        {
            Assert.Same(expected, actual);
            AssertUsedBearerToken();
        }
        
        protected T GetRequestContent<T>()
        {
            return HttpClientService.GetRequestContent<T>();
        }

        protected void AssertUsedBearerToken()
        {
            HttpClientService.AssertHasBearerToken();
        }

    }
}
