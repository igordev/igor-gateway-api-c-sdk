﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Apis.Devices;
using Igor.Gateway.Dtos.Devices;
using Igor.Gateway.Dtos.NetworkNodes;
using Igor.Gateway.Dtos.Spaces;
using Igor.Sdk.Common.Queries;
using Xunit;

namespace Igor.Gateway.Api.Sdk.Tests.Apis
{
    public class DeviceServiceTests : ApiServiceFixture<DeviceService, IDeviceService>
    { 
        [Fact]
        public async Task ShouldGetAllDevices()
        {
            SetupGetResultList<DeviceDto>("/devices/all");

            var actual = await Service.GetAll();
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldGetSpaceForDevice()
        {
            var dto = new SpaceDto {Name = Guid.NewGuid().ToString()};
            SetupGet("/devices/lightsensor/6/space", dto);

            var actual = await Service.GetSpace(DeviceType.LightSensor, 6);
            Assert.Equal(dto.Name, actual.Name);
            AssertUsedBearerToken();
        }

        [Fact]
        public async Task ShouldGetNetworkNodeForDevice()
        {
            var dto = new NetworkNodeDto { Name = Guid.NewGuid().ToString() };
            SetupGet("/devices/lightsensor/6/networknode", dto);

            var actual = await Service.GetNetworkNode(DeviceType.LightSensor, 6);
            Assert.Equal(dto.Name, actual.Name);
            AssertUsedBearerToken();
        }

        [Fact]
        public async Task ShouldSearchForDevices()
        {
            SetupGetPaginatedList<DevicesSearchResultDto>("/devices/search");

            var actual = await Service.Search();
            AssertPaginatedListDto(3, actual);
        }

        [Fact]
        public async Task ShouldSearchDevicesUsingSortDirection()
        {
            SetupGetPaginatedList<DevicesSearchResultDto>("/devices/search?sortDir=desc");

            var actual = await Service.Search(new DeviceSearchParameters {SortDirection = QuerySortDirection.Descending});
            AssertPaginatedListDto(3, actual);
        }

        [Fact]
        public async Task ShouldSearchDevicesUsingPageSizeAndPageNumber()
        {
            SetupGetPaginatedList<DevicesSearchResultDto>("/devices/search?page=3&pageSize=54");

            var actual = await Service.Search(new DeviceSearchParameters { Page = 3, PageSize = 54});
            AssertPaginatedListDto(3, actual);
        }

        [Fact]
        public async Task ShouldGetUnassignedDevices()
        {
            SetupGetResultList<DeviceDto>("/devices/unassigned");

            var actual = await Service.GetUnassigned();
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldDiscoverDevices()
        {
            SetupPost("/devices/discover");

            await Service.Discover();
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Fact]
        public async Task ShouldDeleteAllDevices()
        {
            SetupDelete("/devices/deleteall");

            await Service.DeleteAll();
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }

        [Fact]
        public async Task ShouldUpdateDevice()
        {
            var dto = new UpdateDevicesDto { Name = Guid.NewGuid().ToString()};
            SetupPut("/devices/lightsensor/6");

            await Service.Update(DeviceType.LightSensor, 6, dto);
            Assert.Equal(dto.Name, GetRequestContent<UpdateDevicesDto>().Name);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
        }

        [Fact]
        public async Task ShouldDeleteDevice()
        {
            SetupDelete("/devices/lightsensor/6");

            await Service.Delete(DeviceType.LightSensor, 6);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }

        [Fact]
        public async Task ShouldBringDeviceOnline()
        {
            SetupPost("/devices/Light/11/bringonline");

            await Service.BringOnline(DeviceType.Light, 11);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Fact]
        public async Task ShouldTakeDeviceOffline()
        {
            SetupPost("/devices/Light/11/takeoffline");

            await Service.TakeOffline(DeviceType.Light, 11);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Fact]
        public async Task ShouldQuarantineDevice()
        {
            SetupPost("/devices/LightSensor/11/quarantine");

            await Service.Quarantine(DeviceType.LightSensor, 11);

            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Fact]
        public async Task ShouldReleaseDeviceFromQuarantine()
        {
            SetupDelete("/devices/LightSensor/11/quarantine");

            await Service.ReleaseQuarantine(DeviceType.LightSensor, 11);

            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }

        [Fact]
        public async Task ShouldGetDeviceTypeCounts()
        {
            SetupGetResultList<DeviceDto>("/devices/devicetypes");

            var actual = await Service.GetDeviceTypeCounts();
            AssertListDto(3, actual);
        }
    }
}
