﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Apis.Zones;
using Igor.Gateway.Dtos.Devices;
using Igor.Gateway.Dtos.Events;
using Igor.Gateway.Dtos.LightSensors;
using Igor.Gateway.Dtos.Spaces;
using Igor.Gateway.Dtos.Zones;
using Xunit;

namespace Igor.Gateway.Api.Sdk.Tests.Apis
{
    public class ZoneServiceTests : ApiServiceFixture<ZoneService, IZoneService>
    {
        [Fact]
        public async Task ShouldGetZone()
        {
            var dto = new ZoneDto {Name = Guid.NewGuid().ToString()};
            SetupGet("/zones/43", dto);

            var actual = await Service.Get(43);
            Assert.Equal(dto.Name, actual.Name);
            AssertUsedBearerToken();
        }

        [Fact]
        public async Task ShouldDeleteZone()
        {
            SetupDelete("/zones/99");

            await Service.Delete(99);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }

        [Fact]
        public async Task ShouldUpdateZone()
        {
            var dto = new UpdateZoneDto {Name = Guid.NewGuid().ToString()};
            SetupPut("/zones/88");

            await Service.Update(88, dto);
            Assert.Equal(dto.Name, GetRequestContent<UpdateZoneDto>().Name);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
        }

        [Fact]
        public async Task ShouldGetSpaceForZone()
        {
            var dto = new SpaceDto {Name = Guid.NewGuid().ToString()};
            SetupGet("/zones/43/space", dto);

            var actual = await Service.GetSpace(43);
            Assert.Equal(dto.Name, actual.Name);
            AssertUsedBearerToken();
        }

        [Fact]
        public async Task ShouldGetLightSensorForZone()
        {
            var dto = new LightSensorDto {Name = Guid.NewGuid().ToString()};
            SetupGet("/zones/88/lightsensor", dto);

            var actual = await Service.GetLightSensor(88);
            Assert.Equal(dto.Name, actual.Name);
            AssertUsedBearerToken();
        }

        [Fact]
        public async Task ShouldGetDevicesForZone()
        {
            SetupGetResultList<DeviceDto>("/zones/11/devices");

            var actual = await Service.GetDevices(11);
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldAddDevicetoZone()
        {
            var dto = new AddDeviceToZoneDto {DeviceId = 76};
            SetupPut("/zones/77/devices");

            await Service.AddDevice(77, dto);
            Assert.Equal(dto.DeviceId, GetRequestContent<AddDeviceToZoneDto>().DeviceId);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
        }

        [Fact]
        public async Task ShouldGetDevicesAssignableToZone()
        {
            SetupGetResultList<DeviceDto>("/zones/99/assignabledevices");

            var actual = await Service.GetAssignableDevices(99);
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldDeleteLightFromZone()
        {
            SetupDelete("/zones/33/lights/12");

            await Service.RemoveLight(33, 12);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }

        [Fact]
        public async Task ShouldDeleteLightSensorFromZone()
        {
            SetupDelete("/zones/22/lightsensors/1");

            await Service.RemoveLightSensor(22, 1);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }

        [Fact]
        public async Task ShouldGetEventsForZone()
        {
            SetupGetResultList<EventDto>("/zones/66/events");

            var actual = await Service.GetEvents(66);
            AssertListDto(3, actual);
        }
    }
}
