﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Apis.Diagnostics;
using Igor.Gateway.Dtos.Diagnostics;
using Igor.Sdk.Common.Models;
using Xunit;

namespace Igor.Gateway.Api.Sdk.Tests.Apis
{
    public class DiagnosticsServiceTests : ApiServiceFixture<DiagnosticsService, IDiagnosticsService>
    {
        [Fact]
        public async Task ShouldGetVersion()
        {
            var dto = new VersionDto {Version = "abc"};

            SetupGet("/diagnostics/version", dto);

            var actual = await Service.GetVersion();

            Assert.Equal(dto.Version, actual.Version);
        }

        [Fact]
        public async Task ShouldGetWarnings()
        {
            var dto = new ResultList<DiagnosticWarningDto>(new List<DiagnosticWarningDto>
            {
                new DiagnosticWarningDto {Message = "abc"}
            });


            SetupGet("/diagnostics/warnings", dto);

            var actual = await Service.GetWarnings();

            Assert.Equal(dto.List.First().Message, actual.List.First().Message);
        }

        [Fact]
        public async Task ShouldSyncAll()
        {
            SetupPost("/diagnostics/syncall");

            await Service.SyncAll();
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Fact]
        public async Task ShouldCreateDiagnosticMessage()
        {
            SetupPost("/diagnostics/messages");

            await Service.CreateDiagnosticMessage(new DiagnosticMessageDto() {Message = Message.FirmwareDiagnostics, Parameters = null});
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }
    }
}