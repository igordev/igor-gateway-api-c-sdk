﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Apis.NetworkNodes;
using Igor.Gateway.Dtos.DeviceNodes;
using Igor.Gateway.Dtos.Devices;
using Igor.Gateway.Dtos.EmergencyLighting;
using Igor.Gateway.Dtos.Events;
using Igor.Gateway.Dtos.NetworkNodes;
using Xunit;

namespace Igor.Gateway.Api.Sdk.Tests.Apis
{
    public class NetworkNodeServiceTests : ApiServiceFixture<NetworkNodeService, INetworkNodeService>
    {
        [Fact]
        public async Task ShouldGetAllNetworkNodes()
        {
            SetupGetResultList<NetworkNodeDto>("/networknodes");

            var actual = await Service.GetAll();
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldGetNetworkNode()
        {
            var dto = new NetworkNodeDto { ExternalId = Guid.NewGuid().ToString() };
            SetupGet("/networknodes/45", dto);

            var actual = await Service.Get(45);
            Assert.Equal(dto.ExternalId, actual.ExternalId);
            AssertUsedBearerToken();
        }

        [Fact]
        public async Task ShouldCreateNetworkNode()
        {
            var dto = new CreateNetworkNodeDto { ExternalId = Guid.NewGuid().ToString() };
            SetupPost("/networknodes", 13);

            var actual = await Service.Create(dto);
            Assert.Equal(dto.ExternalId, GetRequestContent<CreateNetworkNodeDto>().ExternalId);
            Assert.Equal(13, actual);
        }

        [Fact]
        public async Task ShouldDeleteNetworkNode()
        {
            SetupDelete("/networknodes/55");

            await Service.Delete(55);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }

        [Fact]
        public async Task ShouldUpdateNetworkNode()
        {
            var dto = new UpdateNetworkNodeDto { Name = Guid.NewGuid().ToString() };
            SetupPut("/networknodes/65");

            await Service.Update(65, dto);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
            Assert.Equal(dto.Name, GetRequestContent<UpdateNetworkNodeDto>().Name);
        }

        [Fact]
        public async Task ShouldDeleteAllNetworkNdoes()
        {
            SetupDelete("/networknodes/deleteall");

            await Service.DeleteAll();
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }

        [Fact]
        public async Task ShouldGetDevicesInNetworkNodeChain()
        {
            SetupGetResultList<DeviceDto>("/networknodes/48/devices");

            var actual = await Service.GetDevicesInChain(48);
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldGetDevicesAttachedToNetworkNode()
        {
            SetupGetResultList<DeviceDto>("/networknodes/4/devicesattached");

            var actual = await Service.GetDevicesAttached(4);
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldGetDeviceNodesForNetworkNode()
        {
            SetupGetResultList<DeviceNodeDto>("/networknodes/12/devicenodes");

            var actual = await Service.GetDeviceNodes(12);
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldBringNetworkNodeOnline()
        {
            SetupPost("/networknodes/11/bringonline");

            await Service.BringOnline(11);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Fact]
        public async Task ShouldTakeNetworkNodeOffline()
        {
            SetupPost("/networknodes/11/takeoffline");

            await Service.TakeOffline(11);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Fact]
        public async Task ShouldUpdateEmergencyLevel()
        {
            var dto = new EmergencyLightingLevelDto { EmergencyLevel = 55};
            SetupPut("/networknodes/65/emergencylevel");

            await Service.UpdateEmergencyLevel(65, dto);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
            Assert.Equal(dto.EmergencyLevel, GetRequestContent<EmergencyLightingLevelDto>().EmergencyLevel);
        }

        [Fact]
        public async Task ShouldUpdateOfflineSettings()
        {
            var dto = new OfflineLightingSettingsDto { OfflineLevel = 2222, OfflineTimeout = 12000 };
            SetupPost("/networknodes/11/offlinesettings");

            await Service.UpdateOfflineSettings(11, dto);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
            Assert.Equal(dto.OfflineLevel, GetRequestContent<OfflineLightingSettingsDto>().OfflineLevel);
            Assert.Equal(dto.OfflineTimeout, GetRequestContent<OfflineLightingSettingsDto>().OfflineTimeout);
        }

        [Fact]
        public async Task ShouldDeleteOfflineSettings()
        {
            SetupDelete("/networknodes/123/offlinesettings");

            await Service.DeleteOfflineSettings(123);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }

        [Fact]
        public async Task ShouldGetEvents()
        {
            SetupGetResultList<EventDto>("/networknodes/123/events");

            var actual = await Service.GetEvents(123);
            AssertListDto(3, actual);
        }
    }
}
