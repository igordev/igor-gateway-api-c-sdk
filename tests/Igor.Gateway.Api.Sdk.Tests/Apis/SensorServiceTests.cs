﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Apis.Sensors;
using Igor.Gateway.Dtos.Sensors;
using Igor.Gateway.Dtos.Events;
using Igor.Gateway.Dtos.Tags;
using Xunit;

namespace Igor.Gateway.Api.Sdk.Tests.Apis
{
    public class SensorServiceTests : ApiServiceFixture<SensorService, ISensorService>
    {
        [Fact]
        public async Task ShouldGetAllSensors()
        {
            SetupGetResultList<SensorDto>("/sensors");

            var actual = await Service.GetAll();
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldGetSensor()
        {
            var dto = new SensorDto { ExternalId = Guid.NewGuid().ToString() };
            SetupGet("/sensors/21", dto);

            var actual = await Service.Get(21);
            Assert.Equal(dto.ExternalId, actual.ExternalId);
            AssertUsedBearerToken();
        }

        [Fact]
        public async Task ShouldCreateSensor()
        {
            var dto = new CreateSensorDto { ExternalId = Guid.NewGuid().ToString() };
            SetupPost("/sensors", 22);

            var actual = await Service.Create(dto);
            Assert.Equal(dto.ExternalId, GetRequestContent<CreateSensorDto>().ExternalId);
            Assert.Equal(22, actual);
        }

        [Fact]
        public async Task ShouldDeleteSensor()
        {
            SetupDelete("/sensors/55");

            await Service.Delete(55);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }

        [Fact]
        public async Task ShouldUpdateSensor()
        {
            var dto = new UpdateSensorDto { Name = Guid.NewGuid().ToString() };
            SetupPut("/sensors/65");

            await Service.Update(65, dto);
            Assert.Equal(dto.Name, GetRequestContent<UpdateSensorDto>().Name);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
        }

        [Fact]
        public async Task ShouldGetEventsForSensor()
        {
            SetupGetResultList<EventDto>("/sensors/75/events");

            var actual = await Service.GetEvents(75);
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldCreateEventForSensor()
        {
            var dto = new CreateSensorEventDto { EventType = "testing" };
            SetupPost("/sensors/54/events");

            await Service.CreateEvent(54, dto);
            Assert.Equal("testing", GetRequestContent<CreateSensorEventDto>().EventType);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Fact]
        public async Task ShouldAddTag()
        {
            SetupPut("/sensors/4/tags");
            
            await Service.AddTag(4, new AddTagsDto {Names = new[] {"test"}});
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
            Assert.Equal("test", GetRequestContent<AddTagsDto>().Names.First());
        }

        [Fact]
        public async Task ShouldRemoveTag()
        {
            SetupDelete("/sensors/4/tags/test");

            await Service.RemoveTag(4, "test");
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }
    }
}
