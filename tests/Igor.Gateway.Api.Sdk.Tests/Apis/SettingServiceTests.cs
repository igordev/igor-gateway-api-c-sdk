﻿using System.Net.Http;
using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Apis.Settings;
using Igor.Gateway.Dtos.Settings;
using Xunit;

namespace Igor.Gateway.Api.Sdk.Tests.Apis
{
    public class SettingServiceTests : ApiServiceFixture<SettingService, ISettingService>
    {
        [Fact]
        public async Task ShouldGetAll()
        {
            SetupGetResultList<SettingDto>("/settings");

            var result = await Service.GetAll();
            AssertListDto(3, result);
        }

        [Fact]
        public async Task ShouldUpdate()
        {
            var dto = new UpdateSettingDto {Value = "update"};
            SetupPut("/settings/3");

            await Service.Update(3, dto);
            Assert.Equal(dto.Value, GetRequestContent<UpdateSettingDto>().Value);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
        }
    }
}