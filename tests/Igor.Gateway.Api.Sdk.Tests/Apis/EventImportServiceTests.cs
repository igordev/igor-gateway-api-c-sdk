﻿using System.IO;
using System.Net;
using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Apis.Data;
using Xunit;

namespace Igor.Gateway.Api.Sdk.Tests.Apis
{
    public class EventImportServiceTests : ApiServiceFixture<EventImportService, IEventImportService>
    {
        [Fact]
        public async Task ImportShouldReturnResponse()
        {
            SetupPostEventFile("/data/events/import", "test");

            var stream = GenerateStreamFromString("testContent");
            var result = await Service.Import("test", stream);

            Assert.Equal(HttpStatusCode.Created, result.StatusCode);
        }

        private static Stream GenerateStreamFromString(string s)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
    }
}
