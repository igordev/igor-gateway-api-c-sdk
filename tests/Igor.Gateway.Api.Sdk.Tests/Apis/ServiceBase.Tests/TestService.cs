﻿using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Apis;
using Igor.Gateway.Api.Sdk.Core.Config;
using Igor.Gateway.Api.Sdk.Core.Http;
using Igor.Gateway.Api.Sdk.Core.Http.Security;
using Igor.Gateway.Api.Sdk.Core.Serialization;

namespace Igor.Gateway.Api.Sdk.Tests.Apis.ServiceBase.Tests
{
    public class GetResponse
    {
        public string Response { get; set; }
    }

    public class TestService : Sdk.Apis.ServiceBase
    {
        public const string Endpoint = "/some/{0}/route/{1}/execute";

        public TestService(string apiKey) : base(apiKey)
        {
        }

        public TestService(string apiKey, string apiAddress) : base(apiKey, apiAddress)
        {
        }

        public TestService(
            IHttpClientService httpClient,
            IJsonSerializer jsonSerializer,
            ISdkConfig config,
            IJwtTokenProvider tokenProvider,
            IRequestHeaderProvider requestHeaderProvider
        ) : base(httpClient, jsonSerializer, config, tokenProvider, requestHeaderProvider)
        {
        }

        public async Task<GetResponse> GetResponse()
        {
            return await GetHttpGetResponse<GetResponse>("");
        }

        public HttpResponseMessage PostResponse(GetResponse dto)
        {
            return GetHttpPostResponse(Endpoint, dto, 1, 2).Result;
        }

        public HttpResponseMessage PutResponse(GetResponse dto)
        {
            return GetHttpPutResponse(Endpoint, dto, 1, 2).Result;
        }

        public HttpResponseMessage DeleteResponse()
        {
            return GetHttpDeleteResponse(Endpoint, 1, 2).Result;
        }

        public GetResponse GetAsync()
        {
            return GetAsync<GetResponse>("").Result;
        }

        public HttpResponseMessage PostAsync(string body = null)
        {
            var endpoint = FormatEndpoint(Endpoint, 1, 2);

            return PostAsync(endpoint, body).Result;
        }

        public HttpResponseMessage PostFileStreamAsync(string content)
        {
            var endpoint = FormatEndpoint(Endpoint, 1, 2);

            var stream = GenerateStreamFromString(content);

            return PostCsvFileAsync(endpoint, stream).Result;
        }

        private static Stream GenerateStreamFromString(string s)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        public int PostReturnIdAsync(string body = null)
        {
            var endpoint = FormatEndpoint(Endpoint, 1, 2);

            var response = PostAsync(endpoint, body).Result;

            return GetId(response);
        }

        public int PostReturnIdIndexAsync(string body = null)
        {
            var endpoint = FormatEndpoint(Endpoint, 1, 2);

            var response = PostAsync(endpoint, body).Result;

            return GetId(response, 1);
        }

        public HttpResponseMessage PutAsync(string body = null)
        {
            var endpoint = FormatEndpoint(Endpoint, 1, 2);

            return PutAsync(endpoint, body).Result;
        }

        public HttpResponseMessage DeleteAsync()
        {
            var endpoint = FormatEndpoint(Endpoint, 1, 2);

            return DeleteAsync(endpoint).Result;
        }

        public string GetFormatEndpoint()
        {
            var endpoint = FormatEndpoint(Endpoint, 1, 2);

            return endpoint;
        }

        public ISdkConfig SdkConfig => Config;

    }
}
