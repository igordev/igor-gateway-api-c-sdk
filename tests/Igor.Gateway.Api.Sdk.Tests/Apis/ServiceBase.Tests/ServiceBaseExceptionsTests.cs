﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Igor.Exceptions;
using Igor.Gateway.Api.Sdk.Apis;
using Igor.Gateway.Api.Sdk.Core.Config;
using Igor.Gateway.Api.Sdk.Core.Exceptions;
using Igor.Gateway.Api.Sdk.Core.Http;
using Igor.Gateway.Api.Sdk.Core.Http.Security;
using Igor.Gateway.Api.Sdk.Core.Serialization;
using Moq;
using Xunit;

namespace Igor.Gateway.Api.Sdk.Tests.Apis.ServiceBase.Tests
{
    public class ServiceBaseExceptionsTests
    {
        private readonly Mock<IHttpClientService> _httpClientServiceMock;
        private readonly Mock<IJwtTokenProvider> _tokenProviderMock;
        private readonly IJsonSerializer _jsonSerializer;
        private readonly Mock<IRequestHeaderProvider> _requestHeaderProviderMock;

        private readonly SdkConfig _sdkConfig;

        private const string ConfigApiEndpoint = "http://localhost/api";
        private const string ErrorMessage = "{\"Message\":\"Some message\", \"ErrorCode\": 1231}";
        private readonly string _uri = string.Format("http://localhost/api" + TestService.Endpoint, 1, 2);

        public ServiceBaseExceptionsTests()
        {
            _httpClientServiceMock = new Mock<IHttpClientService>();

            _tokenProviderMock = new Mock<IJwtTokenProvider>();
            _tokenProviderMock.Setup(p => p.GetToken(It.IsAny<string>())).Returns("token");

            _requestHeaderProviderMock = new Mock<IRequestHeaderProvider>();

            _sdkConfig = new SdkConfig { ApiAddress = "http://localhost", ApiKey = "key" };

            _jsonSerializer = new JsonSerializer();
        }

        [Theory]
        [InlineData(typeof(IgorHttpException), HttpStatusCode.BadGateway)]
        [InlineData(typeof(IgorUnauthorizedException), HttpStatusCode.Unauthorized)]
        [InlineData(typeof(IgorNotFoundException), HttpStatusCode.NotFound)]
        [InlineData(typeof(IgorValidationException), HttpStatusCode.BadRequest)]
        public void TestGetAsyncShouldThrowException(Type exceptionType, HttpStatusCode statusCode)
        {
            var httpResponse = new HttpResponseMessage(statusCode)
            {
                Content = new StringContent(ErrorMessage)
            };

            _httpClientServiceMock
                .Setup(p => p.SendAsync(It.IsAny<IEnumerable<KeyValuePair<string, string>>>(), It.IsAny<HttpRequestMessage>()))
                .Returns(Task.FromResult(httpResponse));

            var service = new TestService(_httpClientServiceMock.Object, _jsonSerializer, _sdkConfig, _tokenProviderMock.Object, _requestHeaderProviderMock.Object);

            var ex = Assert.Throws<AggregateException>(() => service.GetAsync());
            
            Assert.Equal(statusCode, ((IgorHttpException)ex.InnerException).StatusCode);
            Assert.IsType(exceptionType.GetType(), ex.InnerException.GetType());
        }

        [Theory]
        [InlineData(typeof(IgorHttpException), HttpStatusCode.BadGateway)]
        [InlineData(typeof(IgorUnauthorizedException), HttpStatusCode.Unauthorized)]
        [InlineData(typeof(IgorNotFoundException), HttpStatusCode.NotFound)]
        [InlineData(typeof(IgorValidationException), HttpStatusCode.BadRequest)]
        public async Task TestGetResponseShouldThrowException(Type exceptionType, HttpStatusCode statusCode)
        {
            var httpResponse = new HttpResponseMessage(statusCode)
            {
                Content = new StringContent(ErrorMessage)
            };

            _httpClientServiceMock
                .Setup(p => p.SendAsync(It.IsAny<IEnumerable<KeyValuePair<string, string>>>(), It.Is<HttpRequestMessage>(q => q.RequestUri.AbsoluteUri.Contains(ConfigApiEndpoint))))
                .Returns(Task.FromResult(httpResponse));

            var service = new TestService(_httpClientServiceMock.Object, _jsonSerializer, _sdkConfig, _tokenProviderMock.Object, _requestHeaderProviderMock.Object);

            var ex = await Assert.ThrowsAsync(exceptionType, async () => await service.GetResponse());

            Assert.Equal(statusCode, ((IgorHttpException)ex).StatusCode);
            Assert.IsType(exceptionType.GetType(), ex.GetType());
        }

        [Theory]
        [InlineData(typeof(IgorHttpException), HttpStatusCode.BadGateway)]
        [InlineData(typeof(IgorUnauthorizedException), HttpStatusCode.Unauthorized)]
        [InlineData(typeof(IgorNotFoundException), HttpStatusCode.NotFound)]
        [InlineData(typeof(IgorValidationException), HttpStatusCode.BadRequest)]
        public void TestPostAsyncShouldThrowException(Type exceptionType, HttpStatusCode statusCode)
        {
            var httpResponse = new HttpResponseMessage(statusCode)
            {
                Content = new StringContent(ErrorMessage)
            };

            _httpClientServiceMock
                .Setup(p => p.SendAsync(It.IsAny<IEnumerable<KeyValuePair<string, string>>>(), It.Is<HttpRequestMessage>(q => q.RequestUri.AbsoluteUri == _uri)))
                .Returns(Task.FromResult(httpResponse));

            var service = new TestService(_httpClientServiceMock.Object, _jsonSerializer, _sdkConfig, _tokenProviderMock.Object, _requestHeaderProviderMock.Object);
            var ex = Assert.Throws<AggregateException>(() => service.PostAsync());

            Assert.Equal(statusCode, ((IgorHttpException)ex.InnerException).StatusCode);
            Assert.IsType(exceptionType.GetType(), ex.InnerException.GetType());
        }

        [Theory]
        [InlineData(typeof(IgorHttpException), HttpStatusCode.BadGateway)]
        [InlineData(typeof(IgorUnauthorizedException), HttpStatusCode.Unauthorized)]
        [InlineData(typeof(IgorNotFoundException), HttpStatusCode.NotFound)]
        [InlineData(typeof(IgorValidationException), HttpStatusCode.BadRequest)]
        public void TestPostResponseShouldThrowException(Type exceptionType, HttpStatusCode statusCode)
        {
            var httpResponse = new HttpResponseMessage(statusCode)
            {
                Content = new StringContent(ErrorMessage)
            };

            _httpClientServiceMock
                .Setup(p => p.SendAsync(It.IsAny<IEnumerable<KeyValuePair<string, string>>>(), It.Is<HttpRequestMessage>(q => q.RequestUri.AbsoluteUri == _uri)))
                .Returns(Task.FromResult(httpResponse));

            var service = new TestService(_httpClientServiceMock.Object, _jsonSerializer, _sdkConfig, _tokenProviderMock.Object, _requestHeaderProviderMock.Object);

            var ex = Assert.Throws<AggregateException>(() => service.PostResponse(null));

            Assert.Equal(statusCode, ((IgorHttpException)ex.InnerException).StatusCode);
            Assert.IsType(exceptionType.GetType(), ex.InnerException.GetType());
        }

        [Theory]
        [InlineData(typeof(IgorHttpException), HttpStatusCode.BadGateway)]
        [InlineData(typeof(IgorUnauthorizedException), HttpStatusCode.Unauthorized)]
        [InlineData(typeof(IgorNotFoundException), HttpStatusCode.NotFound)]
        [InlineData(typeof(IgorValidationException), HttpStatusCode.BadRequest)]
        public void TestPutAsyncShouldThrowException(Type exceptionType, HttpStatusCode statusCode)
        {
            var httpResponse = new HttpResponseMessage(statusCode)
            {
                Content = new StringContent(ErrorMessage)
            };

            _httpClientServiceMock
                .Setup(p => p.SendAsync(It.IsAny<IEnumerable<KeyValuePair<string, string>>>(), It.Is<HttpRequestMessage>(q => q.RequestUri.AbsoluteUri == _uri)))
                .Returns(Task.FromResult(httpResponse));

            var service = new TestService(_httpClientServiceMock.Object, _jsonSerializer, _sdkConfig, _tokenProviderMock.Object, _requestHeaderProviderMock.Object);
            var ex = Assert.Throws<AggregateException>(() => service.PutAsync());

            Assert.Equal(statusCode, ((IgorHttpException)ex.InnerException).StatusCode);
            Assert.IsType(exceptionType.GetType(), ex.InnerException.GetType());
        }

        [Theory]
        [InlineData(typeof(IgorHttpException), HttpStatusCode.BadGateway)]
        [InlineData(typeof(IgorUnauthorizedException), HttpStatusCode.Unauthorized)]
        [InlineData(typeof(IgorNotFoundException), HttpStatusCode.NotFound)]
        [InlineData(typeof(IgorValidationException), HttpStatusCode.BadRequest)]
        public void TestPutResponseShouldThrowException(Type exceptionType, HttpStatusCode statusCode)
        {
            var httpResponse = new HttpResponseMessage(statusCode)
            {
                Content = new StringContent(ErrorMessage)
            };

            _httpClientServiceMock
                .Setup(p => p.SendAsync(It.IsAny<IEnumerable<KeyValuePair<string, string>>>(), It.Is<HttpRequestMessage>(q => q.RequestUri.AbsoluteUri == _uri)))
                .Returns(Task.FromResult(httpResponse));

            var service = new TestService(_httpClientServiceMock.Object, _jsonSerializer, _sdkConfig, _tokenProviderMock.Object, _requestHeaderProviderMock.Object);

            var ex = Assert.Throws<AggregateException>(() => service.PutResponse(null));

            Assert.Equal(statusCode, ((IgorHttpException)ex.InnerException).StatusCode);
            Assert.IsType(exceptionType.GetType(), ex.InnerException.GetType());
        }

        [Theory]
        [InlineData(typeof(IgorHttpException), HttpStatusCode.BadGateway)]
        [InlineData(typeof(IgorUnauthorizedException), HttpStatusCode.Unauthorized)]
        [InlineData(typeof(IgorNotFoundException), HttpStatusCode.NotFound)]
        [InlineData(typeof(IgorValidationException), HttpStatusCode.BadRequest)]
        public void TestDeleteAsyncShouldThrowException(Type exceptionType, HttpStatusCode statusCode)
        {
            var httpResponse = new HttpResponseMessage(statusCode)
            {
                Content = new StringContent(ErrorMessage)
            };

            _httpClientServiceMock
                .Setup(p => p.SendAsync(It.IsAny<IEnumerable<KeyValuePair<string, string>>>(), It.Is<HttpRequestMessage>(q => q.RequestUri.AbsoluteUri == _uri)))
                .Returns(Task.FromResult(httpResponse));

            var service = new TestService(_httpClientServiceMock.Object, _jsonSerializer, _sdkConfig, _tokenProviderMock.Object, _requestHeaderProviderMock.Object);
            var ex = Assert.Throws<AggregateException>(() => service.DeleteAsync());

            Assert.Equal(statusCode, ((IgorHttpException)ex.InnerException).StatusCode);
            Assert.IsType(exceptionType.GetType(), ex.InnerException.GetType());
        }

        [Theory]
        [InlineData(typeof(IgorHttpException), HttpStatusCode.BadGateway)]
        [InlineData(typeof(IgorUnauthorizedException), HttpStatusCode.Unauthorized)]
        [InlineData(typeof(IgorNotFoundException), HttpStatusCode.NotFound)]
        [InlineData(typeof(IgorValidationException), HttpStatusCode.BadRequest)]
        public void TestDeleteResponseShouldThrowException(Type exceptionType, HttpStatusCode statusCode)
        {
            var httpResponse = new HttpResponseMessage(statusCode)
            {
                Content = new StringContent(ErrorMessage)
            };

            _httpClientServiceMock
                .Setup(p => p.SendAsync(It.IsAny<IEnumerable<KeyValuePair<string, string>>>(), It.Is<HttpRequestMessage>(q => q.RequestUri.AbsoluteUri == _uri)))
                .Returns(Task.FromResult(httpResponse));

            var service = new TestService(_httpClientServiceMock.Object, _jsonSerializer, _sdkConfig, _tokenProviderMock.Object, _requestHeaderProviderMock.Object);

            var ex = Assert.Throws<AggregateException>(() => service.DeleteResponse());

            Assert.Equal(statusCode, ((IgorHttpException)ex.InnerException).StatusCode);
            Assert.IsType(exceptionType.GetType(), ex.InnerException.GetType());
        }

        [Theory]
        [InlineData(typeof(IgorHttpException), HttpStatusCode.BadGateway)]
        [InlineData(typeof(IgorUnauthorizedException), HttpStatusCode.Unauthorized)]
        [InlineData(typeof(IgorNotFoundException), HttpStatusCode.NotFound)]
        [InlineData(typeof(IgorValidationException), HttpStatusCode.BadRequest)]
        public async Task TestShouldThrowIgorValidationResponseWithNoContent(Type exceptionType, HttpStatusCode statusCode)
        {
            var httpResponse = new HttpResponseMessage(statusCode);

            _httpClientServiceMock
                .Setup(p => p.SendAsync(It.IsAny<IEnumerable<KeyValuePair<string, string>>>(), It.Is<HttpRequestMessage>(q => q.RequestUri.AbsoluteUri.Contains(ConfigApiEndpoint))))
                .Returns(Task.FromResult(httpResponse));

            var service = new TestService(_httpClientServiceMock.Object, _jsonSerializer, _sdkConfig, _tokenProviderMock.Object, _requestHeaderProviderMock.Object);

            await Assert.ThrowsAsync(exceptionType, async () => await service.GetResponse());
        }
    }
}
