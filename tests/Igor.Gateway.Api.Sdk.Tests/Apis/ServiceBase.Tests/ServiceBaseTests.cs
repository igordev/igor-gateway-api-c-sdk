﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Apis;
using Igor.Gateway.Api.Sdk.Core.Config;
using Igor.Gateway.Api.Sdk.Core.Http;
using Igor.Gateway.Api.Sdk.Core.Http.Security;
using Igor.Gateway.Api.Sdk.Core.Serialization;
using Moq;
using Xunit;

namespace Igor.Gateway.Api.Sdk.Tests.Apis.ServiceBase.Tests
{
    public class ServiceBaseTests
    {
        private readonly Mock<IHttpClientService> _httpClientServiceMock;
        private readonly Mock<IJwtTokenProvider> _tokenProviderMock;
        private readonly Mock<IRequestHeaderProvider> _requestHeaderProviderMock;

        private SdkConfig _sdkConfig;
        private readonly TestService _service;

        private const string ConfigApiEndpoint = "http://localhost/api";

        public ServiceBaseTests()
        {
            _httpClientServiceMock = new Mock<IHttpClientService>();

            _tokenProviderMock = new Mock<IJwtTokenProvider>();
            _tokenProviderMock.Setup(p => p.GetToken(It.IsAny<string>())).Returns("token");

            _requestHeaderProviderMock = new Mock<IRequestHeaderProvider>();

            _sdkConfig = new SdkConfig {ApiAddress = "http://localhost", ApiKey = "key"};
            
            IJsonSerializer jsonSerializer = new JsonSerializer();

            _service = new TestService(_httpClientServiceMock.Object, jsonSerializer, _sdkConfig, _tokenProviderMock.Object, _requestHeaderProviderMock.Object);
        }

        [Fact]
        public void TestGetAsyncShouldGetResponse()
        {
            const string content = "{\"response\":\"test\"}";

            var httpResponse = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(content)
            };

            _httpClientServiceMock
                .Setup(p => p.SendAsync(It.IsAny<IEnumerable<KeyValuePair<string, string>>>(), It.IsAny<HttpRequestMessage>()))
                .Returns(Task.FromResult(httpResponse));

            var response = _service.GetAsync();

            Assert.Equal("test", response.Response);
        }

        [Fact]
        public async Task TestGetResponseShouldGetResponse()
        {
            const string content = "{\"response\":\"test\"}";

            var httpResponse = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(content)
            };

            _httpClientServiceMock
                .Setup(p => p.SendAsync(It.IsAny<IEnumerable<KeyValuePair<string, string>>>(), It.IsAny<HttpRequestMessage>()))
                .Returns(Task.FromResult(httpResponse));

            var response = await _service.GetResponse();

            Assert.Equal("test", response.Response);
        }

        [Fact]
        public void TestGetAsyncShouldGetResponseAsNull()
        {
            const string content = "";

            var httpResponse = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(content)
            };

            _httpClientServiceMock
                .Setup(p => p.SendAsync(It.IsAny<IEnumerable<KeyValuePair<string, string>>>(), It.IsAny<HttpRequestMessage>()))
                .Returns(Task.FromResult(httpResponse));

            var response = _service.GetAsync();

            Assert.Null(response);
        }

        [Fact]
        public async Task TestGetResponseShouldGetResponseAsNull()
        {
            const string content = "";

            var httpResponse = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(content)
            };
            
            _httpClientServiceMock
                .Setup(p => p.SendAsync(It.IsAny<IEnumerable<KeyValuePair<string, string>>>(), It.IsAny<HttpRequestMessage>()))
                .Returns(Task.FromResult(httpResponse));

            var response = await _service.GetResponse();

            Assert.Null(response);
        }

        [Fact]
        public void TestShouldSetApiKeyValueAndApiAddressToLocalHost()
        {
            const string expectedKey = "ApiKey";
            const string expectedAddress = "http://localhost";

            var service = new TestService(expectedKey);

            Assert.Equal(expectedKey, service.SdkConfig.ApiKey);
            Assert.Equal(expectedAddress, service.SdkConfig.ApiAddress);
        }

        [Fact]
        public void TestShouldSetApiKeyValueAndApiAddressValue()
        {
            const string expectedKey = "ApiKey";
            const string expectedAddress = "ApiAddress";

            var service = new TestService(expectedKey, expectedAddress);

            Assert.Equal(expectedKey, service.SdkConfig.ApiKey);
            Assert.Equal(expectedAddress, service.SdkConfig.ApiAddress);
        }

        [Fact]
        public void TestShouldFormatString()
        {
            var expected = string.Format("http://localhost/api" + TestService.Endpoint, 1, 2);

            var service = new TestService(null, null);

            var actual = service.GetFormatEndpoint();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TestShouldPostToEndpointWithNullBody()
        {
            const HttpStatusCode expectedStatus = HttpStatusCode.Created;

            var httpResponse = new HttpResponseMessage(expectedStatus);

            _sdkConfig = new SdkConfig();

            var uri = string.Format("http://localhost/api" + TestService.Endpoint, 1, 2);

            _httpClientServiceMock
                .Setup(p => p.SendAsync(It.IsAny<IEnumerable<KeyValuePair<string, string>>>(), It.IsAny<HttpRequestMessage>()))
                .Returns(Task.FromResult(httpResponse));

            var response = _service.PostAsync();

            _httpClientServiceMock
                .Verify(p => p.SendAsync(It.IsAny<IEnumerable<KeyValuePair<string, string>>>(), It.Is<HttpRequestMessage>(q => q.RequestUri.AbsoluteUri == uri)), Times.Once);

            Assert.Equal(expectedStatus, response.StatusCode);
        }

        [Fact]
        public void TestGetPostResponseShouldPostToEndpointWithNullBody()
        {
            const HttpStatusCode expectedStatus = HttpStatusCode.Created;

            var httpResponse = new HttpResponseMessage(expectedStatus);

            var uri = string.Format("http://localhost/api" + TestService.Endpoint, 1, 2);

            _httpClientServiceMock
                .Setup(p => p.SendAsync(It.IsAny<IEnumerable<KeyValuePair<string, string>>>(), It.IsAny<HttpRequestMessage>()))
                .Returns(Task.FromResult(httpResponse));

            var response = _service.PostResponse(null);

            _httpClientServiceMock
                .Verify(p => p.SendAsync(It.IsAny<IEnumerable<KeyValuePair<string, string>>>(), It.Is<HttpRequestMessage>(q => q.RequestUri.AbsoluteUri == uri)), Times.Once);

            Assert.Equal(expectedStatus, response.StatusCode);
        }

        [Fact]
        public void TestShouldPostToEndpointWithBody()
        {
            const HttpStatusCode expectedStatus = HttpStatusCode.Created;

            const string requestBody = "body";

            var httpResponse = new HttpResponseMessage(expectedStatus);

            _sdkConfig = new SdkConfig();

            var uri = string.Format("http://localhost/api" + TestService.Endpoint, 1, 2);

            _httpClientServiceMock
                .Setup(p => p.SendAsync(It.IsAny<IEnumerable<KeyValuePair<string, string>>>(), It.IsAny<HttpRequestMessage>()))
                .Returns(Task.FromResult(httpResponse));

            var response = _service.PostAsync(requestBody);

            _httpClientServiceMock
                .Verify(p => p.SendAsync(It.IsAny<IEnumerable<KeyValuePair<string, string>>>(), It.Is<HttpRequestMessage>(q => q.RequestUri.AbsoluteUri == uri)), Times.Once);

            Assert.Equal(expectedStatus, response.StatusCode);
        }

        [Fact]
        public void TestShouldPostFileContentToEndpointWithBody()
        {
            const HttpStatusCode expectedStatus = HttpStatusCode.Created;

            const string requestBody = "fileContent";

            var httpResponse = new HttpResponseMessage(expectedStatus);

            _sdkConfig = new SdkConfig();

            var uri = string.Format("http://localhost/api" + TestService.Endpoint, 1, 2);

            _httpClientServiceMock
                .Setup(p => p.SendAsync(It.IsAny<IEnumerable<KeyValuePair<string, string>>>(), It.IsAny<HttpRequestMessage>()))
                .Returns(Task.FromResult(httpResponse));

            var response = _service.PostFileStreamAsync(requestBody);

            _httpClientServiceMock
                .Verify(p => p.SendAsync(It.IsAny<IEnumerable<KeyValuePair<string, string>>>(), It.Is<HttpRequestMessage>(q => q.RequestUri.AbsoluteUri == uri)), Times.Once);

            Assert.Equal(expectedStatus, response.StatusCode);
        }

        [Fact]
        public void TestPostResponseShouldPostToEndpointWithBody()
        {
            const HttpStatusCode expectedStatus = HttpStatusCode.Created;

            var httpResponse = new HttpResponseMessage(expectedStatus);

            var uri = string.Format("http://localhost/api" + TestService.Endpoint, 1, 2);

            _httpClientServiceMock
                .Setup(p => p.SendAsync(It.IsAny<IEnumerable<KeyValuePair<string, string>>>(), It.IsAny<HttpRequestMessage>()))
                .Returns(Task.FromResult(httpResponse));

            var response = _service.PostResponse(new GetResponse());

            _httpClientServiceMock
                .Verify(p => p.SendAsync(It.IsAny<IEnumerable<KeyValuePair<string, string>>>(), It.Is<HttpRequestMessage>(q => q.RequestUri.AbsoluteUri == uri)), Times.Once);

            Assert.Equal(expectedStatus, response.StatusCode);
        }

        [Fact]
        public void TestShouldPutToEndpointWithNullBody()
        {
            const HttpStatusCode expectedStatus = HttpStatusCode.NoContent;

            var httpResponse = new HttpResponseMessage(expectedStatus);

            _sdkConfig = new SdkConfig();

            var uri = string.Format("http://localhost/api" + TestService.Endpoint, 1, 2);

            _httpClientServiceMock
                .Setup(p => p.SendAsync(It.IsAny<IEnumerable<KeyValuePair<string, string>>>(), It.IsAny<HttpRequestMessage>()))
                .Returns(Task.FromResult(httpResponse));

            var response = _service.PutAsync();

            _httpClientServiceMock
                .Verify(p => p.SendAsync(It.IsAny<IEnumerable<KeyValuePair<string, string>>>(), It.Is<HttpRequestMessage>(q => q.RequestUri.AbsoluteUri == uri)), Times.Once);

            Assert.Equal(expectedStatus, response.StatusCode);
        }

        [Fact]
        public void TestPutResponseShouldPutToEndpointWithNullBody()
        {
            const HttpStatusCode expectedStatus = HttpStatusCode.NoContent;

            var httpResponse = new HttpResponseMessage(expectedStatus);

            _sdkConfig = new SdkConfig();

            var uri = string.Format("http://localhost/api" + TestService.Endpoint, 1, 2);

            _httpClientServiceMock
                .Setup(p => p.SendAsync(It.IsAny<IEnumerable<KeyValuePair<string, string>>>(), It.IsAny<HttpRequestMessage>()))
                .Returns(Task.FromResult(httpResponse));

            var response = _service.PutResponse(null);

            _httpClientServiceMock
                .Verify(p => p.SendAsync(It.IsAny<IEnumerable<KeyValuePair<string, string>>>(), It.Is<HttpRequestMessage>(q => q.RequestUri.AbsoluteUri == uri)), Times.Once);

            Assert.Equal(expectedStatus, response.StatusCode);
        }

        [Fact]
        public void TestShouldDeleteToEndpoint()
        {
            const HttpStatusCode expectedStatus = HttpStatusCode.NoContent;

            var httpResponse = new HttpResponseMessage(expectedStatus);

            _sdkConfig = new SdkConfig();

            var uri = string.Format("http://localhost/api" + TestService.Endpoint, 1, 2);

            _httpClientServiceMock
                .Setup(p => p.SendAsync(It.IsAny<IEnumerable<KeyValuePair<string, string>>>(), It.IsAny<HttpRequestMessage>()))
                .Returns(Task.FromResult(httpResponse));

            var response = _service.DeleteAsync();

            _httpClientServiceMock
                .Verify(p => p.SendAsync(It.IsAny<IEnumerable<KeyValuePair<string, string>>>(), It.Is<HttpRequestMessage>(q => q.RequestUri.AbsoluteUri == uri)), Times.Once);

            Assert.Equal(expectedStatus, response.StatusCode);
        }

        [Fact]
        public void TestDeleteResponseShouldDeleteToEndpoint()
        {
            const HttpStatusCode expectedStatus = HttpStatusCode.NoContent;

            var httpResponse = new HttpResponseMessage(expectedStatus);

            var uri = string.Format("http://localhost/api" + TestService.Endpoint, 1, 2);

            _httpClientServiceMock
                .Setup(p => p.SendAsync(It.IsAny<IEnumerable<KeyValuePair<string, string>>>(), It.IsAny<HttpRequestMessage>()))
                .Returns(Task.FromResult(httpResponse));

            var response = _service.DeleteResponse();

            _httpClientServiceMock
                .Verify(p => p.SendAsync(It.IsAny<IEnumerable<KeyValuePair<string, string>>>(), It.Is<HttpRequestMessage>(q => q.RequestUri.AbsoluteUri == uri)), Times.Once);
            
            Assert.Equal(expectedStatus, response.StatusCode);
        }

        [Fact]
        public void TestShouldPutToEndpointWithBody()
        {
            const HttpStatusCode expectedStatus = HttpStatusCode.NoContent;

            const string requestBody = "body";

            var httpResponse = new HttpResponseMessage(expectedStatus);

            _sdkConfig = new SdkConfig();

            var uri = string.Format("http://localhost/api" + TestService.Endpoint, 1, 2);

            _httpClientServiceMock
                .Setup(p => p.SendAsync(It.IsAny<IEnumerable<KeyValuePair<string, string>>>(), It.IsAny<HttpRequestMessage>()))
                .Returns(Task.FromResult(httpResponse));

            var response = _service.PutAsync(requestBody);

            _httpClientServiceMock
                .Verify(p => p.SendAsync(It.IsAny<IEnumerable<KeyValuePair<string, string>>>(), It.Is<HttpRequestMessage>(q => q.RequestUri.AbsoluteUri == uri)), Times.Once);
            
            Assert.Equal(expectedStatus, response.StatusCode);
        }

        [Fact]
        public void ShouldGetId()
        {
            const HttpStatusCode expectedStatus = HttpStatusCode.Created;

            var httpResponse = new HttpResponseMessage(expectedStatus);
            httpResponse.Headers.Location = new Uri("http://localhost/space/1");

            _sdkConfig = new SdkConfig();

            var uri = string.Format("http://localhost/api" + TestService.Endpoint, 1, 2);

            _httpClientServiceMock
                .Setup(p => p.SendAsync(It.IsAny<IEnumerable<KeyValuePair<string, string>>>(), It.IsAny<HttpRequestMessage>()))
                .Returns(Task.FromResult(httpResponse));

            var id = _service.PostReturnIdAsync();

            Assert.Equal(1, id);
        }

        [Fact]
        public void ShouldGetIdByIndex()
        {
            const HttpStatusCode expectedStatus = HttpStatusCode.Created;

            var httpResponse = new HttpResponseMessage(expectedStatus);
            httpResponse.Headers.Location = new Uri("http://localhost/space/1/policy");

            _sdkConfig = new SdkConfig();

            var uri = string.Format("http://localhost/api" + TestService.Endpoint, 1, 2);

            _httpClientServiceMock
                .Setup(p => p.SendAsync(It.IsAny<IEnumerable<KeyValuePair<string, string>>>(), It.IsAny<HttpRequestMessage>()))
                .Returns(Task.FromResult(httpResponse));

            var id = _service.PostReturnIdIndexAsync();

            Assert.Equal(1, id);
        }

        [Fact]
        public void ShouldGetWithoutToken()
        {
            _tokenProviderMock.Setup(p => p.GetToken(It.IsAny<string>())).Returns("");
            
            var httpResponse = new HttpResponseMessage(HttpStatusCode.OK);
            
            _httpClientServiceMock
                .Setup(p => p.SendAsync(It.IsAny<IEnumerable<KeyValuePair<string, string>>>(), It.IsAny<HttpRequestMessage>()))
                .Returns(Task.FromResult(httpResponse));

            _sdkConfig = new SdkConfig();
            
            _service.GetAsync();
            
            _httpClientServiceMock.Verify(p => p.SendAsync(It.IsAny<IEnumerable<KeyValuePair<string, string>>>(), It.IsAny<HttpRequestMessage>()), Times.Once);
        }
    }
}
