﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Apis.WallControls;
using Igor.Gateway.Dtos.Events;
using Igor.Gateway.Dtos.Tags;
using Igor.Gateway.Dtos.WallControls;
using Xunit;

namespace Igor.Gateway.Api.Sdk.Tests.Apis
{
    public class WallControlServiceTests : ApiServiceFixture<WallControlService, IWallControlService>
    {
        [Fact]
        public async Task ShouldGetAllWallControls()
        {
            SetupGetResultList<WallControlDto>("/wallcontrols");

            var actual = await Service.GetAll();
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldGetWallControl()
        {
            var dto = new WallControlDto { ExternalId = Guid.NewGuid().ToString() };
            SetupGet("/wallcontrols/65", dto);

            var actual = await Service.Get(65);
            Assert.Equal(dto.ExternalId, actual.ExternalId);
            AssertUsedBearerToken();
        }

        [Fact]
        public async Task ShouldCreateWallControl()
        {
            var dto = new CreateWallControlDto { ExternalId = Guid.NewGuid().ToString() };
            SetupPost("/wallcontrols", 2);

            var actual = await Service.Create(dto);
            Assert.Equal(dto.ExternalId, GetRequestContent<CreateWallControlDto>().ExternalId);
            Assert.Equal(2, actual);
        }

        [Fact]
        public async Task ShouldDeleteWallControl()
        {
            SetupDelete("/wallcontrols/98");

            await Service.Delete(98);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }

        [Fact]
        public async Task ShouldUpdateWallControl()
        {
            var dto = new UpdateWallControlDto { Name = Guid.NewGuid().ToString() };
            SetupPut("/wallcontrols/75");

            await Service.Update(75, dto);
            Assert.Equal(dto.Name, GetRequestContent<UpdateWallControlDto>().Name);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
        }

        [Fact]
        public async Task ShouldGetWallControlButtons()
        {
            SetupGetResultList<WallControlButtonDto>("/wallcontrols/22/buttons");

            var actual = await Service.GetWallControlButtons(22);
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldUpdateWallControlButton()
        {
            var dto = new UpdateWallControlButtonDto { Name = "hi" };
            SetupPut("/wallcontrols/33/buttons/3");

            await Service.UpdateWallControlButton(33, 3, dto);
            Assert.Equal(dto.Name, GetRequestContent<UpdateWallControlButtonDto>().Name);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
        }

        [Fact]
        public async Task ShouldGetWallControlButtonGestures()
        {
            SetupGetResultList<WallControlGestureDto>("/wallcontrols/22/buttons/33/gestures");

            var actual = await Service.GetWallControlButtonGestures(22, 33);
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldCreateWallControlButtonGesture()
        {
            var dto = new CreateWallControlGestureDto { Type = WallControlGestureType.DoublePress };
            SetupPost("/wallcontrols/12/buttons/33/gestures", 5);

            await Service.CreateWallControlButtonGesture(12, 33, dto);
            Assert.Equal(WallControlGestureType.DoublePress, GetRequestContent<CreateWallControlGestureDto>().Type);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Fact]
        public async Task ShouldDeleteWallControlButtonGesture()
        {
            SetupDelete("/wallcontrols/98/buttons/33/gestures/9");

            await Service.DeleteWallControlButtonGesture(98, 33, 9);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }

        [Fact]
        public async Task ShouldGetWallControlButtonGesture()
        {
            var dto = new WallControlGestureDto { Type = WallControlGestureType.Hold };
            SetupGet("/wallcontrols/65/buttons/33/gestures/11", dto);

            var actual = await Service.GetWallControlButtonGesture(65, 33, 11);
            Assert.Equal(dto.Type, actual.Type);
            AssertUsedBearerToken();
        }

        [Fact]
        public async Task ShouldUpdateWallControlButtonGesture()
        {
            var dto = new UpdateWallControlGestureDto { Type = WallControlGestureType.Press };
            SetupPut("/wallcontrols/75/buttons/33/gestures/12");

            await Service.UpdateWallControlButtonGesture(75, 33, 12, dto);
            Assert.Equal(WallControlGestureType.Press, GetRequestContent<UpdateWallControlGestureDto>().Type);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
        }

        [Fact]
        public async Task ShouldResetWallControlButtonGesture()
        {
            SetupPost("/wallcontrols/75/buttons/33/gestures/12/reset");

            await Service.ResetWallControlButtonGesture(75, 33, 12);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Fact]
        public async Task ShouldGetEventsForWallControl()
        {
            SetupGetResultList<EventDto>("/wallcontrols/23/events");

            var actual = await Service.GetEvents(23);
            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldCreateEventForWallControl()
        {
            var dto = new CreateWallControlEventDto { EventType = WallControlEventType.Press };
            SetupPost("/wallcontrols/12/events");

            await Service.CreateEvent(12, dto);
            Assert.Equal(WallControlEventType.Press, GetRequestContent<CreateWallControlEventDto>().EventType);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Fact]
        public async Task ShouldUpdateWallControlSettings()
        {
            var button = new WallControlButtonSettingsDto(1, 1, 2);
            var dto = new WallControlSettingsDto(new[] { button });
            SetupPut("/wallcontrols/75/settings");

            await Service.UpdateSettings(75, dto);
            Assert.Equal(1, GetRequestContent<WallControlSettingsDto>().WallControlButtonSettings.First().ButtonId);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
        }

        [Fact]
        public async Task ShouldAddTag()
        {
            SetupPut("/wallcontrols/4/tags");
            
            await Service.AddTag(4, new AddTagsDto {Names = new[] {"test"}});
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
            Assert.Equal("test", GetRequestContent<AddTagsDto>().Names.First());
        }

        [Fact]
        public async Task ShouldRemoveTag()
        {
            SetupDelete("/wallcontrols/4/tags/test");

            await Service.RemoveTag(4, "test");
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }
    }
}
