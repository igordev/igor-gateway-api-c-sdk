﻿using System.Linq;
using Igor.Gateway.Api.Sdk.Apis.Actuators;
using Xunit;

namespace Igor.Gateway.Api.Sdk.Tests.Apis.Actuators
{
    public class CommandTypeDtoBuilderTests
    {
        [Fact]
        public void BuildCommandShouldAddCommandToCollection()
        {
            var dto = new CommandTypeDtoBuilder(new CommandCollectionDtoBuilder("name", new CreateActuatorDtoBuilder()))
                .WithPercentageCommand()
                .BuildCommand()
                .BuildCollection()
                .BuildActuator();

            Assert.Equal("Percentage", dto.Commands.First().Commands.First().CommandType);
        }
    }
}