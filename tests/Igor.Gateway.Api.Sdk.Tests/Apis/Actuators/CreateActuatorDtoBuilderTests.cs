﻿using Igor.Gateway.Api.Sdk.Apis.Actuators;
using Xunit;

namespace Igor.Gateway.Api.Sdk.Tests.Apis.Actuators
{
    public class CreateActuatorDtoBuilderTests
    {
        [Fact]
        public void BuildShouldReturnCreateActuatorDto()
        {
            var dto = new CreateActuatorDtoBuilder()
                .BuildActuator();

            Assert.NotNull(dto);
        }

        [Fact]
        public void WithNameShouldSetName()
        {
            var dto = new CreateActuatorDtoBuilder()
                .WithName("name")
                .BuildActuator();

            Assert.Equal("name", dto.Name);
        }
    }
}