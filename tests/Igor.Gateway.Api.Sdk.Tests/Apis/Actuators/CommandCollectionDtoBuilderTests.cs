﻿using System.Linq;
using Igor.Gateway.Api.Sdk.Apis.Actuators;
using Xunit;

namespace Igor.Gateway.Api.Sdk.Tests.Apis.Actuators
{
    public class CommandCollectionDtoBuilderTests
    {
        [Fact]
        public void BuildCollectionShouldAddCollectionToCreateActuatorDtoBuilder()
        {
            var dto = new CommandCollectionDtoBuilder("test", new CreateActuatorDtoBuilder())
                .BuildCollection()
                .BuildActuator();

            Assert.NotEmpty(dto.Commands);
        }

        [Fact]
        public void WithNameShouldSetName()
        {
            var dto = new CreateActuatorDtoBuilder()
                .WithName("name")
                .BuildActuator();

            Assert.Equal("name", dto.Name);
        }

        [Fact]
        public void WithCommandCollectionShouldSetCommandCollectionName()
        {
            var dto = new CreateActuatorDtoBuilder()
                .WithCommandCollection("name")
                .BuildCollection()
                .BuildActuator();

            Assert.Equal("name", dto.Commands.First().Name);
        }

        [Fact]
        public void WithPercentageCommandShouldAddPercentageCommand()
        {
            var dto = new CreateActuatorDtoBuilder()
                .WithCommandCollection("name")
                .WithPercentageCommand()
                .BuildCollection()
                .BuildActuator();

            Assert.Equal("Percentage", dto.Commands.First().Commands.First().CommandType);
        }

        [Fact]
        public void WithBinaryCommandShouldAddBinaryCommand()
        {
            var dto = new CreateActuatorDtoBuilder()
                .WithCommandCollection("name")
                .WithBinaryCommand()
                .BuildCollection()
                .BuildActuator();

            Assert.Equal("Binary", dto.Commands.First().Commands.First().CommandType);
        }

        [Fact]
        public void WithDiscreteValueShouldAddDiscreteCommandWithValue()
        {
            var dto = new CreateActuatorDtoBuilder()
                .WithCommandCollection("name")
                .WithDiscreteValue("value1")
                .BuildCommand()
                .BuildCollection()
                .BuildActuator();

            Assert.Equal("Discrete", dto.Commands.First().Commands.First().CommandType);
            Assert.Equal("DiscreteValue", dto.Commands.First().Commands.First().Parameters.First().ParameterType);
            Assert.Equal("value1", dto.Commands.First().Commands.First().Parameters.First().Value);
        }

        [Fact]
        public void WithRangeValuesShouldAddRangeCommandWithValues()
        {
            var dto = new CreateActuatorDtoBuilder()
                .WithCommandCollection("name")
                .WithRangeValues(1, 10)
                .BuildCommand()
                .BuildCollection()
                .BuildActuator();

            Assert.Equal("Range", dto.Commands.First().Commands.First().CommandType);
            Assert.Equal("RangeMin", dto.Commands.First().Commands.First().Parameters.First().ParameterType);
            Assert.Equal((decimal)1, dto.Commands.First().Commands.First().Parameters.First().Value);
            Assert.Equal("RangeMax", dto.Commands.First().Commands.First().Parameters.Last().ParameterType);
            Assert.Equal((decimal)10, dto.Commands.First().Commands.First().Parameters.Last().Value);
        }

        [Fact]
        public void WithPassthrough8CommandShouldAddPassthrough8Command()
        {
            var dto = new CreateActuatorDtoBuilder()
                .WithCommandCollection("name")
                .WithPassthrough8Command()
                .BuildCollection()
                .BuildActuator();

            Assert.Equal("Passthrough8", dto.Commands.First().Commands.First().CommandType);
        }

        [Fact]
        public void WithPassthrough8CommandShouldAddPassthrough16Command()
        {
            var dto = new CreateActuatorDtoBuilder()
                .WithCommandCollection("name")
                .WithPassthrough16Command()
                .BuildCollection()
                .BuildActuator();

            Assert.Equal("Passthrough16", dto.Commands.First().Commands.First().CommandType);
        }
    }
}