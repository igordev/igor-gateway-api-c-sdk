﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Apis.Actuators;
using Igor.Gateway.Dtos.Actuators;
using Igor.Gateway.Dtos.Events;
using Igor.Gateway.Dtos.Tags;
using Igor.Sdk.Common;
using Igor.Sdk.Common.Models;
using Moq;
using Xunit;

namespace Igor.Gateway.Api.Sdk.Tests.Apis.Actuators
{
    public class ActuatorServiceTests : ApiServiceFixture<ActuatorService, IActuatorService>
    {
        [Fact]
        public async Task ShouldGetAllActuators()
        {
            SetupGetResultList<ActuatorDto>("/actuators");

            var actual = await Service.GetAll();

            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldGetActuator()
        {
            var expected = new ActuatorDto {Id = 65};
            SetupGet("/actuators/65", expected);

            var actual = await Service.Get(65);

            Assert.Equal(65, actual.Id);
            AssertUsedBearerToken();
        }

        [Fact]
        public async Task ShouldCreateActuator()
        {
            var dto = new CreateActuatorDto {ExternalId = Guid.NewGuid().ToString()};
            SetupPost("/actuators", 1);

            var actual = await Service.Create(dto);

            Assert.Equal(dto.ExternalId, GetRequestContent<CreateActuatorDto>().ExternalId);
            Assert.Equal(1, actual);
        }

        [Fact]
        public async Task ShouldDeleteActuator()
        {
            SetupDelete("/actuators/76");

            await Service.Delete(76);

            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }

        [Fact]
        public async Task ShouldUpdateActuator()
        {
            var dto = new UpdateActuatorDto {Name = Guid.NewGuid().ToString()};
            SetupPut("/actuators/45");

            await Service.Update(45, dto);

            Assert.Equal(dto.Name, GetRequestContent<UpdateActuatorDto>().Name);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
        }

        [Fact]
        public async Task ShouldGetEventsForActuator()
        {
            SetupGetResultList<EventDto>("/actuators/65/events");

            var actual = await Service.GetEvents(65);

            AssertListDto(3, actual);
        }

        [Fact]
        public async Task ShouldCreateEventForActuator()
        {
            var dto = new CreateActuatorEventDto
            {
                CommandType = "Percentage",
                CommandCollection = "Serial1",
                Data = "1"
            };

            SetupPost("/actuators/54/events");

            await Service.CreateEvent(54, dto);

            Assert.Equal("Percentage", GetRequestContent<CreateActuatorEventDto>().CommandType);
            Assert.Equal("Serial1", GetRequestContent<CreateActuatorEventDto>().CommandCollection);
            Assert.Equal("1", GetRequestContent<CreateActuatorEventDto>().Data);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Fact]
        public async Task ShouldSendNativeCommand()
        {
            var dto = new NativeActuatorCommandParametersDto
            {
                CommandType = "Percentage",
                Value = 1
            };

            SetupPost("/actuators/64/command");

            await Service.CommandNative(64, dto, "name");

            var parameters = GetRequestContent<ActuatorCommandDto>()
                .GetParameters<NativeActuatorCommandParametersDto>();

            Assert.Equal("Percentage", parameters.CommandType);
            Assert.Equal(1, int.Parse(parameters.Value.ToString()));
            Assert.Equal("name", GetRequestContent<ActuatorCommandDto>().Name);
            Assert.Equal(ActuatorCommandType.Command, GetRequestContent<ActuatorCommandDto>().Type);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Fact]
        public async Task ShouldSendStringCommandParameters()
        {
            var dto = new StringActuatorCommandParametersDto
            {
                Encoding = "ASCII",
                Value = "test"
            };

            SetupPost("/actuators/64/command");

            await Service.CommandString(64, dto, "name");

            var parameters = GetRequestContent<ActuatorCommandDto>()
                .GetParameters<StringActuatorCommandParametersDto>();

            Assert.Equal("ASCII", parameters.Encoding);
            Assert.Equal("test", parameters.Value);
            Assert.Equal("name", GetRequestContent<ActuatorCommandDto>().Name);
            Assert.Equal(ActuatorCommandType.String, GetRequestContent<ActuatorCommandDto>().Type);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Fact]
        public async Task ShouldSendNativeCommandWithCollectionNameFromActuator()
        {
            var dto = new NativeActuatorCommandParametersDto
            {
                CommandType = "Percentage",
                Value = 1
            };

            var commandCollection = new List<CommandCollectionDto>();

            commandCollection.Add(new CommandCollectionDto("name"));

            var actuatorDto = new ActuatorDto
            {
                Id = 64,
                Commands = commandCollection
            };

            SetupGet("/actuators/74", actuatorDto);
            SetupPost("/actuators/74/command");

            await Service.CommandNative(74, dto);

            var parameters = GetRequestContent<ActuatorCommandDto>()
                .GetParameters<NativeActuatorCommandParametersDto>();

            Assert.Equal("Percentage", parameters.CommandType);
            Assert.Equal(1, int.Parse(parameters.Value.ToString()));
            Assert.Equal("name", GetRequestContent<ActuatorCommandDto>().Name);
            Assert.Equal(ActuatorCommandType.Command, GetRequestContent<ActuatorCommandDto>().Type);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Fact]
        public async Task ShouldThrowExceptionWhenMoreThanOneCommandCollectionExists()
        {
            var dto = new NativeActuatorCommandParametersDto
            {
                CommandType = "Percentage",
                Value = 1
            };

            var commandCollection = new List<CommandCollectionDto>();

            commandCollection.Add(new CommandCollectionDto("name"));
            commandCollection.Add(new CommandCollectionDto("name2"));

            var actuatorDto = new ActuatorDto
            {
                Id = 84,
                Name = "My Actuator",
                Commands = commandCollection
            };

            SetupGet("/actuators/84", actuatorDto);
            SetupPost("/actuators/84/command");

            var exception =
                await Assert.ThrowsAsync<InvalidOperationException>(async () => await Service.CommandNative(84, dto));

            Assert.Equal(
                "My Actuator (ID: 84) has more than one command collection. You must specify a command collection name.",
                exception.Message);
            Assert.Equal(0, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Theory]
        [InlineData(ActuatorStringEncoding.Ascii, "ASCII")]
        [InlineData(ActuatorStringEncoding.Unicode, "Unicode")]
        [InlineData(ActuatorStringEncoding.Utf8, "UTF-8")]
        public async Task ShouldSendStringCommand(ActuatorStringEncoding encoding, string encodingValue)
        {
            SetupPost("/actuators/74/command");

            await Service.CommandString(74, "test", encoding, "name");

            var parameters = GetRequestContent<ActuatorCommandDto>()
                .GetParameters<StringActuatorCommandParametersDto>();

            Assert.Equal(encodingValue, parameters.Encoding);
            Assert.Equal("test", parameters.Value);
            Assert.Equal("name", GetRequestContent<ActuatorCommandDto>().Name);
            Assert.Equal(ActuatorCommandType.String, GetRequestContent<ActuatorCommandDto>().Type);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Fact]
        public async Task ShouldSendPercentageCommandFromHelper()
        {
            SetupPost("/actuators/74/command");

            await Service.CommandPercentage(74, 1000, "name");

            var parameters = GetRequestContent<ActuatorCommandDto>()
                .GetParameters<NativeActuatorCommandParametersDto>();

            Assert.Equal(1000L, parameters.Value);
            Assert.Equal("name", GetRequestContent<ActuatorCommandDto>().Name);
            Assert.Equal(ActuatorCommandType.Command, GetRequestContent<ActuatorCommandDto>().Type);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Fact]
        public async Task ShouldSendBinaryCommandFromHelper()
        {
            SetupPost("/actuators/74/command");

            await Service.CommandBinary(74, true, "name");

            var parameters = GetRequestContent<ActuatorCommandDto>()
                .GetParameters<NativeActuatorCommandParametersDto>();

            Assert.Equal(1L, parameters.Value);
            Assert.Equal("name", GetRequestContent<ActuatorCommandDto>().Name);
            Assert.Equal(ActuatorCommandType.Command, GetRequestContent<ActuatorCommandDto>().Type);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Fact]
        public async Task ShouldDefaultToUtf8EncodingIfNotProvided()
        {
            var commandCollection = new List<CommandCollectionDto>();

            commandCollection.Add(new CommandCollectionDto("name"));

            var actuatorDto = new ActuatorDto
            {
                Id = 74,
                Name = "My Actuator",
                Commands = commandCollection
            };

            SetupGet("/actuators/74", actuatorDto);
            SetupPost("/actuators/74/command");

            await Service.CommandString(74, "test");

            var parameters = GetRequestContent<ActuatorCommandDto>()
                .GetParameters<StringActuatorCommandParametersDto>();

            Assert.Equal("UTF-8", parameters.Encoding);
            Assert.Equal("test", parameters.Value);
            Assert.Equal("name", GetRequestContent<ActuatorCommandDto>().Name);
            Assert.Equal(ActuatorCommandType.String, GetRequestContent<ActuatorCommandDto>().Type);
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Post));
        }

        [Fact]
        public async Task ShouldSendPercentageCommand()
        {
            var serviceMock = new Mock<IActuatorService>();

            await serviceMock.Object.SendPercentage(1, 2);

            serviceMock.Verify(p =>
                p.CommandNative(1,
                    It.Is<NativeActuatorCommandParametersDto>(q =>
                        q.CommandType == "Percentage" && int.Parse(q.Value.ToString()) == 2), null));
        }

        [Fact]
        public async Task ShouldSendBinaryCommand()
        {
            var serviceMock = new Mock<IActuatorService>();

            await serviceMock.Object.SendBinary(1, 1);

            serviceMock.Verify(p =>
                p.CommandNative(1,
                    It.Is<NativeActuatorCommandParametersDto>(q =>
                        q.CommandType == "Binary" && byte.Parse(q.Value.ToString()) == 1), null));
        }

        [Fact]
        public async Task ShouldSendPassthrough8Command()
        {
            var serviceMock = new Mock<IActuatorService>();

            await serviceMock.Object.SendPassthrough(1, new byte[] {1});

            serviceMock.Verify(p =>
                p.CommandNative(1, It.Is<NativeActuatorCommandParametersDto>(q => q.CommandType == "Passthrough8"),
                    null));
        }

        [Fact]
        public async Task ShouldSendPassthrough16Command()
        {
            var serviceMock = new Mock<IActuatorService>();

            var bytes = new List<byte>();

            for (int i = 0; i < 300; i++)
                bytes.Add(1);

            await serviceMock.Object.SendPassthrough(1, bytes.ToArray());

            serviceMock.Verify(p =>
                p.CommandNative(1, It.Is<NativeActuatorCommandParametersDto>(q => q.CommandType == "Passthrough16"),
                    null));
        }

        [Fact]
        public async Task ShouldSendRangeCommand()
        {
            var serviceMock = new Mock<IActuatorService>();

            await serviceMock.Object.SendRange(1, 1);

            serviceMock.Verify(p =>
                p.CommandNative(1,
                    It.Is<NativeActuatorCommandParametersDto>(q =>
                        q.CommandType == "Range" && int.Parse(q.Value.ToString()) == 1), null));
        }

        [Fact]
        public async Task ShouldSendDiscreteCommand()
        {
            var serviceMock = new Mock<IActuatorService>();

            await serviceMock.Object.SendDiscrete(1, 1);

            serviceMock.Verify(p =>
                p.CommandNative(1,
                    It.Is<NativeActuatorCommandParametersDto>(q =>
                        q.CommandType == "Discrete" && int.Parse(q.Value.ToString()) == 1), null));
        }

        [Fact]
        public async Task ShouldAddTag()
        {
            SetupPut("/actuators/4/tags");

            await Service.AddTag(4, new AddTagsDto {Names = new[] {"test"}});
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Put));
            Assert.Equal("test", GetRequestContent<AddTagsDto>().Names.First());
        }

        [Fact]
        public async Task ShouldRemoveTag()
        {
            SetupDelete("/actuators/4/tags/test");

            await Service.RemoveTag(4, "test");
            Assert.Equal(1, HttpClientService.TimesCalled(HttpMethod.Delete));
        }

        [Fact]
        public async Task ShouldGetUniqueCommandCollections()
        {
            SetupGetResultList<CommandCollectionDto>("/actuators/commandCollections", 3);

            var actual = await Service.GetUniqueCommandCollections();

            AssertListDto(3, actual);
        }
    }
}