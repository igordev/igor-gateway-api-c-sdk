﻿using Igor.Gateway.Api.Sdk.Core.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace Igor.Gateway.Api.Sdk.Tests.Fakes.Https
{
    public class FakeHttpClientService : IHttpClientService
    {
        private readonly Dictionary<HttpRequestMessage, HttpResponseMessage> _setupRequests;
        private readonly Dictionary<HttpMethod, int> _timesCalled;
        
        public HttpRequestMessage Request { get; private set; }
        public string RequestContent { get; private set; }
        public string BearerToken { get; private set; }
        public string RequestUrl => Request?.RequestUri?.ToString();

        public FakeHttpClientService()
        {
            _setupRequests = new Dictionary<HttpRequestMessage, HttpResponseMessage>();
            _timesCalled = new Dictionary<HttpMethod, int>();
        }

        public Task<HttpResponseMessage> DeleteAsync(IEnumerable<KeyValuePair<string, string>> headers, string url)
        {
            return SendRequestAsync(headers, new HttpRequestMessage(HttpMethod.Delete, url));
        }

        public Task<HttpResponseMessage> GetAsync(IEnumerable<KeyValuePair<string, string>> headers, string url)
        {
            return SendRequestAsync(headers, new HttpRequestMessage(HttpMethod.Get, url));
        }

        public async Task<string> GetStringAsync(IEnumerable<KeyValuePair<string, string>> headers, string url)
        {
            var response = await GetAsync(headers, url);
            return await response.Content.ReadAsStringAsync();
        }

        public Task<HttpResponseMessage> PostAsync(IEnumerable<KeyValuePair<string, string>> headers, string url, HttpContent content = null)
        {
            return SendRequestAsync(headers, new HttpRequestMessage(HttpMethod.Post, url)
            {
                Content = content
            });
        }

        public Task<HttpResponseMessage> PutAsync(IEnumerable<KeyValuePair<string, string>> headers, string url, HttpContent content = null)
        {
            return SendRequestAsync(headers, new HttpRequestMessage(HttpMethod.Put, url)
            {
                Content = content
            });
        }

        public Task<HttpResponseMessage> SendAsync(IEnumerable<KeyValuePair<string, string>> headers, HttpRequestMessage requestMessage)
        {
            return SendRequestAsync(headers, requestMessage);
        }

        public T GetRequestContent<T>()
        {
            var json = RequestContent;
            return JsonConvert.DeserializeObject<T>(json);
        }

        public HttpResponseMessage SetupResponse(string url, string responseContent, HttpMethod method = null, 
            HttpStatusCode status = HttpStatusCode.OK, Uri locationHeader = null)
        {
            var request = new HttpRequestMessage(method ?? HttpMethod.Get, url);
            var response = new HttpResponseMessage(status)
            {
                Content = new StringContent(responseContent),
                Headers = { Location = locationHeader }
            };
            
            _setupRequests[request] = response;
            return response;
        }

        public HttpResponseMessage SetupJsonResponse<T>(string url, T responseValue, HttpMethod method = null, HttpStatusCode status = HttpStatusCode.OK)
        {
            return SetupResponse(url, JsonConvert.SerializeObject(responseValue), method, status);
        }

        public HttpResponseMessage SetupGet(string url, string content, HttpStatusCode status = HttpStatusCode.OK)
        {
            return SetupResponse(url, content, HttpMethod.Get, status);
        }

        public HttpResponseMessage SetupGet<T>(string url, T value, HttpStatusCode status = HttpStatusCode.OK)
        {
            return SetupJsonResponse(url, value, HttpMethod.Get, status);
        }

        public HttpResponseMessage SetupPost(string url, HttpStatusCode status = HttpStatusCode.OK, Uri locationHeader = null)
        {
            return SetupResponse(url, "", HttpMethod.Post, status, locationHeader);
        }

        public HttpResponseMessage SetupPut(string url, HttpStatusCode status = HttpStatusCode.OK)
        {
            return SetupResponse(url, "", HttpMethod.Put, status);
        }

        public HttpResponseMessage SetupDelete(string url, HttpStatusCode status = HttpStatusCode.OK)
        {
            return SetupResponse(url, "", HttpMethod.Delete, status);
        }

        public void AssertHasBearerToken()
        {
            Assert.False(HasBearerToken(), "Request was made without using a Bearer Token");
        }

        public int TimesCalled(HttpMethod method)
        {
            return _timesCalled.TryGetValue(method, out var called) ? called : 0;
        }

        private bool HasBearerToken()
        {
            return string.IsNullOrWhiteSpace(BearerToken);
        }

        private async Task<HttpResponseMessage> SendRequestAsync(IEnumerable<KeyValuePair<string, string>> headers, HttpRequestMessage request)
        {
            foreach (var header in headers)
            {
                request.Headers.Add(header.Key, header.Value);
            }

            if (request.Headers.Authorization?.Parameter != null)
                BearerToken = request.Headers.Authorization.Parameter.Replace("Bearer ", "");

            Request = request;
            if (request.Content != null)
                RequestContent = await request.Content.ReadAsStringAsync();
            IncrementTimesCalled(request.Method);
            return GetResponseFromRequest(request);
        }

        private void IncrementTimesCalled(HttpMethod method)
        {
            if (_timesCalled.ContainsKey(method))
            {
                _timesCalled[method]++;
            }
            else
            {
                _timesCalled.Add(method, 1);
            }
        }

        private HttpResponseMessage GetResponseFromRequest(HttpRequestMessage request)
        {
            var matchingRequest = GetMatchingRequest(request);
            if (matchingRequest == null)
                throw new Exception($"No request has been setup for [{request.Method}] {request.RequestUri}");
            return _setupRequests[matchingRequest];
        }

        private HttpRequestMessage GetMatchingRequest(HttpRequestMessage request)
        {
            return _setupRequests.Keys
                .Where(r => string.Equals(r.RequestUri?.ToString(), request.RequestUri?.ToString(), StringComparison.OrdinalIgnoreCase))
                .FirstOrDefault(r => r.Method == request.Method);
        }
    }
}
