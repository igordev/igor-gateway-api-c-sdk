﻿using Igor.Gateway.Dtos.SpaceGroups;
using Xunit;

namespace Igor.Gateway.Dtos.Tests.SpaceGroups
{
    public class SpaceGroupDtoExtensionsTests
    {
        [Fact]
        public void TestShouldMapSpaceGroupDtoToUpdateSpaceGroupDto()
        {
            var spaceGroupDto = new SpaceGroupDto
            {
                Name = "name",
                ParentId= 1
            };

            var updateDto = spaceGroupDto.ToUpdateDto();

            Assert.Equal(spaceGroupDto.Name, updateDto.Name);
            Assert.Equal(spaceGroupDto.ParentId, updateDto.ParentId);
        }
    }
}