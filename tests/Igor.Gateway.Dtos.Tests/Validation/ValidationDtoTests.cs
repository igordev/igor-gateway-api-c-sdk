﻿using System.Linq;
using Igor.Gateway.Dtos.Validation;
using Xunit;

namespace Igor.Gateway.Dtos.Tests.Validation
{
    public class ValidationDtoTests
    {
        [Fact]
        public void ShouldGroupErrorsByMessages()
        {
            var errors = new[]
            {
                new ValidationError("one", "good", 0),
                new ValidationError("two", "", 1),
                new ValidationError("one", "bad", 2),
            };

            var dto = new ValidationDto(errors.ToList());
            Assert.Equal(2, dto.ValidationErrors.Count);
            Assert.Equal(2, dto.ValidationErrors[0].Messages.Length);
            Assert.Equal(1, dto.ValidationErrors[1].Messages.Length);
        }

        [Fact]
        public void ShouldHaveEmptyErrors()
        {
            var dto = new ValidationDto(null);
            Assert.Empty(dto.ValidationErrors);
        }
    }
}
