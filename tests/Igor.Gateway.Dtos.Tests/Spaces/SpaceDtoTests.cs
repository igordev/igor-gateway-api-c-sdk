﻿using Igor.Gateway.Dtos.Spaces;
using Xunit;

namespace Igor.Gateway.Dtos.Tests.Spaces
{
    public class SpaceDtoTests
    {
        [Fact]
        public void ConstructorShouldInitializeControlType()
        {
            var spaceDto = new SpaceDto();

            Assert.Equal(3, spaceDto.ControlType);
        }

        [Fact]
        public void TestShouldCastToUpdateDto()
        {
            var spaceDto = new SpaceDto
            {
                Name = "name",
                Mode = SpaceMode.Occupancy,
                ControlType = 1
            };

            var updateDto = (UpdateSpaceDto) spaceDto;

            Assert.Equal(spaceDto.Name, updateDto.Name);
            Assert.Equal(SpaceMode.Occupancy, updateDto.Mode);
            Assert.Equal(spaceDto.ControlType, updateDto.ControlType);
        }
    }
}
