﻿using System;
using Igor.Gateway.Dtos.Spaces;
using Xunit;

namespace Igor.Gateway.Dtos.Tests.Spaces
{
    public class SpaceDtoExtensionsTests
    {
        [Fact]
        public void TestShouldMapSpaceDtoToUpdateSpaceDto()
        {
            var spaceDto = new SpaceDto
            {
                Name = "name",
                Mode = SpaceMode.Occupancy,
                ControlType = 1
            };

            var updateDto = spaceDto.ToUpdateDto();

            Assert.Equal(spaceDto.Name, updateDto.Name);
            Assert.Equal(SpaceMode.Occupancy, updateDto.Mode);
            Assert.Equal(spaceDto.ControlType, updateDto.ControlType);
        }

        [Fact]
        public void TestShouldMapSpaceDtoVacancyToUpdateSpaceDto()
        {
            var spaceDto = new SpaceDto
            {
                Mode = SpaceMode.Vacancy
            };

            var updateDto = spaceDto.ToUpdateDto();

            Assert.Equal(SpaceMode.Vacancy, updateDto.Mode);
        }

        [Fact]
        public void TestShouldThrowExceptionWhenModeIsNull()
        {
            var spaceDto = new SpaceDto()
            {
                Mode = (SpaceMode)(-1)
            };

            Assert.Throws<InvalidOperationException>(() => spaceDto.ToUpdateDto());
        }
    }
}