﻿using System;
using Igor.Gateway.Dtos.Actuators;
using Igor.Gateway.Dtos.Common;
using Igor.Gateway.Dtos.Devices;
using Igor.Gateway.Dtos.DryContacts;
using Igor.Gateway.Dtos.Sensors;
using Igor.Sdk.Common.Queries;
using Xunit;

namespace Igor.Gateway.Dtos.Tests.Devices
{
    public class DeviceSearchParametersTests
    {
        [Fact]
        public void ShouldReturnQueryStringWithDeviceIds()
        {
            var parameters = new DeviceSearchParameters
            {
                DeviceIds = new[] { 1, 2, 3 }
            };

            var queryString = parameters.AsQueryString();
            Assert.Equal("?deviceIds=1%2C2%2C3", queryString);
        }

        [Fact]
        public void ShouldReturnQueryStringWithTerm()
        {
            var parameters = new DeviceSearchParameters
            {
                Term = "Light"
            };

            Assert.Equal("?term=Light", parameters.AsQueryString());
        }

        [Fact]
        public void ShouldReturnQueryStringWithTypes()
        {
            var parameters = new DeviceSearchParameters
            {
                Types = new[] { DeviceType.Relay, DeviceType.Light }
            };

            var queryString = parameters.AsQueryString();
            Assert.Equal("?types=Relay%2CLight", queryString);
        }

        [Fact]
        public void ShouldReturnQueryStringWithActuatorSubtypes()
        {
            var parameters = new DeviceSearchParameters
            {
                ActuatorSubtypes = new[] { ActuatorSubtype.AirValve, ActuatorSubtype.ColdWaterValve }
            };

            var queryString = parameters.AsQueryString();
            Assert.Equal("?actuatorSubtypes=AirValve%2CColdWaterValve", queryString);
        }

        [Fact]
        public void ShouldReturnQueryStringWithSensorSubtypes()
        {
            var parameters = new DeviceSearchParameters
            {
                SensorSubtypes = new[] { SensorSubtype.BarometricPressure, SensorSubtype.OutdoorPhotocell }
            };

            var queryString = parameters.AsQueryString();
            Assert.Equal("?sensorSubtypes=BarometricPressure%2COutdoorPhotocell", queryString);
        }

        [Fact]
        public void ShouldReturnQueryStringWithDryContactType()
        {
            var parameters = new DeviceSearchParameters
            {
                DryContactTypes = new[] { DryContactType.DoorSensor, DryContactType.WaterDetectionSensor }
            };

            var queryString = parameters.AsQueryString();
            Assert.Equal("?dryContactTypes=DoorSensor%2CWaterDetectionSensor", queryString);
        }

        [Fact]
        public void ShouldReturnQueryStringWithIsOnline()
        {
            var parameters = new DeviceSearchParameters
            {
                IsOnline = true
            };

            var queryString = parameters.AsQueryString();
            Assert.Equal("?isOnline=true", queryString);
        }

        [Fact]
        public void ShouldReturnQueryStringWithIsQuarantined()
        {
            var parameters = new DeviceSearchParameters
            {
                IsQuarantined = true
            };

            var queryString = parameters.AsQueryString();
            Assert.Equal("?isQuarantined=true", queryString);
        }

        [Fact]
        public void ShouldReturnQueryStringWithIsEmergency()
        {
            var parameters = new DeviceSearchParameters
            {
                IsEmergency = true
            };

            var queryString = parameters.AsQueryString();
            Assert.Equal("?isEmergency=true", queryString);
        }

        [Fact]
        public void ShouldReturnQueryStringWithSpaceIds()
        {
            var parameters = new DeviceSearchParameters
            {
                SpaceIds = new[] { 5, 4, 1 }
            };

            var queryString = parameters.AsQueryString();
            Assert.Equal("?spaceIds=5%2C4%2C1", queryString);
        }

        [Fact]
        public void ShouldReturnQueryStringWithSwitchNames()
        {
            var parameters = new DeviceSearchParameters
            {
                NetworkSwitchNames = new[] { "one", "two", "three" }
            };

            var queryString = parameters.AsQueryString();
            Assert.Equal("?networkSwitchNames=one%2Ctwo%2Cthree", queryString);
        }

        [Fact]
        public void ShouldReturnQueryStringWithSwitchNamesAsNull()
        {
            var parameters = new DeviceSearchParameters
            {
                NetworkSwitchNames = null
            };

            Assert.Null(parameters.NetworkSwitchNames);
        }

        [Fact]
        public void ShouldReturnQueryStringWithSwitchNamesAsEmptyString()
        {
            var parameters = new DeviceSearchParameters
            {
                NetworkSwitchNames = new[] { "" }
            };

            Assert.Contains("", parameters.NetworkSwitchNames);
        }

        [Fact]
        public void ShouldReturnQueryStringWithNetworkSwitchIpAddresses()
        {
            var parameters = new DeviceSearchParameters
            {
                NetworkSwitchIPAddresses = new[] { "192.168.1.4", "124.45.78.15" }
            };

            var queryString = parameters.AsQueryString();
            Assert.Equal("?networkSwitchIpAddresses=192.168.1.4%2C124.45.78.15", queryString);
        }

        [Fact]
        public void ShouldReturnQueryStringWithOnlyLldpDevices()
        {
            var parameters = new DeviceSearchParameters
            {
                OnlyDevicesWithLLDP = true
            };

            var queryString = parameters.AsQueryString();
            Assert.Equal("?onlyDevicesWithLldp=true", queryString);
        }

        [Fact]
        public void ShouldReturnQueryStringWithAscendingSortDirection()
        {
            var parameters = new DeviceSearchParameters
            {
                SortDirection = QuerySortDirection.Ascending
            };

            var queryString = parameters.AsQueryString();
            Assert.Equal("?sortDir=asc", queryString);
        }

        [Fact]
        public void ShouldReturnQueryStringWithDescendingSortDirection()
        {
            var parameters = new DeviceSearchParameters
            {
                SortDirection = QuerySortDirection.Descending
            };

            var queryString = parameters.AsQueryString();
            Assert.Equal("?sortDir=desc", queryString);
        }

        [Fact]
        public void ShouldReturnQueryStringWithSortBy()
        {
            var parameters = new DeviceSearchParameters
            {
                SortBy = "Something"
            };

            var queryString = parameters.AsQueryString();
            Assert.Equal("?sortBy=Something", queryString);
        }

        [Fact]
        public void ShouldReturnQueryStringWithMinimumDiscoveredDate()
        {
            var parameters = new DeviceSearchParameters
            {
                MinimumDiscoveredDate = DateTime.SpecifyKind(new DateTime(2017, 5, 23), DateTimeKind.Utc)
            };
            var queryString = parameters.AsQueryString();
            Assert.Equal("?minDiscoveredDate=2017-05-23T00%3A00%3A00.0000000Z", queryString);
        }

        [Fact]
        public void ShouldReturnQueryStringWithMaximumDiscoveredDate()
        {
            var parameters = new DeviceSearchParameters
            {
                MaximumDiscoveredDate = DateTime.SpecifyKind(new DateTime(2017, 5, 23), DateTimeKind.Utc)
            };

            var queryString = parameters.AsQueryString();
            Assert.Equal("?maxDiscoveredDate=2017-05-23T00%3A00%3A00.0000000Z", queryString);
        }

        [Fact]
        public void ShouldReturnNullForSpaceIds()
        {
            var parameters = new DeviceSearchParameters();

            Assert.Null(parameters.SpaceIds);
        }

        [Fact]
        public void ShouldReturnNullForDeviceTypes()
        {
            var parameters = new DeviceSearchParameters();

            Assert.Null(parameters.Types);
        }

        [Fact]
        public void ShouldReturnNullForActuatorSubtypes()
        {
            var parameters = new DeviceSearchParameters();

            Assert.Null(parameters.ActuatorSubtypes);
        }

        [Fact]
        public void ShouldReturnNullForSensorSubtypes()
        {
            var parameters = new DeviceSearchParameters();

            Assert.Null(parameters.SensorSubtypes);
        }

        [Fact]
        public void ShouldReturnNullForDryContactTypes()
        {
            var parameters = new DeviceSearchParameters();

            Assert.Null(parameters.DryContactTypes);
        }

        [Fact]
        public void ShouldReturnQueryStringWithMultipleParameters()
        {
            var parameters = new DeviceSearchParameters
            {
                IsOnline = true,
                Types = new[] { DeviceType.Relay, DeviceType.LightSensor },
                SortDirection = QuerySortDirection.Descending,
                SpaceIds = new[] { 4, 3, 1 }
            };

            var queryString = parameters.AsQueryString();
            Assert.Contains("isOnline=true", queryString);
            Assert.Contains("types=Relay%2CLightSensor", queryString);
            Assert.Contains("sortDir=desc", queryString);
            Assert.Contains("spaceIds=4%2C3%2C1", queryString);
        }

        [Fact]
        public void ShouldReturnQueryStringWithSearchTypePartialWords()
        {
            var parameters = new DeviceSearchParameters
            {
                SearchType = SearchType.PartialWords
            };

            var queryString = parameters.AsQueryString();
            Assert.Equal("?searchType=PartialWords", queryString);
        }

        [Fact]
        public void ShouldReturnQueryStringWithSearchTypeWholeWords()
        {
            var parameters = new DeviceSearchParameters
            {
                SearchType = SearchType.WholeWords
            };

            var queryString = parameters.AsQueryString();
            Assert.Equal("?searchType=WholeWords", queryString);
        }

        [Fact]
        public void ShouldReturnQueryStringWithSearchTypeExactPhrase()
        {
            var parameters = new DeviceSearchParameters
            {
                SearchType = SearchType.ExactPhrase
            };

            var queryString = parameters.AsQueryString();
            Assert.Equal("?searchType=ExactPhrase", queryString);
        }
    }
}
