﻿using Igor.Gateway.Dtos.Actuators;
using Xunit;

namespace Igor.Gateway.Dtos.Tests.Actuators
{
    public class ActuatorSubtypeExtensionsTests
    {
        [Fact]
        public void IsOfSubtypeGroupShouldReturnTrue()
        {
            Assert.True(ActuatorSubtype.WindowShade.IsOfSubtypeGroup(ActuatorSubtype.Shade));
            Assert.True(ActuatorSubtype.Shade.IsOfSubtypeGroup(ActuatorSubtype.Shade));
            Assert.True(ActuatorSubtype.Pump.IsOfSubtypeGroup(ActuatorSubtype.ColdWaterPump));
        }

        [Fact]
        public void IsOfSubtypeGroupShouldReturnFalse()
        {
            Assert.False(ActuatorSubtype.WindowShade.IsOfSubtypeGroup(ActuatorSubtype.AirValve));
            Assert.False(ActuatorSubtype.WindowShade.IsOfSubtypeGroup(ActuatorSubtype.Pump));
            Assert.False(ActuatorSubtype.Shade.IsOfSubtypeGroup(ActuatorSubtype.AirValve));
            Assert.False(ActuatorSubtype.Shade.IsOfSubtypeGroup(ActuatorSubtype.Pump));
        }

        [Fact]
        public void IsAcCircuitShouldReturnCorrectValue()
        {
            Assert.True(ActuatorSubtype.AcCircuit.IsAcCircuit());
            Assert.False(ActuatorSubtype.Actuator.IsAcCircuit());
            Assert.False(ActuatorSubtype.DcCircuit.IsAcCircuit());
        }

        [Fact]
        public void IsDcCircuitShouldReturnCorrectValue()
        {
            Assert.True(ActuatorSubtype.DcCircuit.IsDcCircuit());
            Assert.False(ActuatorSubtype.AcCircuit.IsDcCircuit());
            Assert.False(ActuatorSubtype.Fan.IsDcCircuit());
        }

        [Fact]
        public void IsFanShouldReturnCorrectValue()
        {
            Assert.True(ActuatorSubtype.Fan.IsFan());
            Assert.False(ActuatorSubtype.DcCircuit.IsFan());
            Assert.False(ActuatorSubtype.Valve.IsFan());
        }

        [Fact]
        public void IsValveShouldReturnCorrectValue()
        {
            Assert.True(ActuatorSubtype.Valve.IsValve());
            Assert.False(ActuatorSubtype.DuctFan.IsValve());
            Assert.False(ActuatorSubtype.WindowShade.IsValve());
        }

        [Fact]
        public void IsShadeShouldReturnCorrectValue()
        {
            Assert.True(ActuatorSubtype.WindowShade.IsShade());
        }

        [Fact]
        public void IsPumpShouldReturnCorrectValue()
        {
            Assert.True(ActuatorSubtype.ColdWaterPump.IsPump());
        }

        [Fact]
        public void IsMotorShouldReturnCorrectValue()
        {
            Assert.True(ActuatorSubtype.Motor.IsMotor());
        }

        [Fact]
        public void IsLedBeaconShouldReturnCorrectValue()
        {
            Assert.True(ActuatorSubtype.LedBeacon.IsLedBeacon());
        }

        [Fact]
        public void IsLockShouldReturnCorrectValue()
        {
            Assert.True(ActuatorSubtype.ElectricStrikePlateLock.IsLock());
        }

        [Fact]
        public void IsRelayShouldReturnCorrectValue()
        {
            Assert.True(ActuatorSubtype.AcPlugRelay.IsRelay());
        }
    }
}