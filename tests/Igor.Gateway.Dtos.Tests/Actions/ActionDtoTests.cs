﻿using Igor.Gateway.Dtos.Actions;
using Newtonsoft.Json.Linq;
using Xunit;

namespace Igor.Gateway.Dtos.Tests.Actions
{
    public class ActionDtoTests
    {
        [Fact]
        public void ShouldTryGetParameters()
        {
            var dto = new ActionDto {Parameters = new SetLightingActionParametersDto()};

            var success = dto.TryGetParameters<SetLightingActionParametersDto>(out var parameters);

            Assert.True(success);
            Assert.IsType<SetLightingActionParametersDto>(parameters);
        }

        [Fact]
        public void ShouldTryGetParametersJObject()
        {
            var dto = new ActionDto {Parameters = JObject.FromObject(new SetLightingActionParametersDto())};

            var success = dto.TryGetParameters<SetLightingActionParametersDto>(out var parameters);

            Assert.True(success);
            Assert.IsType<SetLightingActionParametersDto>(parameters);
        }

        [Fact]
        public void TryGetParametersShouldReturnFalseOnInvalidType()
        {
            var dto = new ActionDto {Parameters = new SetLightingActionParametersDto()};

            var success = dto.TryGetParameters<ToggleLightingStateActionParametersDto>(out var parameters);

            Assert.False(success);
        }

        [Fact]
        public void TryGetParametersShouldReturnFalseOnException()
        {
            var dto = new ActionDto {Parameters = JObject.Parse(@"{level: 1E+37}")};

            var success = dto.TryGetParameters<SetLightingActionParametersDto>(out var parameters);

            Assert.False(success);
        }
    }
}