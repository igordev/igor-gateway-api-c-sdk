﻿using Igor.Gateway.Dtos.Actions;
using Igor.Gateway.Dtos.Common;
using Igor.Gateway.Dtos.Lighting;
using Xunit;

namespace Igor.Gateway.Dtos.Tests.Actions
{
    public class UpdateActionDtoTests
    {
        [Fact]
        public void ShouldBeAbleToConvertToActionDto()
        {
            var updateDto = new UpdateActionDto<dynamic>
            {
                Type = ActionType.SetLighting,
                Order = 12,
                Delay = 6500,
                PostDelay = 100,
                Parameters = new SetLightingActionParametersDto
                {
                    CCT = 1,
                    Duration = 3,
                    Level = 4,
                    State = OnOffState.On,
                    Behavior = Behavior.ConstantDuration,
                    CurveType = CurveType.SquareLaw,
                    UseAssignedSpace = true,
                    SpaceIds = new[] { 4, 3 },
                    LightIds = new[] { 2, 8 }
                }
            };

            var actionDto = (ActionDto)updateDto;
            var parameters = actionDto.GetParameters<SetLightingActionParametersDto>();
            Assert.Equal(ActionType.SetLighting, actionDto.Type);
            Assert.Equal(12, actionDto.Order);
            Assert.Equal(6500, actionDto.Delay);
            Assert.Equal(1, parameters.CCT);
            Assert.Equal(100, actionDto.PostDelay);
            Assert.Equal(3, parameters.Duration);
            Assert.Equal(4, parameters.Level);
            Assert.Equal(Behavior.ConstantDuration, parameters.Behavior);
            Assert.Equal(CurveType.SquareLaw, parameters.CurveType);
            Assert.Equal(OnOffState.On, parameters.State);
            Assert.True(parameters.UseAssignedSpace);
            Assert.Contains(2, parameters.LightIds);
            Assert.Contains(8, parameters.LightIds);
            Assert.Contains(4, parameters.SpaceIds);
            Assert.Contains(3, parameters.SpaceIds);
           
        }

        [Fact]
        public void ShouldBeAbleToCastNull()
        {
            UpdateActionDto<SetLightingActionParametersDto> updateDto = null;

            var actionDto = (ActionDto)updateDto;
            Assert.Null(actionDto);
        }
    }
}
