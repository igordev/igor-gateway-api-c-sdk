﻿using Igor.Gateway.Dtos.Actions;
using Igor.Gateway.Dtos.Common;
using Igor.Gateway.Dtos.Lighting;
using Xunit;

namespace Igor.Gateway.Dtos.Tests.Actions
{
    public class CreateActionDtoTests
    {
        [Fact]
        public void ShouldBeAbleToConvertToActionDto()
        {
            var createDto = new CreateActionDto<SetLightingActionParametersDto>
            {
                Type = ActionType.SetLighting,
                Order = 8,
                Delay = 9000,
                PostDelay = 100,
                Parameters = new SetLightingActionParametersDto
                {
                    CCT = 1,
                    Duration = 3,
                    Level = 4,
                    State = OnOffState.On,
                    CurveType = CurveType.Linear,
                    Behavior = Behavior.ConstantRate,
                    UseAssignedSpace = true,
                    SpaceIds = new[] {2, 5},
                    LightIds = new[] {9, 1},
                }
            };

            var actionDto = (ActionDto) createDto;
            var parameters = actionDto.GetParameters<SetLightingActionParametersDto>();
            Assert.Equal(ActionType.SetLighting, actionDto.Type);
            Assert.Equal(8, actionDto.Order);
            Assert.Equal(9000, actionDto.Delay);
            Assert.Equal(100, actionDto.PostDelay);
            Assert.Equal(1, parameters.CCT);
            Assert.Equal(3, parameters.Duration);
            Assert.Equal(4, parameters.Level);
            Assert.Equal(Behavior.ConstantRate, parameters.Behavior);
            Assert.Equal(CurveType.Linear, parameters.CurveType);
            Assert.Equal(OnOffState.On, parameters.State);
            Assert.True(parameters.UseAssignedSpace);
            Assert.Contains(2, parameters.SpaceIds);
            Assert.Contains(5, parameters.SpaceIds);
            Assert.Contains(1, parameters.LightIds);
            Assert.Contains(9, parameters.LightIds);
        }

        [Fact]
        public void ShouldReturnNullFromCast()
        {
            var createDto = null as CreateActionDto<SetLightingActionParametersDto>;

            var dto = (ActionDto) createDto;
            Assert.Null(dto);
        }
    }
}