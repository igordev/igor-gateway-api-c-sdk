﻿using System;
using System.Collections.Generic;
using System.Linq;
using Igor.Gateway.Dtos.Common;
using Igor.Gateway.Dtos.Lights;
using Igor.Gateway.Dtos.WebHooks;
using Newtonsoft.Json;
using Xunit;

namespace Igor.Gateway.Dtos.Tests.WebHooks
{
    public class WebHookEventNotificationDtoTests
    {
        [Fact]
        public void ShouldDeserializeValueAsType()
        {
            var notification = new WebHookEventNotificationDto
            {
                Value = JsonConvert.SerializeObject(new LightLightingChangedEventDto { Behavior = 32, CurveType = 12, Duration = 14, CCT = 76, Level = 19, State = OnOffState.On})
            };

            var value = notification.ValueToObject<LightLightingChangedEventDto>();
            Assert.Equal(32, value.Behavior);
            Assert.Equal(12, value.CurveType);
            Assert.Equal(14, value.Duration);
            Assert.Equal(76, value.CCT);
            Assert.Equal(19, value.Level);
            Assert.Equal(OnOffState.On, value.State);
        }

        [Fact]
        public void ShouldConvertObjectToType()
        {
            var @event = new WebHookEventDto
            {
                Attempt = 123,
                Id = Guid.NewGuid().ToString(),
                Notifications = new List<WebHookEventNotificationDto>
                {
                    new WebHookEventNotificationDto
                    {
                        Value = new
                        {
                            Behavior = 32,
                            CurveType = 12,
                            Duration = 14,
                            CCT = 76,
                            Level = 19,
                            State = OnOffState.Off
                        }
                    }
                }
            };

            var json = JsonConvert.SerializeObject(@event);

            var notification = JsonConvert.DeserializeObject<WebHookEventDto>(json).Notifications.First();

            var value = notification.ValueToObject<LightLightingChangedEventDto>();
            Assert.Equal(32, value.Behavior);
            Assert.Equal(12, value.CurveType);
            Assert.Equal(14, value.Duration);
            Assert.Equal(76, value.CCT);
            Assert.Equal(19, value.Level);
            Assert.Equal(OnOffState.Off, value.State);
        }

        [Fact]
        public void ShouldDeserializeArray()
        {
            var items = new[]
            {
                new LightsLightingChangedEventDto {ExternalId = Guid.NewGuid().ToString()},
                new LightsLightingChangedEventDto {ExternalId = Guid.NewGuid().ToString()},
            };
            var notification = new WebHookEventNotificationDto
            {
                Value = JsonConvert.SerializeObject(items)
            };

            var values = notification.ValueToArray<LightsLightingChangedEventDto>();
            Assert.Equal(items[0].ExternalId, values[0].ExternalId);
            Assert.Equal(items[1].ExternalId, values[1].ExternalId);
        }

        [Fact]
        public void ShouldConvertArrayToTypedArray()
        {
            var externalId = Guid.NewGuid().ToString();
            var @event = new WebHookEventDto
            {
                Attempt = 123,
                Id = Guid.NewGuid().ToString(),
                Notifications = new List<WebHookEventNotificationDto>
                {
                    new WebHookEventNotificationDto
                    {
                        Value = new[]
                        {
                            new
                            {
                                ExternalId = externalId,
                                Lighting = new
                                {
                                    Behavior = 32,
                                    CurveType = 123,
                                    Duration = 65,
                                    CCT = 99,
                                    Level = 11,
                                    State = OnOffState.On
                                }
                            }
                        }
                    }
                }
            };

            var json = JsonConvert.SerializeObject(@event);

            var notification = JsonConvert.DeserializeObject<WebHookEventDto>(json).Notifications.First();
            var value = notification.ValueToArray<LightsLightingChangedEventDto>();
            Assert.Equal(externalId, value[0].ExternalId);
            Assert.Equal(32, (int)value[0].Lighting.Behavior);
            Assert.Equal(123, (int)value[0].Lighting.CurveType);
            Assert.Equal(65, (int)value[0].Lighting.Duration);
            Assert.Equal(99, (int)value[0].Lighting.CCT);
            Assert.Equal(11, (int)value[0].Lighting.Level);
            Assert.Equal(OnOffState.On, (OnOffState)value[0].Lighting.State);
        }

        [Fact]
        public void ShouldReturnValueAsString()
        {
            var notification = new WebHookEventNotificationDto
            {
                Value = "On"
            };

            var value = notification.ValueAsString();
            Assert.Equal("On", value);
        }

        [Fact]
        public void ShouldReturnValueAsInt()
        {
            var notification = new WebHookEventNotificationDto
            {
                Value = "54"
            };

            var value = notification.ValueAsInt();
            Assert.Equal(54, value);
        }

        [Fact]
        public void ShouldReturnValueAsDouble()
        {
            var notification = new WebHookEventNotificationDto
            {
                Value = "78.45"
            };

            var value = notification.ValueAsDouble();
            Assert.Equal(78.45, value);
        }
    }
}
