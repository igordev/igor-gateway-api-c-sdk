﻿using Igor.Gateway.Dtos.Dashboards;
using Xunit;

namespace Igor.Gateway.Dtos.Tests.Dashboards
{
    public class DashboardDeviceStatusByDeviceTypeDtoTests
    {
        [Fact]
        public void ShouldReturnDeviceTypeWithOutTrailingS()
        {
            var dto = new DashboardDeviceStatusByDeviceTypeDto
            {
                DeviceType = "Lights"
            };

            Assert.Equal("Light", dto.DeviceTypeSingular);
        }

        [Fact]
        public void ShouldReturnNullIfDeviceTypeIsNull()
        {
            var dto = new DashboardDeviceStatusByDeviceTypeDto();

            Assert.Null(dto.DeviceTypeSingular);
        }
    }
}
