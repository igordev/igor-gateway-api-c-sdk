﻿using System;
using System.Collections.Generic;
using Igor.Gateway.Dtos.Common;
using Igor.Gateway.Dtos.WallControls;
using Xunit;

namespace Igor.Gateway.Dtos.Tests.WallControls
{
    public class CreateWallControlDtoTests
    {
        [Fact]
        public void ShouldPopulateDtoFromDto()
        {
            var createDto = new CreateWallControlDto
            {
                ExternalId = Guid.NewGuid().ToString(),
                Name = Guid.NewGuid().ToString(),
                Properties = Guid.NewGuid().ToString(),
                SystemProperties = "create dto",
                Protocol = Guid.NewGuid().ToString(),
                NetworkNodeId = 1,
                DeviceNodeId = 7,
                Buttons = new List<CreateWallControlButtonDto>
                {
                    new CreateWallControlButtonDto
                    {
                        ControlCommand = 2,
                        DimmingCommand = 6,
                        Name = Guid.NewGuid().ToString()
                    }
                }
            };

            var dto = createDto.ToDto();
            Assert.Equal(createDto.ExternalId, dto.ExternalId);
            Assert.Equal(createDto.Name, dto.Name);
            Assert.Equal(createDto.Properties, dto.Properties);
            Assert.Equal(createDto.SystemProperties, dto.SystemProperties);
            Assert.Equal(createDto.Protocol, dto.Protocol);
            Assert.Equal(createDto.NetworkNodeId, dto.NetworkNodeId);
            Assert.Equal(createDto.DeviceNodeId, dto.DeviceNodeId);
            Assert.Equal(createDto.Buttons.Count, dto.Buttons.Count);
            Assert.Equal(createDto.Buttons[0].ControlCommand, dto.Buttons[0].ControlCommand);
            Assert.Equal(createDto.Buttons[0].DimmingCommand, dto.Buttons[0].DimmingCommand);
            Assert.Equal(createDto.Buttons[0].Name, dto.Buttons[0].Name);
        }

        [Fact]
        public void ShouldPopulateButtonsWithEmptyIfCreateButtonsIsNull()
        {
            var createDto = new CreateWallControlDto { Buttons = null };

            var dto = createDto.ToDto();
            Assert.Empty(dto.Buttons);
        }

        [Fact]
        public void ShouldUseExternalProtocolIfProtocolIsNotSpecified()
        {
            var createDto = new CreateWallControlDto {Protocol = null};

            var dto = createDto.ToDto();
            Assert.Equal(Protocols.External, dto.Protocol);
        }
    }
}
