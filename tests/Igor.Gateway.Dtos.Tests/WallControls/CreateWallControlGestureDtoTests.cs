﻿using Igor.Gateway.Dtos.WallControls;
using Xunit;

namespace Igor.Gateway.Dtos.Tests.WallControls
{
    public class CreateWallControlGestureDtoTests
    {
        [Fact]
        public void ShouldPopulateDtoFromDto()
        {
            var createDto = new CreateWallControlGestureDto
            {
                Type = WallControlGestureType.Hold,
                ActionSetId = 65,
                HoldGestureCancelType = GestureCancelType.EndOfCurrentIteration, 
                HoldGestureTimeout = 5000
            };

            var dto = createDto.ToDto(23, 6);
            Assert.Equal(23, dto.WallControlId);
            Assert.Equal(6, dto.ButtonId);
            Assert.Equal(65, dto.ActionSetId);
            Assert.Equal(WallControlGestureType.Hold, dto.Type);
            Assert.Equal(GestureCancelType.EndOfCurrentIteration, dto.HoldGestureCancelType);
            Assert.Equal(5000, dto.HoldGestureTimeout);
        }
    }
}
