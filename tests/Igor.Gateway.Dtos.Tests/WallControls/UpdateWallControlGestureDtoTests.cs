﻿using Igor.Gateway.Dtos.WallControls;
using Xunit;

namespace Igor.Gateway.Dtos.Tests.WallControls
{
    public class UpdateWallControlGestureDtoTests
    {
        [Fact]
        public void ShouldPopulateDtoFromDto()
        {
            var updateDto = new UpdateWallControlGestureDto
            {
                Type = WallControlGestureType.Hold,
                ActionSetId = 78, 
                HoldGestureCancelType = GestureCancelType.EndOfCurrentIteration, 
                HoldGestureTimeout = 5000
            };

            var dto = updateDto.ToDto(23, 6, 7);
            Assert.Equal(23, dto.WallControlId);
            Assert.Equal(6, dto.ButtonId);
            Assert.Equal(7, dto.Id);
            Assert.Equal(78, dto.ActionSetId);
            Assert.Equal(WallControlGestureType.Hold, dto.Type);
            Assert.Equal(GestureCancelType.EndOfCurrentIteration, dto.HoldGestureCancelType);
            Assert.Equal(5000, dto.HoldGestureTimeout);
        }
    }
}
