﻿using System;
using System.Collections.Generic;
using Igor.Gateway.Dtos.WallControls;
using Xunit;

namespace Igor.Gateway.Dtos.Tests.WallControls
{
    public class UpdateWallControlDtoTests
    {
        [Fact]
        public void ShouldPopulateDtoFromDto()
        {
            var updateDto = new UpdateWallControlDto
            {
                Name = Guid.NewGuid().ToString(),
                Properties = Guid.NewGuid().ToString(),
                SystemProperties = "update dto",
                Buttons = new List<UpdateWallControlButtonDto>
                {
                    new UpdateWallControlButtonDto
                    {
                        ControlCommand = 2,
                        DimmingCommand = 6,
                        Name = Guid.NewGuid().ToString()
                    }
                }
            };

            var dto = updateDto.ToDto(7);
            Assert.Equal(7, dto.Id);
            Assert.Equal(updateDto.Name, dto.Name);
            Assert.Equal(updateDto.Properties, dto.Properties);
            Assert.Equal(updateDto.SystemProperties, dto.SystemProperties);
            Assert.Equal(updateDto.Buttons.Count, dto.Buttons.Count);
            Assert.Equal(updateDto.Buttons[0].ControlCommand, dto.Buttons[0].ControlCommand);
            Assert.Equal(updateDto.Buttons[0].DimmingCommand, dto.Buttons[0].DimmingCommand);
            Assert.Equal(updateDto.Buttons[0].Name, dto.Buttons[0].Name);
        }

        [Fact]
        public void ShouldPopulateButtonsWithEmptyIfCreateButtonsIsNull()
        {
            var updateDto = new UpdateWallControlDto { Buttons = null };

            var dto = updateDto.ToDto(6);
            Assert.Empty(dto.Buttons);
        }
    }
}
