﻿using Igor.Gateway.Dtos.ActionSets;
using Xunit;

namespace Igor.Gateway.Dtos.Tests.ActionSets
{
    public class UpdateActionSetDtoTests
    {
        [Fact]
        public void ShouldBeAbleToConvertToActionSetDto()
        {
            var updateDto = new UpdateActionSetDto
            {
                Name = "hello",
                ExecuteNumberOfTimes = 0,
                ExecuteType = ExecuteType.UntilCancelled
            };

            var dto = (ActionSetDto) updateDto;
            Assert.Equal("hello", dto.Name);
            Assert.Equal(0, dto.ExecuteNumberOfTimes);
            Assert.Equal(ExecuteType.UntilCancelled, dto.ExecuteType);
        }

        [Fact]
        public void ShouldBeAbleToConvertNull()
        {
            UpdateActionSetDto updateDto = null;

            var dto = (ActionSetDto) updateDto;
            Assert.Null(dto);
        }
    }
}