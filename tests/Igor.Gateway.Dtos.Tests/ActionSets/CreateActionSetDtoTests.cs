﻿using Igor.Gateway.Dtos.ActionSets;
using Xunit;

namespace Igor.Gateway.Dtos.Tests.ActionSets
{
    public class CreateActionSetDtoTests
    {
        [Fact]
        public void ShouldBeAbleToConvertToActionSetDto()
        {
            var createDto = new CreateActionSetDto
            {
                Name = "go go",
                ExecuteNumberOfTimes = 2,
                ExecuteType = ExecuteType.NumberOfTimes,
                IsSystem = true
            };

            var dto = (ActionSetDto) createDto;
            Assert.Equal("go go", dto.Name);
            Assert.Equal(2, dto.ExecuteNumberOfTimes);
            Assert.Equal(ExecuteType.NumberOfTimes, dto.ExecuteType);
            Assert.True(dto.IsSystem);
        }

        [Fact]
        public void ShouldBeAbleToConvertNull()
        {
            CreateActionSetDto createDto = null;

            var dto = (ActionSetDto) createDto;
            Assert.Null(dto);
        }
    }
}