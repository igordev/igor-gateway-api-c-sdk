﻿using Igor.Gateway.Dtos.Devices;
using Igor.Gateway.Dtos.Tags;
using Igor.Sdk.Common.Queries;
using Xunit;

namespace Igor.Gateway.Dtos.Tests.Tags
{
    public class TagEntitySearchParametersTests
    {
        [Fact]
        public void ShouldReturnQueryStringWithTagIds()
        {
            var parameters = new TagEntitySearchParameters
            {
                TagIds = new[] {89, 90}
            };

            var queryString = parameters.AsQueryString();
            Assert.Equal("?tagIds=89%2C90", queryString);
        }

        [Fact]
        public void ShouldReturnQueryStringWithEntityTypes()
        {
            var parameters = new TagEntitySearchParameters
            {
                EntityTypes = new[] {TagEntityType.Space, TagEntityType.Relay}
            };

            var queryString = parameters.AsQueryString();
            Assert.Equal("?entityTypes=Space%2CRelay", queryString);
        }

        [Fact]
        public void ShouldReturnQueryStringWithTerm()
        {
            var parameters = new TagEntitySearchParameters
            {
                Term = "Light"
            };

            Assert.Equal("?term=Light", parameters.AsQueryString());
        }

        [Fact]
        public void ShouldReturnQueryStringWithAscendingSortDirection()
        {
            var parameters = new TagEntitySearchParameters
            {
                SortDirection = QuerySortDirection.Ascending
            };

            var queryString = parameters.AsQueryString();
            Assert.Equal("?sortDir=asc", queryString);
        }

        [Fact]
        public void ShouldReturnQueryStringWithDescendingSortDirection()
        {
            var parameters = new TagEntitySearchParameters
            {
                SortDirection = QuerySortDirection.Descending
            };

            var queryString = parameters.AsQueryString();
            Assert.Equal("?sortDir=desc", queryString);
        }

        [Fact]
        public void ShouldReturnQueryStringWithSortBy()
        {
            var parameters = new TagEntitySearchParameters
            {
                SortBy = "Something"
            };

            var queryString = parameters.AsQueryString();
            Assert.Equal("?sortBy=Something", queryString);
        }

        [Fact]
        public void ShouldReturnNullForSpaceIds()
        {
            var parameters = new DeviceSearchParameters();

            Assert.Null(parameters.SpaceIds);
        }

        [Fact]
        public void ShouldReturnNullForEntityTypes()
        {
            var parameters = new TagEntitySearchParameters();

            Assert.Null(parameters.EntityTypes);
        }

        [Fact]
        public void ShouldReturnQueryStringWithMultipleParameters()
        {
            var parameters = new TagEntitySearchParameters
            {
                EntityTypes = new[] {TagEntityType.Relay, TagEntityType.LightSensor},
                SortDirection = QuerySortDirection.Descending
            };

            var queryString = parameters.AsQueryString();
            Assert.Contains("?entityTypes=Relay%2CLightSensor&sortDir=desc", queryString);
            Assert.Contains("sortDir=desc", queryString);
        }
    }
}