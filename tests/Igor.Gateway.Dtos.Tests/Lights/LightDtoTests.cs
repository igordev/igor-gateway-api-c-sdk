﻿using Igor.Gateway.Dtos.Lights;
using Xunit;

namespace Igor.Gateway.Dtos.Tests.Lights
{
    public class LightDtoTests
    {
        [Fact]
        public void TestShouldCastToUpdateDto()
        {
            var lightDto = new LightDto
            {
                Id = 1,
                ExternalId = "externalId",
                Name = "light",
                CCT = 2,
                MinLevel = 3,
                MaxLevel = 4,
                MinimumCCT = 5,
                MaximumCCT = 6,
                Properties = "properties",
                SystemProperties = "system"
            };

            var updateDto = (UpdateLightDto)lightDto;

            Assert.Equal(lightDto.Name, updateDto.Name);
            Assert.Equal(lightDto.MinLevel, updateDto.MinLevel);
            Assert.Equal(lightDto.MaxLevel, updateDto.MaxLevel);
            Assert.Equal(lightDto.MinimumCCT, updateDto.MinimumCCT);
            Assert.Equal(lightDto.MaximumCCT, updateDto.MaximumCCT);
            Assert.Equal(lightDto.Properties, updateDto.Properties);
            Assert.Equal(lightDto.SystemProperties, updateDto.SystemProperties);
        }
    }
}