﻿using Igor.Gateway.Dtos.Relays;
using Xunit;

namespace Igor.Gateway.Dtos.Tests.Relays
{
    public class CreateRelayDtoTests
    {
        [Fact]
        public void ShouldMapToDto()
        {
            var createDto = new CreateRelayDto
            {
                Properties = "properties",
                SystemProperties = "create dto",
                NetworkNodeId = 1,
                DeviceNodeId = 2
            };

            var dto = createDto.ToDto();

            Assert.Equal(dto.Properties, createDto.Properties);
            Assert.Equal(dto.SystemProperties, createDto.SystemProperties);
            Assert.Equal(dto.NetworkNodeId, createDto.NetworkNodeId);
            Assert.Equal(dto.DeviceNodeId, createDto.DeviceNodeId);
        }
    }
}
