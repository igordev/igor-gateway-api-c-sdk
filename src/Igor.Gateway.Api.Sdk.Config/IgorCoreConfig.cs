﻿using System;
using Igor.Gateway.Api.Sdk.Core.Config;
using Igor.Gateway.Api.Sdk.Core.Http;
using Microsoft.Extensions.Configuration;

namespace Igor.Gateway.Api.Sdk.Config
{
    public class IgorCoreConfig : ISdkConfig
    {
        private readonly IConfigurationRoot _configuration;

        public IgorCoreConfig(IConfigurationRoot configuration)
        {
            _configuration = configuration;
        }

        public string ApiAddress => _configuration[ConfigConstants.ApiAddressIndex];
        public string ApiKey => _configuration[ConfigConstants.ApiKeyIndex];
        public HttpVersion HttpVersion => GetVersion();

        private HttpVersion GetVersion()
        {
            var version = _configuration[ConfigConstants.ApiHttpVersionIndex];

            if (version == null) return HttpVersion.Http1_1;

            if (string.Compare(version, ConfigConstants.HttpVersion20Value, StringComparison.OrdinalIgnoreCase) == 0)
                return HttpVersion.Http2_0;

            return HttpVersion.Http1_1;
        }
    }
}