﻿#if NET461
using System.Configuration;
using Igor.Gateway.Api.Sdk.Core.Config;

namespace Igor.Gateway.Api.Sdk.Config
{
    public class IgorNetConfig : ISdkConfig
    {
        public string ApiAddress => ConfigurationManager.AppSettings[ConfigConstants.ApiAddressIndex];
        public string ApiKey => ConfigurationManager.AppSettings[ConfigConstants.ApiKeyIndex];
        public HttpVersion HttpVersion => GetVersion();

        private HttpVersion GetVersion()
        {
            var version = ConfigurationManager.AppSettings[ConfigConstants.ApiHttpVersionIndex];

            if (version == null) return HttpVersion.Http1_1;

            if (string.Compare(version, ConfigConstants.HttpVersion20Value, StringComparison.OrdinalIgnoreCase) == 0)
                return HttpVersion.Http2_0;

            return HttpVersion.Http1_1;
        }
    }
}
#endif