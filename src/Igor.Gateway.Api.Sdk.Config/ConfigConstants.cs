﻿namespace Igor.Gateway.Api.Sdk.Config
{
    public static class ConfigConstants
    {
        public const string ApiKeyIndex = "igor:api-key";
        public const string ApiAddressIndex = "igor:api-address";
        public const string ApiHttpVersionIndex = "igor:api-http-version";

        public const string HttpVersion11Value = "1.1";
        public const string HttpVersion20Value = "2.0";
        public const string HttpVersion30Value = "3.0";
    }
}
