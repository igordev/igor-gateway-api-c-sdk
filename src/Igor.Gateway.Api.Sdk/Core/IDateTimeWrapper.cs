﻿using System;

namespace Igor.Gateway.Api.Sdk.Core
{
    public interface IDateTimeWrapper
    {
        DateTime GetUtcNow();
    }
}