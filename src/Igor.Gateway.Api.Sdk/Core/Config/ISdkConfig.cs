﻿using Igor.Gateway.Api.Sdk.Core.Http;

namespace Igor.Gateway.Api.Sdk.Core.Config
{
    public interface ISdkConfig
    {
        string ApiAddress { get; }
        string ApiKey { get; }
        HttpVersion HttpVersion { get; }
    }
}