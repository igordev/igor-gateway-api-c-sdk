﻿using Igor.Gateway.Api.Sdk.Core.Http;

namespace Igor.Gateway.Api.Sdk.Core.Config
{
    public class SdkConfig : ISdkConfig
    {
        public SdkConfig()
        {
            ApiAddress = "http://localhost";
        }

        public string ApiAddress { get; set; }

        public string ApiKey { get; set; }

        public HttpVersion HttpVersion { get; set; }
    }
}
