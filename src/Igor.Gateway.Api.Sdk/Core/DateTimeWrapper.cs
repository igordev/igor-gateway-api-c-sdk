﻿using System;

namespace Igor.Gateway.Api.Sdk.Core
{
    public class DateTimeWrapper : IDateTimeWrapper
    {
        public DateTime GetUtcNow()
        {
            return DateTime.UtcNow;
        }
    }
}
