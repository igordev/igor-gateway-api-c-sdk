﻿using System.Collections.Generic;
using System.Text;

namespace Igor.Gateway.Api.Sdk.Core.Extensions
{
    internal static class StringDictionaryExtensions
    {
        internal static string BuildQueryString(this IDictionary<string, string> dictionary)
        {
            if (dictionary.Count == 0) return null;

            var sb = new StringBuilder();

            sb.Append("?");

            foreach (var parameter in dictionary)
            {
                sb.Append(parameter.Key);
                sb.Append("=");
                sb.Append(parameter.Value);
                sb.Append("&");
            }

            var output = sb.ToString();

            output = output.Substring(0, output.Length - 1);

            return output;
        }
    }
}
