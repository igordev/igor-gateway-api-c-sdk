﻿using System;

namespace Igor.Gateway.Api.Sdk.Core
{
    public class GuidWrapper : IGuidWrapper
    {
        public Guid NewGuid()
        {
            return Guid.NewGuid();
        }
    }
}
