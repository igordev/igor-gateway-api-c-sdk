﻿using Newtonsoft.Json;

namespace Igor.Gateway.Api.Sdk.Core.Serialization
{
    public interface IJsonSerializer
    {
        T Deserialize<T>(string @object);
        string Serialize(object @object, Formatting formatting = Formatting.None);
    }
}