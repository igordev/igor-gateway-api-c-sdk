﻿using Newtonsoft.Json;

namespace Igor.Gateway.Api.Sdk.Core.Serialization
{
    public class JsonSerializer : IJsonSerializer
    {
        public string Serialize(object @object, Formatting formatting = Formatting.None)
        {
            return JsonConvert.SerializeObject(@object, formatting);
        }

        public T Deserialize<T>(string @object)
        {
            return JsonConvert.DeserializeObject<T>(@object);
        }
    }
}
