﻿#nullable enable

using System.Diagnostics.Tracing;

namespace Igor.Gateway.Api.Sdk.Core.Metrics
{
    /// <summary>
    /// This class exposes overall gateway event counters (summaries) that are useful for initial troubleshooting
    /// and performance testing.  Use more specific event sources for exposing specific gateway features or details.
    /// For example, this class gives overall event rate, but another class gives device type specific event rates.
    /// </summary>
    [EventSource(Name = "Igor.Gateway.ServiceTiming")]
    public class ServiceTimingEventSource : EventSource
    {
        private EventCounter? _httpGetDurationCounter;
        private EventCounter? _httpPutDurationCounter;
        private EventCounter? _httpPostDurationCounter;
        private EventCounter? _httpDeleteDurationCounter;
        private EventCounter? _preSendDurationCounter;
        private EventCounter? _sendDurationCounter;
        private EventCounter? _postSendDurationCounter;

        public static readonly ServiceTimingEventSource Log = new ServiceTimingEventSource();

        private ServiceTimingEventSource()
        {
        }

        [NonEvent]
        public ValueStopwatch? HttpGetStarted()
        {
            if (!IsEnabled()) return null;

            HttpGetStart();

            return ValueStopwatch.StartNew();
        }

        [Event(1, Level = EventLevel.Informational, Message = "Started HTTP GET")]
        private void HttpGetStart()
        {
        }

        [NonEvent]
        public void HttpGetStopped(ValueStopwatch? stopwatch)
        {
            if (!IsEnabled() || !stopwatch.HasValue) return;

            HttpGetStop(stopwatch.Value.IsActive ? stopwatch.Value.GetElapsedTime().TotalMilliseconds : 0.0);
        }

        [Event(2, Level = EventLevel.Informational, Message = "Stopped HTTP GET")]
        private void HttpGetStop(double duration)
        {
            _httpGetDurationCounter?.WriteMetric(duration);
        }

        [NonEvent]
        public ValueStopwatch? HttpPutStarted()
        {
            if (!IsEnabled()) return null;

            HttpPutStart();

            return ValueStopwatch.StartNew();
        }

        [Event(3, Level = EventLevel.Informational, Message = "Started HTTP PUT")]
        private void HttpPutStart()
        {
        }

        [NonEvent]
        public void HttpPutStopped(ValueStopwatch? stopwatch)
        {
            if (!IsEnabled() || !stopwatch.HasValue) return;

            HttpPutStop(stopwatch.Value.IsActive ? stopwatch.Value.GetElapsedTime().TotalMilliseconds : 0.0);
        }

        [Event(4, Level = EventLevel.Informational, Message = "Stopped HTTP PUT")]
        private void HttpPutStop(double duration)
        {
            _httpPutDurationCounter?.WriteMetric(duration);
        }

        [NonEvent]
        public ValueStopwatch? HttpPostStarted()
        {
            if (!IsEnabled()) return null;

            HttpPostStart();

            return ValueStopwatch.StartNew();
        }

        [Event(5, Level = EventLevel.Informational, Message = "Started HTTP POST")]
        private void HttpPostStart()
        {
        }

        [NonEvent]
        public void HttpPostStopped(ValueStopwatch? stopwatch)
        {
            if (!IsEnabled() || !stopwatch.HasValue) return;

            HttpPostStop(stopwatch.Value.IsActive ? stopwatch.Value.GetElapsedTime().TotalMilliseconds : 0.0);
        }

        [Event(6, Level = EventLevel.Informational, Message = "Stopped HTTP POST")]
        private void HttpPostStop(double duration)
        {
            _httpPostDurationCounter?.WriteMetric(duration);
        }

        [NonEvent]
        public ValueStopwatch? HttpDeleteStarted()
        {
            if (!IsEnabled()) return null;

            HttpDeleteStart();

            return ValueStopwatch.StartNew();
        }

        [Event(7, Level = EventLevel.Informational, Message = "Started HTTP DELETE")]
        private void HttpDeleteStart()
        {
        }

        [NonEvent]
        public void HttpDeleteStopped(ValueStopwatch? stopwatch)
        {
            if (!IsEnabled() || !stopwatch.HasValue) return;

            HttpDeleteStop(stopwatch.Value.IsActive ? stopwatch.Value.GetElapsedTime().TotalMilliseconds : 0.0);
        }

        [Event(8, Level = EventLevel.Informational, Message = "Stopped HTTP DELETE")]
        private void HttpDeleteStop(double duration)
        {
            _httpDeleteDurationCounter?.WriteMetric(duration);
        }

        [NonEvent]
        public ValueStopwatch? PreSendStarted()
        {
            if (!IsEnabled()) return null;

            PreSendStart();

            return ValueStopwatch.StartNew();
        }

        [Event(9, Level = EventLevel.Informational, Message = "Started pre-send")]
        private void PreSendStart()
        {
        }

        [NonEvent]
        public void PreSendStopped(ValueStopwatch? stopwatch)
        {
            if (!IsEnabled() || !stopwatch.HasValue) return;

            PreSendStop(stopwatch.Value.IsActive ? stopwatch.Value.GetElapsedTime().TotalMilliseconds : 0.0);
        }

        [Event(10, Level = EventLevel.Informational, Message = "Stopped pre-send")]
        private void PreSendStop(double duration)
        {
            _preSendDurationCounter?.WriteMetric(duration);
        }

        [NonEvent]
        public ValueStopwatch? SendStarted()
        {
            if (!IsEnabled()) return null;

            SendStart();

            return ValueStopwatch.StartNew();
        }

        [Event(11, Level = EventLevel.Informational, Message = "Started send")]
        private void SendStart()
        {
        }

        [NonEvent]
        public void SendStopped(ValueStopwatch? stopwatch)
        {
            if (!IsEnabled() || !stopwatch.HasValue) return;

            SendStop(stopwatch.Value.IsActive ? stopwatch.Value.GetElapsedTime().TotalMilliseconds : 0.0);
        }

        [Event(12, Level = EventLevel.Informational, Message = "Stopped send")]
        private void SendStop(double duration)
        {
            _sendDurationCounter?.WriteMetric(duration);
        }

        [NonEvent]
        public ValueStopwatch? PostSendStarted()
        {
            if (!IsEnabled()) return null;

            PostSendStart();

            return ValueStopwatch.StartNew();
        }

        [Event(13, Level = EventLevel.Informational, Message = "Started post-send")]
        private void PostSendStart()
        {
        }

        [NonEvent]
        public void PostSendStopped(ValueStopwatch? stopwatch)
        {
            if (!IsEnabled() || !stopwatch.HasValue) return;

            PostSendStop(stopwatch.Value.IsActive ? stopwatch.Value.GetElapsedTime().TotalMilliseconds : 0.0);
        }

        [Event(14, Level = EventLevel.Informational, Message = "Stopped post-send")]
        private void PostSendStop(double duration)
        {
            _postSendDurationCounter?.WriteMetric(duration);
        }

        protected override void OnEventCommand(EventCommandEventArgs command)
        {
            if (command.Command == EventCommand.Enable)
            {
                // NOTE: These counters will NOT be disposed on disable command because we may be introducing
                // a race condition by doing that. We still want to create these lazily so that we aren't adding
                // overhead by at all times even when counters aren't enabled.

                _httpGetDurationCounter ??= new EventCounter("http-get-duration", this)
                {
                    DisplayName = "Average HTTP GET duration",
                    DisplayUnits = "ms"
                };

                _httpPutDurationCounter ??= new EventCounter("http-put-duration", this)
                {
                    DisplayName = "Average HTTP PUT duration",
                    DisplayUnits = "ms"
                };

                _httpPostDurationCounter ??= new EventCounter("http-post-duration", this)
                {
                    DisplayName = "Average HTTP POST duration",
                    DisplayUnits = "ms"
                };

                _httpDeleteDurationCounter ??= new EventCounter("http-delete-duration", this)
                {
                    DisplayName = "Average HTTP DELETE duration",
                    DisplayUnits = "ms"
                };

                _preSendDurationCounter ??= new EventCounter("pre-send-duration", this)
                {
                    DisplayName = "Average pre-send duration",
                    DisplayUnits = "ms"
                };

                _sendDurationCounter ??= new EventCounter("send-duration", this)
                {
                    DisplayName = "Average send duration",
                    DisplayUnits = "ms"
                };

                _postSendDurationCounter ??= new EventCounter("post-send-duration", this)
                {
                    DisplayName = "Average post-send duration",
                    DisplayUnits = "ms"
                };
            }
        }
    }
}
