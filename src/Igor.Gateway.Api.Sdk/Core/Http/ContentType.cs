﻿namespace Igor.Gateway.Api.Sdk.Core.Http
{
    public enum ContentType
    {
        Json,
        Xml
    }
}
