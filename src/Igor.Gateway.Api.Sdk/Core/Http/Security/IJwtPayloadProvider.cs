﻿namespace Igor.Gateway.Api.Sdk.Core.Http.Security
{
    public interface IJwtPayloadProvider
    {
        string GetEncodedPayload();
    }
}