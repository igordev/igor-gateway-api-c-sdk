﻿using System.Security.Cryptography;
using System.Text;

namespace Igor.Gateway.Api.Sdk.Core.Http.Security
{
    public class JwtSignatureProvider : IJwtSignatureProvider
    {
        public string Sign(string headerAndPayload, string key)
        {
            var encoding = new ASCIIEncoding();

            byte[] signature;

            using (var crypto = new HMACSHA256(encoding.GetBytes(key)))
                signature = crypto.ComputeHash(encoding.GetBytes(headerAndPayload));

            return UrlSafeConverter.ConvertToBase64UrlSafeString(signature);
        }
    }
}
