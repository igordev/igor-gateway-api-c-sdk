﻿using System.Text;
using Igor.Gateway.Api.Sdk.Core.Serialization;

namespace Igor.Gateway.Api.Sdk.Core.Http.Security
{
    public class JwtHeaderProvider : IJwtHeaderProvider
    {
        private readonly IJsonSerializer _jsonSerializer;

        public JwtHeaderProvider()
        {
            _jsonSerializer = new JsonSerializer();
        }

        public JwtHeaderProvider(IJsonSerializer jsonSerializer)
        {
            _jsonSerializer = jsonSerializer;
        }

        public string GetEncodedHeader()
        {
            Header = _jsonSerializer.Serialize(new JwtHeader());

            return UrlSafeConverter.ConvertToBase64UrlSafeString(Encoding.ASCII.GetBytes(Header));
        }

        public string Header { get; private set; }
    }
}
