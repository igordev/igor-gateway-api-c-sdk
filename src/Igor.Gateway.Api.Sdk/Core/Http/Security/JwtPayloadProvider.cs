﻿using System.Text;
using Igor.Gateway.Api.Sdk.Core.Serialization;

namespace Igor.Gateway.Api.Sdk.Core.Http.Security
{
    public class JwtPayloadProvider : IJwtPayloadProvider
    {
        private readonly IJsonSerializer _jsonSerializer;
        private readonly IDateTimeWrapper _dateTimeWrapper;
        private readonly IGuidWrapper _guidWrapper;

        public JwtPayloadProvider()
        {
            _jsonSerializer = new JsonSerializer();
            _dateTimeWrapper = new DateTimeWrapper();
            _guidWrapper = new GuidWrapper();
        }

        public JwtPayloadProvider(
            IJsonSerializer jsonSerializer, 
            IDateTimeWrapper dateTimeWrapper,
            IGuidWrapper guidWrapper
            )
        {
            _jsonSerializer = jsonSerializer;
            _dateTimeWrapper = dateTimeWrapper;
            _guidWrapper = guidWrapper;
        }

        public string GetEncodedPayload()
        {
            var payload = _jsonSerializer.Serialize(new JwtPayload(_dateTimeWrapper, _guidWrapper));

            return UrlSafeConverter.ConvertToBase64UrlSafeString(Encoding.ASCII.GetBytes(payload));
        }
    }
}
