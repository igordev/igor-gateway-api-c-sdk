﻿using System;
using Newtonsoft.Json;

namespace Igor.Gateway.Api.Sdk.Core.Http.Security
{
    public class JwtPayload
    {
        private readonly IDateTimeWrapper _dateTimeWrapper;
        private readonly IGuidWrapper _guidWrapper;

        public JwtPayload()
        {
            _dateTimeWrapper = new DateTimeWrapper();
            _guidWrapper = new GuidWrapper();
        }

        public JwtPayload(IDateTimeWrapper dateTimeWrapper, IGuidWrapper guidWrapper)
        {
            _dateTimeWrapper = dateTimeWrapper;
            _guidWrapper = guidWrapper;

            JwtId = GenerateId();
        }

        [JsonProperty("iat")]
        public int IssuedAt => GetSecondsPastEpoch();

        [JsonProperty("jti")]
        public long JwtId { get; private set; }

        private long GenerateId()
        {
            var id = _guidWrapper.NewGuid().ToByteArray();

            return BitConverter.ToInt64(id, 0);
        }

        private int GetSecondsPastEpoch()
        {
            var t = _dateTimeWrapper.GetUtcNow() - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);

            var secondsSinceEpoch = (int)t.TotalSeconds;

            return secondsSinceEpoch;
        }
    }
}
