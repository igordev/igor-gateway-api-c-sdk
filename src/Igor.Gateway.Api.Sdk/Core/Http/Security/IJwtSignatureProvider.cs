﻿namespace Igor.Gateway.Api.Sdk.Core.Http.Security
{
    public interface IJwtSignatureProvider
    {
        string Sign(string headerAndPayload, string key);
    }
}