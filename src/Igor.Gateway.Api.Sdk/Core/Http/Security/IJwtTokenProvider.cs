﻿namespace Igor.Gateway.Api.Sdk.Core.Http.Security
{
    public interface IJwtTokenProvider
    {
        string GetToken(string apiKey);
    }
}