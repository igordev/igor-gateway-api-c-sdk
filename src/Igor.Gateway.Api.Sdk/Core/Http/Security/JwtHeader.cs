﻿using Newtonsoft.Json;

namespace Igor.Gateway.Api.Sdk.Core.Http.Security
{
    public class JwtHeader
    {
        [JsonProperty("typ")]
        public string Type => "jwt";

        [JsonProperty("alg")]
        public string Algorithm => "HS256";
    }
}
