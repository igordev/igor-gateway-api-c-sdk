﻿namespace Igor.Gateway.Api.Sdk.Core.Http.Security
{
    public interface IJwtHeaderProvider
    {
        string Header { get; }

        string GetEncodedHeader();
    }
}