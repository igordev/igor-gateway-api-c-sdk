﻿namespace Igor.Gateway.Api.Sdk.Core.Http.Security
{
    public class JwtTokenProvider : IJwtTokenProvider
    {
        private readonly IJwtHeaderProvider _headerProvider;
        private readonly IJwtPayloadProvider _payloadProvider;
        private readonly IJwtSignatureProvider _signatureProvider;

        // PERF: Header is fixed string, generate and store it only once to use less CPU/memory on every call to GetToken.
        private static string _header;

        public JwtTokenProvider()
        {
            _headerProvider = new JwtHeaderProvider();
            _payloadProvider = new JwtPayloadProvider();
            _signatureProvider = new JwtSignatureProvider();
        }

        public JwtTokenProvider(
            IJwtHeaderProvider headerProvider,
            IJwtPayloadProvider payloadProvider,
            IJwtSignatureProvider signatureProvider
        )
        {
            _headerProvider = headerProvider;
            _payloadProvider = payloadProvider;
            _signatureProvider = signatureProvider;
        }

        public string GetToken(string apiKey)
        {
            if (string.IsNullOrEmpty(apiKey)) return null;

            _header = _header ?? _headerProvider.GetEncodedHeader();

            var payload = _payloadProvider.GetEncodedPayload();

            var toSign = $"{_header}.{payload}";

            var signature = _signatureProvider.Sign(toSign, apiKey);

            return toSign + "." + signature;
        }
    }
}