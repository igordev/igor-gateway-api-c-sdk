﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Igor.Gateway.Api.Sdk.Core.Http
{
    public sealed class HttpClientService : IHttpClientService
    {
        private static readonly HttpClient StaticHttpClient;

        public HttpClient HttpClient => StaticHttpClient;

        static HttpClientService()
        {
            var client = CreateNewHttpClient();
            StaticHttpClient = client;
        }

        private static HttpClient CreateNewHttpClient()
        {
            var client = new HttpClient(new SocketsHttpHandler
            {
                // Perf: see these references:
                //   https://docs.microsoft.com/en-us/aspnet/core/grpc/performance?view=aspnetcore-5.0
                //   https://github.com/dotnet/runtime/issues/30596
                //   https://github.com/dotnet/runtime/issues/35088
                //   https://www.youtube.com/watch?v=7XhZsgJTywg&t=0h0m0s
                EnableMultipleHttp2Connections = true
            });

            client.DefaultRequestHeaders.ExpectContinue = false;
            return client;
        }

        public async Task<string> GetStringAsync(IEnumerable<KeyValuePair<string, string>> headers, string url)
        {
            using var requestMessage = new HttpRequestMessage(HttpMethod.Get, url);

            SetHeaders(headers, requestMessage);

            using var responseMessage = await StaticHttpClient.SendAsync(requestMessage).ConfigureAwait(false);

            responseMessage.EnsureSuccessStatusCode();
            var content = responseMessage.Content;

            return await content.ReadAsStringAsync().ConfigureAwait(false);
        }

        public Task<HttpResponseMessage> GetAsync(IEnumerable<KeyValuePair<string, string>> headers, string url)
        {
            using var requestMessage = new HttpRequestMessage(HttpMethod.Get, url);

            SetHeaders(headers, requestMessage);

            return StaticHttpClient.SendAsync(requestMessage);
        }

        public Task<HttpResponseMessage> PostAsync(IEnumerable<KeyValuePair<string, string>> headers, string url, HttpContent content = null)
        {
            using var requestMessage = new HttpRequestMessage(HttpMethod.Post, url) { Content = content };

            SetHeaders(headers, requestMessage);

            return StaticHttpClient.SendAsync(requestMessage);
        }

        public Task<HttpResponseMessage> PutAsync(IEnumerable<KeyValuePair<string, string>> headers, string url, HttpContent content = null)
        {  
            using var requestMessage = new HttpRequestMessage(HttpMethod.Put, url) { Content = content };

            SetHeaders(headers, requestMessage);

            return StaticHttpClient.SendAsync(requestMessage);
        }

        public Task<HttpResponseMessage> DeleteAsync(IEnumerable<KeyValuePair<string, string>> headers, string url)
        {
            using var requestMessage = new HttpRequestMessage(HttpMethod.Delete, url);

            SetHeaders(headers, requestMessage);

            return StaticHttpClient.SendAsync(requestMessage);
        }

        public Task<HttpResponseMessage> SendAsync(IEnumerable<KeyValuePair<string, string>> headers, HttpRequestMessage requestMessage)
        {
            SetHeaders(headers, requestMessage);

            return StaticHttpClient.SendAsync(requestMessage);
        }

        private static void SetHeaders(IEnumerable<KeyValuePair<string, string>> headers, HttpRequestMessage requestMessage)
        {
            foreach (var header in headers)
            {
                requestMessage.Headers.Add(header.Key, header.Value);
            }
        }
    }
}
