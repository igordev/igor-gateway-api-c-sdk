﻿using System;

namespace Igor.Gateway.Api.Sdk.Core.Http
{
    public class UrlSafeConverter
    {
        public static string ConvertToBase64UrlSafeString(byte[] inArray)
        {
            var value = Convert.ToBase64String(inArray);

            value = value
                .Replace('+', '-')
                .Replace('/', '_')
                .Replace("=", "");

            return value;
        }
    }
}
