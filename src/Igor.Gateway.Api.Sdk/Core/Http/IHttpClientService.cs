﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Igor.Gateway.Api.Sdk.Core.Http
{
    public interface IHttpClientService
    {
        Task<HttpResponseMessage> DeleteAsync(IEnumerable<KeyValuePair<string, string>> headers, string url);
        Task<HttpResponseMessage> GetAsync(IEnumerable<KeyValuePair<string, string>> headers, string url);
        Task<string> GetStringAsync(IEnumerable<KeyValuePair<string, string>> headers, string url);
        Task<HttpResponseMessage> PostAsync(IEnumerable<KeyValuePair<string, string>> headers, string url, HttpContent content = null);
        Task<HttpResponseMessage> PutAsync(IEnumerable<KeyValuePair<string, string>> headers, string url, HttpContent content = null);
        Task<HttpResponseMessage> SendAsync(IEnumerable<KeyValuePair<string, string>> headers, HttpRequestMessage requestMessage);
    }
}