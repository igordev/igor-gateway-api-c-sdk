﻿namespace Igor.Gateway.Api.Sdk.Core.Http
{
    public enum HttpVersion
    {
        Http1_1 = 1,
        Http2_0 = 2,
        //Http3_0 = 3
    }
}