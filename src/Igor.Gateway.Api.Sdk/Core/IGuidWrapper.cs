﻿using System;

namespace Igor.Gateway.Api.Sdk.Core
{
    public interface IGuidWrapper
    {
        Guid NewGuid();
    }
}