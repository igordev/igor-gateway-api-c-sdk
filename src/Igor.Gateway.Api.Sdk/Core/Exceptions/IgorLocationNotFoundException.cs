﻿using System.Net.Http;
using Igor.Exceptions;

namespace Igor.Gateway.Api.Sdk.Core.Exceptions
{
    public class IgorLocationNotFoundException : IgorHttpException
    {
        public IgorLocationNotFoundException(HttpResponseMessage response) 
            : this(response, "Location header not found.")
        {

        }

        public IgorLocationNotFoundException(HttpResponseMessage response, string message) : base(response, message)
        {
        }
    }
}
