﻿using System.Net.Http;
using Igor.Exceptions;

namespace Igor.Gateway.Api.Sdk.Core.Exceptions
{
    public class IgorUnauthorizedException : IgorHttpException
    {
        public IgorUnauthorizedException(HttpResponseMessage responseMessage, string message) : base(responseMessage, message)
        {
        }
    }
}
