﻿using System.Net.Http;
using Igor.Exceptions;
using Igor.Gateway.Dtos.Validation;

namespace Igor.Gateway.Api.Sdk.Core.Exceptions
{
    public class IgorValidationException : IgorHttpException
    {
        public IgorValidationException(HttpResponseMessage responseMessage, ValidationDto validationDto)
            : base(responseMessage)
        {
            ValidationMessage = validationDto;
        }

        public ValidationDto ValidationMessage { get; }
    }
}
