﻿using System.Net.Http;
using Igor.Exceptions;

namespace Igor.Gateway.Api.Sdk.Core.Exceptions
{
    public class IgorNotFoundException : IgorHttpException
    {
        public IgorNotFoundException(HttpResponseMessage responseMessage, string message) : base(responseMessage, message)
        {
        }
    }
}
