﻿using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Core.Config;
using Igor.Gateway.Api.Sdk.Core.Http;
using Igor.Gateway.Api.Sdk.Core.Http.Security;
using Igor.Gateway.Api.Sdk.Core.Serialization;
using Igor.Gateway.Dtos.ActionSets;
using Igor.Gateway.Dtos.Events;
using Igor.Sdk.Common.Models;

namespace Igor.Gateway.Api.Sdk.Apis.ActionSets
{
    public interface IActionSetService : IRetrievable<ActionSetDto>
    {
        Task<int> Create(CreateActionSetDto createActionSetDto);
        Task Delete(int actionSetId);
        Task Execute(int actionSetId);
        Task<ResultList<ActionSetDto>> GetAll();
        Task<ResultList<EventDto>> GetEvents(int actionSetId);
        Task Update(int actionSetId, UpdateActionSetDto updateActionSetDto);
        Task Cancel(int actionSetId, CancelOptionsDto cancelOptionsDto);
        Task<PaginatedList<ActionSetsSearchResultsDto>> Search();
        Task<PaginatedList<ActionSetsSearchResultsDto>> Search(ActionSetsSearchParameters queryParameters);
    }

    public class ActionSetService : ServiceBase, IActionSetService
    {
        private const string GetAllEndpoint = "/actionsets";
        private const string CreateEndpoint = "/actionsets";
        private const string DeleteEndpoint = "/actionsets/{0}";
        private const string GetEndpoint = "/actionsets/{0}";
        private const string UpdateEndpoint = "/actionsets/{0}";
        private const string ExecuteEndpoint = "/actionsets/{0}/execute";
        private const string GetEventsEndpoint = "/actionsets/{0}/events";
        private const string CancelEndpoint = "/actionsets/{0}/cancel";
        private const string SearchEndpoint = "/actionsets/search";

        public ActionSetService(string apiKey) : base(apiKey)
        {
        }

        public ActionSetService(string apiKey, string apiAddress) : base(apiKey, apiAddress)
        {
        }

        public ActionSetService(IHttpClientService httpClient, IJsonSerializer jsonSerializer, ISdkConfig config, IJwtTokenProvider tokenProvider, IRequestHeaderProvider requestHeaderProvider)
            : base(httpClient, jsonSerializer, config, tokenProvider, requestHeaderProvider)
        {
        }

        public async Task<ResultList<ActionSetDto>> GetAll()
        {
            return await GetHttpGetResponse<ResultList<ActionSetDto>>(GetAllEndpoint);
        }

        public async Task<ActionSetDto> Get(int actionSetId)
        {
            return await GetHttpGetResponse<ActionSetDto>(GetEndpoint, actionSetId);
        }

        public async Task<int> Create(CreateActionSetDto createActionSetDto)
        {
            using var response = await GetHttpPostResponse(CreateEndpoint, createActionSetDto);

            return GetId(response);
        }

        public async Task Delete(int actionSetId)
        {
            using var _ = await GetHttpDeleteResponse(DeleteEndpoint, actionSetId);
        }

        public async Task Update(int actionSetId, UpdateActionSetDto updateActionSetDto)
        {
            using var _ = await GetHttpPutResponse(UpdateEndpoint, updateActionSetDto, actionSetId);
        }

        public async Task Cancel(int actionSetId, CancelOptionsDto cancelOptionsDto)
        {
            using var _ = await GetHttpPostResponse(CancelEndpoint, cancelOptionsDto, actionSetId);
        }

        public async Task Execute(int actionSetId)
        {
            using var _ = await GetHttpPostResponse(ExecuteEndpoint, (object)null, actionSetId);
        }

        public async Task<ResultList<EventDto>> GetEvents(int actionSetId)
        {
            return await GetHttpGetResponse<ResultList<EventDto>>(GetEventsEndpoint, actionSetId);
        }
        
        public async Task<PaginatedList<ActionSetsSearchResultsDto>> Search()
        {
            return await Search(null);
        }

        public async Task<PaginatedList<ActionSetsSearchResultsDto>> Search(ActionSetsSearchParameters queryParameters)
        {
            var endpoint = FormatEndpoint(SearchEndpoint) + queryParameters;
            var results = await GetAsync<PaginatedList<ActionSetsSearchResultsDto>>(endpoint);

            return results;
        }
    }
}
