﻿using System.Collections.Generic;

namespace Igor.Gateway.Api.Sdk.Apis
{
    public interface IRequestHeaderProvider
    {
        IEnumerable<KeyValuePair<string, string>> GetHeaders();
    }
}