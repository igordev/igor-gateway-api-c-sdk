﻿using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Core.Config;
using Igor.Gateway.Api.Sdk.Core.Http;
using Igor.Gateway.Api.Sdk.Core.Http.Security;
using Igor.Gateway.Api.Sdk.Core.Serialization;
using Igor.Gateway.Dtos.Actions;
using Igor.Gateway.Dtos.Events;
using Igor.Sdk.Common.Models;

namespace Igor.Gateway.Api.Sdk.Apis.Actions
{
    public interface IActionService : IHttpService
    {
        Task<int> Create<T>(int actionSetId, CreateActionDto<T> createActionDto);
        Task Delete(int actionSetId, int actionId);
        Task Execute(int actionSetId, int actionId);
        Task<ActionDto> Get(int actionSetId, int actionId);
        Task<ResultList<ActionDto>> GetAll(int actionSetId);
        Task<ResultList<EventDto>> GetEvents(int actionSetId, int actionId);
        Task Update<T>(int actionSetId, int actionId, UpdateActionDto<T> actionDto);
    }

    public class ActionService : ServiceBase, IActionService
    {
        private const string GetAllEndpoint = "/actionsets/{0}/actions";
        private const string CreateEndpoint = "/actionsets/{0}/actions";
        private const string DeleteEndpoint = "/actionsets/{0}/actions/{1}";
        private const string GetEndpoint = "/actionsets/{0}/actions/{1}";
        private const string UpdateEndpoint = "/actionsets/{0}/actions/{1}";
        private const string ExecuteEndpoint = "/actionsets/{0}/actions/{1}/execute";
        private const string GetEventsEndpoint = "/actionsets/{0}/actions/{1}/events";

        public ActionService(string apiKey) : base(apiKey)
        {
        }

        public ActionService(string apiKey, string apiAddress) : base(apiKey, apiAddress)
        {
        }

        public ActionService(IHttpClientService httpClient, IJsonSerializer jsonSerializer, ISdkConfig config, IJwtTokenProvider tokenProvider, IRequestHeaderProvider requestHeaderProvider) 
            : base(httpClient, jsonSerializer, config, tokenProvider, requestHeaderProvider)
        {
        }

        public async Task<ResultList<ActionDto>> GetAll(int actionSetId)
        {
            return await GetHttpGetResponse<ResultList<ActionDto>>(GetAllEndpoint, actionSetId);
        }

        public async Task<ActionDto> Get(int actionSetId, int actionId)
        {
            return await GetHttpGetResponse<ActionDto>(GetEndpoint, actionSetId, actionId);
        }

        public async Task<int> Create<T>(int actionSetId, CreateActionDto<T> createActionDto)
        {
            using var response = await GetHttpPostResponse(CreateEndpoint, createActionDto, actionSetId);

            return GetId(response);
        }

        public async Task Delete(int actionSetId, int actionId)
        {
            using var _ = await GetHttpDeleteResponse(DeleteEndpoint, actionSetId, actionId);
        }

        public async Task Update<T>(int actionSetId, int actionId, UpdateActionDto<T> updateActionDto)
        {
            using var _ = await GetHttpPutResponse(UpdateEndpoint, updateActionDto, actionSetId, actionId);
        }

        public async Task Execute(int actionSetId, int actionId)
        {
            using var _ = await GetHttpPostResponse(ExecuteEndpoint, (object)null, actionSetId, actionId);
        }

        public async Task<ResultList<EventDto>> GetEvents(int actionSetId, int actionId)
        {
            return await GetHttpGetResponse<ResultList<EventDto>>(GetEventsEndpoint, actionSetId, actionId);
        }
    }
}
