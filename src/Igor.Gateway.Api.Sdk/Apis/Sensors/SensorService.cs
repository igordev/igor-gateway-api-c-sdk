﻿using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Core.Config;
using Igor.Gateway.Api.Sdk.Core.Http;
using Igor.Gateway.Api.Sdk.Core.Http.Security;
using Igor.Gateway.Api.Sdk.Core.Serialization;
using Igor.Gateway.Dtos.Sensors;
using Igor.Gateway.Dtos.Events;
using Igor.Gateway.Dtos.Tags;
using Igor.Sdk.Common.Models;

namespace Igor.Gateway.Api.Sdk.Apis.Sensors
{
    public interface ISensorService : IRetrievable<SensorDto>
    {
        Task<int> Create(CreateSensorDto createSensorDto);
        Task CreateEvent(int sensorId, CreateSensorEventDto createSensorEventDto);
        Task Delete(int sensorId);
        Task<ResultList<SensorDto>> GetAll();
        Task<ResultList<EventDto>> GetEvents(int sensorId);
        Task Update(int sensorId, UpdateSensorDto updateSensorDto);
        Task AddTag(int sensorId, AddTagsDto tagDto);
        Task RemoveTag(int sensorId, string tagName);
    }

    public class SensorService : ServiceBase, ISensorService
    {
        private const string GetAllEndpoint = "/sensors";
        private const string CreateEndpoint = "/sensors";
        private const string DeleteEndpoint = "/sensors/{0}";
        private const string GetEndpoint = "/sensors/{0}";
        private const string UpdateEndpoint = "/sensors/{0}";
        private const string GetEventsEndpoint = "/sensors/{0}/events";
        private const string CreateEventEndpoint = "/sensors/{0}/events";
        private const string AddTagEndpoint = "/sensors/{0}/tags";
        private const string RemoveTagEndpoint = "/sensors/{0}/tags/{1}";

        public SensorService(string apiKey) : base(apiKey)
        {
        }

        public SensorService(string apiKey, string apiAddress) : base(apiKey, apiAddress)
        {
        }

        public SensorService(IHttpClientService httpClient, IJsonSerializer jsonSerializer, ISdkConfig config,
            IJwtTokenProvider tokenProvider, IRequestHeaderProvider requestHeaderProvider) : base(httpClient, jsonSerializer, config, tokenProvider, requestHeaderProvider)
        {
        }

        public async Task<SensorDto> Get(int sensorId)
        {
            return await GetHttpGetResponse<SensorDto>(GetEndpoint, sensorId);
        }

        public async Task<ResultList<SensorDto>> GetAll()
        {
            return await GetHttpGetResponse<ResultList<SensorDto>>(GetAllEndpoint);
        }

        public async Task<int> Create(CreateSensorDto createSensorDto)
        {
            using var response = await GetHttpPostResponse(CreateEndpoint, createSensorDto);

            return GetId(response);
        }

        public async Task Update(int sensorId, UpdateSensorDto updateSensorDto)
        {
            using var _ = await GetHttpPutResponse(UpdateEndpoint, updateSensorDto, sensorId);
        }

        public async Task AddTag(int sensorId, AddTagsDto tagDto)
        {
            using var _ = await GetHttpPutResponse(AddTagEndpoint, tagDto, sensorId);
        }

        public async Task RemoveTag(int sensorId, string tagName)
        {
            using var _ = await GetHttpDeleteResponse(RemoveTagEndpoint, sensorId, tagName);
        }

        public async Task Delete(int sensorId)
        {
            using var _ = await GetHttpDeleteResponse(DeleteEndpoint, sensorId);
        }

        public async Task<ResultList<EventDto>> GetEvents(int sensorId)
        {
            return await GetHttpGetResponse<ResultList<EventDto>>(GetEventsEndpoint, sensorId);
        }

        public async Task CreateEvent(int sensorId, CreateSensorEventDto createSensorEventDto)
        {
            using var _ = await GetHttpPostResponse(CreateEventEndpoint, createSensorEventDto, sensorId);
        }
    }
}