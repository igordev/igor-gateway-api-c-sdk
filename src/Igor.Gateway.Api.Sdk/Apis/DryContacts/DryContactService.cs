﻿using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Core.Config;
using Igor.Gateway.Api.Sdk.Core.Http;
using Igor.Gateway.Api.Sdk.Core.Http.Security;
using Igor.Gateway.Api.Sdk.Core.Serialization;
using Igor.Gateway.Dtos.DryContacts;
using Igor.Gateway.Dtos.Events;
using Igor.Gateway.Dtos.Tags;
using Igor.Sdk.Common.Models;

namespace Igor.Gateway.Api.Sdk.Apis.DryContacts
{
    public interface IDryContactService : IRetrievable<DryContactDto>
    {
        Task<int> Create(CreateDryContactDto createDryContactDto);
        Task CreateEvent(int dryContactId, CreateDryContactEventDto createDryContactEventDto);
        Task Delete(int dryContactId);
        Task<ResultList<DryContactDto>> GetAll();
        Task<ResultList<EventDto>> GetEvents(int dryContactId);
        Task Update(int dryContactId, UpdateDryContactDto updateDryContactDto);
        Task AddTag(int dryContactId, AddTagsDto tagDto);
        Task RemoveTag(int dryContactId, string tagName);
    }

    public class DryContactService : ServiceBase, IDryContactService
    {
        private const string GetAllEndpoint = "/drycontacts";
        private const string CreateEndpoint = "/drycontacts";
        private const string DeleteEndpoint = "/drycontacts/{0}";
        private const string GetEndpoint = "/drycontacts/{0}";
        private const string UpdateEndpoint = "/drycontacts/{0}";
        private const string GetEventsEndpoint = "/drycontacts/{0}/events";
        private const string CreateEventEndpoint = "/drycontacts/{0}/events";
        private const string AddTagEndpoint = "/drycontacts/{0}/tags";
        private const string RemoveTagEndpoint = "/drycontacts/{0}/tags/{1}";

        public DryContactService(string apiKey) : base(apiKey)
        {
        }

        public DryContactService(string apiKey, string apiAddress) : base(apiKey, apiAddress)
        {
        }

        public DryContactService(IHttpClientService httpClient, IJsonSerializer jsonSerializer, ISdkConfig config,
            IJwtTokenProvider tokenProvider, IRequestHeaderProvider requestHeaderProvider) : base(httpClient, jsonSerializer, config, tokenProvider, requestHeaderProvider)
        {
        }

        public async Task<DryContactDto> Get(int dryContactId)
        {
            return await GetHttpGetResponse<DryContactDto>(GetEndpoint, dryContactId);
        }

        public async Task<ResultList<DryContactDto>> GetAll()
        {
            return await GetHttpGetResponse<ResultList<DryContactDto>>(GetAllEndpoint);
        }

        public async Task<int> Create(CreateDryContactDto createDryContactDto)
        {
            using var response = await GetHttpPostResponse(CreateEndpoint, createDryContactDto);

            return GetId(response);
        }

        public async Task Update(int dryContactId, UpdateDryContactDto updateDryContactDto)
        {
            using var _ = await GetHttpPutResponse(UpdateEndpoint, updateDryContactDto, dryContactId);
        }

        public async Task AddTag(int dryContactId, AddTagsDto tagDto)
        {
            using var _ = await GetHttpPutResponse(AddTagEndpoint, tagDto, dryContactId);
        }

        public async Task RemoveTag(int dryContactId, string tagName)
        {
            using var _ = await GetHttpDeleteResponse(RemoveTagEndpoint, dryContactId, tagName);
        }

        public async Task Delete(int dryContactId)
        {
            using var _ = await GetHttpDeleteResponse(DeleteEndpoint, dryContactId);
        }

        public async Task<ResultList<EventDto>> GetEvents(int dryContactId)
        {
            return await GetHttpGetResponse<ResultList<EventDto>>(GetEventsEndpoint, dryContactId);
        }

        public async Task CreateEvent(int dryContactId, CreateDryContactEventDto createDryContactEventDto)
        {
            using var _ = await GetHttpPostResponse(CreateEventEndpoint, createDryContactEventDto, dryContactId);
        }
    }
}