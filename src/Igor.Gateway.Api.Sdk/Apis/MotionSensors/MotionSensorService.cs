﻿using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Core.Config;
using Igor.Gateway.Api.Sdk.Core.Http;
using Igor.Gateway.Api.Sdk.Core.Http.Security;
using Igor.Gateway.Api.Sdk.Core.Serialization;
using Igor.Gateway.Dtos.Events;
using Igor.Gateway.Dtos.MotionSensors;
using Igor.Gateway.Dtos.Tags;
using Igor.Sdk.Common.Models;

namespace Igor.Gateway.Api.Sdk.Apis.MotionSensors
{
    public interface IMotionSensorService : IRetrievable<MotionSensorDto>
    {
        Task<int> Create(CreateMotionSensorDto createMotionSensorDto);
        Task CreateEvent(int motionSensorId, CreateMotionSensorEventDto createMotionSensorEventDto);
        Task Delete(int motionSensorId);
        Task<ResultList<MotionSensorDto>> GetAll();
        Task<ResultList<EventDto>> GetEvents(int motionSensorId);
        Task Update(int motionSensorId, UpdateMotionSensorDto updateMotionSensorDto);
        Task AddTag(int motionSensorId, AddTagsDto tagDto);
        Task RemoveTag(int motionSensorId, string tagName);
    }

    public class MotionSensorService : ServiceBase, IMotionSensorService
    {
        private const string GetAllEndpoint = "/motionsensors";
        private const string CreateEndpoint = "/motionsensors";
        private const string DeleteEndpoint = "/motionsensors/{0}";
        private const string GetEndpoint = "/motionsensors/{0}";
        private const string UpdateEndpoint = "/motionsensors/{0}";
        private const string GetEventsEndpoint = "/motionsensors/{0}/events";
        private const string CreateEventEndpoint = "/motionsensors/{0}/events";
        private const string AddTagEndpoint = "/motionsensors/{0}/tags";
        private const string RemoveTagEndpoint = "/motionsensors/{0}/tags/{1}";

        public MotionSensorService(string apiKey) : base(apiKey)
        {
        }

        public MotionSensorService(string apiKey, string apiAddress) : base(apiKey, apiAddress)
        {
        }

        public MotionSensorService(IHttpClientService httpClient, IJsonSerializer jsonSerializer, ISdkConfig config,
            IJwtTokenProvider tokenProvider, IRequestHeaderProvider requestHeaderProvider)
            : base(httpClient, jsonSerializer, config, tokenProvider, requestHeaderProvider)
        {
        }

        public async Task<ResultList<MotionSensorDto>> GetAll()
        {
            return await GetHttpGetResponse<ResultList<MotionSensorDto>>(GetAllEndpoint);
        }

        public async Task<MotionSensorDto> Get(int motionSensorId)
        {
            return await GetHttpGetResponse<MotionSensorDto>(GetEndpoint, motionSensorId);
        }

        public async Task<int> Create(CreateMotionSensorDto createMotionSensorDto)
        {
            using var response = await GetHttpPostResponse(CreateEndpoint, createMotionSensorDto);

            return GetId(response);
        }

        public async Task Delete(int motionSensorId)
        {
            using var _ = await GetHttpDeleteResponse(DeleteEndpoint, motionSensorId);
        }

        public async Task Update(int motionSensorId, UpdateMotionSensorDto updateMotionSensorDto)
        {
            using var _ = await GetHttpPutResponse(UpdateEndpoint, updateMotionSensorDto, motionSensorId);
        }

        public async Task AddTag(int motionSensorId, AddTagsDto tagDto)
        {
            using var _ = await GetHttpPutResponse(AddTagEndpoint, tagDto, motionSensorId);
        }

        public async Task RemoveTag(int motionSensorId, string tagName)
        {
            using var _ = await GetHttpDeleteResponse(RemoveTagEndpoint, motionSensorId, tagName);
        }

        public async Task<ResultList<EventDto>> GetEvents(int motionSensorId)
        {
            return await GetHttpGetResponse<ResultList<EventDto>>(GetEventsEndpoint, motionSensorId);
        }

        public async Task CreateEvent(int motionSensorId, CreateMotionSensorEventDto createMotionSensorEventDto)
        {
            using var _ = await GetHttpPostResponse(CreateEventEndpoint, createMotionSensorEventDto, motionSensorId);
        }
    }
}