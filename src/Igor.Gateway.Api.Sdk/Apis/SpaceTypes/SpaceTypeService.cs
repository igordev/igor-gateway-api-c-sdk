﻿using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Core.Config;
using Igor.Gateway.Api.Sdk.Core.Http;
using Igor.Gateway.Api.Sdk.Core.Http.Security;
using Igor.Gateway.Api.Sdk.Core.Serialization;
using Igor.Gateway.Dtos.Policies;
using Igor.Gateway.Dtos.SpaceTypes;
using Igor.Sdk.Common.Models;

namespace Igor.Gateway.Api.Sdk.Apis.SpaceTypes
{
    public interface ISpaceTypeService : IRetrievable<SpaceTypeDto>
    {
        Task<ResultList<SpaceTypeDto>> GetAll();
        Task<PolicyDto> GetPolicy(int spaceTypeId);
    }

    public class SpaceTypeService : ServiceBase, ISpaceTypeService
    {
        private const string GetAllEndpoint = "/spacetypes";
        private const string GetEndpoint = "/spacetypes/{0}";
        private const string GetPolicyEndpoint = "/spacetypes/{0}/policy";

        public SpaceTypeService(string apiKey) : base(apiKey)
        {
        }

        public SpaceTypeService(string apiKey, string apiAddress) : base(apiKey, apiAddress)
        {
        }

        public SpaceTypeService(IHttpClientService httpClient, IJsonSerializer jsonSerializer, ISdkConfig config, IJwtTokenProvider tokenProvider, IRequestHeaderProvider requestHeaderProvider)
            : base(httpClient, jsonSerializer, config, tokenProvider, requestHeaderProvider)
        {
        }

        public async Task<ResultList<SpaceTypeDto>> GetAll()
        {
            return await GetHttpGetResponse<ResultList<SpaceTypeDto>>(GetAllEndpoint);
        }

        public async Task<SpaceTypeDto> Get(int spaceTypeId)
        {
            return await GetHttpGetResponse<SpaceTypeDto>(GetEndpoint, spaceTypeId);
        }

        public async Task<PolicyDto> GetPolicy(int spaceTypeId)
        {
            return await GetHttpGetResponse<PolicyDto>(GetPolicyEndpoint, spaceTypeId);
        }
    }
}
