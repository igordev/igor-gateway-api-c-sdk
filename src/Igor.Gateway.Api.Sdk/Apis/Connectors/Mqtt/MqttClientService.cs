﻿using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Core.Config;
using Igor.Gateway.Api.Sdk.Core.Http;
using Igor.Gateway.Api.Sdk.Core.Http.Security;
using Igor.Gateway.Api.Sdk.Core.Serialization;
using Igor.Gateway.Dtos.Connectors.Mqtt;
using Igor.Sdk.Common.Models;

namespace Igor.Gateway.Api.Sdk.Apis.Connectors.Mqtt
{
    public interface IMqttClientService : IHttpService
    {
        Task<IPaginatedList<MqttClientsSearchResultDto>> Search();
        Task<IPaginatedList<MqttClientsSearchResultDto>> Search(MqttClientsSearchParameters queryParameters);
        Task<MqttClientDto> Get(int mqttClientId);
        Task<int> Create(CreateMqttClientDto createMqttClientDto);
        Task Delete(int mqttClientId);
        Task DeleteCredentials(int mqttClientId); 
        Task Update(int mqttClientId, UpdateMqttClientDto updateMqttClientDto);
        Task Test(int mqttClientId); 
        Task<ResultList<string>> GetSupportedEvents();
    }

    public class MqttClientService : ServiceBase, IMqttClientService
    {
        private const string SearchEndpoint = "/connectors/mqtt/clients";
        private const string CreateEndpoint = "/connectors/mqtt/clients";
        private const string DeleteEndpoint = "/connectors/mqtt/clients/{0}";
        private const string DeleteCredentialsEndpoint = "/connectors/mqtt/clients/{0}/credentials";
        private const string GetEndpoint = "/connectors/mqtt/clients/{0}";
        private const string UpdateEndpoint = "/connectors/mqtt/clients/{0}";
        private const string TestEndpoint = "/connectors/mqtt/clients/{0}/test";
        private const string SupportedEventsEndpoint = "/connectors/mqtt/clients/supported-events";

        public MqttClientService(string apiKey) : base(apiKey)
        {
        }

        public MqttClientService(string apiKey, string apiAddress) : base(apiKey, apiAddress)
        {
        }

        public MqttClientService(IHttpClientService httpClient, IJsonSerializer jsonSerializer, ISdkConfig config, IJwtTokenProvider tokenProvider, IRequestHeaderProvider requestHeaderProvider)
            : base(httpClient, jsonSerializer, config, tokenProvider, requestHeaderProvider)
        {
        }

        public async Task<IPaginatedList<MqttClientsSearchResultDto>> Search()
        {
            return await Search(null);
        }

        public async Task<IPaginatedList<MqttClientsSearchResultDto>> Search(
            MqttClientsSearchParameters queryParameters)
        {
            var endpoint = FormatEndpoint(SearchEndpoint) + queryParameters;
            var results = await GetAsync<PaginatedList<MqttClientsSearchResultDto>>(endpoint);

            return results;
        }

        public async Task<MqttClientDto> Get(int mqttClientId)
        {
            return await GetHttpGetResponse<MqttClientDto>(GetEndpoint, mqttClientId);
        }

        public async Task<int> Create(CreateMqttClientDto createMqttClientDto)
        {
            using var response = await GetHttpPostResponse(CreateEndpoint, createMqttClientDto);

            return GetId(response);
        }

        public async Task Delete(int mqttClientId)
        {
            using var _ = await GetHttpDeleteResponse(DeleteEndpoint, mqttClientId);
        }

        public async Task DeleteCredentials(int mqttClientId)
        {
            using var _ = await GetHttpDeleteResponse(DeleteCredentialsEndpoint, mqttClientId);
        }

        public async Task Update(int mqttClientId,
            UpdateMqttClientDto updateMqttClientDto)
        {
            using var _ = await GetHttpPutResponse(UpdateEndpoint, updateMqttClientDto, mqttClientId);
        }

        public async Task Test(int mqttClientId)
        {
            using var _ = await GetHttpPostResponse(TestEndpoint, (object)null, mqttClientId);
        }

        public async Task<ResultList<string>> GetSupportedEvents()
        {
            return await GetHttpGetResponse<ResultList<string>>(SupportedEventsEndpoint);
        }
    }
}
