﻿using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Core.Config;
using Igor.Gateway.Api.Sdk.Core.Http;
using Igor.Gateway.Api.Sdk.Core.Http.Security;
using Igor.Gateway.Api.Sdk.Core.Serialization;
using Igor.Gateway.Dtos.Events;
using Igor.Gateway.Dtos.Relays;
using Igor.Gateway.Dtos.Tags;
using Igor.Sdk.Common.Models;

namespace Igor.Gateway.Api.Sdk.Apis.Relays
{
    public interface IRelayService : IRetrievable<RelayDto>
    {
        Task<ResultList<RelayDto>> GetAll();
        Task<int> Create(CreateRelayDto dto);
        Task Delete(int relayId);
        Task Update(int relayId, UpdateRelayDto dto);
        Task<ResultList<EventDto>> GetEvents(int relayId);
        Task Close(int relayId);
        Task Open(int relayId);
        Task AddTag(int relayId, AddTagsDto tagDto);
        Task RemoveTag(int relayId, string tagName);
    }

    public class RelayService : ServiceBase, IRelayService
    {
        private const string AddTagEndpoint = "/relays/{0}/tags";
        private const string RemoveTagEndpoint = "/relays/{0}/tags/{1}";

        public RelayService(string apiKey)
            : base(apiKey)
        {
        }

        public RelayService(string apiKey, string apiAddress)
            : base(apiKey, apiAddress)
        {
        }

        public RelayService(IHttpClientService httpClient, IJsonSerializer jsonSerializer, ISdkConfig config,
            IJwtTokenProvider tokenProvider, IRequestHeaderProvider requestHeaderProvider)
            : base(httpClient, jsonSerializer, config, tokenProvider, requestHeaderProvider)
        {
        }

        public Task<ResultList<RelayDto>> GetAll()
        {
            return GetHttpGetResponse<ResultList<RelayDto>>("/relays");
        }

        public Task<RelayDto> Get(int relayId)
        {
            return GetHttpGetResponse<RelayDto>($"/relays/{relayId}");
        }

        public async Task<int> Create(CreateRelayDto dto)
        {
            using var response = await GetHttpPostResponse("/relays", dto);

            return GetId(response);
        }

        public async Task Delete(int relayId)
        {
            using var _ = await GetHttpDeleteResponse($"/relays/{relayId}");
        }

        public async Task Update(int relayId, UpdateRelayDto dto)
        {
            using var _ = await GetHttpPutResponse($"/relays/{relayId}", dto);
        }

        public Task<ResultList<EventDto>> GetEvents(int relayId)
        {
            return GetHttpGetResponse<ResultList<EventDto>>($"/relays/{relayId}/events");
        }

        public async Task Close(int relayId)
        {
            using var _ = await GetHttpPostResponse($"/relays/{relayId}/close", (object) null);
        }

        public async Task Open(int relayId)
        {
            using var _ = await GetHttpPostResponse($"/relays/{relayId}/open", (object) null);
        }

        public async Task AddTag(int relayId, AddTagsDto tagDto)
        {
            using var _ = await GetHttpPutResponse(AddTagEndpoint, tagDto, relayId);
        }

        public async Task RemoveTag(int relayId, string tagName)
        {
            using var _ = await GetHttpDeleteResponse(RemoveTagEndpoint, relayId, tagName);
        }
    }
}