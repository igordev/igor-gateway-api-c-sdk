﻿using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Core.Config;
using Igor.Gateway.Api.Sdk.Core.Http;
using Igor.Gateway.Api.Sdk.Core.Http.Security;
using Igor.Gateway.Api.Sdk.Core.Serialization;
using Igor.Gateway.Dtos.Events;
using Igor.Gateway.Dtos.LightSensors;
using Igor.Gateway.Dtos.Tags;
using Igor.Sdk.Common.Models;

namespace Igor.Gateway.Api.Sdk.Apis.LightSensors
{
    public interface ILightSensorService : IRetrievable<LightSensorDto>
    {
        Task<int> Create(CreateLightSensorDto createLightSensorDto);
        Task CreateEvent(int lightSensorId, CreateLightSensorEventDto createLightSensorEventDto);
        Task Delete(int lightSensorId);
        Task<ResultList<LightSensorDto>> GetAll();
        Task<ResultList<EventDto>> GetEvents(int lightSensorId);
        Task Update(int lightSensorId, UpdateLightSensorDto updateLightSensorDto);
        Task AddTag(int lightSensorId, AddTagsDto tagDto);
        Task RemoveTag(int lightSensorId, string tagName);
    }

    public class LightSensorService : ServiceBase, ILightSensorService
    {
        private const string GetAllEndpoint = "/lightsensors";
        private const string CreateEndpoint = "/lightsensors";
        private const string DeleteEndpoint = "/lightsensors/{0}";
        private const string GetEndpoint = "/lightsensors/{0}";
        private const string UpdateEndpoint = "/lightsensors/{0}";
        private const string GetEventsEndpoint = "/lightsensors/{0}/events";
        private const string CreateEventEndpoint = "/lightsensors/{0}/events";
        private const string AddTagEndpoint = "/lightsensors/{0}/tags";
        private const string RemoveTagEndpoint = "/lightsensors/{0}/tags/{1}";

        public LightSensorService(string apiKey) : base(apiKey)
        {
        }

        public LightSensorService(string apiKey, string apiAddress) : base(apiKey, apiAddress)
        {
        }

        public LightSensorService(IHttpClientService httpClient, IJsonSerializer jsonSerializer, ISdkConfig config,
            IJwtTokenProvider tokenProvider, IRequestHeaderProvider requestHeaderProvider)
            : base(httpClient, jsonSerializer, config, tokenProvider, requestHeaderProvider)
        {
        }

        public async Task<ResultList<LightSensorDto>> GetAll()
        {
            return await GetHttpGetResponse<ResultList<LightSensorDto>>(GetAllEndpoint);
        }

        public async Task<LightSensorDto> Get(int lightSensorId)
        {
            return await GetHttpGetResponse<LightSensorDto>(GetEndpoint, lightSensorId);
        }

        public async Task<int> Create(CreateLightSensorDto createLightSensorDto)
        {
            using var response = await GetHttpPostResponse(CreateEndpoint, createLightSensorDto);

            return GetId(response);
        }

        public async Task Delete(int lightSensorId)
        {
            using var _ = await GetHttpDeleteResponse(DeleteEndpoint, lightSensorId);
        }

        public async Task Update(int lightSensorId, UpdateLightSensorDto updateLightSensorDto)
        {
            using var _ = await GetHttpPutResponse(UpdateEndpoint, updateLightSensorDto, lightSensorId);
        }

        public async Task AddTag(int lightSensorId, AddTagsDto tagDto)
        {
            using var _ = await GetHttpPutResponse(AddTagEndpoint, tagDto, lightSensorId);
        }

        public async Task RemoveTag(int lightSensorId, string tagName)
        {
            using var _ = await GetHttpDeleteResponse(RemoveTagEndpoint, lightSensorId, tagName);
        }

        public async Task<ResultList<EventDto>> GetEvents(int lightSensorId)
        {
            return await GetHttpGetResponse<ResultList<EventDto>>(GetEventsEndpoint, lightSensorId);
        }

        public async Task CreateEvent(int lightSensorId,
            CreateLightSensorEventDto createLightSensorEventDto)
        {
            using var _ = await GetHttpPostResponse(CreateEventEndpoint, createLightSensorEventDto, lightSensorId);
        }
    }
}