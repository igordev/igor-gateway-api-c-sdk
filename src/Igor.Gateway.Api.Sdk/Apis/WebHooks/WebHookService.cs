﻿using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Core.Config;
using Igor.Gateway.Api.Sdk.Core.Http;
using Igor.Gateway.Api.Sdk.Core.Http.Security;
using Igor.Gateway.Api.Sdk.Core.Serialization;
using Igor.Gateway.Dtos.WebHooks;
using Igor.Sdk.Common.Models;

namespace Igor.Gateway.Api.Sdk.Apis.WebHooks
{
    public interface IWebHookService : IHttpService
    {
        Task<int> Create(int applicationKeyId, CreateWebHookDto createWebHookDto);
        Task Delete(int applicationKeyId, int webHookId);
        Task<WebHookDto> Get(int applicationKeyId, int webHookId);
        Task<ResultList<WebHookDto>> GetAll(int applicationKeyId);
        Task<ResultList<string>> GetSupportedEvents();
        Task Update(int applicationKeyId, int webHookId, UpdateWebHookDto updateWebHookDto);
    }

    public class WebHookService : ServiceBase, IWebHookService
    {
        private const string GetAllEndpoint = "/applicationkeys/{0}/webhooks";
        private const string CreateEndpoint = "/applicationkeys/{0}/webhooks";
        private const string DeleteEndpoint = "/applicationkeys/{0}/webhooks/{1}";
        private const string GetEndpoint = "/applicationkeys/{0}/webhooks/{1}";
        private const string UpdateEndpoint = "/applicationkeys/{0}/webhooks/{1}";
        private const string SupportedEventsEndpoint = "/webhooks/supported-events";

        public WebHookService(string apiKey) : base(apiKey)
        {
        }

        public WebHookService(string apiKey, string apiAddress) : base(apiKey, apiAddress)
        {
        }

        public WebHookService(IHttpClientService httpClient, IJsonSerializer jsonSerializer, ISdkConfig config, IJwtTokenProvider tokenProvider, IRequestHeaderProvider requestHeaderProvider)
            : base(httpClient, jsonSerializer, config, tokenProvider, requestHeaderProvider)
        {
        }

        public async Task<ResultList<WebHookDto>> GetAll(int applicationKeyId)
        {
            return await GetHttpGetResponse<ResultList<WebHookDto>>(GetAllEndpoint, applicationKeyId);
        }

        public async Task<int> Create(int applicationKeyId, CreateWebHookDto createWebHookDto)
        {
            using var response = await GetHttpPostResponse(CreateEndpoint, createWebHookDto, applicationKeyId);

            return GetId(response);
        }

        public async Task<ResultList<string>> GetSupportedEvents()
        {
            return await GetHttpGetResponse<ResultList<string>>(SupportedEventsEndpoint);
        }

        public async Task Delete(int applicationKeyId, int webHookId)
        {
            using var _ = await GetHttpDeleteResponse(DeleteEndpoint, applicationKeyId, webHookId);
        }

        public async Task<WebHookDto> Get(int applicationKeyId, int webHookId)
        {
            return await GetHttpGetResponse<WebHookDto>(GetEndpoint, applicationKeyId, webHookId);
        }

        public async Task Update(int applicationKeyId, int webHookId,
            UpdateWebHookDto updateWebHookDto)
        {
            using var _ = await GetHttpPutResponse(UpdateEndpoint, updateWebHookDto, applicationKeyId, webHookId);
        }
    }
}
