﻿using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Core.Config;
using Igor.Gateway.Api.Sdk.Core.Http;
using Igor.Gateway.Api.Sdk.Core.Http.Security;
using Igor.Gateway.Api.Sdk.Core.Serialization;
using Igor.Gateway.Dtos.Settings;
using Igor.Sdk.Common.Models;

namespace Igor.Gateway.Api.Sdk.Apis.Settings
{
    public interface ISettingService
    {
        Task<ResultList<SettingDto>> GetAll();
        Task Update(int settingId, UpdateSettingDto updateSettingDto);
    }

    public class SettingService : ServiceBase, ISettingService
    {
        private const string GetAllEndpoint = "/settings";
        private const string UpdateEndpoint = "/settings/{0}";

        public SettingService(string apiKey) : base(apiKey)
        {
        }

        public SettingService(string apiKey, string apiAddress) : base(apiKey, apiAddress)
        {
        }

        public SettingService(IHttpClientService httpClient, IJsonSerializer jsonSerializer, ISdkConfig config, IJwtTokenProvider tokenProvider, IRequestHeaderProvider requestHeaderProvider)
            : base(httpClient, jsonSerializer, config, tokenProvider, requestHeaderProvider)
        {
        }

        public async Task<ResultList<SettingDto>> GetAll()
        {
            return await GetHttpGetResponse<ResultList<SettingDto>>(GetAllEndpoint);
        }

        public async Task Update(int settingId, UpdateSettingDto updateSettingDto)
        {
            using var _ = await GetHttpPutResponse(UpdateEndpoint, updateSettingDto, settingId);
        }
    }
}
