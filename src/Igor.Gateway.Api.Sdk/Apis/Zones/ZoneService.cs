﻿using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Core.Config;
using Igor.Gateway.Api.Sdk.Core.Http;
using Igor.Gateway.Api.Sdk.Core.Http.Security;
using Igor.Gateway.Api.Sdk.Core.Serialization;
using Igor.Gateway.Dtos.Devices;
using Igor.Gateway.Dtos.Events;
using Igor.Gateway.Dtos.LightSensors;
using Igor.Gateway.Dtos.Spaces;
using Igor.Gateway.Dtos.Zones;
using Igor.Sdk.Common.Models;

namespace Igor.Gateway.Api.Sdk.Apis.Zones
{
    public interface IZoneService : IRetrievable<ZoneDto>
    {
        Task AddDevice(int zoneId, AddDeviceToZoneDto addDeviceToZoneDto);
        Task Delete(int zoneId);
        Task<ResultList<DeviceDto>> GetAssignableDevices(int zoneId);
        Task<ResultList<DeviceDto>> GetDevices(int zoneId);
        Task<ResultList<EventDto>> GetEvents(int zoneId);
        Task<LightSensorDto> GetLightSensor(int zoneId);
        Task<SpaceDto> GetSpace(int zoneId);
        Task RemoveLight(int zoneId, int lightId);
        Task RemoveLightSensor(int zoneId, int lightSensorId);
        Task Update(int zoneId, UpdateZoneDto updateZoneDto);
    }

    public class ZoneService : ServiceBase, IZoneService
    {
        private const string DeleteEndpoint = "/zones/{0}";
        private const string GetEndpoint = "/zones/{0}";
        private const string UpdateEndpoint = "/zones/{0}";
        private const string GetSpaceEndpoint = "/zones/{0}/space";
        private const string GetLightSensorEndpoint = "/zones/{0}/lightsensor";
        private const string GetDevicesEndpoint = "/zones/{0}/devices";
        private const string AddDeviceEndpoint = "/zones/{0}/devices";
        private const string AssignableDevicesEndpoint = "/zones/{0}/assignabledevices";
        private const string RemoveLightEndpoint = "/zones/{0}/lights/{1}";
        private const string RemoveLightSensorEndpoint = "/zones/{0}/lightsensors/{1}";
        private const string GetEventsEndpoint = "/zones/{0}/events";

        public ZoneService(string apiKey) : base(apiKey)
        {
        }

        public ZoneService(string apiKey, string apiAddress) : base(apiKey, apiAddress)
        {
        }

        public ZoneService(IHttpClientService httpClient, IJsonSerializer jsonSerializer, ISdkConfig config, IJwtTokenProvider tokenProvider, IRequestHeaderProvider requestHeaderProvider)
            : base(httpClient, jsonSerializer, config, tokenProvider, requestHeaderProvider)
        {
        }

        public async Task Delete(int zoneId)
        {
            using var _ = await GetHttpDeleteResponse(DeleteEndpoint, zoneId);
        }

        public async Task<ZoneDto> Get(int zoneId)
        {
            return await GetHttpGetResponse<ZoneDto>(GetEndpoint, zoneId);
        }

        public async Task Update(int zoneId, UpdateZoneDto updateZoneDto)
        {
            using var _ = await GetHttpPutResponse(UpdateEndpoint, updateZoneDto, zoneId);
        }

        public async Task<SpaceDto> GetSpace(int zoneId)
        {
            return await GetHttpGetResponse<SpaceDto>(GetSpaceEndpoint, zoneId);
        }

        public async Task<LightSensorDto> GetLightSensor(int zoneId)
        {
            return await GetHttpGetResponse<LightSensorDto>(GetLightSensorEndpoint, zoneId);
        }

        public async Task<ResultList<DeviceDto>> GetDevices(int zoneId)
        {
            return await GetHttpGetResponse<ResultList<DeviceDto>>(GetDevicesEndpoint, zoneId);
        }

        public async Task AddDevice(int zoneId, AddDeviceToZoneDto addDeviceToZoneDto)
        {
            using var _ = await GetHttpPutResponse(AddDeviceEndpoint, addDeviceToZoneDto, zoneId);
        }

        public async Task<ResultList<DeviceDto>> GetAssignableDevices(int zoneId)
        {
            return await GetHttpGetResponse<ResultList<DeviceDto>>(AssignableDevicesEndpoint, zoneId);
        }

        public async Task RemoveLight(int zoneId, int lightId)
        {
            using var _ = await GetHttpDeleteResponse(RemoveLightEndpoint, zoneId, lightId);
        }

        public async Task RemoveLightSensor(int zoneId, int lightSensorId)
        {
            using var _ = await GetHttpDeleteResponse(RemoveLightSensorEndpoint, zoneId, lightSensorId);
        }

        public async Task<ResultList<EventDto>> GetEvents(int zoneId)
        {
            return await GetHttpGetResponse<ResultList<EventDto>>(GetEventsEndpoint, zoneId);
        }
    }
}
