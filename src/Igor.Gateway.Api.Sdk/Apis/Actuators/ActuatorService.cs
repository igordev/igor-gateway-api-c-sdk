﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Core.Config;
using Igor.Gateway.Api.Sdk.Core.Http;
using Igor.Gateway.Api.Sdk.Core.Http.Security;
using Igor.Gateway.Api.Sdk.Core.Serialization;
using Igor.Gateway.Dtos.Actuators;
using Igor.Gateway.Dtos.Events;
using Igor.Gateway.Dtos.Tags;
using Igor.Sdk.Common.Models;

namespace Igor.Gateway.Api.Sdk.Apis.Actuators
{
    public interface IActuatorService : IRetrievable<ActuatorDto>
    {
        Task<int> Create(CreateActuatorDto createActuatorDto);
        Task Delete(int actuatorId);
        Task<ResultList<ActuatorDto>> GetAll();
        Task<ResultList<CommandCollectionDto>> GetUniqueCommandCollections(); 
        Task<ResultList<EventDto>> GetEvents(int actuatorId);
        Task CreateEvent(int actuatorId, CreateActuatorEventDto createActuatorEventDto);
        Task Update(int actuatorId, UpdateActuatorDto updateActuatorDto);

        Task CommandNative(int actuatorId, NativeActuatorCommandParametersDto nativeActuatorCommandParametersDto,
            string commandCollectionName = null);

        Task CommandString(int actuatorId, StringActuatorCommandParametersDto stringActuatorCommandParametersDto,
            string commandCollectionName = null);

        Task CommandString(int actuatorId, string value, ActuatorStringEncoding encoding = ActuatorStringEncoding.Utf8,
            string commandCollectionName = null);

        /// <summary>
        /// Helper method for sending a binary command to an actuator. true = 1, false = 0
        /// </summary>
        /// <param name="actuatorId">ID of the actuator</param>
        /// <param name="binaryValue">Value to send</param>
        /// <param name="commandCollectionName">Name of collection containing the command you want to execute</param>
        Task CommandBinary(int actuatorId, bool binaryValue, string commandCollectionName = null);

        /// <summary>
        /// Helper method for sending a percentage command to an actuator. Valid values 0 - 10,000
        /// </summary>
        /// <param name="actuatorId">ID of the actuator</param>
        /// <param name="percentage">Value to send</param>
        /// <param name="commandCollectionName">Name of collection containing the command you want to execute</param>
        /// <returns></returns>
        Task CommandPercentage(int actuatorId, int percentage, string commandCollectionName = null);

        /// <summary>
        /// Helper method for sending a discrete command to an actuator.
        /// </summary>
        /// <param name="actuatorId">ID of the actuator</param>
        /// <param name="value">Value to send</param>
        /// <param name="commandCollectionName">Name of collection containing the command you want to execute</param>
        /// <returns></returns>
        Task CommandDiscrete(int actuatorId, object value, string commandCollectionName = null);

        /// <summary>
        /// Helper method for sending a range command to an actuator.
        /// </summary>
        /// <param name="actuatorId">ID of the actuator</param>
        /// <param name="value">Value to send</param>
        /// <param name="commandCollectionName">Name of collection containing the command you want to execute</param>
        Task CommandRange(int actuatorId, object value, string commandCollectionName = null);

        Task AddTag(int actuatorId, AddTagsDto tagDto);
        Task RemoveTag(int actuatorId, string tagName);
    }

    public class ActuatorService : ServiceBase, IActuatorService
    {
        private const string GetAllEndpoint = "/actuators";
        private const string CreateEndpoint = "/actuators";
        private const string DeleteEndpoint = "/actuators/{0}";
        private const string GetEndpoint = "/actuators/{0}";
        private const string UpdateEndpoint = "/actuators/{0}";
        private const string GetEventsEndpoint = "/actuators/{0}/events";
        private const string CreateEventEndpoint = "/actuators/{0}/events";
        private const string CommandEndpoint = "/actuators/{0}/command";
        private const string AddTagEndpoint = "/actuators/{0}/tags";
        private const string RemoveTagEndpoint = "/actuators/{0}/tags/{1}";
        private const string GetUniqueCommandCollectionsEndpoint = "/actuators/commandCollections";

        public ActuatorService(string apiKey) : base(apiKey)
        {
        }

        public ActuatorService(string apiKey, string apiAddress) : base(apiKey, apiAddress)
        {
        }

        public ActuatorService(IHttpClientService httpClient, IJsonSerializer jsonSerializer, ISdkConfig config,
            IJwtTokenProvider tokenProvider, IRequestHeaderProvider requestHeaderProvider) : base(httpClient, jsonSerializer, config, tokenProvider, requestHeaderProvider)
        {
        }

        public async Task AddTag(int actuatorId, AddTagsDto tagDto)
        {
            using var _ = await GetHttpPutResponse(AddTagEndpoint, tagDto, actuatorId);
        }

        public async Task RemoveTag(int actuatorId, string tagName)
        {
            using var _ = await GetHttpDeleteResponse(RemoveTagEndpoint, actuatorId, tagName);
        }

        public async Task<ActuatorDto> Get(int actuatorId)
        {
            return await GetHttpGetResponse<ActuatorDto>(GetEndpoint, actuatorId);
        }

        public async Task<ResultList<ActuatorDto>> GetAll()
        {
            return await GetHttpGetResponse<ResultList<ActuatorDto>>(GetAllEndpoint);
        }

        public async Task<ResultList<CommandCollectionDto>> GetUniqueCommandCollections()
        {
            return await GetHttpGetResponse<ResultList<CommandCollectionDto>>(GetUniqueCommandCollectionsEndpoint); 
        }

        public async Task<int> Create(CreateActuatorDto createActuatorDto)
        {
            using var response = await GetHttpPostResponse(CreateEndpoint, createActuatorDto);

            return GetId(response);
        }

        public async Task Update(int actuatorId, UpdateActuatorDto updateActuatorDto)
        {
            using var _ = await GetHttpPutResponse(UpdateEndpoint, updateActuatorDto, actuatorId);
        }

        public async Task Delete(int actuatorId)
        {
            using var _ = await GetHttpDeleteResponse(DeleteEndpoint, actuatorId);
        }

        public async Task<ResultList<EventDto>> GetEvents(int actuatorId)
        {
            return await GetHttpGetResponse<ResultList<EventDto>>(GetEventsEndpoint, actuatorId);
        }

        public async Task CreateEvent(int actuatorId, CreateActuatorEventDto createActuatorEventDto)
        {
            using var _ = await GetHttpPostResponse(CreateEventEndpoint, createActuatorEventDto, actuatorId);
        }

        /// <summary>
        /// Helper method for sending a binary command to an actuator. true = 1, false = 0
        /// </summary>
        /// <param name="actuatorId">ID of the actuator</param>
        /// <param name="percentage">Value to send</param>
        /// <param name="commandCollectionName">Name of collection containing the command you want to execute</param>
        public async Task CommandPercentage(int actuatorId, int percentage, string commandCollectionName = null)
        {
            await CommandNative(actuatorId, new NativeActuatorCommandParametersDto
            {
                CommandType = "Percentage",
                Value = percentage
            }, commandCollectionName);
        }

        /// <summary>
        /// Helper method for sending a discrete command to an actuator.
        /// </summary>
        /// <param name="actuatorId">ID of the actuator</param>
        /// <param name="value">Value to send</param>
        /// <param name="commandCollectionName">Name of collection containing the command you want to execute</param>
        /// <returns></returns>
        public async Task CommandDiscrete(int actuatorId, object value, string commandCollectionName = null)
        {
            await CommandNative(actuatorId, new NativeActuatorCommandParametersDto
            {
                CommandType = "Discrete",
                Value = value
            }, commandCollectionName);
        }

        /// <summary>
        /// Helper method for sending a range command to an actuator.
        /// </summary>
        /// <param name="actuatorId">ID of the actuator</param>
        /// <param name="value">Value to send</param>
        /// <param name="commandCollectionName">Name of collection containing the command you want to execute</param>
        public async Task CommandRange(int actuatorId, object value, string commandCollectionName = null)
        {
            await CommandNative(actuatorId, new NativeActuatorCommandParametersDto
            {
                CommandType = "Range",
                Value = value
            }, commandCollectionName);
        }

        /// <summary>
        /// Helper method for sending a binary command to an actuator. true = 1, false = 0
        /// </summary>
        /// <param name="actuatorId">ID of the actuator</param>
        /// <param name="binaryValue">Value to send</param>
        /// <param name="commandCollectionName">Name of collection containing the command you want to execute</param>
        public async Task CommandBinary(int actuatorId, bool binaryValue, string commandCollectionName = null)
        {
            await CommandNative(actuatorId, new NativeActuatorCommandParametersDto
            {
                CommandType = "Binary",
                Value = binaryValue ? 1 : 0
            }, commandCollectionName);
        }

        public async Task CommandNative(int actuatorId,
            NativeActuatorCommandParametersDto nativeActuatorCommandParametersDto, string commandCollectionName = null)
        {
            var command = new ActuatorCommandDto
            {
                Name = await GetCommandCollectionName(actuatorId, commandCollectionName),
                Type = ActuatorCommandType.Command,
                Parameters = nativeActuatorCommandParametersDto
            };

            using var _ = await GetHttpPostResponse(CommandEndpoint, command, actuatorId);
        }

        public async Task CommandString(int actuatorId,
            StringActuatorCommandParametersDto stringActuatorCommandParametersDto, string commandCollectionName = null)
        {
            var command = new ActuatorCommandDto
            {
                Name = await GetCommandCollectionName(actuatorId, commandCollectionName),
                Type = ActuatorCommandType.String,
                Parameters = stringActuatorCommandParametersDto
            };

            using var _ = await GetHttpPostResponse(CommandEndpoint, command, actuatorId);
        }

        public async Task CommandString(int actuatorId, string value,
            ActuatorStringEncoding encoding = ActuatorStringEncoding.Utf8, string commandCollectionName = null)
        {
            var encodingValue = GetEncoding(encoding);

            var command = new ActuatorCommandDto
            {
                Name = await GetCommandCollectionName(actuatorId, commandCollectionName),
                Type = ActuatorCommandType.String,
                Parameters = new StringActuatorCommandParametersDto
                {
                    Encoding = encodingValue,
                    Value = value
                }
            };

            using var _ = await GetHttpPostResponse(CommandEndpoint, command, actuatorId);
        }

        private string GetEncoding(ActuatorStringEncoding encoding)
        {
            switch (encoding)
            {
                case ActuatorStringEncoding.Ascii:
                    return "ASCII";
                case ActuatorStringEncoding.Unicode:
                    return "Unicode";
                case ActuatorStringEncoding.Utf8:
                    return "UTF-8";
                default:
                    return "UTF-8";
            }
        }

        private async Task<string> GetCommandCollectionName(int actuatorId, string commandCollectionName)
        {
            string name = null;

            if (!string.IsNullOrEmpty(commandCollectionName)) return commandCollectionName;

            var actuator = await Get(actuatorId);

            if (actuator?.Commands.Count > 1)
                throw new InvalidOperationException(
                    $"{actuator.Name} (ID: {actuatorId}) has more than one command collection. You must specify a command collection name.");

            if (actuator?.Commands.Count == 1)
                name = actuator.Commands.First().Name;

            return name;
        }
    }
}