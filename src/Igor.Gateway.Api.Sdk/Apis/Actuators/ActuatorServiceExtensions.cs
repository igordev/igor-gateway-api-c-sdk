﻿using System.Threading.Tasks;
using Igor.Gateway.Dtos.Actuators;

namespace Igor.Gateway.Api.Sdk.Apis.Actuators
{
    public static class ActuatorServiceExtensions
    {
        public static async Task SendPercentage(this IActuatorService service, int actuatorId, int percent, string commandCollectionName = null)
        {
            var command = CreateCommand("Percentage", percent);

            await service.CommandNative(actuatorId, command, commandCollectionName);
        }

        public static async Task SendBinary(this IActuatorService service, int actuatorId, byte value,
            string commandCollectionName = null)
        {
            var command = CreateCommand("Binary", value);

            await service.CommandNative(actuatorId, command, commandCollectionName);
        }

        public static async Task SendPassthrough(this IActuatorService service, int actuatorId, byte[] value,
            string commandCollectionName = null)
        {
            string commandType = null;

            if (value.Length <= 255)
                commandType = "Passthrough8";
            else
                commandType = "Passthrough16";

            var command = CreateCommand(commandType, value);

            await service.CommandNative(actuatorId, command, commandCollectionName);
        }

        public static async Task SendRange(this IActuatorService service, int actuatorId, int value,
            string commandCollectionName = null)
        {
            var command = CreateCommand("Range", value);

            await service.CommandNative(actuatorId, command, commandCollectionName);
        }

        public static async Task SendDiscrete(this IActuatorService service, int actuatorId, object value,
            string commandCollectionName = null)
        {
            var command = CreateCommand("Discrete", value);

            await service.CommandNative(actuatorId, command, commandCollectionName);
        }

        private static NativeActuatorCommandParametersDto CreateCommand(string type, object value)
        {
            return new NativeActuatorCommandParametersDto
            {
                CommandType = type,
                Value = value
            };
        }
    }
}