﻿using Igor.Gateway.Dtos.Actuators;

namespace Igor.Gateway.Api.Sdk.Apis.Actuators
{
    public class CommandCollectionDtoBuilder
    {
        private readonly CommandCollectionDto _dto;
        private readonly CreateActuatorDtoBuilder _actuatorDtoBuilder;

        private CommandTypeDtoBuilder _commandTypeBuilder;

        public CommandCollectionDtoBuilder(string name, CreateActuatorDtoBuilder actuatorDtoBuilder)
        {
            _actuatorDtoBuilder = actuatorDtoBuilder;
            _dto = new CommandCollectionDto(name);
        }

        public CommandCollectionDtoBuilder WithBinaryCommand()
        {
            return new CommandTypeDtoBuilder(this)
                .WithBinaryCommand()
                .BuildCommand();
        }

        public CommandCollectionDtoBuilder WithPassthrough8Command()
        {
            return new CommandTypeDtoBuilder(this)
                .WithPassthrough8Command()
                .BuildCommand();
        }

        public CommandCollectionDtoBuilder WithPassthrough16Command()
        {
            return new CommandTypeDtoBuilder(this)
                .WithPassthrough16Command()
                .BuildCommand();
        }

        public CommandCollectionDtoBuilder WithPercentageCommand()
        {
            return new CommandTypeDtoBuilder(this)
                .WithPercentageCommand()
                .BuildCommand();
        }

        public CommandTypeDtoBuilder WithDiscreteValue(object value)
        {
            if (_commandTypeBuilder == null)
                _commandTypeBuilder = new CommandTypeDtoBuilder(this);

            _commandTypeBuilder.WithDiscreteValue(value);

            return _commandTypeBuilder;
        }

        public CommandTypeDtoBuilder WithRangeValues(decimal min, decimal max)
        {
            if (_commandTypeBuilder == null)
                _commandTypeBuilder = new CommandTypeDtoBuilder(this);

            _commandTypeBuilder.WithRangeCommand(min, max);

            return _commandTypeBuilder;
        }

        public CreateActuatorDtoBuilder BuildCollection()
        {
            _actuatorDtoBuilder.AddCommandCollection(_dto);

            _commandTypeBuilder = null;

            return _actuatorDtoBuilder;
        }

        internal void AddCommand(CommandTypeDto command)
        {
            _dto.Commands.Add(command);
        }
    }
}