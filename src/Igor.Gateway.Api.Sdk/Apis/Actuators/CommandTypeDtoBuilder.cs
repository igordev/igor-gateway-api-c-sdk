﻿using System;
using Igor.Gateway.Dtos.Actuators;

namespace Igor.Gateway.Api.Sdk.Apis.Actuators
{
    public class CommandTypeDtoBuilder
    {
        private const string BuildExceptionMessage = "You must first build your existing command by calling 'BuildCommand' before adding a different command type.";

        private readonly CommandCollectionDtoBuilder _collectionBuilder;
        private CommandTypeDto _dto;

        public CommandTypeDtoBuilder(CommandCollectionDtoBuilder collectionBuilder)
        {
            _collectionBuilder = collectionBuilder;
        }

        public CommandTypeDtoBuilder WithBinaryCommand()
        {
            if (_dto == null)
                _dto = new CommandTypeDto("Binary");
            else
                throw new Exception(BuildExceptionMessage);

            _dto = new CommandTypeDto("Binary");

            return this;
        }

        public CommandTypeDtoBuilder WithPassthrough8Command()
        {
            if (_dto == null)
                _dto = new CommandTypeDto("Passthrough8");
            else
                throw new Exception(BuildExceptionMessage);

            _dto = new CommandTypeDto("Passthrough8");

            return this;
        }

        public CommandTypeDtoBuilder WithPassthrough16Command()
        {
            if (_dto == null)
                _dto = new CommandTypeDto("Passthrough16");
            else
                throw new Exception(BuildExceptionMessage);

            _dto = new CommandTypeDto("Passthrough16");

            return this;
        }

        public CommandTypeDtoBuilder WithPercentageCommand()
        {
            if (_dto == null)
                _dto = new CommandTypeDto("Percentage");
            else
                throw new Exception(BuildExceptionMessage);

            _dto = new CommandTypeDto("Percentage");

            return this;
        }

        public CommandTypeDtoBuilder WithDiscreteValue(object value)
        {
            if (_dto == null)
                _dto = new CommandTypeDto("Discrete");
            else if (_dto.CommandType != "Discrete")
                throw new Exception(BuildExceptionMessage);

            _dto.Parameters.Add(new CommandParameterDto("DiscreteValue", value));

            return this;
        }

        public CommandTypeDtoBuilder WithRangeCommand(decimal min, decimal max)
        {
            if (_dto == null)
                _dto = new CommandTypeDto("Range");
            else if (_dto.CommandType != "Range")
                throw new Exception(BuildExceptionMessage);

            _dto.Parameters.Add(new CommandParameterDto("RangeMin", min));
            _dto.Parameters.Add(new CommandParameterDto("RangeMax", max));

            return this;
        }

        public CommandCollectionDtoBuilder BuildCommand()
        {
            _collectionBuilder.AddCommand(_dto);

            _dto = null;

            return _collectionBuilder;
        }
    }
}