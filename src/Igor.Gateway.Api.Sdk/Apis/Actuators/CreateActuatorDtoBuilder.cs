﻿using Igor.Gateway.Dtos.Actuators;

namespace Igor.Gateway.Api.Sdk.Apis.Actuators
{
    public class CreateActuatorDtoBuilder
    {
        private readonly CreateActuatorDto _dto;
        private CommandCollectionDtoBuilder _collectionBuilder;

        public CreateActuatorDtoBuilder()
        {
            _dto = new CreateActuatorDto();
        }

        public CreateActuatorDtoBuilder WithName(string name)
        {
            _dto.Name = name;

            return this;
        }

        public CommandCollectionDtoBuilder WithCommandCollection(string name)
        {
            _collectionBuilder = new CommandCollectionDtoBuilder(name, this);

            return _collectionBuilder;
        }

        public CreateActuatorDto BuildActuator()
        {
            return _dto;
        }

        internal void AddCommandCollection(CommandCollectionDto collection)
        {
            _dto.Commands.Add(collection);
        }
    }
}