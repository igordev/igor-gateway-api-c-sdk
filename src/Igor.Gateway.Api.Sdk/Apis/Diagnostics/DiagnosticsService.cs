﻿using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Core.Config;
using Igor.Gateway.Api.Sdk.Core.Http;
using Igor.Gateway.Api.Sdk.Core.Http.Security;
using Igor.Gateway.Api.Sdk.Core.Serialization;
using Igor.Gateway.Dtos.Diagnostics;
using Igor.Sdk.Common.Models;

namespace Igor.Gateway.Api.Sdk.Apis.Diagnostics
{
    public interface IDiagnosticsService : IHttpService
    {
        Task<VersionDto> GetVersion();
        Task<IResultList<DiagnosticWarningDto>> GetWarnings();
        Task SyncAll();
        Task CreateDiagnosticMessage(DiagnosticMessageDto diagnosticMessageDto); 
    }

    public class DiagnosticsService : ServiceBase, IDiagnosticsService
    {
        private const string GetVersionEndpoint = "/diagnostics/version";
        private const string GetWarningsEndpoint = "/diagnostics/warnings";
        private const string SyncAllEndpoint = "/diagnostics/syncall";
        private const string MessagesEndpoint = "/diagnostics/messages"; 

        public DiagnosticsService(string apiKey) : base(apiKey)
        {
        }

        public DiagnosticsService(string apiKey, string apiAddress) : base(apiKey, apiAddress)
        {
        }

        public DiagnosticsService(IHttpClientService httpClient, IJsonSerializer jsonSerializer, ISdkConfig config, IJwtTokenProvider tokenProvider, IRequestHeaderProvider requestHeaderProvider) : base(httpClient, jsonSerializer, config, tokenProvider, requestHeaderProvider)
        {
        }

        public async Task<VersionDto> GetVersion()
        {
            return await GetHttpGetResponse<VersionDto>(GetVersionEndpoint, (object)null);
        }

        public async Task<IResultList<DiagnosticWarningDto>> GetWarnings()
        {
            return await GetHttpGetResponse<ResultList<DiagnosticWarningDto>>(GetWarningsEndpoint);
        }

        public async Task SyncAll()
        {
            using var _ = await GetHttpPostResponse(SyncAllEndpoint, (object)null);
        }

        public async Task CreateDiagnosticMessage(DiagnosticMessageDto diagnosticMessageDto)
        {
            using var _ = await GetHttpPostResponse(MessagesEndpoint, diagnosticMessageDto); 
        }
    }
}
