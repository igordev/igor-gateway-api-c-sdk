﻿using System.Collections.Generic;
using Igor.Gateway.Api.Sdk.Core.Config;
using Igor.Gateway.Api.Sdk.Core.Http.Security;

namespace Igor.Gateway.Api.Sdk.Apis
{
    public class RequestHeaderProvider : IRequestHeaderProvider
    {
        private readonly IJwtTokenProvider _tokenProvider;
        private readonly ISdkConfig _config;

        public RequestHeaderProvider(IJwtTokenProvider tokenProvider, ISdkConfig config)
        {
            _tokenProvider = tokenProvider;
            _config = config;
        }

        public IEnumerable<KeyValuePair<string, string>> GetHeaders()
        {
            var token = _tokenProvider.GetToken(_config.ApiKey);
            return new[] { new KeyValuePair<string, string>("Authorization", "Bearer " + token) };
        }
    }
}
