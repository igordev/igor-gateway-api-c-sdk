﻿using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Core.Config;
using Igor.Gateway.Api.Sdk.Core.Http;
using Igor.Gateway.Api.Sdk.Core.Http.Security;
using Igor.Gateway.Api.Sdk.Core.Serialization;
using Igor.Gateway.Dtos.Tags;
using Igor.Sdk.Common.Models;

namespace Igor.Gateway.Api.Sdk.Apis.Tags
{
    public interface ITagService : IRetrievable<TagDto>
    {
        Task<int> Create(CreateTagDto createTagDto);
        Task Update(int tagId, UpdateTagDto updateTagDto);
        Task<PaginatedList<TagsSearchResultsDto>> Search();
        Task<PaginatedList<TagsSearchResultsDto>> Search(TagsSearchParameters queryParameters);
        Task Delete(int tagId);

        Task<PaginatedList<TagEntitySearchResultsDto>> SearchEntities(int tagId,
            TagEntitySearchParameters queryParameters);
    }

    public class TagService : ServiceBase, ITagService
    {
        private const string SearchEndpoint = "/tags";
        private const string EntitySearchEndpoint = "/tags/{0}/entities";
        private const string CreateEndpoint = "/tags";
        private const string DeleteEndpoint = "/tags/{0}";
        private const string GetEndpoint = "/tags/{0}";
        private const string UpdateEndpoint = "/tags/{0}";

        public TagService(string apiKey) : base(apiKey)
        {
        }

        public TagService(string apiKey, string apiAddress) : base(apiKey, apiAddress)
        {
        }

        public TagService(IHttpClientService httpClient, IJsonSerializer jsonSerializer, ISdkConfig config,
            IJwtTokenProvider tokenProvider, IRequestHeaderProvider requestHeaderProvider) : base(httpClient, jsonSerializer, config, tokenProvider, requestHeaderProvider)
        {
        }

        public async Task<TagDto> Get(int tagId)
        {
            return await GetHttpGetResponse<TagDto>(GetEndpoint, tagId);
        }

        public async Task<int> Create(CreateTagDto createTagDto)
        {
            using var response = await GetHttpPostResponse(CreateEndpoint, createTagDto);

            return GetId(response);
        }

        public async Task Update(int tagId, UpdateTagDto updateTagDto)
        {
            using var _ = await GetHttpPutResponse(UpdateEndpoint, updateTagDto, tagId);
        }

        public async Task<PaginatedList<TagsSearchResultsDto>> Search()
        {
            return await Search(null);
        }

        public async Task<PaginatedList<TagsSearchResultsDto>> Search(TagsSearchParameters queryParameters)
        {
            var endpoint = FormatEndpoint(SearchEndpoint) + queryParameters;
            var results = await GetAsync<PaginatedList<TagsSearchResultsDto>>(endpoint);

            return results;
        }

        public async Task<PaginatedList<TagEntitySearchResultsDto>> SearchEntities(int tagId,
            TagEntitySearchParameters queryParameters)
        {
            var endpoint = FormatEndpoint(EntitySearchEndpoint, tagId) + queryParameters;
            var results = await GetAsync<PaginatedList<TagEntitySearchResultsDto>>(endpoint);

            return results;
        }

        public async Task Delete(int tagId)
        {
            using var _ = await GetHttpDeleteResponse(DeleteEndpoint, tagId);
        }
    }
}