﻿using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Core.Config;
using Igor.Gateway.Api.Sdk.Core.Http;
using Igor.Gateway.Api.Sdk.Core.Http.Security;
using Igor.Gateway.Api.Sdk.Core.Serialization;
using Igor.Gateway.Dtos.Events;
using Igor.Gateway.Dtos.Tags;
using Igor.Gateway.Dtos.WallControls;
using Igor.Sdk.Common.Models;

namespace Igor.Gateway.Api.Sdk.Apis.WallControls
{
    public interface IWallControlService : IRetrievable<WallControlDto>
    {
        Task<ResultList<WallControlDto>> GetAll();
        Task<int> Create(CreateWallControlDto createWallControlDto);
        Task Delete(int wallControlId);
        Task Update(int wallControlId, UpdateWallControlDto updateWallControlDto);

        Task<ResultList<WallControlButtonDto>> GetWallControlButtons(int wallControlId);
        Task UpdateWallControlButton(int wallControlId, int buttonId, UpdateWallControlButtonDto dto);
        Task<ResultList<WallControlGestureDto>> GetWallControlButtonGestures(int wallControlId, int buttonId);
        Task<int> CreateWallControlButtonGesture(int wallControlId, int buttonId, CreateWallControlGestureDto dto);
        Task DeleteWallControlButtonGesture(int wallControlId, int buttonId, int gestureId);
        Task<WallControlGestureDto> GetWallControlButtonGesture(int wallControlId, int buttonId, int gestureId);
        Task UpdateWallControlButtonGesture(int wallControlId, int buttonId, int gestureId, UpdateWallControlGestureDto dto);
        Task ResetWallControlButtonGesture(int wallControlId, int buttonId, int gestureId);

        Task<ResultList<EventDto>> GetEvents(int wallControlId);
        Task CreateEvent(int wallControlId, CreateWallControlEventDto createWallControlEventDto);
        Task UpdateSettings(int wallControlId, WallControlSettingsDto settingsDto);
        Task AddTag(int wallControlId, AddTagsDto tagDto);
        Task RemoveTag(int wallControlId, string tagName);
    }

    public class WallControlService : ServiceBase, IWallControlService
    {
        private const string GetAllEndpoint = "/wallcontrols";
        private const string CreateEndpoint = "/wallcontrols";
        private const string DeleteEndpoint = "/wallcontrols/{0}";
        private const string GetEndpoint = "/wallcontrols/{0}";
        private const string UpdateEndpoint = "/wallcontrols/{0}";

        private const string GetWallControlButtonsEndpoint = "/wallcontrols/{0}/buttons";
        private const string UpdateWallControlButtonEndpoint = "/wallcontrols/{0}/buttons/{1}";
        private const string GetWallControlButtonGesturesEndpoint = "/wallcontrols/{0}/buttons/{1}/gestures";
        private const string CreateWallControlButtonGestureEndpoint = "/wallcontrols/{0}/buttons/{1}/gestures";
        private const string DeleteWallControlButtonGestureEndpoint = "/wallcontrols/{0}/buttons/{1}/gestures/{2}";
        private const string GetWallControlButtonGestureEndpoint = "/wallcontrols/{0}/buttons/{1}/gestures/{2}";
        private const string UpdateWallControlButtonGestureEndpoint = "/wallcontrols/{0}/buttons/{1}/gestures/{2}";
        private const string ResetWallControlButtonGestureEndpoint = "/wallcontrols/{0}/buttons/{1}/gestures/{2}/reset";

        private const string GetEventsEndpoint = "/wallcontrols/{0}/events";
        private const string CreateEventEndpoint = "/wallcontrols/{0}/events";
        private const string UpdateSettingsEndpoint = "/wallcontrols/{0}/settings";
        private const string AddTagEndpoint = "/wallcontrols/{0}/tags";
        private const string RemoveTagEndpoint = "/wallcontrols/{0}/tags/{1}";

        public WallControlService(string apiKey) : base(apiKey)
        {
        }

        public WallControlService(string apiKey, string apiAddress) : base(apiKey, apiAddress)
        {
        }

        public WallControlService(IHttpClientService httpClient, IJsonSerializer jsonSerializer, ISdkConfig config,
            IJwtTokenProvider tokenProvider, IRequestHeaderProvider requestHeaderProvider)
            : base(httpClient, jsonSerializer, config, tokenProvider, requestHeaderProvider)
        {
        }

        public async Task<ResultList<WallControlDto>> GetAll()
        {
            return await GetHttpGetResponse<ResultList<WallControlDto>>(GetAllEndpoint);
        }

        public async Task<int> Create(CreateWallControlDto createWallControlDto)
        {
            using var response = await GetHttpPostResponse(CreateEndpoint, createWallControlDto);

            return GetId(response);
        }

        public async Task Delete(int wallControlId)
        {
            using var _ = await GetHttpDeleteResponse(DeleteEndpoint, wallControlId);
        }

        public async Task<WallControlDto> Get(int wallControlId)
        {
            return await GetHttpGetResponse<WallControlDto>(GetEndpoint, wallControlId);
        }

        public async Task Update(int wallControlId, UpdateWallControlDto updateWallControlDto)
        {
            using var _ = await GetHttpPutResponse(UpdateEndpoint, updateWallControlDto, wallControlId);
        }

        public async Task<ResultList<WallControlButtonDto>> GetWallControlButtons(int wallControlId)
        {
            return await GetHttpGetResponse<ResultList<WallControlButtonDto>>(GetWallControlButtonsEndpoint,
                wallControlId);
        }

        public async Task UpdateWallControlButton(int wallControlId, int buttonId, UpdateWallControlButtonDto dto)
        {
            using var _ = await GetHttpPutResponse(UpdateWallControlButtonEndpoint, dto, wallControlId, buttonId);
        }

        public async Task<ResultList<WallControlGestureDto>> GetWallControlButtonGestures(int wallControlId,
            int buttonId)
        {
            return await GetHttpGetResponse<ResultList<WallControlGestureDto>>(GetWallControlButtonGesturesEndpoint,
                wallControlId, buttonId);
        }

        public async Task<int> CreateWallControlButtonGesture(int wallControlId, int buttonId,
            CreateWallControlGestureDto dto)
        {
            using var response = await GetHttpPostResponse(CreateWallControlButtonGestureEndpoint, dto, wallControlId, buttonId);

            return GetId(response);
        }

        public async Task DeleteWallControlButtonGesture(int wallControlId, int buttonId, int gestureId)
        {
            using var _ = await GetHttpDeleteResponse(DeleteWallControlButtonGestureEndpoint, wallControlId, buttonId, gestureId);
        }

        public async Task<WallControlGestureDto> GetWallControlButtonGesture(int wallControlId, int buttonId,
            int gestureId)
        {
            return await GetHttpGetResponse<WallControlGestureDto>(GetWallControlButtonGestureEndpoint, wallControlId,
                buttonId, gestureId);
        }

        public async Task UpdateWallControlButtonGesture(int wallControlId, int buttonId, int gestureId,
            UpdateWallControlGestureDto dto)
        {
            using var _ = await GetHttpPutResponse(UpdateWallControlButtonGestureEndpoint, dto, wallControlId, buttonId, gestureId);
        }

        public async Task ResetWallControlButtonGesture(int wallControlId, int buttonId, int gestureId)
        {
            using var _ = await GetHttpPostResponse<WallControlGestureDto>(ResetWallControlButtonGestureEndpoint, null, wallControlId, buttonId, gestureId);
        }

        public async Task<ResultList<EventDto>> GetEvents(int wallControlId)
        {
            return await GetHttpGetResponse<ResultList<EventDto>>(GetEventsEndpoint, wallControlId);
        }

        public async Task CreateEvent(int wallControlId, CreateWallControlEventDto createWallControlEventDto)
        {
            using var _ = await GetHttpPostResponse(CreateEventEndpoint, createWallControlEventDto, wallControlId);
        }

        public async Task UpdateSettings(int wallControlId, WallControlSettingsDto settingsDto)
        {
            using var _ = await GetHttpPutResponse(UpdateSettingsEndpoint, settingsDto, wallControlId);
        }

        public async Task AddTag(int wallControlId, AddTagsDto tagDto)
        {
            using var _ = await GetHttpPutResponse(AddTagEndpoint, tagDto, wallControlId);
        }

        public async Task RemoveTag(int wallControlId, string tagName)
        {
            using var _ = await GetHttpDeleteResponse(RemoveTagEndpoint, wallControlId, tagName);
        }
    }
}