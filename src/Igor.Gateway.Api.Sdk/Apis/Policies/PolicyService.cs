﻿using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Core.Config;
using Igor.Gateway.Api.Sdk.Core.Http;
using Igor.Gateway.Api.Sdk.Core.Http.Security;
using Igor.Gateway.Api.Sdk.Core.Serialization;
using Igor.Gateway.Dtos.Policies;
using Igor.Sdk.Common.Models;

namespace Igor.Gateway.Api.Sdk.Apis.Policies
{
    public interface IPolicyService : IRetrievable<PolicyDto>
    {
        Task<ResultList<PolicyDto>> GetAll();
        Task Update(int policyId, UpdatePolicyDto updatePolicyDto);
    }

    public class PolicyService : ServiceBase, IPolicyService
    {
        private const string GetAllEndpoint = "/policies";
        private const string GetEndpoint = "/policies/{0}";
        private const string UpdateEndpoint = "/policies/{0}";

        public PolicyService(string apiKey) : base(apiKey)
        {
        }

        public PolicyService(string apiKey, string apiAddress) : base(apiKey, apiAddress)
        {
        }

        public PolicyService(IHttpClientService httpClient, IJsonSerializer jsonSerializer, ISdkConfig config, IJwtTokenProvider tokenProvider, IRequestHeaderProvider requestHeaderProvider)
            : base(httpClient, jsonSerializer, config, tokenProvider, requestHeaderProvider)
        {
        }

        public async Task<ResultList<PolicyDto>> GetAll()
        {
            return await GetHttpGetResponse<ResultList<PolicyDto>>(GetAllEndpoint);
        }

        public async Task<PolicyDto> Get(int policyId)
        {
            return await GetHttpGetResponse<PolicyDto>(GetEndpoint, policyId);
        }

        public async Task Update(int policyId, UpdatePolicyDto updatePolicyDto)
        {
            using var _ = await GetHttpPutResponse(UpdateEndpoint, updatePolicyDto, policyId);
        }
    }
}
