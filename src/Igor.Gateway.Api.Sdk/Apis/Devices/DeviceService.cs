﻿using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Core.Config;
using Igor.Gateway.Api.Sdk.Core.Http;
using Igor.Gateway.Api.Sdk.Core.Http.Security;
using Igor.Gateway.Api.Sdk.Core.Serialization;
using Igor.Gateway.Dtos.Devices;
using Igor.Gateway.Dtos.NetworkNodes;
using Igor.Gateway.Dtos.Spaces;
using Igor.Sdk.Common.Models;

namespace Igor.Gateway.Api.Sdk.Apis.Devices
{
    public interface IDeviceService : IHttpService
    {
        Task DeleteAll();
        Task Discover();
        Task<ResultList<DeviceDto>> GetAll();
        Task<NetworkNodeDto> GetNetworkNode(DeviceType type, int deviceId);
        Task<SpaceDto> GetSpace(DeviceType type, int deviceId);
        Task<ResultList<DeviceDto>> GetUnassigned();
        Task Update(DeviceType type, int deviceId, UpdateDevicesDto updateDeviceDto);
        Task Delete(DeviceType type, int deviceId);
        Task<PaginatedList<DevicesSearchResultDto>> Search();
        Task<PaginatedList<DevicesSearchResultDto>> Search(DeviceSearchParameters queryParameters);
        Task BringOnline(DeviceType type, int deviceId);
        Task TakeOffline(DeviceType type, int deviceId);
        Task Quarantine(DeviceType type, int deviceId);
        Task ReleaseQuarantine(DeviceType type, int deviceId);
        Task<ResultList<DeviceTypeCountDto>> GetDeviceTypeCounts();
    }

    public class DeviceService : ServiceBase, IDeviceService
    {
        private const string GetAllEndpoint = "/devices/all";
        private const string GetSpaceEndpoint = "/devices/{0}/{1}/space";
        private const string GetNetworkNodeEndpoint = "/devices/{0}/{1}/networknode";
        private const string SearchEndpoint = "/devices/search";
        private const string UnassignedEndpoint = "/devices/unassigned";
        private const string DiscoverEndpoint = "/devices/discover";
        private const string DeleteAllEndpoint = "/devices/deleteall";
        private const string UpdateEndpoint = "/devices/{0}/{1}";
        private const string DeleteEndpoint = "/devices/{0}/{1}";
        private const string BringOnlineEndpoint = "/devices/{0}/{1}/bringonline";
        private const string TakeOfflineEndpoint = "/devices/{0}/{1}/takeoffline";
        private const string QuarantineEndpoint = "/devices/{0}/{1}/quarantine";
        private const string GetDeviceTypeCountsEndpoint = "/devices/devicetypes";

        public DeviceService(string apiKey) : base(apiKey)
        {
        }

        public DeviceService(string apiKey, string apiAddress) : base(apiKey, apiAddress)
        {
        }

        public DeviceService(IHttpClientService httpClient, IJsonSerializer jsonSerializer, ISdkConfig config, IJwtTokenProvider tokenProvider, IRequestHeaderProvider requestHeaderProvider)
            : base(httpClient, jsonSerializer, config, tokenProvider, requestHeaderProvider)
        {
        }

        public async Task<ResultList<DeviceDto>> GetAll()
        {
            return await GetHttpGetResponse<ResultList<DeviceDto>>(GetAllEndpoint);
        }

        public async Task<SpaceDto> GetSpace(DeviceType type, int deviceId)
        {
            return await GetHttpGetResponse<SpaceDto>(GetSpaceEndpoint, type, deviceId);
        }

        public async Task<NetworkNodeDto> GetNetworkNode(DeviceType type, int deviceId)
        {
            return await GetHttpGetResponse<NetworkNodeDto>(GetNetworkNodeEndpoint, type, deviceId);
        }

        public async Task<PaginatedList<DevicesSearchResultDto>> Search()
        {
            return await Search(null);
        }

        public async Task<PaginatedList<DevicesSearchResultDto>> Search(DeviceSearchParameters queryParameters)
        {
            var endpoint = FormatEndpoint(SearchEndpoint) + queryParameters;
            var results = await GetAsync<PaginatedList<DevicesSearchResultDto>>(endpoint);

            return results;
        }

        public async Task<ResultList<DeviceDto>> GetUnassigned()
        {
            return await GetHttpGetResponse<ResultList<DeviceDto>>(UnassignedEndpoint);
        }

        public async Task Discover()
        {
            using var _ = await GetHttpPostResponse(DiscoverEndpoint, (object) null);
        }

        public async Task DeleteAll()
        {
            using var _ = await GetHttpDeleteResponse(DeleteAllEndpoint);
        }

        public async Task Update(DeviceType type, int deviceId, UpdateDevicesDto updateDeviceDto)
        {
            using var _ = await GetHttpPutResponse(UpdateEndpoint, updateDeviceDto, type, deviceId);
        }

        public async Task Delete(DeviceType type, int deviceId)
        {
            using var _ = await GetHttpDeleteResponse(DeleteEndpoint, type, deviceId);
        }

        public async Task BringOnline(DeviceType type, int deviceId)
        {
            using var _ = await GetHttpPostResponse(BringOnlineEndpoint, (object)null, type, deviceId);
        }

        public async Task TakeOffline(DeviceType type, int deviceId)
        {
            using var _ = await GetHttpPostResponse(TakeOfflineEndpoint, (object)null, type, deviceId);
        }

        public async Task Quarantine(DeviceType type, int deviceId)
        {
            using var _ = await GetHttpPostResponse(QuarantineEndpoint, (object)null, type, deviceId);
        }

        public async Task ReleaseQuarantine(DeviceType type, int deviceId)
        {
            using var _ = await GetHttpDeleteResponse(QuarantineEndpoint, type, deviceId);
        }

        public async Task<ResultList<DeviceTypeCountDto>> GetDeviceTypeCounts()
        {
            return await GetHttpGetResponse<ResultList<DeviceTypeCountDto>>(GetDeviceTypeCountsEndpoint);
        }
    }
}
