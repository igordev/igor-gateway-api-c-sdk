﻿using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Core.Config;
using Igor.Gateway.Api.Sdk.Core.Http;
using Igor.Gateway.Api.Sdk.Core.Http.Security;
using Igor.Gateway.Api.Sdk.Core.Serialization;
using Igor.Gateway.Dtos.Dashboards;
using Igor.Sdk.Common.Models;

namespace Igor.Gateway.Api.Sdk.Apis.Dashboards
{
    public interface IDashboardService : IHttpService
    {
        Task<ResultList<DashboardDeviceStatusByDeviceTypeDto>> GetDeviceStatusByDeviceType();
        Task<ResultList<DashboardDeviceStatusByNetworkSwitchDto>> GetDeviceStatusByNetworkSwitch();
        Task<ResultList<DashboardDeviceStatusBySpaceDto>> GetDeviceStatusBySpace();
        Task<ResultList<DashboardDeviceActivityByHourDto>> GetDeviceActivityByHour();
    }

    public class DashboardService : ServiceBase, IDashboardService
    {
        private const string GetDeviceStatusByDeviceTypeEndpoint = "/dashboards/devicestatusbydevicetype";
        private const string GetDeviceStatusByNetworkSwitchEndpoint = "/dashboards/devicestatusbynetworkswitch";
        private const string GetDeviceStatusBySpaceEndpoint = "/dashboards/devicestatusbyspace";
        private const string GetDeviceActivityByHourEndpoint = "/dashboards/deviceactivitybyhour";

        public DashboardService(string apiKey) : base(apiKey)
        {
        }

        public DashboardService(string apiKey, string apiAddress) : base(apiKey, apiAddress)
        {
        }

        public DashboardService(IHttpClientService httpClient, IJsonSerializer jsonSerializer, ISdkConfig config, IJwtTokenProvider tokenProvider, IRequestHeaderProvider requestHeaderProvider)
            : base(httpClient, jsonSerializer, config, tokenProvider, requestHeaderProvider)
        {
        }

        public async Task<ResultList<DashboardDeviceStatusByDeviceTypeDto>> GetDeviceStatusByDeviceType()
        {
            return await GetHttpGetResponse<ResultList<DashboardDeviceStatusByDeviceTypeDto>>(
                GetDeviceStatusByDeviceTypeEndpoint);
        }

        public async Task<ResultList<DashboardDeviceStatusByNetworkSwitchDto>> GetDeviceStatusByNetworkSwitch()
        {
            return await GetHttpGetResponse<ResultList<DashboardDeviceStatusByNetworkSwitchDto>>(
                GetDeviceStatusByNetworkSwitchEndpoint);
        }

        public async Task<ResultList<DashboardDeviceStatusBySpaceDto>> GetDeviceStatusBySpace()
        {
            return await GetHttpGetResponse<ResultList<DashboardDeviceStatusBySpaceDto>>(
                GetDeviceStatusBySpaceEndpoint);
        }

        public async Task<ResultList<DashboardDeviceActivityByHourDto>> GetDeviceActivityByHour()
        {
            return await GetHttpGetResponse<ResultList<DashboardDeviceActivityByHourDto>>(
                GetDeviceActivityByHourEndpoint);
        }
    }
}
