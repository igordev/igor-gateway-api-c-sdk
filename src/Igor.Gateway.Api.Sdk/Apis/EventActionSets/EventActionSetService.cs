﻿using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Core.Config;
using Igor.Gateway.Api.Sdk.Core.Http;
using Igor.Gateway.Api.Sdk.Core.Http.Security;
using Igor.Gateway.Api.Sdk.Core.Serialization;
using Igor.Gateway.Dtos.EventActionSets;
using Igor.Sdk.Common.Models;

namespace Igor.Gateway.Api.Sdk.Apis.EventActionSets
{
    public interface IEventActionSetService : IRetrievable<EventActionSetDto>
    {
        Task<ResultList<EventActionSetDto>> GetAll();
        Task<ResultList<EventActionSetOfEntityDto>> GetEventActionSets(EntityType entityType, int entityId);
        Task<int> Create(CreateEventActionSetDto createDto);
        Task Update(int eventActionSetId, UpdateEventActionSetDto updateDto);
        Task Delete(int eventActionSetId);
    }

    public class EventActionSetService : ServiceBase, IEventActionSetService
    {
        private const string GetAllEndpoint = "/eventactionsets";
        private const string GetEventActionSetsEndpoint = "/eventactionsets/{0}/{1}";
        private const string CreateEndpoint = "/eventactionsets";
        private const string DeleteEndpoint = "/eventactionsets/{0}";
        private const string GetEndpoint = "/eventactionsets/{0}";
        private const string UpdateEndpoint = "/eventactionsets/{0}";

        public EventActionSetService(string apiKey) : base(apiKey)
        {
        }

        public EventActionSetService(string apiKey, string apiAddress) : base(apiKey, apiAddress)
        {
        }

        public EventActionSetService(IHttpClientService httpClient, IJsonSerializer jsonSerializer, ISdkConfig config, IJwtTokenProvider tokenProvider, IRequestHeaderProvider requestHeaderProvider) : base(httpClient, jsonSerializer, config, tokenProvider, requestHeaderProvider)
        {
        }

        public async Task<EventActionSetDto> Get(int eventActionSetId)
        {
            return await GetHttpGetResponse<EventActionSetDto>(GetEndpoint, eventActionSetId);
        }

        public async Task<ResultList<EventActionSetDto>> GetAll()
        {
            return await GetHttpGetResponse<ResultList<EventActionSetDto>>(GetAllEndpoint);
        }

        public async Task<ResultList<EventActionSetOfEntityDto>> GetEventActionSets(EntityType entityType, int entityId)
        {
            return await GetHttpGetResponse<ResultList<EventActionSetOfEntityDto>>(GetEventActionSetsEndpoint, entityType, entityId);
        }

        public async Task<int> Create(CreateEventActionSetDto createDto)
        {
            using var response = await GetHttpPostResponse(CreateEndpoint, createDto);
            return GetId(response);
        }

        public async Task Update(int eventActionSetId, UpdateEventActionSetDto updateDto)
        {
            using var _ = await GetHttpPutResponse(UpdateEndpoint, updateDto, eventActionSetId);
        }

        public async Task Delete(int eventActionSetId)
        {
            using var _ = await GetHttpDeleteResponse(DeleteEndpoint, eventActionSetId);
        }
    }
}
