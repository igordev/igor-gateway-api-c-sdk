﻿using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Core.Config;
using Igor.Gateway.Api.Sdk.Core.Http;
using Igor.Gateway.Api.Sdk.Core.Http.Security;
using Igor.Gateway.Api.Sdk.Core.Serialization;
using Igor.Gateway.Dtos.DeviceNodes;
using Igor.Gateway.Dtos.Devices;
using Igor.Gateway.Dtos.Events;
using Igor.Gateway.Dtos.NetworkNodes;
using Igor.Gateway.Dtos.Tags;
using Igor.Sdk.Common.Models;

namespace Igor.Gateway.Api.Sdk.Apis.DeviceNodes
{
    public interface IDeviceNodeService : IRetrievable<DeviceNodeDto>
    {
        Task<int> Create(CreateDeviceNodeDto createDeviceNodeDto);
        Task Delete(int deviceNodeId);
        Task DeleteAll();
        Task<ResultList<DeviceNodeDto>> GetAll();
        Task<NetworkNodeDto> GetNetworkNode(int deviceNodeId);
        Task Update(int deviceNodeId, UpdateDeviceNodeDto updateDeviceNodeDto);
        Task BringOnline(int deviceNodeId);
        Task TakeOffline(int deviceNodeId);
        Task<ResultList<DeviceDto>> GetDevices(int deviceNodeId);
        Task<ResultList<EventDto>> GetEvents(int deviceNodeId);

        Task AddTag(int deviceNodeId, AddTagsDto tagDto);
        Task RemoveTag(int deviceNodeId, string tagName);
    }

    public class DeviceNodeService : ServiceBase, IDeviceNodeService
    {
        private const string GetAllEndpoint = "/devicenodes";
        private const string CreateEndpoint = "/devicenodes";
        private const string DeleteEndpoint = "/devicenodes/{0}";
        private const string GetEndpoint = "/devicenodes/{0}";
        private const string UpdateEndpoint = "/devicenodes/{0}";
        private const string DeleteAllEndpoint = "/devicenodes/deleteall";
        private const string GetNetworkNodeEndpoint = "/devicenodes/{0}/networknode";
        private const string BringOnlineEndpoint = "/devicenodes/{0}/bringonline";
        private const string TakeOfflineEndpoint = "/devicenodes/{0}/takeoffline";
        private const string GetDevicesEndpoint = "/devicenodes/{0}/devices";
        private const string GetEventsEndpoint = "/devicenodes/{0}/events";
        private const string AddTagEndpoint = "/devicenodes/{0}/tags";
        private const string RemoveTagEndpoint = "/devicenodes/{0}/tags/{1}";

        public DeviceNodeService(string apiKey) : base(apiKey)
        {
        }

        public DeviceNodeService(string apiKey, string apiAddress) : base(apiKey, apiAddress)
        {
        }

        public DeviceNodeService(IHttpClientService httpClient, IJsonSerializer jsonSerializer, ISdkConfig config, IJwtTokenProvider tokenProvider, IRequestHeaderProvider requestHeaderProvider)
            : base(httpClient, jsonSerializer, config, tokenProvider, requestHeaderProvider)
        {
        }

        public async Task<ResultList<DeviceNodeDto>> GetAll()
        {
            return await GetHttpGetResponse<ResultList<DeviceNodeDto>>(GetAllEndpoint);
        }

        public async Task<DeviceNodeDto> Get(int deviceNodeId)
        {
            return await GetHttpGetResponse<DeviceNodeDto>(GetEndpoint, deviceNodeId);
        }

        public async Task<int> Create(CreateDeviceNodeDto createDeviceNodeDto)
        {
            using var response = await GetHttpPostResponse(CreateEndpoint, createDeviceNodeDto);

            return GetId(response);
        }

        public async Task Delete(int deviceNodeId)
        {
            using var _ = await GetHttpDeleteResponse(DeleteEndpoint, deviceNodeId);
        }

        public async Task Update(int deviceNodeId, UpdateDeviceNodeDto updateDeviceNodeDto)
        {
            using var _ = await GetHttpPutResponse(UpdateEndpoint, updateDeviceNodeDto, deviceNodeId);
        }

        public async Task DeleteAll()
        {
            using var _ = await GetHttpDeleteResponse(DeleteAllEndpoint);
        }

        public async Task<NetworkNodeDto> GetNetworkNode(int deviceNodeId)
        {
            return await GetHttpGetResponse<NetworkNodeDto>(GetNetworkNodeEndpoint, deviceNodeId);
        }

        public async Task BringOnline(int deviceNodeId)
        {
            using var _ = await GetHttpPostResponse(BringOnlineEndpoint, (object)null, deviceNodeId);
        }

        public async Task TakeOffline(int deviceNodeId)
        {
            using var _ = await GetHttpPostResponse(TakeOfflineEndpoint, (object)null, deviceNodeId);
        }

        public async Task<ResultList<DeviceDto>> GetDevices(int deviceNodeId)
        {
            return await GetHttpGetResponse<ResultList<DeviceDto>>(GetDevicesEndpoint, deviceNodeId);
        }

        public async Task<ResultList<EventDto>> GetEvents(int deviceNodeId)
        {
            return await GetHttpGetResponse<ResultList<EventDto>>(GetEventsEndpoint, deviceNodeId);
        }

        public async Task AddTag(int deviceNodeId, AddTagsDto tagDto)
        {
            using var _ = await GetHttpPutResponse(AddTagEndpoint, tagDto, deviceNodeId);
        }

        public async Task RemoveTag(int deviceNodeId, string tagName)
        {
            using var _ = await GetHttpDeleteResponse(RemoveTagEndpoint, deviceNodeId, tagName);
        }
    }
}
