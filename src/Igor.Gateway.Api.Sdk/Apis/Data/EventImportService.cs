﻿using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Core.Config;
using Igor.Gateway.Api.Sdk.Core.Http;
using Igor.Gateway.Api.Sdk.Core.Http.Security;
using Igor.Gateway.Api.Sdk.Core.Serialization;

namespace Igor.Gateway.Api.Sdk.Apis.Data
{
    public interface IEventImportService
    {
        Task<HttpResponseMessage> Import(string eventType, Stream stream);
    }

    public class EventImportService : ServiceBase, IEventImportService
    {
        private const string EventImportEndpoint = "/data/events/import/{0}";

        public EventImportService(string apiKey) : base(apiKey)
        {
        }

        public EventImportService(string apiKey, string apiAddress) : base(apiKey, apiAddress)
        {
        }

        public EventImportService(IHttpClientService httpClient, IJsonSerializer jsonSerializer, ISdkConfig config, IJwtTokenProvider tokenProvider, IRequestHeaderProvider requestHeaderProvider) : base(httpClient, jsonSerializer, config, tokenProvider, requestHeaderProvider)
        {
        }

        public async Task<HttpResponseMessage> Import(string eventType, Stream stream)
        {
            return await GetHttpPostCsvFileResponse(EventImportEndpoint, eventType, stream);
        }
    }
}
