﻿using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Core.Config;
using Igor.Gateway.Api.Sdk.Core.Http;
using Igor.Gateway.Api.Sdk.Core.Http.Security;
using Igor.Gateway.Api.Sdk.Core.Serialization;
using Igor.Gateway.Dtos.DataReceivers;
using Igor.Sdk.Common.Models;

namespace Igor.Gateway.Api.Sdk.Apis.Data
{
    public interface IDataReceiverService : IRetrievable<DataReceiverDto>
    {
        Task<int> Create(CreateDataReceiverDto createDataReceiverDto);
        Task Update(int dataReceiverId, UpdateDataReceiverDto updateDataReceiverDto);
        Task Delete(int dataReceiverId);
        Task<IPaginatedList<DataReceiversSearchResultsDto>> Search();
        Task<IPaginatedList<DataReceiversSearchResultsDto>> Search(DataReceiversSearchParameters queryParameters);
    }

    public class DataReceiverService : ServiceBase, IDataReceiverService
    {
        private const string SearchEndpoint = "/data/receivers";
        private const string CreateEndpoint = "/data/receivers";
        private const string UpdateEndpoint = "/data/receivers/{0}";
        private const string DeleteEndpoint = "/data/receivers/{0}";
        private const string GetEndpoint = "/data/receivers/{0}";

        public DataReceiverService(string apiKey) : base(apiKey)
        {
        }

        public DataReceiverService(string apiKey, string apiAddress) : base(apiKey, apiAddress)
        {
        }

        public DataReceiverService(IHttpClientService httpClient, IJsonSerializer jsonSerializer, ISdkConfig config,
            IJwtTokenProvider tokenProvider, IRequestHeaderProvider requestHeaderProvider) : base(httpClient, jsonSerializer, config, tokenProvider, requestHeaderProvider)
        {
        }

        public async Task<DataReceiverDto> Get(int id)
        {
            return await GetHttpGetResponse<DataReceiverDto>(GetEndpoint, id);
        }

        public async Task<int> Create(CreateDataReceiverDto createDataReceiverDto)
        {
            using var response = await GetHttpPostResponse(CreateEndpoint, createDataReceiverDto);

            return GetId(response);
        }

        public async Task Update(int dataReceiverId, UpdateDataReceiverDto dataReceiverDto)
        {
            using var _ = await GetHttpPutResponse(UpdateEndpoint, dataReceiverDto, dataReceiverId);
        }

        public async Task Delete(int dataReceiverId)
        {
            using var _ = await GetHttpDeleteResponse(DeleteEndpoint, dataReceiverId);
        }

        public async Task<IPaginatedList<DataReceiversSearchResultsDto>> Search()
        {
            return await Search(null);
        }

        public async Task<IPaginatedList<DataReceiversSearchResultsDto>> Search(
            DataReceiversSearchParameters queryParameters)
        {
            var endpoint = FormatEndpoint(SearchEndpoint) + queryParameters;
            var results = await GetAsync<PaginatedList<DataReceiversSearchResultsDto>>(endpoint);

            return results;
        }
    }
}