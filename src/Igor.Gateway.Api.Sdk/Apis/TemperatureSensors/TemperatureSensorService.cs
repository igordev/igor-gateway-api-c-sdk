﻿using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Core.Config;
using Igor.Gateway.Api.Sdk.Core.Http;
using Igor.Gateway.Api.Sdk.Core.Http.Security;
using Igor.Gateway.Api.Sdk.Core.Serialization;
using Igor.Gateway.Dtos.Events;
using Igor.Gateway.Dtos.Tags;
using Igor.Gateway.Dtos.TemperatureSensors;
using Igor.Sdk.Common.Models;

namespace Igor.Gateway.Api.Sdk.Apis.TemperatureSensors
{
    public interface ITemperatureSensorService : IRetrievable<TemperatureSensorDto>
    {
        Task<int> Create(CreateTemperatureSensorDto createTemperatureSensorDto);
        Task CreateEvent(int temperatureSensorId, CreateTemperatureSensorEventDto createTemperatureSensorEventDto);
        Task Delete(int temperatureSensorId);
        Task<ResultList<TemperatureSensorDto>> GetAll();
        Task<ResultList<EventDto>> GetEvents(int temperatureSensorId);
        Task Update(int temperatureSensorId, UpdateTemperatureSensorDto updateTemperatureSensorDto);
        Task AddTag(int temperatureSensorId, AddTagsDto tagDto);
        Task RemoveTag(int temperatureSensorId, string tagName);
    }

    public class TemperatureSensorService : ServiceBase, ITemperatureSensorService
    {
        private const string GetAllEndpoint = "/temperaturesensors";
        private const string CreateEndpoint = "/temperaturesensors";
        private const string DeleteEndpoint = "/temperaturesensors/{0}";
        private const string GetEndpoint = "/temperaturesensors/{0}";
        private const string UpdateEndpoint = "/temperaturesensors/{0}";
        private const string GetEventsEndpoint = "/temperaturesensors/{0}/events";
        private const string CreateEventEndpoint = "/temperaturesensors/{0}/events";
        private const string AddTagEndpoint = "/temperaturesensors/{0}/tags";
        private const string RemoveTagEndpoint = "/temperaturesensors/{0}/tags/{1}";

        public TemperatureSensorService(string apiKey) : base(apiKey)
        {
        }

        public TemperatureSensorService(string apiKey, string apiAddress) : base(apiKey, apiAddress)
        {
        }

        public TemperatureSensorService(IHttpClientService httpClient, IJsonSerializer jsonSerializer,
            ISdkConfig config, IJwtTokenProvider tokenProvider, IRequestHeaderProvider requestHeaderProvider)
            : base(httpClient, jsonSerializer, config, tokenProvider, requestHeaderProvider)
        {
        }

        public async Task<ResultList<TemperatureSensorDto>> GetAll()
        {
            return await GetHttpGetResponse<ResultList<TemperatureSensorDto>>(GetAllEndpoint);
        }

        public async Task<TemperatureSensorDto> Get(int temperatureSensorId)
        {
            return await GetHttpGetResponse<TemperatureSensorDto>(GetEndpoint, temperatureSensorId);
        }

        public async Task<int> Create(CreateTemperatureSensorDto createTemperatureSensorDto)
        {
            using var response = await GetHttpPostResponse(CreateEndpoint, createTemperatureSensorDto);

            return GetId(response);
        }

        public async Task Delete(int temperatureSensorId)
        {
            using var _ = await GetHttpDeleteResponse(DeleteEndpoint, temperatureSensorId);
        }

        public async Task Update(int temperatureSensorId, UpdateTemperatureSensorDto updateTemperatureSensorDto)
        {
            using var _ = await GetHttpPutResponse(UpdateEndpoint, updateTemperatureSensorDto, temperatureSensorId);
        }

        public async Task AddTag(int temperatureSensorId, AddTagsDto tagDto)
        {
            using var _ = await GetHttpPutResponse(AddTagEndpoint, tagDto, temperatureSensorId);
        }

        public async Task RemoveTag(int temperatureSensorId, string tagName)
        {
            using var _ = await GetHttpDeleteResponse(RemoveTagEndpoint, temperatureSensorId, tagName);
        }

        public async Task<ResultList<EventDto>> GetEvents(int temperatureSensorId)
        {
            return await GetHttpGetResponse<ResultList<EventDto>>(GetEventsEndpoint, temperatureSensorId);
        }

        public async Task CreateEvent(int temperatureSensorId,
            CreateTemperatureSensorEventDto createTemperatureSensorEventDto)
        {
            using var _ = await GetHttpPostResponse(CreateEventEndpoint, createTemperatureSensorEventDto, temperatureSensorId);
        }
    }
}