﻿using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Core.Config;
using Igor.Gateway.Api.Sdk.Core.Http;
using Igor.Gateway.Api.Sdk.Core.Http.Security;
using Igor.Gateway.Api.Sdk.Core.Serialization;
using Igor.Gateway.Dtos.SpaceGroups;
using Igor.Gateway.Dtos.Spaces;
using Igor.Gateway.Dtos.Tags;
using Igor.Sdk.Common.Models;

namespace Igor.Gateway.Api.Sdk.Apis.SpaceGroups
{
    public interface ISpaceGroupService : IRetrievable<SpaceGroupDto>
    {
        Task<int> Create(CreateSpaceGroupDto createSpaceGroupDto);
        Task Delete(int spaceGroupId);
        Task Update(int spaceId, UpdateSpaceGroupDto updateSpaceGroupDto);
        Task<PaginatedList<SpaceGroupsSearchResultsDto>> Search();
        Task<PaginatedList<SpaceGroupsSearchResultsDto>> Search(SpaceGroupsSearchParameters queryParameters);
        Task<ResultList<SpaceDto>> GetSpaces(int spaceGroupId);
        Task AddSpace(int spaceGroupId, int spaceId);
        Task RemoveSpace(int spaceGroupId, int spaceId);
        Task AddTag(int spaceGroupId, AddTagsDto tagDto);
        Task RemoveTag(int spaceGroupId, string tagName);
    }

    public class SpaceGroupService : ServiceBase, ISpaceGroupService
    {
        private const string SearchEndpoint = "/spacegroups";
        private const string CreateEndpoint = "/spacegroups";
        private const string DeleteEndpoint = "/spacegroups/{0}";
        private const string GetEndpoint = "/spacegroups/{0}";
        private const string UpdateEndpoint = "/spacegroups/{0}";
        private const string GetSpacesEndpoint = "/spacegroups/{0}/spaces";
        private const string SpacesEndpoint = "/spacegroups/{0}/spaces/{1}";
        private const string AddTagEndpoint = "/spacegroups/{0}/tags";
        private const string RemoveTagEndpoint = "/spacegroups/{0}/tags/{1}";

        public SpaceGroupService(string apiKey) : base(apiKey)
        {
        }

        public SpaceGroupService(string apiKey, string apiAddress) : base(apiKey, apiAddress)
        {
        }

        public SpaceGroupService(IHttpClientService httpClient, IJsonSerializer jsonSerializer, ISdkConfig config,
            IJwtTokenProvider tokenProvider, IRequestHeaderProvider requestHeaderProvider) : base(httpClient, jsonSerializer, config, tokenProvider, requestHeaderProvider)
        {
        }

        public async Task<int> Create(CreateSpaceGroupDto createSpaceGroupDto)
        {
            using var response = await GetHttpPostResponse(CreateEndpoint, createSpaceGroupDto);

            return GetId(response);
        }

        public async Task<SpaceGroupDto> Get(int spaceGroupId)
        {
            return await GetHttpGetResponse<SpaceGroupDto>(GetEndpoint, spaceGroupId);
        }

        public async Task Delete(int spaceGroupId)
        {
            using var _ = await GetHttpDeleteResponse(DeleteEndpoint, spaceGroupId);
        }

        public async Task Update(int spaceGroupId, UpdateSpaceGroupDto updateSpaceGroupDto)
        {
            using var _ = await GetHttpPutResponse(UpdateEndpoint, updateSpaceGroupDto, spaceGroupId);
        }

        public async Task<PaginatedList<SpaceGroupsSearchResultsDto>> Search()
        {
            return await Search(null);
        }

        public async Task<PaginatedList<SpaceGroupsSearchResultsDto>> Search(SpaceGroupsSearchParameters queryParameters)
        {
            var endpoint = FormatEndpoint(SearchEndpoint) + queryParameters;
            var results = await GetAsync<PaginatedList<SpaceGroupsSearchResultsDto>>(endpoint);

            return results;
        }

        public async Task<ResultList<SpaceDto>> GetSpaces(int spaceGroupId)
        {
            return await GetHttpGetResponse<ResultList<SpaceDto>>(GetSpacesEndpoint, spaceGroupId);
        }

        public async Task AddSpace(int spaceGroupId, int spaceId)
        {
            using var _ = await GetHttpPutResponse(SpacesEndpoint, new {}, spaceGroupId, spaceId);
        }

        public async Task RemoveSpace(int spaceGroupId, int spaceId)
        {
            using var _ = await GetHttpDeleteResponse(SpacesEndpoint, spaceGroupId, spaceId);
        }

        public async Task AddTag(int spaceGroupId, AddTagsDto tagDto)
        {
            using var _ = await GetHttpPutResponse(AddTagEndpoint, tagDto, spaceGroupId);
        }

        public async Task RemoveTag(int spaceGroupId, string tagName)
        {
            using var _ = await GetHttpDeleteResponse(RemoveTagEndpoint, spaceGroupId, tagName);
        }
    }
}