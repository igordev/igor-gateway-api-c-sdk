﻿using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Core.Config;
using Igor.Gateway.Api.Sdk.Core.Http;
using Igor.Gateway.Api.Sdk.Core.Http.Security;
using Igor.Gateway.Api.Sdk.Core.Serialization;
using Igor.Gateway.Dtos.ActionSets;
using Igor.Gateway.Dtos.Events;
using Igor.Gateway.Dtos.Schedules;
using Igor.Sdk.Common.Models;

namespace Igor.Gateway.Api.Sdk.Apis.Schedules
{
    public interface IScheduleService : IRetrievable<ScheduleDto>
    {
        Task<int> Create(CreateScheduleDto createScheduleDto);
        Task Delete(int scheduleId);
        Task<ActionSetDto> GetActionSet(int scheduleId);
        Task<ResultList<ScheduleDto>> GetAll();
        Task<ResultList<EventDto>> GetEvents(int scheduleId);
        Task RemoveActionSet(int scheduleId);
        Task SetActionSet(int scheduleId, UpdateScheduleActionSetDto updateScheduleActionSetDto);
        Task Update(int scheduleId, UpdateScheduleDto updateScheduleDto);
        Task<PaginatedList<SchedulesSearchResultsDto>> Search();
        Task<PaginatedList<SchedulesSearchResultsDto>> Search(SchedulesSearchParameters queryParameters);
    }

    public class ScheduleService : ServiceBase, IScheduleService
    {
        private const string GetAllEndpoint = "/schedules";
        private const string CreateEndpoint = "/schedules";
        private const string DeleteEndpoint = "/schedules/{0}";
        private const string GetEndpoint = "/schedules/{0}";
        private const string UpdateEndpoint = "/schedules/{0}";
        private const string GetEventsEndpoint = "/schedules/{0}/events";
        private const string DeleteActionSetEndpoint = "/schedules/{0}/actionset";
        private const string GetActionSetEndpoint = "/schedules/{0}/actionset";
        private const string SetActionSetEndpoint = "/schedules/{0}/actionset";
        private const string SearchEndpoint = "/schedules/search";

        public ScheduleService(string apiKey) : base(apiKey)
        {
        }

        public ScheduleService(string apiKey, string apiAddress) : base(apiKey, apiAddress)
        {
        }

        public ScheduleService(IHttpClientService httpClient, IJsonSerializer jsonSerializer, ISdkConfig config,
            IJwtTokenProvider tokenProvider, IRequestHeaderProvider requestHeaderProvider)
            : base(httpClient, jsonSerializer, config, tokenProvider, requestHeaderProvider)
        {
        }

        public async Task<ResultList<ScheduleDto>> GetAll()
        {
            return await GetHttpGetResponse<ResultList<ScheduleDto>>(GetAllEndpoint);
        }

        public async Task<ScheduleDto> Get(int scheduleId)
        {
            return await GetHttpGetResponse<ScheduleDto>(GetEndpoint, scheduleId);
        }

        public async Task<int> Create(CreateScheduleDto createScheduleDto)
        {
            using var response = await GetHttpPostResponse(CreateEndpoint, createScheduleDto);

            return GetId(response);
        }

        public async Task Delete(int scheduleId)
        {
            using var _ = await GetHttpDeleteResponse(DeleteEndpoint, scheduleId);
        }

        public async Task Update(int scheduleId, UpdateScheduleDto updateScheduleDto)
        {
            using var _ = await GetHttpPutResponse(UpdateEndpoint, updateScheduleDto, scheduleId);
        }

        public async Task<ResultList<EventDto>> GetEvents(int scheduleId)
        {
            return await GetHttpGetResponse<ResultList<EventDto>>(GetEventsEndpoint, scheduleId);
        }

        public async Task RemoveActionSet(int scheduleId)
        {
            using var _ = await GetHttpDeleteResponse(DeleteActionSetEndpoint, scheduleId);
        }

        public async Task<ActionSetDto> GetActionSet(int scheduleId)
        {
            return await GetHttpGetResponse<ActionSetDto>(GetActionSetEndpoint, scheduleId);
        }

        public async Task SetActionSet(int scheduleId, UpdateScheduleActionSetDto updateScheduleActionSetDto)
        {
            using var _ = await GetHttpPutResponse(SetActionSetEndpoint, updateScheduleActionSetDto, scheduleId);
        }

        public async Task<PaginatedList<SchedulesSearchResultsDto>> Search()
        {
            return await Search(null);
        }

        public async Task<PaginatedList<SchedulesSearchResultsDto>> Search(SchedulesSearchParameters queryParameters)
        {
            var endpoint = FormatEndpoint(SearchEndpoint) + queryParameters;
            var results = await GetAsync<PaginatedList<SchedulesSearchResultsDto>>(endpoint);

            return results;
        }
    }
}