﻿using System.Net;

namespace Igor.Gateway.Api.Sdk.Apis
{
    public interface IHttpService
    {
        HttpStatusCode StatusCode { get; }
    }
}