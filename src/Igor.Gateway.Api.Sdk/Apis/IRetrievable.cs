﻿using System.Threading.Tasks;

namespace Igor.Gateway.Api.Sdk.Apis
{
    public interface IRetrievable<T> : IHttpService
    {
        Task<T> Get(int id);
    }
}