﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Core.Config;
using Igor.Gateway.Api.Sdk.Core.Http;
using Igor.Gateway.Api.Sdk.Core.Http.Security;
using Igor.Gateway.Api.Sdk.Core.Serialization;
using Igor.Gateway.Dtos.Events;
using Igor.Gateway.Dtos.Lighting;
using Igor.Gateway.Dtos.Lights;
using Igor.Gateway.Dtos.Tags;
using Igor.Sdk.Common.Models;

namespace Igor.Gateway.Api.Sdk.Apis.Lights
{
    public interface ILightService : IRetrievable<LightDto>
    {
        Task<int> Create(CreateLightDto createLightDto);
        Task Delete(int lightId);
        Task<ResultList<LightDto>> GetAll();
        Task<ResultList<EventDto>> GetEvents(int lightId);
        Task SetLighting(int lightId, LightingDto lightingDto);
        Task SetLighting(IEnumerable<BatchLightingDto> batchLightingDtos);
        Task TurnOff(int lightId);
        Task TurnOn(int lightId);
        Task Update(int lightId, UpdateLightDto updateLightDto);

        Task AddTag(int lightId, AddTagsDto tagDto);
        Task RemoveTag(int lightId, string tagName);
    }

    public class LightService : ServiceBase, ILightService
    {
        private const string GetAllEndpoint = "/lights";
        private const string CreateEndpoint = "/lights";
        private const string DeleteEndpoint = "/lights/{0}";
        private const string GetEndpoint = "/lights/{0}";
        private const string UpdateEndpoint = "/lights/{0}";
        private const string TurnOnEndpoint = "/lights/{0}/turnon";
        private const string TurnOffEndpoint = "/lights/{0}/turnoff";
        private const string LightingEndpoint = "/lights/{0}/lighting";
        private const string BatchLightingEndpoint = "/lights/lighting";
        private const string GetEventsEndpoint = "/lights/{0}/events";
        private const string AddTagEndpoint = "/lights/{0}/tags";
        private const string RemoveTagEndpoint = "/lights/{0}/tags/{1}";

        public LightService(string apiKey) : base(apiKey)
        {
        }

        public LightService(string apiKey, string apiAddress) : base(apiKey, apiAddress)
        {
        }

        public LightService(IHttpClientService httpClient, IJsonSerializer jsonSerializer, ISdkConfig config,
            IJwtTokenProvider tokenProvider, IRequestHeaderProvider requestHeaderProvider)
            : base(httpClient, jsonSerializer, config, tokenProvider, requestHeaderProvider)
        {
        }

        public async Task<ResultList<LightDto>> GetAll()
        {
            return await GetHttpGetResponse<ResultList<LightDto>>(GetAllEndpoint);
        }

        public async Task<int> Create(CreateLightDto createLightDto)
        {
            using var response = await GetHttpPostResponse(CreateEndpoint, createLightDto);

            return GetId(response);
        }

        public async Task Delete(int lightId)
        {
            using var _ = await GetHttpDeleteResponse(DeleteEndpoint, lightId);
        }

        public async Task<LightDto> Get(int lightId)
        {
            return await GetHttpGetResponse<LightDto>(GetEndpoint, lightId);
        }

        public async Task Update(int lightId, UpdateLightDto updateLightDto)
        {
            using var _ = await GetHttpPutResponse(UpdateEndpoint, updateLightDto, lightId);
        }

        public async Task AddTag(int lightId, AddTagsDto tagDto)
        {
            using var _ = await GetHttpPutResponse(AddTagEndpoint, tagDto, lightId);
        }

        public async Task RemoveTag(int lightId, string tagName)
        {
            using var _ = await GetHttpDeleteResponse(RemoveTagEndpoint, lightId, tagName);
        }

        public async Task TurnOn(int lightId)
        {
            using var _ = await GetHttpPostResponse(TurnOnEndpoint, (object) null, lightId);
        }

        public async Task TurnOff(int lightId)
        {
            using var _ = await GetHttpPostResponse(TurnOffEndpoint, (object) null, lightId);
        }

        public async Task SetLighting(int lightId, LightingDto lightingDto)
        {
            using var _ = await GetHttpPostResponse(LightingEndpoint, lightingDto, lightId);
        }

        public async Task SetLighting(IEnumerable<BatchLightingDto> batchLightingDtos)
        {
            var listDto = new BatchLightingListDto(batchLightingDtos);

            using var _ = await GetHttpPostResponse(BatchLightingEndpoint, listDto);
        }

        public async Task<ResultList<EventDto>> GetEvents(int lightId)
        {
            return await GetHttpGetResponse<ResultList<EventDto>>(GetEventsEndpoint, lightId);
        }
    }
}