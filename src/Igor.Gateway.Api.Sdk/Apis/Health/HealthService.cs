﻿using System.Net.Http;
using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Core.Config;
using Igor.Gateway.Api.Sdk.Core.Http;
using Igor.Gateway.Api.Sdk.Core.Http.Security;
using Igor.Gateway.Api.Sdk.Core.Serialization;

namespace Igor.Gateway.Api.Sdk.Apis.Health
{
    public interface IHealthService : IHttpService
    {
        Task Ping();
    }

    public class HealthService : ServiceBase, IHealthService
    {
        private const string PingEndpoint = "/health/ping";

        public HealthService(string apiKey) : base(apiKey)
        {
        }

        public HealthService(string apiKey, string apiAddress) : base(apiKey, apiAddress)
        {
        }

        public HealthService(IHttpClientService httpClient, IJsonSerializer jsonSerializer, ISdkConfig config, IJwtTokenProvider tokenProvider, IRequestHeaderProvider requestHeaderProvider)
            : base(httpClient, jsonSerializer, config, tokenProvider, requestHeaderProvider)
        {
        }

        public async Task Ping()
        {
            using var _ = await GetHttpGetResponse<HttpResponseMessage>(PingEndpoint, (object)null);
        }
    }
}
