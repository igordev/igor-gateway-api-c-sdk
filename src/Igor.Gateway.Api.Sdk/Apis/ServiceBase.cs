﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Igor.Exceptions;
using Igor.Gateway.Api.Sdk.Core.Config;
using Igor.Gateway.Api.Sdk.Core.Exceptions;
using Igor.Gateway.Api.Sdk.Core.Http;
using Igor.Gateway.Api.Sdk.Core.Http.Security;
using Igor.Gateway.Api.Sdk.Core.Metrics;
using Igor.Gateway.Api.Sdk.Core.Serialization;
using Igor.Gateway.Dtos;
using Igor.Gateway.Dtos.Validation;
using HttpVersion = Igor.Gateway.Api.Sdk.Core.Http.HttpVersion;
using JsonSerializer = Igor.Gateway.Api.Sdk.Core.Serialization.JsonSerializer;

namespace Igor.Gateway.Api.Sdk.Apis
{
    public abstract class ServiceBase : IHttpService
    {
        private readonly IJwtTokenProvider _tokenProvider;
        private readonly IRequestHeaderProvider _requestHeaderProvider;
        protected const string ApiRoot = "/api";
        
        protected ServiceBase(string apiKey)
            : this(apiKey, null)
        {
        }

        protected ServiceBase(string apiKey, string apiAddress)
            : this(new HttpClientService(), new JsonSerializer(), CreateConfig(apiKey, apiAddress),
                new JwtTokenProvider(), null)
        {
        }

        protected ServiceBase(
            IHttpClientService httpClient,
            IJsonSerializer jsonSerializer,
            ISdkConfig config,
            IJwtTokenProvider tokenProvider,
            IRequestHeaderProvider requestHeaderProvider)
        {
            _tokenProvider = tokenProvider;
            _requestHeaderProvider = requestHeaderProvider ?? new RequestHeaderProvider(tokenProvider, config);

            Config = config;
            HttpClient = httpClient;
            Serializer = jsonSerializer;
        }

        protected async Task<T> GetAsync<T>(string endpoint) where T : new()
        {
            var stopwatch = ServiceTimingEventSource.Log.HttpGetStarted();


            var preSendStopwatch = ServiceTimingEventSource.Log.PreSendStarted();
            
            using var request = BuildRequest(HttpMethod.Get, endpoint, Config.HttpVersion);

            ServiceTimingEventSource.Log.PreSendStopped(preSendStopwatch);


            var sendStopwatch = ServiceTimingEventSource.Log.SendStarted();

            using var response = await HttpClient.SendAsync(_requestHeaderProvider.GetHeaders(), request).ConfigureAwait(false);
            
            ServiceTimingEventSource.Log.SendStopped(sendStopwatch);


            var postSendStopwatch = ServiceTimingEventSource.Log.PostSendStarted();

            var responseMessage = await GetResponseContent(response).ConfigureAwait(false);

            ValidateResponse(responseMessage, response);

            StatusCode = responseMessage.StatusCode;

            var result = !string.IsNullOrEmpty(responseMessage.Content)
                ? DeserializeResponse<T>(responseMessage.Content)
                : default(T);

            ServiceTimingEventSource.Log.PostSendStopped(postSendStopwatch);


            ServiceTimingEventSource.Log.HttpGetStopped(stopwatch);

            return result;
        }

        protected async Task<HttpResponseMessage> PostAsync(string endpoint, string body = null)
        {
            var stopwatch = ServiceTimingEventSource.Log.HttpPostStarted();

            
            var preSendStopwatch = ServiceTimingEventSource.Log.PreSendStarted();
            
            using var request = BuildRequest(HttpMethod.Post, endpoint, Config.HttpVersion);

            request.Content = GetContent(body);

            ServiceTimingEventSource.Log.PreSendStopped(preSendStopwatch);


            var sendStopwatch = ServiceTimingEventSource.Log.SendStarted();

            var response = await HttpClient.SendAsync(_requestHeaderProvider.GetHeaders(), request).ConfigureAwait(false);

            ServiceTimingEventSource.Log.SendStopped(sendStopwatch);


            var postSendStopwatch = ServiceTimingEventSource.Log.PostSendStarted();

            await ValidateResponse(response).ConfigureAwait(false);

            StatusCode = response.StatusCode;

            ServiceTimingEventSource.Log.PostSendStopped(postSendStopwatch);

            
            ServiceTimingEventSource.Log.HttpPostStopped(stopwatch);

            return response;
        }

        protected async Task<HttpResponseMessage> PostCsvFileAsync(string endpoint, Stream stream)
        {
            var stopwatch = ServiceTimingEventSource.Log.HttpPostStarted();

            
            var preSendStopwatch = ServiceTimingEventSource.Log.PreSendStarted();
            
            using var httpContent = new ByteArrayContent(GetStreamBytes(stream))
            {
                Headers = { ContentType = MediaTypeHeaderValue.Parse("text/csv") }
            };

            using var multipartContent = new MultipartFormDataContent
            {
                {httpContent, "\"eventFile\"", "\"csvFile.csv\""}
            };

            ServiceTimingEventSource.Log.PreSendStopped(preSendStopwatch);


            var sendStopwatch = ServiceTimingEventSource.Log.SendStarted();
            
            using var request = BuildHttpMessage(multipartContent, endpoint);

            var response = await HttpClient.SendAsync(_requestHeaderProvider.GetHeaders(), request);

            ServiceTimingEventSource.Log.SendStopped(sendStopwatch);


            var postSendStopwatch = ServiceTimingEventSource.Log.PostSendStarted();
            
            StatusCode = response.StatusCode;

            ServiceTimingEventSource.Log.PostSendStopped(postSendStopwatch);

            
            ServiceTimingEventSource.Log.HttpPostStopped(stopwatch);


            return response;
        }

        private static HttpRequestMessage BuildHttpMessage(MultipartFormDataContent content, string endpoint)
        {
            return new HttpRequestMessage(HttpMethod.Post, endpoint)
            {
                Content = content,
                Headers = { Accept = { new MediaTypeWithQualityHeaderValue("application/json") } }
            };
        }

        private static byte[] GetStreamBytes(Stream stream)
        {
            using var memoryStream = new MemoryStream();
            stream.CopyTo(memoryStream);
            return memoryStream.ToArray();
        }

        protected async Task<HttpResponseMessage> PutAsync(string endpoint, string body = null)
        {
            var stopwatch = ServiceTimingEventSource.Log.HttpPutStarted();
            

            var preSendStopwatch = ServiceTimingEventSource.Log.PreSendStarted();

            var request = BuildRequest(HttpMethod.Put, endpoint, Config.HttpVersion);

            request.Content = GetContent(body);

            ServiceTimingEventSource.Log.PreSendStopped(preSendStopwatch);


            var sendStopwatch = ServiceTimingEventSource.Log.SendStarted();

            var response = await HttpClient.SendAsync(_requestHeaderProvider.GetHeaders(), request).ConfigureAwait(false);

            ServiceTimingEventSource.Log.SendStopped(sendStopwatch);


            var postSendStopwatch = ServiceTimingEventSource.Log.PostSendStarted();

            await ValidateResponse(response).ConfigureAwait(false);

            StatusCode = response.StatusCode;

            ServiceTimingEventSource.Log.PostSendStopped(postSendStopwatch);


            ServiceTimingEventSource.Log.HttpPutStopped(stopwatch);

            return response;
        }

        protected async Task<HttpResponseMessage> DeleteAsync(string endpoint)
        {
            var stopwatch = ServiceTimingEventSource.Log.HttpDeleteStarted();
            

            var preSendStopwatch = ServiceTimingEventSource.Log.PreSendStarted();

            var request = BuildRequest(HttpMethod.Delete, endpoint, Config.HttpVersion);

            ServiceTimingEventSource.Log.PreSendStopped(preSendStopwatch);


            var sendStopwatch = ServiceTimingEventSource.Log.SendStarted();

            var response = await HttpClient.SendAsync(_requestHeaderProvider.GetHeaders(), request).ConfigureAwait(false);

            ServiceTimingEventSource.Log.SendStopped(sendStopwatch);


            var postSendStopwatch = ServiceTimingEventSource.Log.PostSendStarted();

            await ValidateResponse(response).ConfigureAwait(false);

            StatusCode = response.StatusCode;

            ServiceTimingEventSource.Log.PostSendStopped(postSendStopwatch);


            ServiceTimingEventSource.Log.HttpDeleteStopped(stopwatch);

            return response;
        }

        private async Task ValidateResponse(HttpResponseMessage response)
        {
            var responseMessage = await GetResponseContent(response).ConfigureAwait(false);

            ValidateResponse(responseMessage, response);
        }

        private void ValidateResponse(ResponseMessage responseMessage, HttpResponseMessage response)
        {
            if (response.IsSuccessStatusCode) return;

            if (responseMessage.StatusCode == HttpStatusCode.BadRequest)
            {
                var content = "";

                if (responseMessage.Content != null)
                    content = responseMessage.Content;

                var validationMessage = DeserializeResponse<ValidationDto>(content);

                ThrowException(response, validationMessage?.Message, validationMessage);
            }

            var errorMessage = DeserializeResponse<MessageDto>(responseMessage.Content);

            ThrowException(response, errorMessage?.Message ?? "No error message provided.");
        }

        private static void ThrowException(HttpResponseMessage response, string message,
            ValidationDto validationMessage = null)
        {
            switch (response.StatusCode)
            {
                case HttpStatusCode.BadRequest:
                    throw new IgorValidationException(response, validationMessage);
                case HttpStatusCode.Unauthorized:
                    throw new IgorUnauthorizedException(response, message);
                case HttpStatusCode.NotFound:
                    throw new IgorNotFoundException(response, message);
                default:
                    throw new IgorHttpException(response, message);
            }
        }

        private T DeserializeResponse<T>(string content)
        {
            if (string.IsNullOrEmpty(content))
                return default(T);

            return Serializer.Deserialize<T>(content);
        }

        protected virtual int GetId(HttpResponseMessage response)
        {
            if (response.Headers.Location == null)
                throw new IgorLocationNotFoundException(response);

            var path = response.Headers.Location.AbsolutePath;
            var id = path.Substring(path.LastIndexOf('/') + 1);

            return int.Parse(id);
        }

        protected virtual int GetId(HttpResponseMessage response, int positionFromEnd)
        {
            if (response.Headers.Location == null)
                throw new IgorLocationNotFoundException(response);

            var path = response.Headers.Location.AbsolutePath.Split('/');

            if (positionFromEnd >= path.Length)
                throw new IgorLocationNotFoundException(response);

            var id = path[path.Length - 1 - positionFromEnd];

            return int.Parse(id);
        }
        
        public Task<T> GetHttpGetResponse<T>(string endpoint, params object[] @params) where T : new()
        {
            return ExecuteHttpMethod(endpoint, GetAsync<T>, @params);
        }

        public Task<HttpResponseMessage> GetHttpDeleteResponse(string endpoint, params object[] @params)
        {
            return ExecuteHttpMethod(endpoint, DeleteAsync, @params);
        }

        public Task<HttpResponseMessage> GetHttpPutResponse<T>(string endpoint, T dto, params object[] @params)
            where T : class
        {
            return ExecuteHttpMethod(endpoint, dto, PutAsync, @params);
        }

        public Task<HttpResponseMessage> GetHttpPutResponse(string endpoint, int id1, int id2)
        {
            return ExecuteHttpMethod(endpoint, id1, PutAsync, id2);
        }

        public Task<HttpResponseMessage> GetHttpPostResponse<T>(string endpoint, T dto,
            params object[] @params) where T : class
        {
            return ExecuteHttpMethod(endpoint, dto, PostAsync, @params);
        }

        public Task<HttpResponseMessage> GetHttpPostCsvFileResponse(string endpoint, string param, Stream stream)
        {
            var formattedEndpoint = FormatEndpoint(endpoint, param);

            return PostCsvFileAsync(formattedEndpoint, stream);
        }

        private async Task<T> ExecuteHttpMethod<T, TU>(string endpoint, TU dto,
            Func<string, string, Task<T>> httpMethod, params object[] @params) where T : HttpResponseMessage
        {
            var formattedEndpoint = FormatEndpoint(endpoint, @params);

            string body = null;

            if (dto != null)
                body = Serializer.Serialize(dto);

            var response = await httpMethod(formattedEndpoint, body).ConfigureAwait(false);

            return response;
        }

        private async Task<T> ExecuteHttpMethod<T>(string endpoint, Func<string, Task<T>> httpMethod,
            params object[] @params)
        {
            var formattedEndpoint = FormatEndpoint(endpoint, @params);

            var response = await httpMethod(formattedEndpoint).ConfigureAwait(false);

            return response;
        }

        protected virtual string FormatEndpoint(string s, params object[] @params)
        {
            return string.Format($"{ApiBaseEndpoint}{s}", @params);
        }

        private static HttpContent GetContent(string body)
        {
            if (string.IsNullOrEmpty(body)) return null;

            var content = new StringContent(body);

            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            return content;
        }

        private async Task<ResponseMessage> GetResponseContent(HttpResponseMessage response)
        {
            var responseMessage = response.Content == null
                ? null
                : await response.Content.ReadAsStringAsync().ConfigureAwait(false);

            return new ResponseMessage(response.StatusCode, responseMessage);
        }

        private static ISdkConfig CreateConfig(string apiKey, string apiAddress = null)
        {
            var config = new SdkConfig {ApiKey = apiKey};

            if (!string.IsNullOrEmpty(apiAddress))
                config.ApiAddress = apiAddress;

            return config;
        }

        private static HttpRequestMessage BuildRequest(HttpMethod method, string url, HttpVersion httpVersion)
        {
            if (httpVersion == HttpVersion.Http2_0)
                return new HttpRequestMessage(method, url)
                {
                    Version = new Version(2, 0)
                };

            return new HttpRequestMessage(method, url);
        }

        public string ApiBaseEndpoint => $"{Config.ApiAddress}{ApiRoot}";

        protected IHttpClientService HttpClient { get; }

        protected IJsonSerializer Serializer { get; }

        public ISdkConfig Config { get; }

        public HttpStatusCode StatusCode { get; private set; }

        private class ResponseMessage
        {
            public ResponseMessage(HttpStatusCode statusCode, string content)
            {
                StatusCode = statusCode;
                Content = content;
            }

            public HttpStatusCode StatusCode { get; }

            public string Content { get; }
        }
    }
}