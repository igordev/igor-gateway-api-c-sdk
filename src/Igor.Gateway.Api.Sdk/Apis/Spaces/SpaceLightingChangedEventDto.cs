﻿using Igor.Gateway.Dtos.Spaces;

namespace Igor.Gateway.Api.Sdk.Apis.Spaces
{
    public class SpaceLightingChangedEventDto : SpaceDto
    {
        public int Behavior { get; set; }

        public int CurveType { get; set; }

        public int Duration { get; set; }
    }
}
