﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Igor.Exceptions;
using Igor.Gateway.Api.Sdk.Core.Config;
using Igor.Gateway.Api.Sdk.Core.Http;
using Igor.Gateway.Api.Sdk.Core.Http.Security;
using Igor.Gateway.Api.Sdk.Core.Serialization;
using Igor.Gateway.Dtos.Commisioning;
using Igor.Gateway.Dtos.Devices;
using Igor.Gateway.Dtos.Events;
using Igor.Gateway.Dtos.Lighting;
using Igor.Gateway.Dtos.Lights;
using Igor.Gateway.Dtos.LightSensors;
using Igor.Gateway.Dtos.MotionSensors;
using Igor.Gateway.Dtos.Policies;
using Igor.Gateway.Dtos.SpaceGroups;
using Igor.Gateway.Dtos.Spaces;
using Igor.Gateway.Dtos.SpaceTypes;
using Igor.Gateway.Dtos.Tags;
using Igor.Gateway.Dtos.TemperatureSensors;
using Igor.Gateway.Dtos.Zones;
using Igor.Sdk.Common.Models;

namespace Igor.Gateway.Api.Sdk.Apis.Spaces
{
    public interface ISpaceService : IRetrievable<SpaceDto>
    {
        Task BeginZoneConfiguration(int spaceId);
        Task<int> Create(CreateSpaceDto createSpaceDto);
        Task<int> CreateZone(int spaceId, CreateZoneDto createZoneDto);
        Task Delete(int spaceId);
        Task<ResultList<ActivePolicyValueDto>> GetActivePolicies(int spaceId);
        Task<ResultList<SpaceDto>> GetAll();
        Task<ResultList<ZoneDto>> GetZones(int spaceId);
        Task SaveZoneConfiguration(int spaceId);
        Task SetLighting(int spaceId, LightingDto lightingDto);
        Task SetLighting(IEnumerable<BatchLightingDto> lightingDto);
        Task Synchronize();
        Task TurnOff(int spaceId);
        Task TurnOn(int spaceId);
        Task Update(int spaceId, UpdateSpaceDto updateSpaceDto);
        Task EndZoneConfiguration(int spaceId);
        Task<ResultList<DeviceDto>> GetDevices(int spaceId);
        Task AddDevice(int spaceId, AddDeviceToSpaceDto addDeviceToSpaceDto);
        Task RemoveDevice(int spaceId, int deviceId, DeviceType type);
        Task DeletePolicy(int spaceId);
        Task<PolicyDto> GetPolicy(int spaceId);
        Task<int> CreatePolicy(int spaceId, CreatePolicyDto createPolicyDto);
        Task UpdatePolicy(int spaceId, UpdatePolicyDto updatePolicyDto);
        Task<SpaceTypeDto> GetSpaceType(int spaceId);
        Task SetSpaceType(int spaceId, SpaceTypeDto spaceTypeDto);
        Task<ResultList<TemperatureSensorDto>> GetTemperatureSensors(int spaceId);
        Task<ResultList<LightDto>> GetLights(int spaceId);
        Task<ResultList<LightSensorDto>> GetLightSensors(int spaceId);
        Task<ResultList<MotionSensorDto>> GetMotionSensors(int spaceId);
        Task<SpaceTimerDto> GetVacancyTimer(int spaceId);
        Task<ResultList<EventDto>> GetRecentEvents(int spaceId);
        Task<ResultList<EventDto>> GetAllEvents(int spaceId);
        Task CreateSpaceEvent(int spaceId, CreateSpaceEventDto dto);
        Task<ResultList<SpaceGroupDto>> GetSpaceGroups(int spaceId);
        Task<PaginatedList<SpacesSearchResultsDto>> Search();
        Task<PaginatedList<SpacesSearchResultsDto>> Search(SpacesSearchParameters queryParameters);
        Task ResumeAutomations(int spaceId);
        Task<PausedSpaceAutomationDto> GetPausedAutomations(int spaceId);
        Task<int> PauseAutomations(int spaceId, PausedSpaceAutomationDto pausedSpaceAutomationDto);
        Task AddTag(int spaceId, AddTagsDto tagDto);
        Task RemoveTag(int spaceId, string tagName);
        Task CreateEvent(int spaceId, CreateSpaceEventActionSetDto createSpaceEventActionSetDto);
        Task<ResultList<SpaceMacAddressDto>> GetSpaceMacAddresses(int spaceId);
        Task UpdateSpaceMacAddress(int spaceId, SpaceMacAddressDto spaceMacAddressDto);
        Task ImportSpaceMacAddresses(int spaceId, ImportSpaceMacAddressDto importSpaceMacAddressDto);
        Task CreateSpaceMacAddresses(int spaceId, string macAddress);
        Task DeleteSpaceMacAddresses(int spaceId, string macAddress);
    }

    public class SpaceService : ServiceBase, ISpaceService
    {
        private const string GetAllEndpoint = "/spaces";
        private const string CreateEndpoint = "/spaces";
        private const string DeleteEndpoint = "/spaces/{0}";
        private const string GetEndpoint = "/spaces/{0}";
        private const string UpdateEndpoint = "/spaces/{0}";
        private const string TurnOnEndpoint = "/spaces/{0}/turnon";
        private const string TurnOffEndpoint = "/spaces/{0}/turnoff";
        private const string LightingEndpoint = "/spaces/{0}/lighting";
        private const string BatchLightingEndpoint = "/spaces/lighting";
        private const string SynchronizeEndpoint = "/spaces/synchronize";
        private const string ActivePolicyEndpoint = "/spaces/{0}/activepolicy";
        private const string GetZonesEndpoint = "/spaces/{0}/zones";
        private const string CreateZoneEndpoint = "/spaces/{0}/zones";
        private const string BeginZoneConfigEndpoint = "/spaces/{0}/zones/config/enter";
        private const string SaveZoneConfigEndpoint = "/spaces/{0}/zones/config/save";
        private const string EndZoneConfigEndpoint = "/spaces/{0}/zones/config/exit";
        private const string GetDevicesEndpoint = "/spaces/{0}/devices";
        private const string AddDeviceEndpoint = "/spaces/{0}/devices";
        private const string DeleteActuatorEndpoint = "/spaces/{0}/actuators/{1}";
        private const string DeleteDryContactEndpoint = "/spaces/{0}/drycontacts/{1}";
        private const string DeleteLightEndpoint = "/spaces/{0}/lights/{1}";
        private const string DeleteMotionSensorEndpoint = "/spaces/{0}/motionsensors/{1}";
        private const string DeleteLightSensorEndpoint = "/spaces/{0}/lightsensors/{1}";
        private const string DeleteTemperatureSensorEndpoint = "/spaces/{0}/temperaturesensors/{1}";
        private const string DeleteRelayEndpoint = "/spaces/{0}/relays/{1}";
        private const string DeleteSensorEndpoint = "/spaces/{0}/sensors/{1}";
        private const string DeleteWallControlEndpoint = "/spaces/{0}/wallcontrols/{1}";
        private const string GetLightsEndpoint = "/spaces/{0}/lights";
        private const string GetMotionSensorsEndpoint = "/spaces/{0}/motionsensors";
        private const string GetLightSensorsEndpoint = "/spaces/{0}/lightsensors";
        private const string GetTemperatureSensorsEndpoint = "/spaces/{0}/temperaturesensors";
        private const string DeletePolicyEndpoint = "/spaces/{0}/policy";
        private const string GetPolicyEndpoint = "/spaces/{0}/policy";
        private const string CreatePolicyEndpoint = "/spaces/{0}/policy";
        private const string UpdatePolicyEndpoint = "/spaces/{0}/policy";
        private const string GetSpaceTypeEndpoint = "/spaces/{0}/spacetype";
        private const string UpdateSpaceTypeEndpoint = "/spaces/{0}/spacetype";
        private const string TimerEndpoint = "/spaces/{0}/timer";
        private const string GetRecentEventsEndpoint = "/spaces/{0}/events";
        private const string GetAllEventsEndpoint = "/spaces/{0}/events/all";
        private const string CreateSpaceEventEndpoint = "/spaces/{0}/events";
        private const string GetSpaceGroupsEndpoint = "/spaces/{0}/spacegroups";
        private const string SearchEndpoint = "/spaces/search";
        private const string ResumeAutomationsEndpoint = "/spaces/{0}/pausedautomations";
        private const string GetPausedAutomationsEndpoint = "/spaces/{0}/pausedautomations";
        private const string PauseAutomationsEndpoint = "/spaces/{0}/pausedautomations";
        private const string AddTagEndpoint = "/spaces/{0}/tags";
        private const string RemoveTagEndpoint = "/spaces/{0}/tags/{1}";
        private const string CreateEventEndpoint = "/spaces/{0}/events";
        private const string GetMacAddressesEndpoint = "/spaces/{0}/macaddresses";
        private const string UpdateMacAddressesEndpoint = "/spaces/{0}/macaddresses";
        private const string ImportMacAddressesEndpoint = "/spaces/{0}/macaddresses/import";
        private const string CreateMacAddressEndpoint = "/spaces/{0}/macaddresses";
        private const string DeleteMacAddressEndpoint = "/spaces/{0}/macaddresses/{1}";

        public SpaceService(string apiKey) : base(apiKey)
        {
        }

        public SpaceService(string apiKey, string apiAddress) : base(apiKey, apiAddress)
        {
        }

        public SpaceService(IHttpClientService httpClient, IJsonSerializer jsonSerializer, ISdkConfig config,
            IJwtTokenProvider tokenProvider, IRequestHeaderProvider requestHeaderProvider)
            : base(httpClient, jsonSerializer, config, tokenProvider, requestHeaderProvider)
        {
        }

        public async Task BeginZoneConfiguration(int spaceId)
        {
            using var _ = await GetHttpPostResponse(BeginZoneConfigEndpoint, (object) null, spaceId);
        }

        public async Task<int> Create(CreateSpaceDto createSpaceDto)
        {
            using var response = await GetHttpPostResponse(CreateEndpoint, createSpaceDto);

            return GetId(response);
        }

        public async Task<int> CreateZone(int spaceId, CreateZoneDto createZoneDto)
        {
            using var response = await GetHttpPostResponse(CreateZoneEndpoint, createZoneDto, spaceId);

            return GetId(response);
        }

        public async Task Delete(int spaceId)
        {
            using var _ = await GetHttpDeleteResponse(DeleteEndpoint, spaceId);
        }

        public async Task<SpaceDto> Get(int spaceId)
        {
            return await GetHttpGetResponse<SpaceDto>(GetEndpoint, spaceId);
        }

        public async Task<ResultList<ActivePolicyValueDto>> GetActivePolicies(int spaceId)
        {
            return await GetHttpGetResponse<ResultList<ActivePolicyValueDto>>(ActivePolicyEndpoint, spaceId);
        }

        public async Task<ResultList<SpaceDto>> GetAll()
        {
            return await GetHttpGetResponse<ResultList<SpaceDto>>(GetAllEndpoint);
        }

        public async Task<ResultList<ZoneDto>> GetZones(int spaceId)
        {
            return await GetHttpGetResponse<ResultList<ZoneDto>>(GetZonesEndpoint, spaceId);
        }

        public async Task SaveZoneConfiguration(int spaceId)
        {
            using var _ = await GetHttpPostResponse(SaveZoneConfigEndpoint, (object) null, spaceId);
        }

        public async Task SetLighting(int spaceId, LightingDto lightingDto)
        {
            using var _ = await GetHttpPostResponse(LightingEndpoint, lightingDto, spaceId);
        }

        public async Task SetLighting(IEnumerable<BatchLightingDto> batchLightingDtos)
        {
            var listDto = new BatchLightingListDto(batchLightingDtos);

            using var _ = await GetHttpPostResponse(BatchLightingEndpoint, listDto);
        }

        public async Task Synchronize()
        {
            using var _ = await GetHttpPostResponse(SynchronizeEndpoint, (object) null);
        }

        public async Task TurnOff(int spaceId)
        {
            using var _ = await GetHttpPostResponse(TurnOffEndpoint, (object) null, spaceId);
        }

        public async Task TurnOn(int spaceId)
        {
            using var _ = await GetHttpPostResponse(TurnOnEndpoint, (object) null, spaceId);
        }

        public async Task Update(int spaceId, UpdateSpaceDto updateSpaceDto)
        {
            using var _ = await GetHttpPutResponse(UpdateEndpoint, updateSpaceDto, spaceId);
        }

        public async Task EndZoneConfiguration(int spaceId)
        {
            using var _ = await GetHttpPostResponse(EndZoneConfigEndpoint, (object) null, spaceId);
        }

        public async Task<ResultList<DeviceDto>> GetDevices(int spaceId)
        {
            return await GetHttpGetResponse<ResultList<DeviceDto>>(GetDevicesEndpoint, spaceId);
        }

        public async Task AddDevice(int spaceId, AddDeviceToSpaceDto addDeviceToSpaceDto)
        {
            using var _ = await GetHttpPutResponse(AddDeviceEndpoint, addDeviceToSpaceDto, spaceId);
        }

        public async Task RemoveDevice(int spaceId, int deviceId, DeviceType type)
        {
            var deleteEndpoint = GetDeviceDeleteEndpoint(type);

            using var _ = await GetHttpDeleteResponse(deleteEndpoint, spaceId, deviceId);
        }

        public async Task DeletePolicy(int spaceId)
        {
            using var _ = await GetHttpDeleteResponse(DeletePolicyEndpoint, spaceId);
        }

        public async Task<PolicyDto> GetPolicy(int spaceId)
        {
            return await GetHttpGetResponse<PolicyDto>(GetPolicyEndpoint, spaceId);
        }

        public async Task<int> CreatePolicy(int spaceId, CreatePolicyDto createPolicyDto)
        {
            using var response = await GetHttpPostResponse(CreatePolicyEndpoint, createPolicyDto, spaceId);

            return GetId(response, 1);
        }

        public async Task UpdatePolicy(int spaceId, UpdatePolicyDto updatePolicyDto)
        {
            using var _ = await GetHttpPutResponse(UpdatePolicyEndpoint, updatePolicyDto, spaceId);
        }

        public async Task<SpaceTypeDto> GetSpaceType(int spaceId)
        {
            return await GetHttpGetResponse<SpaceTypeDto>(GetSpaceTypeEndpoint, spaceId);
        }

        public async Task SetSpaceType(int spaceId, SpaceTypeDto spaceTypeDto)
        {
            using var _ = await GetHttpPutResponse(UpdateSpaceTypeEndpoint, spaceTypeDto, spaceId);
        }

        public async Task<ResultList<TemperatureSensorDto>> GetTemperatureSensors(int spaceId)
        {
            return await GetHttpGetResponse<ResultList<TemperatureSensorDto>>(GetTemperatureSensorsEndpoint, spaceId);
        }

        public async Task<ResultList<LightDto>> GetLights(int spaceId)
        {
            return await GetHttpGetResponse<ResultList<LightDto>>(GetLightsEndpoint, spaceId);
        }

        public async Task<ResultList<LightSensorDto>> GetLightSensors(int spaceId)
        {
            return await GetHttpGetResponse<ResultList<LightSensorDto>>(GetLightSensorsEndpoint, spaceId);
        }

        public async Task<ResultList<MotionSensorDto>> GetMotionSensors(int spaceId)
        {
            return await GetHttpGetResponse<ResultList<MotionSensorDto>>(GetMotionSensorsEndpoint, spaceId);
        }

        public async Task<SpaceTimerDto> GetVacancyTimer(int spaceId)
        {
            return await GetHttpGetResponse<SpaceTimerDto>(TimerEndpoint, spaceId);
        }

        public async Task<ResultList<EventDto>> GetRecentEvents(int spaceId)
        {
            return await GetHttpGetResponse<ResultList<EventDto>>(GetRecentEventsEndpoint, spaceId);
        }

        public async Task<ResultList<EventDto>> GetAllEvents(int spaceId)
        {
            return await GetHttpGetResponse<ResultList<EventDto>>(GetAllEventsEndpoint, spaceId);
        }

        public async Task CreateSpaceEvent(int spaceId, CreateSpaceEventDto dto)
        {
            using var _ = await GetHttpPostResponse(CreateSpaceEventEndpoint, dto, spaceId);
        }

        public async Task<ResultList<SpaceGroupDto>> GetSpaceGroups(int spaceId)
        {
            return await GetHttpGetResponse<ResultList<SpaceGroupDto>>(GetSpaceGroupsEndpoint, spaceId);
        }

        public async Task<PaginatedList<SpacesSearchResultsDto>> Search()
        {
            return await Search(null);
        }

        public async Task<PaginatedList<SpacesSearchResultsDto>> Search(SpacesSearchParameters queryParameters)
        {
            var endpoint = FormatEndpoint(SearchEndpoint) + queryParameters;
            var results = await GetAsync<PaginatedList<SpacesSearchResultsDto>>(endpoint);

            return results;
        }

        public async Task ResumeAutomations(int spaceId)
        {
            using var _ = await GetHttpDeleteResponse(ResumeAutomationsEndpoint, spaceId);
        }

        public async Task<PausedSpaceAutomationDto> GetPausedAutomations(int spaceId)
        {
            return await GetHttpGetResponse<PausedSpaceAutomationDto>(GetPausedAutomationsEndpoint, spaceId);
        }

        public async Task<int> PauseAutomations(int spaceId, PausedSpaceAutomationDto pausedSpaceAutomationDto)
        {
            using var response = await GetHttpPostResponse(PauseAutomationsEndpoint, pausedSpaceAutomationDto, spaceId);

            return GetId(response, 1);
        }

        public async Task AddTag(int spaceId, AddTagsDto tagDto)
        {
            using var _ = await GetHttpPutResponse(AddTagEndpoint, tagDto, spaceId);
        }

        public async Task RemoveTag(int spaceId, string tagName)
        {
            using var _ = await GetHttpDeleteResponse(RemoveTagEndpoint, spaceId, tagName);
        }

        public async Task CreateEvent(int spaceId, CreateSpaceEventActionSetDto createSpaceEventActionSetDto)
        {
            using var _ = await GetHttpPostResponse(CreateEventEndpoint, createSpaceEventActionSetDto, spaceId);
        }

        public async Task<ResultList<SpaceMacAddressDto>> GetSpaceMacAddresses(int spaceId)
        {
            return await GetHttpGetResponse<ResultList<SpaceMacAddressDto>>(GetMacAddressesEndpoint, spaceId);
        }

        public async Task UpdateSpaceMacAddress(int spaceId, SpaceMacAddressDto spaceMacAddressDto)
        {
            using var _ = await GetHttpPutResponse(UpdateMacAddressesEndpoint, spaceMacAddressDto, spaceId);
        }

        public async Task ImportSpaceMacAddresses(int spaceId, ImportSpaceMacAddressDto importSpaceMacAddressDto)
        {
            using var _ = await GetHttpPutResponse(ImportMacAddressesEndpoint, importSpaceMacAddressDto, spaceId);
        }
        
        public async Task CreateSpaceMacAddresses(int spaceId, string macAddress)
        {
            using var _ = await GetHttpPostResponse(CreateMacAddressEndpoint, macAddress, spaceId);
        }

        public async Task DeleteSpaceMacAddresses(int spaceId, string macAddress)
        {
            using var _ = await GetHttpDeleteResponse(DeleteMacAddressEndpoint, spaceId, macAddress);
        }

        private static string GetDeviceDeleteEndpoint(DeviceType type)
        {
            switch (type)
            {
                case DeviceType.Actuator:
                    return DeleteActuatorEndpoint;
                case DeviceType.DryContact:
                    return DeleteDryContactEndpoint;
                case DeviceType.Light:
                    return DeleteLightEndpoint;
                case DeviceType.MotionSensor:
                    return DeleteMotionSensorEndpoint;
                case DeviceType.LightSensor:
                    return DeleteLightSensorEndpoint;
                case DeviceType.TemperatureSensor:
                    return DeleteTemperatureSensorEndpoint;
                case DeviceType.Relay:
                    return DeleteRelayEndpoint;
                case DeviceType.Sensor:
                    return DeleteSensorEndpoint;
                case DeviceType.WallControl:
                    return DeleteWallControlEndpoint;
                default:
                    throw new IgorInvalidOperationException($"Delete not setup for {type}");
            }
        }
    }
}