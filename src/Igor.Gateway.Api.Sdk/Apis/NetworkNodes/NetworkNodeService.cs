﻿using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Core.Config;
using Igor.Gateway.Api.Sdk.Core.Http;
using Igor.Gateway.Api.Sdk.Core.Http.Security;
using Igor.Gateway.Api.Sdk.Core.Serialization;
using Igor.Gateway.Dtos.DeviceNodes;
using Igor.Gateway.Dtos.Devices;
using Igor.Gateway.Dtos.EmergencyLighting;
using Igor.Gateway.Dtos.Events;
using Igor.Gateway.Dtos.NetworkNodes;
using Igor.Gateway.Dtos.Tags;
using Igor.Sdk.Common.Models;

namespace Igor.Gateway.Api.Sdk.Apis.NetworkNodes
{
    public interface INetworkNodeService : IRetrievable<NetworkNodeDto>
    {
        Task<ResultList<NetworkNodeDto>> GetAll();
        Task<int> Create(CreateNetworkNodeDto createNetworkNodeDto);
        Task Delete(int networkNodeId);
        Task Update(int networkNodeId, UpdateNetworkNodeDto updateNetworkNodeDto);
        Task DeleteAll();
        Task<ResultList<DeviceNodeDto>> GetDeviceNodes(int networkNodeId);
        Task<ResultList<DeviceDto>> GetDevicesInChain(int networkNodeId);
        Task<ResultList<DeviceDto>> GetDevicesAttached(int networkNodeId);
        Task BringOnline(int networkNodeId);
        Task TakeOffline(int networkNodeId);
        Task UpdateEmergencyLevel(int networkNodeId, EmergencyLightingLevelDto emergencyLightingLevelDto);
        Task UpdateOfflineSettings(int networkNodeId, OfflineLightingSettingsDto offlineLightingSettingsDto);
        Task DeleteOfflineSettings(int networkNodeId);
        Task<ResultList<EventDto>> GetEvents(int networkNodeId);
        Task AddTag(int networkNodeId, AddTagsDto tagDto);
        Task RemoveTag(int networkNodeId, string tagName);
    }

    public class NetworkNodeService : ServiceBase, INetworkNodeService
    {
        private const string GetAllEndpoint = "/networknodes";
        private const string CreateEndpoint = "/networknodes";
        private const string DeleteEndpoint = "/networknodes/{0}";
        private const string GetEndpoint = "/networknodes/{0}";
        private const string UpdateEndpoint = "/networknodes/{0}";
        private const string DeleteAllEndpoint = "/networknodes/deleteall";
        private const string GetDevicesEndpoint = "/networknodes/{0}/devices";
        private const string GetDevicesAttachedEndpoint = "/networknodes/{0}/devicesattached";
        private const string GetDeviceNodesEndpoint = "/networknodes/{0}/devicenodes";
        private const string BringOnlineEndpoint = "/networknodes/{0}/bringonline";
        private const string TakeOfflineEndpoint = "/networknodes/{0}/takeoffline";
        private const string UpdateEmergencyLevelEndpoint = "/networknodes/{0}/emergencylevel";
        private const string OfflineSettingsEndpoint = "/networknodes/{0}/offlinesettings";
        private const string GetEventsEndpoint = "/networknodes/{0}/events";
        private const string AddTagEndpoint = "/networknodes/{0}/tags";
        private const string RemoveTagEndpoint = "/networknodes/{0}/tags/{1}";

        public NetworkNodeService(string apiKey) : base(apiKey)
        {
        }

        public NetworkNodeService(string apiKey, string apiAddress) : base(apiKey, apiAddress)
        {
        }

        public NetworkNodeService(IHttpClientService httpClient, IJsonSerializer jsonSerializer, ISdkConfig config, IJwtTokenProvider tokenProvider, IRequestHeaderProvider requestHeaderProvider)
            : base(httpClient, jsonSerializer, config, tokenProvider, requestHeaderProvider)
        {
        }

        public async Task<ResultList<NetworkNodeDto>> GetAll()
        {
            return await GetHttpGetResponse<ResultList<NetworkNodeDto>>(GetAllEndpoint);
        }

        public async Task<NetworkNodeDto> Get(int networkNodeId)
        {
            return await GetHttpGetResponse<NetworkNodeDto>(GetEndpoint, networkNodeId);
        }

        public async Task<int> Create(CreateNetworkNodeDto createNetworkNodeDto)
        {
            using var response = await GetHttpPostResponse(CreateEndpoint, createNetworkNodeDto);

            return GetId(response);
        }

        public async Task Delete(int networkNodeId)
        {
            using var _ = await GetHttpDeleteResponse(DeleteEndpoint, networkNodeId);
        }

        public async Task Update(int networkNodeId, UpdateNetworkNodeDto updateNetworkNodeDto)
        {
            using var _ = await GetHttpPutResponse(UpdateEndpoint, updateNetworkNodeDto, networkNodeId);
        }

        public async Task DeleteAll()
        {
            using var _ = await GetHttpDeleteResponse(DeleteAllEndpoint, (object) null);
        }

        public async Task<ResultList<DeviceDto>> GetDevicesInChain(int networkNodeId)
        {
            return await GetHttpGetResponse<ResultList<DeviceDto>>(GetDevicesEndpoint, networkNodeId);
        }

        public async Task<ResultList<DeviceDto>> GetDevicesAttached(int networkNodeId)
        {
            return await GetHttpGetResponse<ResultList<DeviceDto>>(GetDevicesAttachedEndpoint, networkNodeId);
        }

        public async Task<ResultList<DeviceNodeDto>> GetDeviceNodes(int networkNodeId)
        {
            return await GetHttpGetResponse<ResultList<DeviceNodeDto>>(GetDeviceNodesEndpoint, networkNodeId);
        }

        public async Task BringOnline(int networkNodeId)
        {
            using var _ = await GetHttpPostResponse(BringOnlineEndpoint, (object)null, networkNodeId);
        }

        public async Task TakeOffline(int networkNodeId)
        {
            using var _ = await GetHttpPostResponse(TakeOfflineEndpoint, (object)null, networkNodeId);
        }

        public async Task UpdateEmergencyLevel(int networkNodeId, EmergencyLightingLevelDto emergencyLightingLevelDto)
        {
            using var _ = await GetHttpPutResponse(UpdateEmergencyLevelEndpoint, emergencyLightingLevelDto, networkNodeId);
        }

        public async Task UpdateOfflineSettings(int networkNodeId,
            OfflineLightingSettingsDto offlineLightingSettingsDto)
        {
            using var _ = await GetHttpPostResponse(OfflineSettingsEndpoint, offlineLightingSettingsDto, networkNodeId);
        }

        public async Task DeleteOfflineSettings(int networkNodeId)
        {
            using var _ = await GetHttpDeleteResponse(OfflineSettingsEndpoint, networkNodeId);
        }

        public async Task<ResultList<EventDto>> GetEvents(int networkNodeId)
        {
            return await GetHttpGetResponse<ResultList<EventDto>>(GetEventsEndpoint, networkNodeId);
        }

        public async Task AddTag(int networkNodeId, AddTagsDto tagDto)
        {
            using var _ = await GetHttpPutResponse(AddTagEndpoint, tagDto, networkNodeId);
        }

        public async Task RemoveTag(int networkNodeId, string tagName)
        {
            using var _ = await GetHttpDeleteResponse(RemoveTagEndpoint, networkNodeId, tagName);
        }
    }
}
