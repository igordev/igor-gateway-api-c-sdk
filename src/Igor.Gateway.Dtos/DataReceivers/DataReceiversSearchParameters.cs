﻿using Igor.Sdk.Common.Queries;

namespace Igor.Gateway.Dtos.DataReceivers
{

    public class DataReceiversSearchParameters : PagingQueryParameters
    {
        //private const string DataReceiverIdsKey = "dataReceiverIds";
        private const string ApplicationKeyIdsKey = "applicationKeyIds";

        public string Term
        {
            get => this["term"];
            set => this["term"] = value;
        }

        //public int[] DataReceiverIds
        //{
        //    get => GetIntArray(DataReceiverIdsKey);
        //    set => SetIntArray(DataReceiverIdsKey, value);
        //}

        public int[] ApplicationKeyIds
        {
            get => GetIntArray(ApplicationKeyIdsKey);
            set => SetIntArray(ApplicationKeyIdsKey, value);
        }

        //public string GetDataReceiverIdsKeyAsString()
        //{
        //    return this[DataReceiverIdsKey];
        //}

        public string GetApplicationKeyIdsKeyAsString()
        {
            return this[ApplicationKeyIdsKey];
        }

        public QuerySortDirection? SortDirection
        {
            get
            {
                if (string.IsNullOrEmpty(this["sortDir"]))
                    return null;

                return this["sortDir"] == "asc" ? QuerySortDirection.Ascending : QuerySortDirection.Descending;
            }
            set
            {
                if (value.HasValue)
                    this["sortDir"] = value.Value == QuerySortDirection.Ascending ? "asc" : "desc";
            }
        }

        public string SortBy
        {
            get => this["sortBy"];
            set => this["sortBy"] = value;
        }

        public string GetSortDirectionAsString()
        {
            if (SortDirection.HasValue)
                return SortDirection.Value == QuerySortDirection.Ascending ? "asc" : "desc";

            return null;
        }
    }
}
