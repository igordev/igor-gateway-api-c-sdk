﻿namespace Igor.Gateway.Dtos.DataReceivers
{
    public class UpdateDataReceiverDto
    {
        public string Address { get; set; }
        public string DeviceType { get; set; }
        public string DeviceSubtype { get; set; }
        public int? DeviceId { get; set; }
        public bool IsEnabled { get; set; }
    }
}
