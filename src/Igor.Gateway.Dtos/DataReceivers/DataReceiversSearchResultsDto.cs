﻿using Igor.Gateway.Dtos.Devices;

namespace Igor.Gateway.Dtos.DataReceivers
{
    public class DataReceiversSearchResultsDto
    {
        public int Id { get; set; }

        public string Address { get; set; }

        public bool IsEnabled { get; set; }

        public DeviceType DeviceType { get; set; }

        //public int? DeviceSubtype { get; set; }
        public string DeviceSubtype { get; set; }

        public int? DeviceId { get; set; }

        public DataType DataType { get; set; }

        public Protocol Protocol { get; set; }

        public int ApplicationKeyId { get; set; }
    }
}
