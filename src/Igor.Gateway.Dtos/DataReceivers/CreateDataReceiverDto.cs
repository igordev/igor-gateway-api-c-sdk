﻿namespace Igor.Gateway.Dtos.DataReceivers
{
    public class CreateDataReceiverDto : UpdateDataReceiverDto
    {
        public int ApplicationKeyId { get; set; }
        public string DataType { get; set; }
        public string Protocol { get; set; }
    }
}
