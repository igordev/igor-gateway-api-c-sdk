﻿using Igor.Gateway.Dtos.Common;

namespace Igor.Gateway.Dtos.DataReceivers
{
    /// <summary>
    /// The data receiver
    /// </summary>
    public class DataReceiverDto : IEntityDto
    {
        public int Id { get; set; }
        public int ApplicationKeyId { get; set; }
        public string Address { get; set; }
        public string DeviceType { get; set; }
        public string DeviceSubtype { get; set; }
        public int? DeviceId { get; set; }
        public string DataType { get; set; }
        public string Protocol { get; set; }
        public bool IsEnabled { get; set; }
    }
}
