﻿namespace Igor.Gateway.Dtos.Relays
{
    public enum RelayState
    {
        Open,
        Closed
    }
}
