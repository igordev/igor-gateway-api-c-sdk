﻿using System.Collections.Generic;

namespace Igor.Gateway.Dtos.Relays
{
    public class BatchRelayStateListDto
    {
        /// <summary>
        /// The list of relay states
        /// </summary>
        public List<BatchRelayStateDto> List { get; set; }
    }

    public class BatchRelayStateDto : RelayStateDto
    {
        /// <summary>
        /// The relay ID
        /// </summary>
        public int Id { get; set; }

        public BatchRelayStateDto(int id, string relayState) : base(relayState)
        {
            Id = id;
        }
    }
}
