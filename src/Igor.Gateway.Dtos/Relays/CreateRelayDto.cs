﻿namespace Igor.Gateway.Dtos.Relays
{
    /// <summary>
    /// The relay to be created
    /// </summary>
    public class CreateRelayDto : CreateDeviceDto
    {
        /// <summary>
        /// The external system's ID for this relay.
        /// </summary>
        public string ExternalId { get; set; }

        /// <summary>
        /// Should this relay invert its commands. This means turning a space "on" would open the relay.
        /// </summary>
        public bool IsInverted { get; set; }

        public RelayDto ToDto()
        {
            return new RelayDto
            {
                Properties = Properties,
                SystemProperties = SystemProperties,
                ExternalId = ExternalId,
                IsInverted = IsInverted,
                Name = Name,
                Protocol = string.IsNullOrEmpty(Protocol) ? "external" : Protocol,
                NetworkNodeId = NetworkNodeId,
                DeviceNodeId = DeviceNodeId
            };
        }
    }
}
