﻿namespace Igor.Gateway.Dtos.Relays
{
    /// <summary>
    /// The relay to be updated
    /// </summary>
    public class UpdateRelayDto : UpdateDeviceDto
    {
        /// <summary>
        /// Should this relay invert its commands. This means turning a space "on" would open the relay.
        /// </summary>
        public bool IsInverted { get; set; }

        public RelayDto ToDto(int id)
        {
            return new RelayDto
            {
                Id = id,
                Name = Name,
                Properties = Properties,
                SystemProperties = SystemProperties,
                IsInverted = IsInverted
            };
        }
    }
}