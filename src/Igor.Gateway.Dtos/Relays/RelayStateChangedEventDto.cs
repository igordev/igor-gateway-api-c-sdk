﻿namespace Igor.Gateway.Dtos.Relays
{
    public class RelayStateChangedEventDto
    {
        public int EntityId { get; set; }
        public string ExternalId { get; set; }
        public string RelayState { get; set; }
    }
}
