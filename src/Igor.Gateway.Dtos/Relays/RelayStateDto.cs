﻿namespace Igor.Gateway.Dtos.Relays
{
    public class RelayStateDto
    {
        /// <summary>
        /// The relay state
        /// </summary>
        public string State { get; set; }

        public RelayStateDto(string relayState)
        {
            State = relayState;
        }
    }
}