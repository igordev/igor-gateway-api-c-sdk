﻿namespace Igor.Gateway.Dtos.Diagnostics
{
    public class DiagnosticMessageDto
    {
        public DiagnosticMessageDto()
        {
        }

        public DiagnosticMessageDto(Message message, object parameters)
        {
            Message = message;
            Parameters = parameters;
        }

        public Message Message { get; set; }

        public object Parameters { get; set; }
    }
}