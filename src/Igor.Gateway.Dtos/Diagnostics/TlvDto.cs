﻿namespace Igor.Gateway.Dtos.Diagnostics
{
    public class TlvDto
    {
        public ushort Type { get; }

        public ushort Length { get; }

        public string Base64Value { get; }
    }
}