﻿using Igor.Gateway.Dtos.Devices;

namespace Igor.Gateway.Dtos.Diagnostics
{
    public class DeviceMessage : DiagnosticMessageBase
    {
        public DeviceType DeviceType { get; set; }

        public int RuntimeId { get; set; }
    }
}