﻿namespace Igor.Gateway.Dtos.Diagnostics
{
    public class NodeOfflineMessageDto : DiagnosticMessageBase
    {
        public NodeType NodeType { get; set; }

        public int NodeId { get; set; }
    }
}