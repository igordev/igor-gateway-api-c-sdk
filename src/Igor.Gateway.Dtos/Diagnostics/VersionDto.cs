﻿using System;

namespace Igor.Gateway.Dtos.Diagnostics
{
    /// <summary>
    /// The gateway software version
    /// </summary>
    public class VersionDto
    {
        /// <summary>
        /// The gateway software version
        /// </summary>
        public string Version { get; set; }

        public string GatewayUniqueId { get; set; }
    }
}
