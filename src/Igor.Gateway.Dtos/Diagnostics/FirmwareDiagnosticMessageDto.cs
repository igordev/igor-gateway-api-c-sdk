﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Igor.Gateway.Dtos.Diagnostics
{
    public class FirmwareDiagnosticMessageDto : DiagnosticMessageDto
    {
        public MessageSubtype MessageSubtype { get; set; }
    }
}
