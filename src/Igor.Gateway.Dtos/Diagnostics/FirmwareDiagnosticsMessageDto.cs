﻿using System.Collections.Generic;

namespace Igor.Gateway.Dtos.Diagnostics
{
    public class FirmwareDiagnosticsMessageDto : DiagnosticMessageBase
    {
        public string Protocol { get; set; }

        public string Uuid { get; set; }

        public Queue<TlvDto> Tlvs { get; set; }
    }
}