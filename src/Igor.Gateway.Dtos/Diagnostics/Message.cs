﻿namespace Igor.Gateway.Dtos.Diagnostics
{
    public enum Message
    {
        Unknown = 0,
        DeviceOffline = 1,
        FirmwareDiagnostics = 2,
        Log = 3,
        NodeOffline = 4
    }
}