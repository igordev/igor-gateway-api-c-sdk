﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Igor.Gateway.Dtos.Diagnostics
{
    public enum MessageSubtype
    {
        Undefined = 0,
        SafeBootMode = 1,
    }
}
