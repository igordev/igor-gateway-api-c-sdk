﻿namespace Igor.Gateway.Dtos.Diagnostics
{
    public abstract class DiagnosticMessageBase
    {
        public string Description { get; set; }
    }
}