﻿namespace Igor.Gateway.Dtos.Diagnostics
{
    public class DiagnosticWarningDto
    {
        public string Message { get; set; }
    }
}
