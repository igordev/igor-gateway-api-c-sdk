﻿using System;

namespace Igor.Gateway.Dtos.Devices
{
    public interface IDeviceDto : IPropertiesDto
    {
        int Id { get; set; }

        string Name { get; set; }

        string Protocol { get; set; }

        string ExternalId { get; set; }

        DateTime DiscoveredDate { get; set; }

        bool IsOnline { get; set; }
        DateTime? IsOnlineChanged { get; set; }

        bool IsQuarantined { get; set; }
        DateTime? IsQuarantinedChanged { get; set; }

        int? NetworkNodeId { get; set; }

        int? DeviceNodeId { get; set; }
    }
}
