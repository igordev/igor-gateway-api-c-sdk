﻿using System;
using Igor.Gateway.Dtos.Actuators;
using Igor.Gateway.Dtos.Common;
using Igor.Gateway.Dtos.DryContacts;
using Igor.Gateway.Dtos.Sensors;
using Igor.Sdk.Common.Queries;

namespace Igor.Gateway.Dtos.Devices
{
    public class DeviceSearchParameters : PagingQueryParameters
    {
        private const string TypesKey = "types";
        private const string ActuatorSubtypesKey = "actuatorSubtypes";
        private const string SensorSubtypesKey = "sensorSubtypes";
        private const string DryContactTypesKey = "dryContactTypes";
        private const string NetworkSwitchNamesKey = "networkSwitchNames";
        private const string NetworkSwitchIpAddressesKey = "networkSwitchIpAddresses";
        private const string SpaceIdsKey = "spaceIds";
        private const string DeviceIdsKey = "deviceIds";
        private const string TagIdsKey = "tagIds";

        public string Term
        {
            get => this["term"];
            set => this["term"] = value;
        }

        public int[] DeviceIds
        {
            get => GetIntArray(DeviceIdsKey);
            set => SetIntArray(DeviceIdsKey, value);
        }

        public DeviceType[] Types
        {
            get => GetEnumArray<DeviceType>(TypesKey);
            set => SetEnumArray(TypesKey, value);
        }

        public ActuatorSubtype[] ActuatorSubtypes
        {
            get => GetEnumArray<ActuatorSubtype>(ActuatorSubtypesKey);
            set => SetEnumArray(ActuatorSubtypesKey, value);
        }

        public SensorSubtype[] SensorSubtypes
        {
            get => GetEnumArray<SensorSubtype>(SensorSubtypesKey);
            set => SetEnumArray(SensorSubtypesKey, value);
        }

        public DryContactType[] DryContactTypes
        {
            get => GetEnumArray<DryContactType>(DryContactTypesKey);
            set => SetEnumArray(DryContactTypesKey, value);
        }

        public bool? IsOnline
        {
            get => GetBool("isOnline");
            set => SetBool("isOnline", value);
        }

        public bool? IsQuarantined
        {
            get => GetBool("isQuarantined");
            set => SetBool("isQuarantined", value);
        }

        public bool? IsEmergency
        {
            get => GetBool("isEmergency");
            set => SetBool("isEmergency", value);
        }

        public bool? IsAssigned
        {
            get => GetBool("isAssigned");
            set => SetBool("isAssigned", value);
        }

        public int[] SpaceIds
        {
            get => GetIntArray(SpaceIdsKey);
            set => SetIntArray(SpaceIdsKey, value);
        }

        public int[] TagIds
        {
            get => GetIntArray(TagIdsKey);
            set => SetIntArray(TagIdsKey, value);
        }

        public string[] NetworkSwitchNames
        {
            get => GetArray(NetworkSwitchNamesKey);
            set => SetArray(NetworkSwitchNamesKey, value);
        }

        public string[] NetworkSwitchIPAddresses
        {
            get => GetArray(NetworkSwitchIpAddressesKey);
            set => SetArray(NetworkSwitchIpAddressesKey, value);
        }

        public bool? OnlyDevicesWithLLDP
        {
            get => GetBool("onlyDevicesWithLldp");
            set => SetBool("onlyDevicesWithLldp", value);
        }

        public QuerySortDirection? SortDirection
        {
            get
            {
                if (string.IsNullOrEmpty(this["sortDir"]))
                    return null;

                return this["sortDir"] == "asc" ? QuerySortDirection.Ascending : QuerySortDirection.Descending;
            }
            set
            {
                if (value.HasValue)
                    this["sortDir"] = value.Value == QuerySortDirection.Ascending ? "asc" : "desc";
            }
        }

        public string SortBy
        {
            get => this["sortBy"];
            set => this["sortBy"] = value;
        }

        public DateTime? MinimumDiscoveredDate
        {
            get => GetDateTime("minDiscoveredDate");
            set => SetDateTime("minDiscoveredDate", value);
        }

        public DateTime? MaximumDiscoveredDate
        {
            get => GetDateTime("maxDiscoveredDate");
            set => SetDateTime("maxDiscoveredDate", value);
        }

        public SearchType? SearchType
        {
            get
            {
                var value = Get("searchType");

                if (value == null) return null;

                if (Enum.TryParse(typeof(SearchType), value, true, out var result))
                    return (SearchType)result;

                return null;
            }
            set => Set("searchType", value.HasValue ? value.ToString() : null);
        }
    }
}