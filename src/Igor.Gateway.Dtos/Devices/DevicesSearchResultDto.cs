﻿using System;

namespace Igor.Gateway.Dtos.Devices
{
    public class DevicesSearchResultDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }

        public string Protocol { get; set; }

        public string ExternalId { get; set; }

        public DateTime DiscoveredDate { get; set; }

        public bool IsOnline { get; set; }

        public bool IsQuarantined { get; set; }

        public int? NodeId { get; set; }
        public int? DeviceNodeId { get; set; }

        public int? SpaceId { get; set; }

        public string SpaceName { get; set; }

        public string NetworkSwitchName { get; set; }

        public string NetworkSwitchIpAddress { get; set; }

        public string NetworkSwitchPortId { get; set; }

        public string NetworkSwitchPortDescription { get; set; }

        public bool IsEmergency { get; set; }

        public bool IsLight => Type == "Light";

        public bool IsRelay => Type == "Relay";

        public string LightType { get; set; }

        public string Properties { get; set; }

        public string SystemProperties { get; set; }

        public string IpAddress { get; set; }

        public string MacAddress { get; set; }
    }
}
