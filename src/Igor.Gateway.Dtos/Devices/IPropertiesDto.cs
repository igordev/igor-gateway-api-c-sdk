﻿
namespace Igor.Gateway.Dtos.Devices
{
    public interface IPropertiesDto
    {
        string Properties { get; set; }

        string SystemProperties { get; set; }
    }
}
