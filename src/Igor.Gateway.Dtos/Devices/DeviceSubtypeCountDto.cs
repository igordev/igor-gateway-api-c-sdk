﻿namespace Igor.Gateway.Dtos.Devices
{
    public class DeviceSubtypeCountDto
    {
        public string DeviceSubtype { get; set; }
        public int Count { get; set; }
    }
}
