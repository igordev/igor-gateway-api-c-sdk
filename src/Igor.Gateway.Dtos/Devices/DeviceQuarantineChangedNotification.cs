﻿namespace Igor.Gateway.Dtos.Devices
{
    public class DeviceQuarantineChangedNotification
    {
        public int EntityId { get; set; }
        public string ExternalId { get; set; }
        public bool IsQuarantined { get; set; }
        public string Reason { get; set; }
    }
}