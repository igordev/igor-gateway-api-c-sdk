﻿namespace Igor.Gateway.Dtos.Devices
{
    public enum IncludeAttachedDevices
    {
        None = 0,
        AllOnNode = 1,
        AllOnChain = 2
    }
}
