﻿using System;

namespace Igor.Gateway.Dtos.Devices
{
    public class UnitOfMeasureLabelAttribute : Attribute
    {
        public string Description { get; }

        public UnitOfMeasureLabelAttribute(string description)
        {
            Description = description;
        }
    }

    public class UnitOfMeasureAttribute : Attribute
    {
        public UnitOfMeasure UnitOfMeasure { get; }

        public UnitOfMeasureAttribute(UnitOfMeasure unitOfMeasure)
        {
            UnitOfMeasure = unitOfMeasure;
        }
    }

    public enum UnitOfMeasure
    {
        [UnitOfMeasureLabel("")] Unknown = 0,

        /// <summary>
        /// No unit
        /// </summary>
        [UnitOfMeasureLabel("")] None = 1,

        /// <summary>
        /// e.g. Shade drop length (m - meters)
        /// </summary>
        [UnitOfMeasureLabel("m")] Length = 2,

        /// <summary>
        /// Electric voltage (V)
        /// </summary>
        [UnitOfMeasureLabel("V")] Voltage = 3,

        /// <summary>
        /// Electric current (A)
        /// </summary>
        [UnitOfMeasureLabel("A")] Ampere = 4,

        /// <summary>
        /// Air temperature (C - Celsius)
        /// </summary>
        [UnitOfMeasureLabel("C")] Temperature = 5,

        /// <summary>
        /// Barometric pressure (Pa)
        /// </summary>
        [UnitOfMeasureLabel("Pa")] Pressure = 6,

        /// <summary>
        /// Percentage (e.g. relative humidity)
        /// </summary>
        [UnitOfMeasureLabel("%")] Percentage = 7,

        /// <summary>
        /// e.g. CO2, O2, CO (ppm - parts per million)
        /// </summary>
        [UnitOfMeasureLabel("ppm")] GasConcentration = 8,

        /// <summary>
        /// Sound level (B)
        /// </summary>
        [UnitOfMeasureLabel("B")] Bel = 9,

        /// <summary>
        /// Count of something (qty)
        /// </summary>
        [UnitOfMeasureLabel("qty")] Quantity = 10,

        /// <summary>
        /// Count of something (qty)
        /// </summary>
        [UnitOfMeasureLabel("lx")] Lux = 11,

        // 12-21 Reserved

        /// <summary>
        /// e.g. CO2 flow rate (g/s - grams per second)
        /// </summary>
        [UnitOfMeasureLabel("g/s")] MassFlowRate = 22,

        /// <summary>
        /// e.g. Air flow rate (m3/s - cubic meters per second)
        /// </summary>
        [UnitOfMeasureLabel("m3/s")] VolumeFlowRateGas = 23,

        /// <summary>
        /// e.g. Water flow rate (L/s - liters per second)
        /// </summary>
        [UnitOfMeasureLabel("L/s")] VolumeFlowRateLiquid = 24,

        /// <summary>
        /// e.g. Electricity flow rate (W - watts)
        /// </summary>
        [UnitOfMeasureLabel("W")] Power = 25,

        /// <summary>
        /// e.g. Shade drop speed (m/s - meters per second)
        /// </summary>
        [UnitOfMeasureLabel("m/s")] Velocity = 26,

        /// <summary>
        /// e.g. Fan speed (rpm - rotations per minute)
        /// </summary>
        [UnitOfMeasureLabel("rpm")] RotationalSpeed = 27,

        //27-40 Reserved
    }
}