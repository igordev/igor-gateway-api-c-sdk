﻿using System;
using Igor.Gateway.Dtos.Common;

namespace Igor.Gateway.Dtos.Devices
{
    public class DeviceDto : IEntityDto, IDeviceDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }

        public string Protocol { get; set; }

        public string ExternalId { get; set; }

        public DateTime DiscoveredDate { get; set; }

        public bool IsOnline { get; set; }
        public DateTime? IsOnlineChanged { get; set; }

        public bool IsQuarantined { get; set; }
        public DateTime? IsQuarantinedChanged { get; set; }

        public int SpaceId { get; set; }

        public string SpaceName { get; set; }

        public bool IsEmergency { get; set; }

        public bool IsLight => Type == "Light";
        public bool IsRelay => Type == "Relay";

        public string LightType { get; set; }

        public string Properties { get; set; }
        public string SystemProperties { get; set; }

        public DeviceType? GetDeviceType()
        {
            if (Enum.TryParse(Type, out DeviceType deviceType))
                return deviceType;

            return null;
        }

        public int? NetworkNodeId { get; set; }

        public int? DeviceNodeId { get; set; }
    }
}
