﻿namespace Igor.Gateway.Dtos.Devices
{
    /// <summary>
    /// The device
    /// </summary>
    public class UpdateDevicesDto
    {
        /// <summary>
        /// The device name
        /// </summary>
        public string Name { get; set; }
    }
}
