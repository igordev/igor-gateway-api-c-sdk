﻿namespace Igor.Gateway.Dtos.Devices
{
    public class DeviceSpaceDto
    {
        public int SpaceId { get; set; }
        public int DeviceId { get; set; }
        public DeviceType DeviceType { get; set; }

        /// <summary>
        /// Specifies whether other attached devices should also be added to the space
        /// </summary>
        public IncludeAttachedDevices IncludeAttachedDevices { get; set; }
    }
}
