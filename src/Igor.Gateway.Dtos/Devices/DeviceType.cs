﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Igor.Gateway.Dtos.Devices
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum DeviceType
    {
        Light = 0,
        //Switch = 1, // Legacy - DO NOT REUSE
        //Dimmer = 2, // Legacy - DO NOT REUSE
        MotionSensor = 3,
        LightSensor = 4,
        TemperatureSensor = 5,
        Relay = 6,
        WallControl = 7,
        DryContact = 8,
        Actuator = 9,
        Sensor = 10,
        DeviceNode = 11,
        NetworkNode = 12
    }
}
