﻿using System.Collections.Generic;

namespace Igor.Gateway.Dtos.Devices
{
    public class DeviceTypeCountDto
    {
        public string DeviceType { get; set; }
        public int Count { get; set; }

        public IEnumerable<DeviceSubtypeCountDto> Subtypes { get; set; }
    }
}
