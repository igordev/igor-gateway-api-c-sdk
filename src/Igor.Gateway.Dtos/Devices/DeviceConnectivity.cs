﻿namespace Igor.Gateway.Dtos.Devices
{
    public enum DeviceConnectivity
    {
        Online,
        Offline
    }
}
