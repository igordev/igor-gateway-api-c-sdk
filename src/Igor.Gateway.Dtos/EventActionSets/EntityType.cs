﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Igor.Gateway.Dtos.EventActionSets
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum EntityType
    {
        DryContact,
        MotionSensor,
        Space
    }
}
