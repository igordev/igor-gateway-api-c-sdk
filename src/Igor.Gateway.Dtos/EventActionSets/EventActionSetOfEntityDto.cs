﻿namespace Igor.Gateway.Dtos.EventActionSets
{
    public class EventActionSetOfEntityDto : EventActionSetDto
    {
        public string ActionSetName { get; set; }
    }
}
