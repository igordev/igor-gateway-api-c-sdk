﻿namespace Igor.Gateway.Dtos.EventActionSets
{
    /// <summary>
    /// The event action set
    /// </summary>
    public class UpdateEventActionSetDto
    {
        
        public int ActionSetId { get; set; }

        public EventActionSetDto AsEventActionSetDto()
        {
            return new EventActionSetDto
            {
                ActionSetId = ActionSetId,
            };
        }

        public static implicit operator EventActionSetDto(UpdateEventActionSetDto dto)
        {
            return dto?.AsEventActionSetDto();
        }
    }
}
