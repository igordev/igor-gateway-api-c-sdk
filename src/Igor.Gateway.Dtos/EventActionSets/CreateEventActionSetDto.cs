﻿namespace Igor.Gateway.Dtos.EventActionSets
{
    /// <summary>
    /// The event action set
    /// </summary>
    public class CreateEventActionSetDto : UpdateEventActionSetDto
    {
        public int EntityId { get; set; }
        
        public string EntityType { get; set; }
        
        public string EventType { get; set; }

        public new EventActionSetDto AsEventActionSetDto()
        {
            return new EventActionSetDto
            {
                ActionSetId = ActionSetId,
                EntityType = EntityType,
                EntityId = EntityId,
                EventType = EventType
            };
        }

        public static implicit operator EventActionSetDto(CreateEventActionSetDto dto)
        {
            return dto?.AsEventActionSetDto();
        }
    }
}
