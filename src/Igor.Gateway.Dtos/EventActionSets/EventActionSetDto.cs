﻿using Igor.Gateway.Dtos.Common;

namespace Igor.Gateway.Dtos.EventActionSets
{
    public class EventActionSetDto : IEntityDto
    {
        public int Id { get; set; }

        public int EntityId { get; set; }

        public int ActionSetId { get; set; }

        public string EntityType { get; set; }

        public string EventType { get; set; }
    }
}
