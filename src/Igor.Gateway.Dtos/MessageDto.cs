﻿
namespace Igor.Gateway.Dtos
{
    public class MessageDto
    {
        /// <summary>
        /// The message
        /// </summary>
        public string Message { get; set; }

        public int ErrorCode { get; set; }

        public MessageDto(string message, int errorCode)
        {
            Message = message;
            ErrorCode = errorCode;
        }
    }
}
