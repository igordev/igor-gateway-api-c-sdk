﻿namespace Igor.Gateway.Dtos.EmergencyLighting
{
    public class EmergencyLightingLevelDto
    {
        /// <summary>
        /// The emergency lighting level for the node
        /// </summary>
        public int EmergencyLevel { get; set; }
    }
}
