﻿namespace Igor.Gateway.Dtos.EmergencyLighting
{
    /// <summary>
    /// The emergency lighting settings
    /// </summary>
    public class EmergencyLightingSettingsDto
    {
        public static readonly EmergencyLightingSettingsDto DisableSettings = new EmergencyLightingSettingsDto
        {
            Timeout = 0,
            LightLevel = 2000
        };

        /// <summary>
        /// The timeout in milliseconds (5000-30000) before the emergency light turns on (0=disable)
        /// </summary>
        public int? Timeout { get; set; }

        /// <summary>
        /// The lighting level during emergency
        /// </summary>
        public int? LightLevel { get; set; }
    }
}
