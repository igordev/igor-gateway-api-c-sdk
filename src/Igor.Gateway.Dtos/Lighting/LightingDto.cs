﻿using Igor.Gateway.Dtos.Common;

namespace Igor.Gateway.Dtos.Lighting
{
    public class LightingDto
    {
        /// <summary>
        /// The lighting level
        /// </summary>
        public int? Level { get; set; }
        
        /// <summary>
        /// The color temperature of the lighting in Kelvin
        /// </summary>
        public int? CCT { get; set; }

        /// <summary>
        /// The smooth ramp behavior
        /// </summary>
        public Behavior? Behavior { get; set; }

        /// <summary>
        /// The smooth ramp curve type
        /// </summary>
        public CurveType? CurveType { get; set; }

        /// <summary>
        /// The smooth ramp duration in milliseconds
        /// </summary>
        public int? Duration { get; set; }

        /// <summary>
        /// The on-off state
        /// </summary>
        public OnOffState? State { get; set; }

        public LightingDto(int? level = null, int? cct = null, Behavior? behavior = null, CurveType? curveType = null, int? duration = null, OnOffState? state = null)
        {
            Level = level;
            CCT = cct;
            Behavior = behavior;
            CurveType = curveType;
            Duration = duration;
            State = state;
        }
    }
}
