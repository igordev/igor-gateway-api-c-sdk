﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Igor.Gateway.Dtos.Lighting
{
    public enum Behavior
    {
        ConstantDuration = 0,
        Variable = 1,
        ConstantRate = 2
    }
}
