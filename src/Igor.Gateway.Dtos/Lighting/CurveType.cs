﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Igor.Gateway.Dtos.Lighting
{
    public enum CurveType
    {
        None = 0,
        Linear = 1,
        SquareLaw = 2,
        Dali = 3
    }
}
