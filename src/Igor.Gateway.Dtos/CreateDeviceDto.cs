﻿namespace Igor.Gateway.Dtos
{
    public class CreateDeviceDto : CreateDto
    {
        /// <summary>
        /// The device's network node ID
        /// </summary>
        public int? NetworkNodeId { get; set; }

        /// <summary>
        /// The device's device node ID
        /// </summary>
        public int? DeviceNodeId { get; set; }
    }
}
