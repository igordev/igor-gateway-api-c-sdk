﻿using System.Collections.Generic;

namespace Igor.Gateway.Dtos.WebHooks
{
    /// <summary>
    /// The web hook
    /// </summary>
    public class UpdateWebHookDto
    {
        /// <summary>
        /// The domain events associated with this web hook
        /// </summary>
        public List<string> DomainEvents { get; set; }

        /// <summary>
        /// The callback URL
        /// </summary>
        public string CallbackUrl { get; set; }

        /// <summary>
        /// Whether the web hook is enabled
        /// </summary>
        public bool? IsEnabled { get; set; }
    }
}
