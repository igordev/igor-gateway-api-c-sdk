﻿using System.Collections.Generic;

namespace Igor.Gateway.Dtos.WebHooks
{
    public class WebHookEventDto
    {
        public string Id { get; set; }

        public int Attempt { get; set; }

        public IList<WebHookEventNotificationDto> Notifications { get; set; }
    }
}
