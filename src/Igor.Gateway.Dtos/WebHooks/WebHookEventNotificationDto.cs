﻿using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Igor.Gateway.Dtos.WebHooks
{
    public class WebHookEventNotificationDto
    {
        public string Action { get; set; }

        public string Entity { get; set; }

        public int EntityId { get; set; }

        public object Value { get; set; }

        public string ExternalId { get; set; }

        public int? SpaceId { get; set; }

        /// <summary>
        /// Deserializes or converts the object in Value to a the specified type
        /// </summary>
        /// <typeparam name="T">Type to convert Value to.</typeparam>
        /// <returns></returns>
        public T ValueToObject<T>()
        {
            if (Value is string value)
                return JsonConvert.DeserializeObject<T>(value);

            return JObject.FromObject(Value).ToObject<T>();
        }

        /// <summary>
        /// Deserializes or converts the object in Value to a strongly typed array.
        /// </summary>
        /// <typeparam name="T">Type to convert each item to.</typeparam>
        /// <returns></returns>
        public T[] ValueToArray<T>()
        {
            if (Value is string value)
                return JsonConvert.DeserializeObject<T[]>(value);

            return JArray.FromObject(Value).Children()
                .Select(t => t.ToObject<T>())
                .ToArray();
        }

        /// <summary>
        /// Returns the value as a string. If Value is not a string returns the specified default.
        /// </summary>
        /// <param name="default">Default value to use if Value is not string</param>
        /// <returns></returns>
        public string ValueAsString(string @default = null)
        {
            return Value is string value ? value : @default;
        }

        /// <summary>
        /// Returns the value as an integer. If Value cannot be converted to an int the specified default will be returned.
        /// </summary>
        /// <param name="default">Default Value to use if Value cannot be converted to int.</param>
        /// <returns></returns>
        public int? ValueAsInt(int? @default = null)
        {
            return int.TryParse(Value as string, out var value)
                ? value
                : @default;
        }

        /// <summary>
        /// Returns the value as an double. If Value cannot be converted to an double the specified default will be returned.
        /// </summary>
        /// <param name="default">Default value to use if Value cannot be converted to double.</param>
        /// <returns></returns>
        public double? ValueAsDouble(double? @default = null)
        {
            return double.TryParse(Value as string, out var value)
                ? value
                : @default;
        }
    }
}
