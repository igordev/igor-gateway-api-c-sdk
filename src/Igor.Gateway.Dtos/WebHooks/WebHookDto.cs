﻿using System.Collections.Generic;
using Igor.Gateway.Dtos.Common;

namespace Igor.Gateway.Dtos.WebHooks
{
    public class WebHookDto : IEntityDto
    {
        public int Id { get; set; }
        
        public List<string> DomainEvents { get; set; }

        public string CallbackUrl { get; set; }

        public int ApplicationKeyId { get; set; }

        public string ApplicationKey { get; set; }

        public bool IsEnabled { get; set; }
    }
}
