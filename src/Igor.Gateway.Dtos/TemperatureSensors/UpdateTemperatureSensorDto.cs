﻿namespace Igor.Gateway.Dtos.TemperatureSensors
{
    /// <summary>
    /// The temperature sensor
    /// </summary>
    public class UpdateTemperatureSensorDto : UpdateDeviceDto
    {
    }
}
