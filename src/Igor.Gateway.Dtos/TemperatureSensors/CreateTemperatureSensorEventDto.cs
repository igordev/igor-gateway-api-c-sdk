﻿namespace Igor.Gateway.Dtos.TemperatureSensors
{
    /// <summary>
    /// The temperature sensor event
    /// </summary>
    public class CreateTemperatureSensorEventDto
    {
        /// <summary>
        /// The temperature
        /// </summary>
        
        public double Temperature { get; set; }
    }
}
