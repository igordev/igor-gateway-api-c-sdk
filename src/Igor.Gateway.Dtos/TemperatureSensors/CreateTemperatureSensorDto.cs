﻿namespace Igor.Gateway.Dtos.TemperatureSensors
{
    /// <summary>
    /// The temperature sensor
    /// </summary>
    public class CreateTemperatureSensorDto : CreateDeviceDto
    {
        /// <summary>
        /// The external system's ID for this temperature sensor
        /// </summary>
        
        public string ExternalId { get; set; }
    }
}
