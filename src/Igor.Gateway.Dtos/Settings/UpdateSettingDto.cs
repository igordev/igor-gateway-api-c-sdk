﻿namespace Igor.Gateway.Dtos.Settings
{
    public class UpdateSettingDto
    {
        public string Value { get; set; }
    }
}
