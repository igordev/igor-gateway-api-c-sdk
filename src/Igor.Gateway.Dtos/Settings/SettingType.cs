﻿namespace Igor.Gateway.Dtos.Settings
{
    public enum SettingType
    {
        Basic = 0,
        Advanced = 1
    }
}
