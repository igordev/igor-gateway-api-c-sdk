﻿namespace Igor.Gateway.Dtos.Settings
{
    public static class SettingDtoExtensions
    {
        public static UpdateSettingDto ToUpdateDto(this SettingDto settingDto)
        {
            return new UpdateSettingDto
            {
                Value = settingDto.Value
            };
        }
    }
}
