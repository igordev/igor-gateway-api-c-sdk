﻿using System.Collections.Generic;
using Igor.Gateway.Dtos.Common;

namespace Igor.Gateway.Dtos.Settings
{
    public class SettingDto : IEntityDto
    {
        public int Id { get; set; }
        public SettingApplication Application { get; set; }
        public SettingType Type { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Value { get; set; }
        public string Default { get; set; }
        public SettingValueType ValueType { get; set; }
        public IEnumerable<string> Options { get; set; }
        public string Category { get; set; }
        public string SubCategory { get; set; }
        public string DisplayName { get; set; }

        public static implicit operator UpdateSettingDto(SettingDto dto)
        {
            return dto?.ToUpdateDto();
        }
    }
}