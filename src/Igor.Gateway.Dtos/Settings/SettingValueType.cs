﻿namespace Igor.Gateway.Dtos.Settings
{
    public enum SettingValueType
    {
        String = 0,
        Integer = 1,
        Decimal = 2,
        Boolean = 3,
        IpAddress = 4,
        DateTime = 5
    }
}
