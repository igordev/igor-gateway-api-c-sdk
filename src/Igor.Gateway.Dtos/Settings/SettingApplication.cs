﻿namespace Igor.Gateway.Dtos.Settings
{
    public enum SettingApplication
    {
        Global = 0,
        RuntimeService = 1,
        CloudService = 2,
        CoapService = 3,
        EmergencyLightingService = 4,
        ManagementService = 5,
        SamplingService = 6,
        AdminService = 7
    }
}
