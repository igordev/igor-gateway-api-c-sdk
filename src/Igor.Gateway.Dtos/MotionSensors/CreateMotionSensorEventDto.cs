﻿namespace Igor.Gateway.Dtos.MotionSensors
{
    /// <summary>
    /// The motion sensor
    /// </summary>
    public class CreateMotionSensorEventDto
    {
        /// <summary>
        /// The motion sensor state
        /// </summary>
        public MotionSensorState State { get; set; }
    }
}
