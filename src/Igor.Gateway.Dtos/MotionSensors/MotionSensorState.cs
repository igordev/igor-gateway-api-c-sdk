﻿namespace Igor.Gateway.Dtos.MotionSensors
{
    public enum MotionSensorState
    {
        Vacancy,
        Occupancy
    }
}
