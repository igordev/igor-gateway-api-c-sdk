﻿namespace Igor.Gateway.Dtos.MotionSensors
{
    /// <summary>
    /// The motion sensor
    /// </summary>
    public class CreateMotionSensorDto : CreateDeviceDto
    {
        /// <summary>
        /// The external system's ID for this motion sensor
        /// </summary>
        public string ExternalId { get; set; }
    }
}
