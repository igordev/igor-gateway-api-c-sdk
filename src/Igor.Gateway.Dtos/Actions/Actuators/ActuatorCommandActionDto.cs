﻿using Igor.Gateway.Dtos.Actuators;

namespace Igor.Gateway.Dtos.Actions.Actuators
{
    public class ActuatorCommandActionDto : IActuatorCommandDto
    {
        public CommandType CommandType { get; set; }
        public CommandParameterDto[] Parameters { get; set; }
        public string Value { get; set; }
        public string Encoding { get; set; }
    }
}