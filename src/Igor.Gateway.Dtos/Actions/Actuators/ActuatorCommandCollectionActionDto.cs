﻿using System.Collections.Generic;

namespace Igor.Gateway.Dtos.Actions.Actuators
{
    public class ActuatorCommandCollectionActionDto
    {
        public string Name{ get; set; }
        public ActuatorCommandActionDto[] Commands { get; set; }
    }
}