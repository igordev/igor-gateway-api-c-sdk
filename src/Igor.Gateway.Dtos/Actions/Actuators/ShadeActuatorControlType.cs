﻿namespace Igor.Gateway.Dtos.Actions.Actuators
{
    public enum ShadeActuatorControlType
    {
        Set = 0,
        Increment = 1,
        Decrement = 2
    }
}
