﻿namespace Igor.Gateway.Dtos.Actions.Actuators
{
    public class CommandParameterDto
    {
        public string ParameterType { get; set; }

        public object Value { get; set; }
    }
}
