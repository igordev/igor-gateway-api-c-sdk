﻿using Igor.Gateway.Dtos.Actuators;

namespace Igor.Gateway.Dtos.Actions.Actuators
{
    public class CommandActuatorsCollectionsActionParametersDto
    {
        public ActuatorSubtype Subtype { get; set; }

        public ActuatorCommandCollectionActionDto[] Collection { get; set; }

        public int[] ActuatorIds { get; set; }
        public bool UseAssignedSpace { get; set; }
        public int[] SpaceIds { get; set; }
        public int[] SpaceGroupIds { get; set; }    
    }
}