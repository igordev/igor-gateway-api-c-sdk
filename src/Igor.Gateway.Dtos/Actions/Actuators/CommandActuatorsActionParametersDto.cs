﻿using Igor.Gateway.Dtos.Actuators;
using Newtonsoft.Json.Linq;

namespace Igor.Gateway.Dtos.Actions.Actuators
{
    public class CommandActuatorsActionParametersDto
    {
        public ActuatorSubtype Subtype { get; set; }

        public object Command { get; set; }

        public T GetCommand<T>() where T : IActuatorCommandDto
        {
            switch (Command)
            {
                case T variable:
                    return variable;
                case JObject jObject:
                    return jObject.ToObject<T>();
                default:
                    return default;
            }
        }

        public int[] ActuatorIds { get; set; }
        public bool UseAssignedSpace { get; set; }
        public int[] SpaceIds { get; set; }
        public int[] SpaceGroupIds { get; set; }    
    }
}