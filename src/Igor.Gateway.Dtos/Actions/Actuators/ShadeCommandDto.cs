﻿using Igor.Gateway.Dtos.Actuators;

namespace Igor.Gateway.Dtos.Actions.Actuators
{
    public class ShadeCommandDto : IActuatorCommandDto
    {
        public CommandType CommandType { get; set; }

        public ShadeActuatorControlType ControlType { get; set; }

        public string Value { get; set; }
    }
}