﻿using Igor.Gateway.Dtos.ActionSets;

namespace Igor.Gateway.Dtos.Actions
{
    public class CancelActionParametersDto
    {
        public CancelType? CancelType { get; set; }
        public int[] ActionSetIds { get; set; }
    }
}
