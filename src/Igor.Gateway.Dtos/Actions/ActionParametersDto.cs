﻿using Igor.Gateway.Dtos.Lighting;

namespace Igor.Gateway.Dtos.Actions
{
    public class ActionParametersDto
    {
        public CurveType? CurveType { get; set; }
        public Behavior? Behavior { get; set; }
        public int? Duration { get; set; }
        public bool UseAssignedSpace { get; set; }
        public int[] SpaceIds { get; set; }
        public int[] LightIds { get; set; }
        public int[] SpaceGroupIds { get; set; }      
    }
}
