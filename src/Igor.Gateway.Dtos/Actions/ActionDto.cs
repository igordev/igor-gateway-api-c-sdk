﻿using System;
using Newtonsoft.Json.Linq;

namespace Igor.Gateway.Dtos.Actions
{
    public class ActionDto
    {
        public int Id { get; set; }
        public ActionType Type { get; set; }
        public int? Order { get; set; }
        public int? Delay { get; set; }
        public int? PostDelay { get; set; }

        public string Name { get; set; }

        public object Parameters { get; set; }

        public T GetParameters<T>()
        {
            if (Parameters is T variable)
                return variable;

            if (Parameters is JObject jObject)
                return jObject.ToObject<T>();

            return default(T);
        }

        public bool TryGetParameters<T>(out T parameters)
        {
            try
            {
                switch (Parameters)
                {
                    case T variable:
                        parameters = variable;
                        break;
                    case JObject jObject:
                        parameters = jObject.ToObject<T>();
                        break;
                    default:
                        parameters = default(T);
                        return false;
                }

                return true;
            }
            catch (Exception)
            {
                parameters = default(T);
                return false;
            }
        }
    }
}