﻿namespace Igor.Gateway.Dtos.Actions
{
    public class IncrementActionParametersDto : ActionParametersDto
    {
        public int? Level { get; set; }
        public int? CCT { get; set; }
        public bool? CanTurnOn { get; set; }
        public TurnOnFrom? TurnOnFrom { get; set; }
    }
}