﻿namespace Igor.Gateway.Dtos.Actions
{
    public enum TurnOnFrom
    {
        Zero = 0,
        LastLevel = 1
    }
}