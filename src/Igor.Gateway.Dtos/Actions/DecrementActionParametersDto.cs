﻿namespace Igor.Gateway.Dtos.Actions
{
    public class DecrementActionParametersDto : ActionParametersDto
    {
        public int? Level { get; set; }
        public int? CCT { get; set; }
        public bool? CanTurnOff { get; set; }
    }
}