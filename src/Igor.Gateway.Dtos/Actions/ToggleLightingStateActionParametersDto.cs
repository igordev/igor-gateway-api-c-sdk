﻿using System;
using System.Collections.Generic;
using System.Text;
using Igor.Gateway.Dtos.Lighting;

namespace Igor.Gateway.Dtos.Actions
{
    public class ToggleLightingStateActionParametersDto : ActionParametersDto
    {
        //public CurveType? CurveType { get; set; }
        //public Behavior? Behavior { get; set; }
        //public int? Duration { get; set; }
        public int[] RelayIds { get; set; }
    }
}
