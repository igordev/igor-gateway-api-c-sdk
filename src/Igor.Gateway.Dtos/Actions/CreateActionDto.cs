﻿
namespace Igor.Gateway.Dtos.Actions
{
    /// <summary>
    /// The action
    /// </summary>
    public class CreateActionDto<T> : UpdateActionDto<T>
    {
        public static implicit operator ActionDto(CreateActionDto<T> dto)
        {
            return dto?.AsActionDto();
        }
    }
}
