﻿using Igor.Gateway.Dtos.Automations;

namespace Igor.Gateway.Dtos.Actions
{
    public class ResumeAutomationActionParametersDto : ActionParametersDto
    {
        /// <summary>
        /// The type of automation to resume.
        /// </summary>
        public AutomationType? AutomationType { get; set; }
    }
}
