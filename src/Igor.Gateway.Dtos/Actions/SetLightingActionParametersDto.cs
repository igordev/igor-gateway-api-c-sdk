﻿using Igor.Gateway.Dtos.Common;

namespace Igor.Gateway.Dtos.Actions
{
    public class SetLightingActionParametersDto : ActionParametersDto
    {
        public OnOffState? State { get; set; }
        public int? Level { get; set; }
        public int? CCT { get; set; }
    }
}