﻿namespace Igor.Gateway.Dtos.Actions
{
    /// <summary>
    /// The action
    /// </summary>
    public class UpdateActionDto<T>
    {
        /// <summary>
        /// The action type
        /// </summary>
        
        public ActionType Type { get; set; }

        public T Parameters { get; set; }

        public int? Delay { get; set; }
        public int? PostDelay { get; set; }
        public int? Order { get; set; }
        
        public ActionDto AsActionDto()
        {
            return new ActionDto
            {
                Type = Type,
                Order = Order,
                Delay = Delay,
                PostDelay = PostDelay,
                Parameters = Parameters
            };
        }

        public static implicit operator ActionDto(UpdateActionDto<T> dto)
        {
            return dto?.AsActionDto();
        }
    }
}
