﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Igor.Gateway.Dtos.Actions
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ActionType
    {
        SetLighting = 3,
        ToggleLightingState = 4, 
        Increment = 5,
        Decrement = 6,
        Cancel = 7,
        PauseAutomation = 8,
        ResumeAutomation = 9,
        CommandActuators = 10,
        CommandActuatorsExtended = 11
    }
}
