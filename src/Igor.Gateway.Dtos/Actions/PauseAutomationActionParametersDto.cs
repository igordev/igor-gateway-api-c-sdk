﻿using Igor.Gateway.Dtos.Automations;

namespace Igor.Gateway.Dtos.Actions
{
    public class PauseAutomationActionParametersDto : ActionParametersDto
    {
        /// <summary>
        /// The type of automation to pause
        /// </summary>
        public AutomationType? AutomationType { get; set; }

        /// <summary>
        /// The resume type
        /// </summary>
        public int? ResumeType { get; set; }

        /// <summary>
        /// The timeout in milliseconds the paused automations will resume
        /// </summary>
        public uint? ExpirationTimeout { get; set; }
        
        /// <summary>
        /// The automations to pause
        /// </summary>
        public AutomationDto[] Automations { get; set; }
    }
}
