﻿namespace Igor.Gateway.Dtos.Commisioning
{
    public class CommissioningDto
    {
        public int UnassignedLights { get; set; }
        public int RemainingLights { get; set; }
        public decimal PercentComplete { get; set; }
        public bool IsEveryLightAssigned { get; set; }
    }
}
