﻿using Igor.Gateway.Dtos.Devices;

namespace Igor.Gateway.Dtos.Commisioning
{
    public class CommissioningStartDto
    {
        public IncludeAttachedDevices IncludeAttachedDevices { get; set; }
    }
}
