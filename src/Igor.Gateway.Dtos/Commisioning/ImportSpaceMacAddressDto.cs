﻿using System.Collections.Generic;

namespace Igor.Gateway.Dtos.Commisioning
{
    public class ImportSpaceMacAddressDto
    {
        public List<string> MacAddresses { get; set; }
        public bool ApplyToUnassignedDevices { get; set; }
        public bool ForceReassignment { get; set; }
        public bool UpdateExistingSpaceMacAddresses { get; set; }
    }
}
