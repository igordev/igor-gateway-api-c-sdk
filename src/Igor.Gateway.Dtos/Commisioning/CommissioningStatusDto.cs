﻿using System.Collections.Generic;
using Igor.Gateway.Dtos.Devices;

namespace Igor.Gateway.Dtos.Commisioning
{
    public class CommissioningStatusDto
    {
        public CommissioningStatusDto()
        {
            FailedToFind = false;
        }

        public bool FailedToFind { get; set; }
        public int UnassignedLights { get; set; }
        public int RemainingLightsToIdentify { get; set; }
        public IEnumerable<DeviceDto> DevicesAdded { get; set; }
    }
}
