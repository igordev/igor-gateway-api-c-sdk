﻿namespace Igor.Gateway.Dtos
{
    public enum NodeType
    {
        NetworkNode,
        DeviceNode
    }
}