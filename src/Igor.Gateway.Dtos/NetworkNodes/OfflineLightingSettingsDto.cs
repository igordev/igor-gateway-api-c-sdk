﻿namespace Igor.Gateway.Dtos.NetworkNodes
{
    public class OfflineLightingSettingsDto
    {
        /// <summary>
        /// The timeout in seconds (120-3600) before the node goes to offline mode (0=reset to defaults)
        /// </summary>
        public int? OfflineTimeout { get; set; }

        /// <summary>
        /// The light level during offline mode
        /// </summary>
        public int? OfflineLevel { get; set; }
    }
}
