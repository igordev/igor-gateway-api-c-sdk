﻿namespace Igor.Gateway.Dtos.NetworkNodes
{
    /// <summary>
    /// The network node
    /// </summary>
    public class CreateNetworkNodeDto : CreateDto
    {
        /// <summary>
        /// The external system's ID for this network node
        /// </summary>
        public string ExternalId { get; set; }
    }
}
