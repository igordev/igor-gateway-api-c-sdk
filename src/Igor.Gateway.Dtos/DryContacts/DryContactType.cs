﻿namespace Igor.Gateway.Dtos.DryContacts
{
    public enum DryContactType
    {
        DryContact = 0,
        BedSensor = 1,
        FlowSwitch = 2,
        WaterDetectionSensor = 3,
        DoorSensor = 4,
        None = int.MaxValue
    }
}
