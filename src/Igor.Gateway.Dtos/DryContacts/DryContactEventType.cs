﻿namespace Igor.Gateway.Dtos.DryContacts
{
    public enum DryContactEventType
    {
        Opened = 0,
        Closed = 1
    }
}
