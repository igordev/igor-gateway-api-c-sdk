﻿namespace Igor.Gateway.Dtos.DryContacts
{
    /// <summary>
    /// The dry contact
    /// </summary>
    public class CreateDryContactDto : CreateDeviceDto
    {
        /// <summary>
        /// The external system's ID for this dry contact
        /// </summary>
        public string ExternalId { get; set; }

        /// <summary>
        /// The type of dry contact (Contact, Bed)
        /// </summary>
        public DryContactType DryContactType { get; set; }
    }
}
