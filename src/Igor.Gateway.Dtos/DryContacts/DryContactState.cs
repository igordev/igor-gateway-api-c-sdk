﻿namespace Igor.Gateway.Dtos.DryContacts
{
    public enum DryContactState
    {
        Open,
        Closed
    }
}
