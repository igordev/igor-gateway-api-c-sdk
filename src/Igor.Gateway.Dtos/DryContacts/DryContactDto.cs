﻿using System;
using System.Collections.Generic;
using Igor.Gateway.Dtos.Common;
using Igor.Gateway.Dtos.Devices;
using Igor.Gateway.Dtos.Tags;

namespace Igor.Gateway.Dtos.DryContacts
{
    public class DryContactDto : IDeviceDto, IEntityDto, ITagDto
    {
        public int Id { get; set; }
        
        public string Name { get; set; }

        public DryContactType DryContactType { get; set; }
        public string Properties { get; set; }
        public string SystemProperties { get; set; }
        public string Protocol { get; set; }
        public string ExternalId { get; set; }
        public DateTime DiscoveredDate { get; set; }
        public bool IsOnline { get; set; }
        public DateTime? IsOnlineChanged { get; set; }
        public bool IsQuarantined { get; set; }
        public DateTime? IsQuarantinedChanged { get; set; }
        public int? NetworkNodeId { get; set; }
        public int? DeviceNodeId { get; set; }
        public int? SpaceId { get; set; }
        public string State { get; set; }
        public IEnumerable<TagDto> Tags { get; set; }
    }
}
