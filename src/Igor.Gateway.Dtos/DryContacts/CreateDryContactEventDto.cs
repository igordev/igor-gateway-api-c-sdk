﻿namespace Igor.Gateway.Dtos.DryContacts
{
    /// <summary>
    /// The dry contact event
    /// </summary>
    public class CreateDryContactEventDto
    {
        /// <summary>
        /// The dry contact event type
        /// </summary>
        public DryContactEventType EventType { get; set; }
    }
}
