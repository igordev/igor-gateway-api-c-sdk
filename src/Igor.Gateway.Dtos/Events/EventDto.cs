﻿using System;

namespace Igor.Gateway.Dtos.Events
{
    public class EventDto
    {
        public long Id { get; set; }

        public DateTime DateTime { get; set; }

        public string Type { get; set; }

        public string EntityType { get; set; }

        public int EntityId { get; set; }

        public string Value { get; set; }
    }
}
