﻿namespace Igor.Gateway.Dtos.SpaceTypes
{
    public class SpaceTypeDto
    {
        /// <summary>
        /// The space type ID
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The space type name
        /// </summary>
        public string Name { get; set; }
    }
}
