﻿namespace Igor.Gateway.Dtos.Messaging
{
    public enum MessageSeverity
    {
        Low = 0,
        Medium = 1,
        High = 2,
        None = 3
    }
}