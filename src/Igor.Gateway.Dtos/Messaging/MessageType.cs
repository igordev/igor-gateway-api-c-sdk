﻿namespace Igor.Gateway.Dtos.Messaging
{
    public enum MessageType
    {
        Unknown = 0,
        //system messages
        SystemLowDiskSpace = 1,
        SystemLogQueueFull = 2,
        SystemApipaNodesFound = 3,

        //device messages
        DeviceQuarantined = 100,
        DeviceOffline = 101,

        //network messages
        NetworkStaticIpNotFound = 200,
        NetworkInvalidIpAddress = 201,
        NetworkInternetConnectionNotAvailable = 202,
        
        //licensing messages
        LicensingInvalidLicense = 300,
        
        //node messages
        NodeOffline = 400,

        //500 thru 999 reserved for firmware messages
        FirmwareMaxSubscribersReached = 500,
        FirmwareDeviceLimitExceeded = 501,
        FirmwareChainLimitExceeded = 502,
        FirmwareChainUpdateIssue = 503,
        FirmwareLinearLightConnectionIssue = 504,
        FirmwareInvalidWallControlConfiguration = 505,
        FirmwareConfigBadCrc = 506,
        FirmwareConfigMissingFields = 507,
        FirmwareConfigLockedDevice = 508,
        FirmwareConfigLocked = 509,
        FirmwareConfigUnsupportedVersion = 510,
        FirmwareConfigMaxSizeExceeded = 511,
        FirmwareConfigWriteError = 512,
        FirmwareLldpPowerGranted = 513,
        FirmwareUsbError = 514,
        FirmwareIvaniBadChecksum = 515,
        FirmwareIvaniBadSensorChecksum = 516,
        FirmwareIvaniRxBufferOverflow = 517,
        FirmwareDownstreamNoResponse = 518,
        FirmwareCvEnableGoodSignalError = 519,
        FirmwareCvLedCurrentLimitExceeded = 520
    }
}