﻿using System;

namespace Igor.Gateway.Dtos.Messaging
{
    public class MessageDto
    {
        public long Id { get; set; }

        public DateTime Created { get; set; }

        public DateTime LastReceived { get; set; }

        public MessageSeverity Severity { get; set; }

        public string EntityType { get; set; }

        public int? EntityId { get; set; }

        public MessageType Type { get; set; }

        public string Value { get; set; }

        public string Description { get; set; }

        public bool UserDeletable { get; set; }
    }
}