﻿using System;
using System.Collections.Generic;
using Igor.Gateway.Dtos.Common;
using Igor.Gateway.Dtos.Devices;
using Igor.Gateway.Dtos.Tags;

namespace Igor.Gateway.Dtos.DeviceNodes
{
    public class DeviceNodeDto : IEntityDto, IPropertiesDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Protocol { get; set; }

        public string ExternalId { get; set; }

        public DateTime DiscoveredDate { get; set; }

        public bool IsOnline { get; set; }
        public DateTime? IsOnlineChanged { get; set; }

        public string Properties { get; set; }
        public string SystemProperties { get; set; }

        public int? NetworkNodeId { get; set; }
        public int? UpstreamDeviceNodeId { get; set; }
        public int? DownstreamDeviceNodeId { get; set; }

        public IEnumerable<TagDto> Tags { get; set; }
    }
}
