﻿namespace Igor.Gateway.Dtos.DeviceNodes
{
    /// <summary>
    /// The device node
    /// </summary>
    public class CreateDeviceNodeDto : CreateDto
    {
        /// <summary>
        /// The external system's ID for this device node
        /// </summary>
        public string ExternalId { get; set; }

        /// <summary>
        /// The network node id
        /// </summary>
        public int? NetworkNodeId { get; set; }

        /// <summary>
        /// The upstream device node id
        /// </summary>
        public int? UpstreamDeviceNodeId { get; set; }

    }
}
