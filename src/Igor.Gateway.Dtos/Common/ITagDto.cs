﻿using System.Collections.Generic;
using Igor.Gateway.Dtos.Tags;

namespace Igor.Gateway.Dtos.Common
{
    public interface ITagDto
    {
        IEnumerable<TagDto> Tags { get; set; }
    }
}