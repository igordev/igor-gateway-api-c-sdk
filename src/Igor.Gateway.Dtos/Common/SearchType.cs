﻿namespace Igor.Gateway.Dtos.Common
{
    public enum SearchType
    {
        PartialWords,
        WholeWords,
        ExactPhrase
    }
}

/*
    SEARCH TYPE    SINGLE-TERM QUERY    QUERY
    --------------------------------------------------------------
    Partial Words   term1*              term1* AND term2*
    Whole Words     term1               term1 AND term2
    Exact Phrase    "term1"             "term1 term2"
*/