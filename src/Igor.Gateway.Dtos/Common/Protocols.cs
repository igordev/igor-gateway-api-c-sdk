﻿namespace Igor.Gateway.Dtos.Common
{
    public static class Protocols
    {
        public const string External = "external";
        public const string IgorNative = "igor-native";
    }
}
