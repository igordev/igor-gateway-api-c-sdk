﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Igor.Gateway.Dtos.Common
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum OpenClosedState
    {
        Open = 0,
        Closed = 1
    }
}
