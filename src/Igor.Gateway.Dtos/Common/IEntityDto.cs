﻿namespace Igor.Gateway.Dtos.Common
{
    public interface IEntityDto
    {
        int Id { get; }
    }
}
