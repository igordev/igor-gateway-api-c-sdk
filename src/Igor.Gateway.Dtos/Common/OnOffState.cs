﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Igor.Gateway.Dtos.Common
{
    public enum OnOffState
    {
        Off = 0,
        On = 1
    }
}
