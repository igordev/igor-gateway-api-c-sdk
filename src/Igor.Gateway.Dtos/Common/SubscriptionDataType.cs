﻿namespace Igor.Gateway.Dtos.Common
{
    public enum SubscriptionDataType : byte
    {
        All = 0,
        PassthroughOnly = 1,
        NonPassthrough = 2
    }
}