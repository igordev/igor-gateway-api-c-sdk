﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Igor.Gateway.Dtos.Common
{
    public class NameValueDto<T>
    {
        public string Name { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public T Value { get; set; }
    }
}
