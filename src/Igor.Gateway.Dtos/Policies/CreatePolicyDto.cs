﻿
namespace Igor.Gateway.Dtos.Policies
{
    /// <summary>
    /// The policy
    /// </summary>
    public class CreatePolicyDto
    {
        /// <summary>
        /// The minimum light level
        /// </summary>
        public int? MinimumLightLevel { get; set; }

        /// <summary>
        /// The maximum light level
        /// </summary>
        public int? MaximumLightLevel { get; set; }
        
        /// <summary>
        /// The occupancy timeout
        /// </summary>
        public int? OccupancyTimeout { get; set; }

        /// <summary>
        /// The occupant warning time
        /// </summary>
        public int? OccupantWarningTime { get; set; }
    }
}
