﻿namespace Igor.Gateway.Dtos.Policies
{
    public class ActivePolicyValueDto
    {
        public string Name { get; set; }

        public int? Value { get; set; }

        public int PolicyId { get; set; }

        public string PolicyType { get; set; }

        public string PolicyName { get; set; }
    }
}
