﻿namespace Igor.Gateway.Dtos.Policies
{
    public class ActivePolicyDto
    {
        public int MinimumLightLevel { get; set; }

        public int MaximumLightLevel { get; set; }
        
        public int OccupancyTimeout { get; set; }
        
        public int OccupantWarningTime { get; set; }
    }
}
