﻿namespace Igor.Gateway.Dtos.Policies
{
    public class PolicyDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }

        public int? MinimumLightLevel { get; set; }

        public int? MaximumLightLevel { get; set; }
        
        public int? OccupancyTimeout { get; set; }
        
        public int? OccupantWarningTime { get; set; }

        public int? SpaceTypeId { get; set; }
    }
}
