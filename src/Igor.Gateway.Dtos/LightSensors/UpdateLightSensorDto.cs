﻿namespace Igor.Gateway.Dtos.LightSensors
{
    /// <summary>
    /// The light sensor
    /// </summary>
    public class UpdateLightSensorDto : UpdateDeviceDto
    {
        /// <summary>
        /// The minimum sensor level
        /// </summary>
        public int MinSensorLevel { get; set; }

        /// <summary>
        /// The maximum sensor level
        /// </summary>
        public int MaxSensorLevel { get; set; }

        /// <summary>
        /// The minimum illuminance
        /// </summary>
        public int MinIlluminance { get; set; }

        /// <summary>
        /// The maximum illuminance
        /// </summary>
        public int MaxIlluminance { get; set; }
    }
}
