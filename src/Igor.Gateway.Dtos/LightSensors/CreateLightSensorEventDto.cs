﻿namespace Igor.Gateway.Dtos.LightSensors
{
    /// <summary>
    /// The light sensor event
    /// </summary>
    public class CreateLightSensorEventDto
    {
        /// <summary>
        /// The sensor level
        /// </summary>
        public int SensorLevel { get; set; }
    }
}
