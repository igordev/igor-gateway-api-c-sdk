﻿namespace Igor.Gateway.Dtos
{
    public class UpdateDto
    {
        /// <summary>
        /// The device name
        /// </summary>
        
        public string Name { get; set; }

        /// <summary>
        /// The external system's device properties
        /// </summary>
        public string Properties { get; set; }

        /// <summary>
        /// The device's system properties
        /// </summary>
        public string SystemProperties { get; set; }
    }
}
