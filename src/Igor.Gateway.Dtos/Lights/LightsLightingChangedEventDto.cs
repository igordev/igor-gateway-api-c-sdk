﻿using Igor.Gateway.Dtos.Lighting;

namespace Igor.Gateway.Dtos.Lights
{
    public class LightsLightingChangedEventDto
    {
        public int EntityId { get; set; }
        public string ExternalId { get; set; }
        public LightingDto Lighting { get; set; }
    }
}
