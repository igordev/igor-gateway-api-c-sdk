﻿namespace Igor.Gateway.Dtos.Lights
{
    /// <summary>
    /// The light
    /// </summary>
    public class CreateLightDto : CreateDeviceDto
    {
        /// <summary>
        /// The external system's ID for this light
        /// </summary>
        public string ExternalId { get; set; }

        /// <summary>
        /// The type of light (Individual, Tunable, Rgb)
        /// </summary>
        public LightType LightType { get; set; }
        
        /// <summary>
        /// The minimum Kelvin value of the light or null
        /// </summary>
        public int? MinimumCCT { get; set; }
        
        /// <summary>
        /// The maximum Kelvin value of the light or null
        /// </summary>
        public int? MaximumCCT { get; set; }

        /// <summary>
        /// The minimum light level (0-10000)
        /// </summary>
        public int? MinLevel { get; set; }

        /// <summary>
        /// The maximum light level (0-10000)
        /// </summary>
        public int? MaxLevel { get; set; }
    }
}
