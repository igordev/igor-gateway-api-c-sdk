﻿namespace Igor.Gateway.Dtos.Lights
{
    public static class LightDtoExtensions
    {
        public static UpdateLightDto ToUpdateDto(this LightDto lightDto)
        {
            return new UpdateLightDto
            {
                Name = lightDto.Name,
                MinLevel = lightDto.MinLevel,
                MaxLevel = lightDto.MaxLevel,
                MinimumCCT = lightDto.MinimumCCT,
                MaximumCCT = lightDto.MaximumCCT,
                Properties = lightDto.Properties,
                SystemProperties = lightDto.SystemProperties,
            };
        }
    }
}