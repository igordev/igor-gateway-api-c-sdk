﻿using Igor.Gateway.Dtos.Common;

namespace Igor.Gateway.Dtos.Lights
{
    public class LightLightingChangedEventDto
    {
        public int Level { get; set; }

        public int CCT { get; set; }

        public int Behavior { get; set; }

        public int CurveType { get; set; }

        public int Duration { get; set; }
        
        public OnOffState State { get; set; }
    }
}
