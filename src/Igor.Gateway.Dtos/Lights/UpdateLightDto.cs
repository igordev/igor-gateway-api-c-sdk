﻿namespace Igor.Gateway.Dtos.Lights
{
    /// <summary>
    /// The light
    /// </summary>
    public class UpdateLightDto : UpdateDeviceDto
    {
        /// <summary>
        /// The minimum light level (0-10000)
        /// </summary>
        public int? MinLevel { get; set; }

        /// <summary>
        /// The maximum light level (0-10000)
        /// </summary>
        public int? MaxLevel { get; set; }
        
        /// <summary>
        /// The minimum Kelvin value of the light or null
        /// </summary>
        public int? MinimumCCT { get; set; }
        
        /// <summary>
        /// The maximum Kelvin value of the light or null
        /// </summary>
        public int? MaximumCCT { get; set; }
    }
}
