﻿using System;
using System.Collections.Generic;
using System.Linq;
using Igor.Gateway.Dtos.Common;
using Igor.Gateway.Dtos.Lighting;

namespace Igor.Gateway.Dtos.Lights
{
    public class BatchLightingListDto
    {
        /// <summary>
        /// The list of lighting parameters
        /// </summary>
        public List<BatchLightingDto> List { get; set; }

        public BatchLightingListDto()
        {
            List = new List<BatchLightingDto>();
        }
        
        public BatchLightingListDto(IEnumerable<BatchLightingDto> batchLightingDtos)
        {
            List = batchLightingDtos.ToList();
        }
    }

    public class BatchLightingDto : LightingDto
    {
        /// <summary>
        /// The light IDs
        /// </summary>
        public IEnumerable<int> Ids { get; set; }

        /// <summary>
        /// The light ID
        /// </summary>
        [Obsolete]
        public int Id { get; set; }

        public BatchLightingDto()
        {
            Id = default;
            Ids = default;
        }

        [Obsolete]
        public BatchLightingDto(int id = default(int), int? level = null, int? cct = null, Behavior? behavior = null, CurveType? curveType = null, int? duration = null, OnOffState? state = null)
            : base(level, cct, behavior, curveType, duration, state)
        {
            Id = id;
        }

        public BatchLightingDto(IEnumerable<int> ids = default, int? level = null, int? cct = null, Behavior? behavior = null, CurveType? curveType = null, int? duration = null, OnOffState? state = null) 
            : base(level, cct, behavior, curveType, duration, state)
        {
            Ids = ids;
        }
    }
}