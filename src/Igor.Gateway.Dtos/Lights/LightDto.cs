﻿using System;
using System.Collections.Generic;
using Igor.Gateway.Dtos.Common;
using Igor.Gateway.Dtos.Devices;
using Igor.Gateway.Dtos.Tags;

namespace Igor.Gateway.Dtos.Lights
{
    public class LightDto : IDeviceDto, IEntityDto, ITagDto
    {
        public int Id { get; set; }
        
        public string Name { get; set; }

        public string Protocol { get; set; }

        public string ExternalId { get; set; }

        public DateTime DiscoveredDate { get; set; }

        public bool IsOnline { get; set; }
        public DateTime? IsOnlineChanged { get; set; }
        public bool IsQuarantined { get; set; }
        public DateTime? IsQuarantinedChanged { get; set; }

        public OnOffState State { get; set; }

        /// <summary>
        /// The current light level.
        /// </summary>
        public int Level { get; set; }

        public int? MinLevel { get; set; }

        public int? MaxLevel { get; set; }

        public bool IsEmergency { get; set; }

        public int? EmergencyTimeout { get; set; }

        public int? EmergencyLightLevel { get; set; }

        public LightType LightType { get; set; }
        
        public int? MinimumCCT { get; set; }
        public int? MaximumCCT { get; set; }
        public int CCT { get; set; }

        public string Properties { get; set; }
        public string SystemProperties { get; set; }

        public int? NetworkNodeId { get; set; }

        public int? DeviceNodeId { get; set; }

        public int? SpaceId { get; set; }
        public IEnumerable<TagDto> Tags { get; set; }

        public static implicit operator UpdateLightDto(LightDto dto)
        {
            return dto?.ToUpdateDto();
        }
    }
}
