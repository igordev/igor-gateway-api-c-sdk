﻿namespace Igor.Gateway.Dtos.Lights
{
    public enum LightType
    {
        Individual = 0,
        Tunable = 1,
        Rgb = 2,
        Rgbw = 3,
        None = int.MaxValue
    }
}
