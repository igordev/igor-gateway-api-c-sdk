﻿namespace Igor.Gateway.Dtos.Dashboards
{
    public class DashboardDeviceStatusByDeviceTypeDto
    {
        public string DeviceType { get; set; }

        public string DeviceTypeSingular => string.IsNullOrEmpty(DeviceType) ? null : DeviceType.TrimEnd('s');

        public int OnlineCount { get; set; }
        public int OfflineCount { get; set; }
        public int QuarantinedCount { get; set; }
        public int Total => OnlineCount + OfflineCount;
    }
}
