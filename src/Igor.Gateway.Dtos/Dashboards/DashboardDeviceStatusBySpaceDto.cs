﻿namespace Igor.Gateway.Dtos.Dashboards
{
    public class DashboardDeviceStatusBySpaceDto
    {
        public int? SpaceId { get; set; }
        public string SpaceName { get; set; }
        public int OnlineCount { get; set; }
        public int OfflineCount { get; set; }
        public int Total => OnlineCount + OfflineCount;
    }
}
