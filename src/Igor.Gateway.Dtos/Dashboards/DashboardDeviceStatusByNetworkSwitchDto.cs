﻿namespace Igor.Gateway.Dtos.Dashboards
{
    public class DashboardDeviceStatusByNetworkSwitchDto
    {
        public string NetworkSwitchName { get; set; }
        public string NetworkSwitchIpAddress { get; set; }
        public string NetworkSwitchMacAddress { get; set; }
        public int OnlineCount { get; set; }
        public int OfflineCount { get; set; }
        public int Total => OnlineCount + OfflineCount;
    }
}
