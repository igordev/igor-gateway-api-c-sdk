﻿using System;

namespace Igor.Gateway.Dtos.Dashboards
{
    public class DashboardDeviceActivityByHourDto
    {
        public DateTime DateTime { get; set; }
        public int Count { get; set; }
    }
}
