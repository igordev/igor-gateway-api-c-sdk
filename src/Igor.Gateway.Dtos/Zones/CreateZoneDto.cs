﻿namespace Igor.Gateway.Dtos.Zones
{
    /// <summary>
    /// The zone
    /// </summary>
    public class CreateZoneDto
    {
        /// <summary>
        /// The zone name
        /// </summary>
        public string Name { get; set; }
    }
}
