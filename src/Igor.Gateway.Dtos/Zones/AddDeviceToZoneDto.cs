﻿using Igor.Gateway.Dtos.Devices;

namespace Igor.Gateway.Dtos.Zones
{
    /// <summary>
    /// The device
    /// </summary>
    public class AddDeviceToZoneDto
    {
        /// <summary>
        /// The device ID
        /// </summary>
        public int DeviceId { get; set; }

        /// <summary>
        /// The device type
        /// </summary>
        public DeviceType DeviceType { get; set; }
    }
}
