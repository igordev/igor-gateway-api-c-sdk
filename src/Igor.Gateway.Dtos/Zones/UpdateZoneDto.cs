﻿namespace Igor.Gateway.Dtos.Zones
{
    /// <summary>
    /// The zone
    /// </summary>
    public class UpdateZoneDto
    {
        /// <summary>
        /// The zone name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The setpoint
        /// </summary>
        public double Setpoint { get; set; }

        /// <summary>
        /// The deadband
        /// </summary>
        public double Deadband { get; set; }

        /// <summary>
        /// The bias
        /// </summary>
        public int Bias { get; set; }

        /// <summary>
        /// The gain
        /// </summary>
        public double Gain { get; set; }

        /// <summary>
        /// The time delay
        /// </summary>
        public int TimeDelay { get; set; }

        /// <summary>
        /// The raise dim rate
        /// </summary>
        public double RaiseDimRate { get; set; }

        /// <summary>
        /// The lower dim rate
        /// </summary>
        public double LowerDimRate { get; set; }

        /// <summary>
        /// The dimming floor
        /// </summary>
        public int DimmingFloor { get; set; }
    }
}
