﻿using Igor.Gateway.Dtos.Devices;

namespace Igor.Gateway.Dtos.Zones
{
    public class DeviceZoneDto
    {
        public int ZoneId { get; set; }
        public int DeviceId { get; set; }
        public DeviceType DeviceType { get; set; }
    }
}
