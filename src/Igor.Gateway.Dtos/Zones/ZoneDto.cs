﻿using Igor.Gateway.Dtos.Common;

namespace Igor.Gateway.Dtos.Zones
{
    public class ZoneDto : IEntityDto
    {
        public int Id { get; set; }

        
        public string Name { get; set; }

        public double Setpoint { get; set; }

        public double Deadband { get; set; }

        public int Bias { get; set; }

        public double Gain { get; set; }

        public int TimeDelay { get; set; }

        public double RaiseDimRate { get; set; }

        public double LowerDimRate { get; set; }

        public int Level { get; set; }

        public int DimmingFloor { get; set; }

        public int? SensorLevel { get; set; }

        public int? MinSensorLevel { get; set; }

        public int? MaxSensorLevel { get; set; }

        public int SpaceId { get; set; }
    }
}
