﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Igor.Gateway.Dtos.Automations
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum AutomationType
    {
        Space
    }
}
