﻿namespace Igor.Gateway.Dtos.Automations
{
    public class AutomationDto
    {
        /// <summary>
        /// The entity type.
        /// </summary>
        public string EntityType { get; set; }

        /// <summary>
        /// The entity's event type.
        /// </summary>
        public string EventType { get; set; }
    }
}
