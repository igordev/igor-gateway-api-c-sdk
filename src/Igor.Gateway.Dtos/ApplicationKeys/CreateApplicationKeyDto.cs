﻿
namespace Igor.Gateway.Dtos.ApplicationKeys
{
    /// <summary>
    /// The application key
    /// </summary>
    public class CreateApplicationKeyDto : UpdateApplicationKeyDto
    {
    }
}
