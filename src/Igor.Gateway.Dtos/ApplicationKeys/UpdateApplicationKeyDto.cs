﻿namespace Igor.Gateway.Dtos.ApplicationKeys
{
    /// <summary>
    /// The application key
    /// </summary>
    public class UpdateApplicationKeyDto
    {
        /// <summary>
        /// The application key name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The role id associated with the application key 
        /// </summary>
        public int? RoleId { get; set; }
    }
}
