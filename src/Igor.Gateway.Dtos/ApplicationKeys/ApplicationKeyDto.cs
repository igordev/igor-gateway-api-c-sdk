﻿namespace Igor.Gateway.Dtos.ApplicationKeys
{
    public class ApplicationKeyDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Key { get; set; }
        public int? RoleId { get; set; }
    }
}