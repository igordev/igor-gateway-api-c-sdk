﻿using Igor.Sdk.Common.Queries;

namespace Igor.Gateway.Dtos.SpaceGroups
{
    public class SpaceGroupsSearchParameters : PagingQueryParameters
    {
        private const string SpaceGroupIdsKey = "spaceGroupIds";
        private const string SpaceGroupParentIdsKey = "spaceGroupParentIds";

        public string Term
        {
            get => this["term"];
            set => this["term"] = value;
        }

        public int[] SpaceGroupIds
        {
            get => GetIntArray(SpaceGroupIdsKey);
            set => SetIntArray(SpaceGroupIdsKey, value);
        }

        public int[] SpaceGroupParentIds
        {
            get => GetIntArray(SpaceGroupParentIdsKey);
            set => SetIntArray(SpaceGroupParentIdsKey, value);
        }

        public QuerySortDirection? SortDirection
        {
            get
            {
                if (string.IsNullOrEmpty(this["sortDir"]))
                    return null;

                return this["sortDir"] == "asc" ? QuerySortDirection.Ascending : QuerySortDirection.Descending;
            }
            set
            {
                if (value.HasValue)
                    this["sortDir"] = value.Value == QuerySortDirection.Ascending ? "asc" : "desc";
            }
        }

        public string SortBy
        {
            get => this["sortBy"];
            set => this["sortBy"] = value;
        }

        public string GetSortDirectionAsString()
        {
            if (SortDirection.HasValue)
                return SortDirection.Value == QuerySortDirection.Ascending ? "asc" : "desc";

            return null;
        }

        public string GetSpaceGroupIdsAsString()
        {
            return this[SpaceGroupIdsKey];
        }

        public string GetSpaceGroupParentIdsAsString()
        {
            return this[SpaceGroupParentIdsKey];
        }
    }
}
