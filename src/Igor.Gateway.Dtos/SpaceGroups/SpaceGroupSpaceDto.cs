﻿namespace Igor.Gateway.Dtos.SpaceGroups
{
    public class SpaceGroupSpaceDto
    {
        public int SpaceId { get; set; }
        public int SpaceGroupId { get; set; }
    }
}
