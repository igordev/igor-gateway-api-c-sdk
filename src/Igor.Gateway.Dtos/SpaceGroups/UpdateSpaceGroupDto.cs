﻿namespace Igor.Gateway.Dtos.SpaceGroups
{
    /// <summary>
    /// The space group
    /// </summary>
    public class UpdateSpaceGroupDto
    {
        /// <summary>
        /// The space group name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The id of the parent space group
        /// </summary>
        public int? ParentId { get; set; }
    }
}
