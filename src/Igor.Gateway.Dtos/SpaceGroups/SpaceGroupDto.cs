﻿using System.Collections.Generic;
using Igor.Gateway.Dtos.Common;
using Igor.Gateway.Dtos.Tags;

namespace Igor.Gateway.Dtos.SpaceGroups
{
    public class SpaceGroupDto : IEntityDto, ITagDto
    {
        /// <summary>
        /// The space group ID
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The space group name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The id of the space group's parent
        /// </summary>
        public int? ParentId { get; set; }
        
        /// <summary>
        /// Json string representing the space group's hierarchy
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// The collection of space group tags
        /// </summary>
        public IEnumerable<TagDto> Tags { get; set; }

        public static implicit operator UpdateSpaceGroupDto(SpaceGroupDto dto)
        {
            return dto?.ToUpdateDto();
        }
    }
}
