﻿namespace Igor.Gateway.Dtos.SpaceGroups
{
    public static class SpaceGroupDtoExtensions
    {
        public static UpdateSpaceGroupDto ToUpdateDto(this SpaceGroupDto spaceGroupDto)
        {
            return new UpdateSpaceGroupDto
            {
                Name = spaceGroupDto.Name,
                ParentId = spaceGroupDto.ParentId
            };
        }
    }
}