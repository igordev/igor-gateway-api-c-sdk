﻿namespace Igor.Gateway.Dtos.SpaceGroups
{
    public class SpaceGroupsSearchResultsDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int? ParentId { get; set; }

        public int ChildCount { get; set; }
        public int ChildSpaceCount { get; set; }

        public string Path { get; set; }
    }
}
