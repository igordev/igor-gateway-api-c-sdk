﻿namespace Igor.Gateway.Dtos.Schedules
{
    public class SchedulesSearchResultsDto
    {
        /// <summary>
        /// The schedule ID
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The schedule name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The schedule cron expression
        /// </summary>
        public string CronExpression { get; set; }

        /// <summary>
        /// The schedule action set ID
        /// </summary>
        public int? ActionSetId { get; set; }

        /// <summary>
        /// The schedule action set name
        /// </summary>
        public string ActionSetName { get; set; }

        /// <summary>
        /// The celestial time of day
        /// </summary>
        public CelestialTimeOfDay? CelestialTimeOfDay { get; set; }

        /// <summary>
        /// The offset in minutes from the celestial time of day
        /// </summary>
        public int CelestialOffsetMinutes { get; set; }
    }
}
