﻿namespace Igor.Gateway.Dtos.Schedules
{
    /// <summary>
    /// The schedule
    /// </summary>
    public class UpdateScheduleDto
    {
        /// <summary>
        /// The schedule name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The cron expression
        /// </summary>
        public string CronExpression { get; set; }

        /// <summary>
        /// The celestial times of day
        /// </summary>
        public CelestialTimeOfDay? CelestialTimeOfDay { get; set; }

        /// <summary>
        /// The offset in minutes from the celestial time of day
        /// </summary>
        public int? CelestialOffsetMinutes { get; set; }
    }
}
