﻿using Igor.Sdk.Common.Queries;

namespace Igor.Gateway.Dtos.Schedules
{
    public class SchedulesSearchParameters : PagingQueryParameters
    {
        public string Term
        {
            get => this["term"];
            set => this["term"] = value;
        }

        public QuerySortDirection? SortDirection
        {
            get
            {
                if (string.IsNullOrEmpty(this["sortDir"]))
                    return null;

                return this["sortDir"] == "asc" ? QuerySortDirection.Ascending : QuerySortDirection.Descending;
            }
            set
            {
                if (value.HasValue)
                    this["sortDir"] = value.Value == QuerySortDirection.Ascending ? "asc" : "desc";
            }
        }

        public string SortBy
        {
            get => this["sortBy"];
            set => this["sortBy"] = value;
        }

        public string GetSortDirectionAsString()
        {
            if (SortDirection.HasValue)
                return SortDirection.Value == QuerySortDirection.Ascending ? "asc" : "desc";

            return null;
        }
    }
}
