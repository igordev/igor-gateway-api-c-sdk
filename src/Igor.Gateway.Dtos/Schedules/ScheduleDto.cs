﻿using Igor.Gateway.Dtos.Common;

namespace Igor.Gateway.Dtos.Schedules
{
    public class ScheduleDto : IEntityDto
    {
        public int Id { get; set; }
        
        public string Name { get; set; }
        
        public string CronExpression { get; set; }

        public CelestialTimeOfDay CelestialTimeOfDay { get; set; }

        public int CelestialOffsetMinutes { get; set; }
    }
}
