﻿namespace Igor.Gateway.Dtos.Schedules
{
    /// <summary>
    /// The action set
    /// </summary>
    public class UpdateScheduleActionSetDto
    {
        /// <summary>
        /// The action set ID
        /// </summary>
        public int Id { get; set; }
    }
}
