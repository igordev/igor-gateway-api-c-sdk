﻿namespace Igor.Gateway.Dtos.Schedules
{
    public enum CelestialTimeOfDay
    {
        None = 0,
        Sunrise = 1,
        Sunset = 2,
        MiddleOfDay = 4
    }
}