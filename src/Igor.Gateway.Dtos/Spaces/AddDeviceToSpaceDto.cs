﻿using Igor.Gateway.Dtos.Devices;

namespace Igor.Gateway.Dtos.Spaces
{
    /// <summary>
    /// The device
    /// </summary>
    public class AddDeviceToSpaceDto
    {
        /// <summary>
        /// The device ID
        /// </summary>
        public int DeviceId { get; set; }

        /// <summary>
        /// The device type
        /// </summary>
        public DeviceType DeviceType { get; set; }

        /// <summary>
        /// Specifies whether other attached devices should also be added to the space
        /// </summary>
        public IncludeAttachedDevices IncludeAttachedDevices { get; set; }
    }
}
