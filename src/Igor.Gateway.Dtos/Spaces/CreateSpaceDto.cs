﻿namespace Igor.Gateway.Dtos.Spaces
{
    /// <summary>
    /// The space
    /// </summary>
    public class CreateSpaceDto : UpdateSpaceDto
    {
        /// <summary>
        /// The id of the space group
        /// </summary>
        public int? SpaceGroupId { get; set; }
    }
}
