﻿namespace Igor.Gateway.Dtos.Spaces
{
    public enum SpaceOccupiedState
    {
        Vacant = 0,
        Occupied = 1
    }
}
