﻿using System;
using Igor.Gateway.Dtos.Automations;

namespace Igor.Gateway.Dtos.Spaces
{
    public class PausedSpaceAutomationDto
    {
        /// <summary>
        /// The resume type of the paused space automation
        /// </summary>
        public SpaceAutomationResumeType? ResumeType { get; set; }

        /// <summary>
        /// The date and time when the paused space automation will resume
        /// </summary>
        public DateTime? ResumeDate { get; set; }
        
        /// <summary>
        /// The automations that are paused
        /// </summary>
        public AutomationDto[] Automations { get; set; }
    }
}
