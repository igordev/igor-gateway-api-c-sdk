﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Igor.Gateway.Dtos.Spaces
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum SpaceAutomationSpaceEventType
    {
        OccupancyDetection,
        VacancyDetection
    }
}
