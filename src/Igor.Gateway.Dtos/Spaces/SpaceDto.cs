﻿using System;
using System.Collections.Generic;
using Igor.Gateway.Dtos.Common;
using Igor.Gateway.Dtos.Tags;

namespace Igor.Gateway.Dtos.Spaces
{
    /// <summary>
    /// The space
    /// </summary>
    public class SpaceDto : IEntityDto, ITagDto
    {
        public SpaceDto()
        {
            ControlType = (int)(SpaceControlType.Space | SpaceControlType.SpaceGroup);
        }

        /// <summary>
        /// The space ID
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The space name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The space mode (Occupancy, Vacancy)
        /// </summary>
        public SpaceMode Mode { get; set; }

        /// <summary>
        /// Whether zones are disabled for this space
        /// </summary>
        public bool AreZonesDisabled { get; set; }

        /// <summary>
        /// The space state (On, Off)
        /// </summary>
        public OnOffState State { get; set; }

        /// <summary>
        /// The space level (0-10000)
        /// </summary>
        public int Level { get; set; }
        
        /// <summary>
        /// The color temperature of the space in Kelvin
        /// </summary>
        public int CCT { get; set; }

        /// <summary>
        /// Whether the space has tunable lights
        /// </summary>
        public bool HasTunableLights { get; set; }
        
        /// <summary>
        /// The minimum color temperature in Kelvin
        /// </summary>
        public int MinimumCCT { get; set; }
        
        /// <summary>
        /// The maximum color temperature in Kelvin
        /// </summary>
        public int MaximumCCT { get; set; }

        /// <summary>
        /// The type of control allowed for a space
        /// </summary>
        public int? ControlType { get; set; }

        /// <summary>
        /// The resume type of the paused space automation
        /// </summary>
        public SpaceAutomationResumeType? ResumeType { get; set; }

        /// <summary>
        /// The date and time when the paused space automation will resume
        /// </summary>
        public DateTime? ResumeDate { get; set; }

        /// <summary>
        /// The space occupied state (Vacant, Occupied)
        /// </summary>
        public SpaceOccupiedState OccupiedState { get; set; }

        /// <summary>
        /// The collection of space tags
        /// </summary>
        public IEnumerable<TagDto> Tags { get; set; }

        public static implicit operator UpdateSpaceDto(SpaceDto dto)
        {
            return dto?.ToUpdateDto();
        }
    }
}
