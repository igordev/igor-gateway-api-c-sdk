﻿using Igor.Sdk.Common.Queries;

namespace Igor.Gateway.Dtos.Spaces
{
    public class SpacesSearchParameters : PagingQueryParameters
    {
        private const string SpaceIdsKey = "spaceIds";
        private const string SpaceGroupIdsKey = "spaceGroupIds";

        public string Term
        {
            get => this["term"];
            set => this["term"] = value;
        }

        public int[] SpaceIds
        {
            get => GetIntArray(SpaceIdsKey);
            set => SetIntArray(SpaceIdsKey, value);
        }

        public int[] SpaceGroupIds
        {
            get => GetIntArray(SpaceGroupIdsKey);
            set => SetIntArray(SpaceGroupIdsKey, value);
        }

        public QuerySortDirection? SortDirection
        {
            get
            {
                if (string.IsNullOrEmpty(this["sortDir"]))
                    return null;

                return this["sortDir"] == "asc" ? QuerySortDirection.Ascending : QuerySortDirection.Descending;
            }
            set
            {
                if (value.HasValue)
                    this["sortDir"] = value.Value == QuerySortDirection.Ascending ? "asc" : "desc";
            }
        }

        public string SortBy
        {
            get => this["sortBy"];
            set => this["sortBy"] = value;
        }

        public string GetSortDirectionAsString()
        {
            if (SortDirection.HasValue)
                return SortDirection.Value == QuerySortDirection.Ascending ? "asc" : "desc";

            return null;
        }

        public string GetSpaceIdsAsString()
        {
            return this[SpaceIdsKey];
        }

        public string GetSpaceGroupIdsAsString()
        {
            return this[SpaceGroupIdsKey];
        }
    }
}
