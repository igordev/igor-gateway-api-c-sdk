﻿namespace Igor.Gateway.Dtos.Spaces
{
    /// <summary>
    /// The space
    /// </summary>
    public class UpdateSpaceDto
    {
        /// <summary>
        /// The space name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The space mode
        /// </summary>
        public SpaceMode Mode { get; set; }

        /// <summary>
        /// The type of control allowed for a space
        /// </summary>
        public int? ControlType { get; set; }
    }
}
