﻿namespace Igor.Gateway.Dtos.Spaces
{
    public enum SpaceMode
    {
        Occupancy = 0,
        Vacancy = 1
    }
}
