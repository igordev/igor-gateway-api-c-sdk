﻿using System;

namespace Igor.Gateway.Dtos.Spaces
{
    [Flags]
    public enum SpaceControlType
    {
        None = 0,
        Space = 1,
        SpaceGroup = 2,
        All = (Space | SpaceGroup)
    }
}
