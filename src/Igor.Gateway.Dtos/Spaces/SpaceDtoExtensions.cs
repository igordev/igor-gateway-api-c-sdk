﻿using System;
using System.Data;

namespace Igor.Gateway.Dtos.Spaces
{
    public static class SpaceDtoExtensions
    {
        public static UpdateSpaceDto ToUpdateDto(this SpaceDto spaceDto)
        {
            if (Enum.IsDefined(typeof(SpaceMode), spaceDto.Mode))
                return new UpdateSpaceDto
                {
                    Name = spaceDto.Name,
                    Mode = spaceDto.Mode,
                    ControlType = (int)spaceDto.ControlType
                };
            else
                throw new InvalidOperationException();
        }
    }
}