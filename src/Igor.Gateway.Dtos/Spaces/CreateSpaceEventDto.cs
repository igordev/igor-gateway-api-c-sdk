﻿using Newtonsoft.Json;

namespace Igor.Gateway.Dtos.Spaces
{
    /// <summary>
    /// The space event
    /// </summary>
    public class CreateSpaceEventDto
    {
        /// <summary>
        /// The event name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The event value
        /// </summary>
        public string Value { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
