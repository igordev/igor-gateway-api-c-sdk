﻿using System;
using Igor.Gateway.Dtos.Common;

namespace Igor.Gateway.Dtos.Spaces
{
    public class SpaceMacAddressDto : IEntityDto
    {
        public int Id { get; set; }
        public int SpaceId { get; set; }
        public string MacAddress { get; set; }
    }
}
