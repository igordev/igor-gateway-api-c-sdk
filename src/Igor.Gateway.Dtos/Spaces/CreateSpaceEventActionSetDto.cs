﻿namespace Igor.Gateway.Dtos.Spaces
{
    public class CreateSpaceEventActionSetDto
    {
        public SpaceEventType EventType { get; set; }
    }
}
