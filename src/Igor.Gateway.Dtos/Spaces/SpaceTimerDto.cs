﻿using System;

namespace Igor.Gateway.Dtos.Spaces
{
    public class SpaceTimerDto
    {
        public int SpaceId { get; set; }

        public int TimeOut { get; set; }

        public int ExpiresIn { get; set; }

        public DateTime DateTimeStarted { get; set; }

        public DateTime DateTimeExpires { get; set; }
    }
}
