﻿using System;

namespace Igor.Gateway.Dtos.Spaces
{
    [Flags]
    public enum SpaceAutomationResumeType
    {
        Undefined = 0,
        Resumed = 1,
        Expired = 2,
        SpaceIsTurnedOff = 4
    }
}
