﻿namespace Igor.Gateway.Dtos.Spaces
{
    public enum SpaceEventType
    {
        Occupied = 0,
        Vacant = 1
    }
}
