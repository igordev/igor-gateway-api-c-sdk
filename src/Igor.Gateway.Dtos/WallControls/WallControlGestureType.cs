﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Igor.Gateway.Dtos.WallControls
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum WallControlGestureType
    {
        Press = 0,
        Hold = 1,
        DoublePress = 2
    }
}
