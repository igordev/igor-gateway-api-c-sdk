﻿namespace Igor.Gateway.Dtos.WallControls
{
    public class WallControlButtonDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? ControlCommand { get; set; }
        public int? DimmingCommand { get; set; }
    }
}
