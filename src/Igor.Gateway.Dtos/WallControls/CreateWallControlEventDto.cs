﻿namespace Igor.Gateway.Dtos.WallControls
{
    /// <summary>
    /// The wall control button event
    /// </summary>
    public class CreateWallControlEventDto
    {
        /// <summary>
        /// The button Id
        /// </summary>
        
        public int ButtonId { get; set; }

        /// <summary>
        /// The gesture
        /// </summary>
        
        public WallControlEventType EventType { get; set; }
    }
}
