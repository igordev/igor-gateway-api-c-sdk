﻿namespace Igor.Gateway.Dtos.WallControls
{
    /// <summary>
    /// The wall control gesture
    /// </summary>
    public class UpdateWallControlGestureDto
    {
        /// <summary>
        /// The gesture type
        /// </summary>
        
        public WallControlGestureType Type { get; set; }

        /// <summary>
        /// The action set ID
        /// </summary>
        public int? ActionSetId { get; set; }

        /// <summary>
        /// The hold gesture's release cancel type
        /// </summary>
        public GestureCancelType? HoldGestureCancelType { get; set; }

        /// <summary>
        /// The hold gesture timeout(ms)
        /// </summary>
        public int? HoldGestureTimeout { get; set; }

        public WallControlGestureDto ToDto(int wallControlId, int buttonId, int gestureId)
        {
            return new WallControlGestureDto
            {
                WallControlId = wallControlId,
                ButtonId = buttonId,
                Id = gestureId,
                Type = Type,
                ActionSetId = ActionSetId,
                HoldGestureCancelType = HoldGestureCancelType,
                HoldGestureTimeout = HoldGestureTimeout
            };
        }
    }
}