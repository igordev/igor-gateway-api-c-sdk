﻿namespace Igor.Gateway.Dtos.WallControls
{
    public enum GestureCancelType
    {
        DoNotCancel = 0,
        Immediate = 1,
        EndOfCurrentIteration = 2
    }
}