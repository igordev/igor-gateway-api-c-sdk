﻿namespace Igor.Gateway.Dtos.WallControls
{
    /// <summary>
    /// The wall control gesture
    /// </summary>
    public class CreateWallControlGestureDto
    {
        /// <summary>
        /// The gesture type
        /// </summary>
        
        public WallControlGestureType Type { get; set; }

        /// <summary>
        /// The action set ID
        /// </summary>
        public int? ActionSetId { get; set; }

        /// <summary>
        /// The hold gesture's release cancel type
        /// </summary>
        public GestureCancelType? HoldGestureCancelType { get; set; }

        /// <summary>
        /// The hold gesture timeout(ms)
        /// </summary>
        public int? HoldGestureTimeout { get; set; }

        public WallControlGestureDto ToDto(int wallControlId, int buttonId)
        {
            return new WallControlGestureDto
            {
                WallControlId = wallControlId,
                ButtonId = buttonId,
                Type = Type,
                ActionSetId = ActionSetId,
                HoldGestureCancelType = HoldGestureCancelType,
                HoldGestureTimeout = HoldGestureTimeout
            };
        }
    }
}