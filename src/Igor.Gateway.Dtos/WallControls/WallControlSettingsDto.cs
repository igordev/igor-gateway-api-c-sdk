﻿namespace Igor.Gateway.Dtos.WallControls
{
    public class WallControlSettingsDto
    {
        public WallControlButtonSettingsDto[] WallControlButtonSettings { get; }

        public WallControlSettingsDto(WallControlButtonSettingsDto[] wallControlButtonSettings)
        {
            WallControlButtonSettings = wallControlButtonSettings;
        }
    }
}
