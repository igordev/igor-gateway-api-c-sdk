﻿using System.Collections.Generic;
using System.Linq;
using Igor.Gateway.Dtos.Common;

namespace Igor.Gateway.Dtos.WallControls
{
    /// <summary>
    /// The wall control
    /// </summary>
    public class CreateWallControlDto : CreateDeviceDto
    {
        /// <summary>
        /// The external system's ID for this wall control
        /// </summary>
        
        public string ExternalId { get; set; }

        /// <summary>
        /// The wall control buttons
        /// </summary>
        public List<CreateWallControlButtonDto> Buttons { get; set; }

        public WallControlDto ToDto()
        {
            return new WallControlDto
            {
                Buttons = Buttons?.Select(b => b.ToDto()).ToList() ?? new List<WallControlButtonDto>(),
                ExternalId = ExternalId,
                Properties = Properties,
                SystemProperties = SystemProperties,
                Name = Name,
                Protocol = string.IsNullOrWhiteSpace(Protocol) ? Protocols.External : Protocol,
                NetworkNodeId = NetworkNodeId,
                DeviceNodeId = DeviceNodeId
            };
        }
    }
}
