﻿namespace Igor.Gateway.Dtos.WallControls
{
    public class WallControlButtonSettingsDto
    {
        public int? ButtonId { get; }
        public int? ControlCommand { get; }
        public int? DimmingCommand { get; }

        public WallControlButtonSettingsDto(int? buttonId, int? controlCommand, int? dimmingCommand)
        {
            ButtonId = buttonId;
            ControlCommand = controlCommand;
            DimmingCommand = dimmingCommand;
        }
    }
}
