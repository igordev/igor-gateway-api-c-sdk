﻿namespace Igor.Gateway.Dtos.WallControls
{
    public enum WallControlEventType
    {
        Down = 0,
        Up = 1,
        Press = 2,
        HoldStarted = 3,
        HoldEnded = 4,
        DoublePress = 5
    }
}
