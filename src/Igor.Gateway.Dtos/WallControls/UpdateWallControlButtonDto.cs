﻿namespace Igor.Gateway.Dtos.WallControls
{
    /// <summary>
    /// The wall control button
    /// </summary>
    public class UpdateWallControlButtonDto
    {
        /// <summary>
        /// The button name
        /// </summary>
        
        public string Name { get; set; }

        /// <summary>
        /// The control command
        /// </summary>
        public int? ControlCommand { get; set; }

        /// <summary>
        /// The dimming command
        /// </summary>
        public int? DimmingCommand { get; set; }

        public WallControlButtonDto ToDto()
        {
            return new WallControlButtonDto
            {
                ControlCommand = ControlCommand,
                DimmingCommand = DimmingCommand,
                Name = Name
            };
        }
    }
}
