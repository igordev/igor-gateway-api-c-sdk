﻿namespace Igor.Gateway.Dtos.WallControls
{
    public class WallControlGestureDto
    {
        /// <summary>
        /// The gesture ID
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The wall control ID
        /// </summary>
        public int WallControlId { get; set; }

        /// <summary>
        /// The button ID
        /// </summary>
        public int ButtonId { get; set; }

        /// <summary>
        /// The gesture type
        /// </summary>
        public WallControlGestureType Type { get; set; }

        /// <summary>
        /// The action set ID
        /// </summary>
        public int? ActionSetId { get; set; }

        /// <summary>
        /// The hold gesture's release cancel type
        /// </summary>
        public GestureCancelType? HoldGestureCancelType { get; set; }

        /// <summary>
        /// The hold gesture timeout(ms)
        /// </summary>
        public int? HoldGestureTimeout { get; set; }

    }
}