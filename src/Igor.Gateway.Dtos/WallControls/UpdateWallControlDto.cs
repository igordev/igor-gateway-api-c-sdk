﻿using System.Collections.Generic;
using System.Linq;

namespace Igor.Gateway.Dtos.WallControls
{
    /// <summary>
    /// The wall control
    /// </summary>
    public class UpdateWallControlDto : UpdateDeviceDto
    {
        /// <summary>
        /// The wall control buttons
        /// </summary>
        public List<UpdateWallControlButtonDto> Buttons { get; set; }

        public WallControlDto ToDto(int id)
        {
            return new WallControlDto
            {
                Id = id,
                Buttons = Buttons?.Select(b => b.ToDto()).ToList() ?? new List<WallControlButtonDto>(),
                Name = Name,
                Properties = Properties,
                SystemProperties = SystemProperties
            };
        }
    }
}
