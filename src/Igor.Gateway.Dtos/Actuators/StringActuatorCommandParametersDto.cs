﻿namespace Igor.Gateway.Dtos.Actuators
{
    public class StringActuatorCommandParametersDto
    {
        public string Encoding { get; set; }

        public string Value { get; set; }
    }
}