﻿namespace Igor.Gateway.Dtos.Actuators
{
    public class CreateActuatorEventDto
    {
        public string CommandCollection { get; set; }
        
        public string CommandType { get; set; }
        
        public string Data { get; set; }
    }
}