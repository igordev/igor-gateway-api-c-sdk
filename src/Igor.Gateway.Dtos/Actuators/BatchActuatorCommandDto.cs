﻿using System.Collections.Generic;

namespace Igor.Gateway.Dtos.Actuators
{
    public class BatchActuatorCommandListDto
    {
        public List<BatchActuatorCommandDto> List { get; set; }
    }

    public class BatchActuatorCommandDto : ActuatorCommandCollectionDto
    {
        
        public int Id { get; set; }
    }
}