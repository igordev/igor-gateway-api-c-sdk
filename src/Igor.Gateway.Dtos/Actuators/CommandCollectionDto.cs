﻿using System.Collections.Generic;

namespace Igor.Gateway.Dtos.Actuators
{
    public class CommandCollectionDto
    {
        public CommandCollectionDto()
        {
            
        }
        public CommandCollectionDto(string name)
        {
            Name = name;

            Commands = new List<CommandTypeDto>();
        }

        public string Name { get; set; }

        public IList<CommandTypeDto> Commands { get; set; }
    }

    public class CommandTypeDto
    {
        public CommandTypeDto(string commandType)
        {
            CommandType = commandType;

            Parameters = new List<CommandParameterDto>();
        }

        public string CommandType { get; set; }

        public IList<CommandParameterDto> Parameters { get; set; }
    }

    public class CommandParameterDto
    {
        public CommandParameterDto(string parameterType, object value)
        {
            ParameterType = parameterType;
            Value = value;
        }

        public string ParameterType { get; }

        public object Value { get; }
    }
}