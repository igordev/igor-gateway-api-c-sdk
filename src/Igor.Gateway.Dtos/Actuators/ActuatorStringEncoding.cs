﻿namespace Igor.Gateway.Dtos.Actuators
{
    public enum ActuatorStringEncoding
    {
        Utf8,
        Ascii,
        Unicode
    }
}