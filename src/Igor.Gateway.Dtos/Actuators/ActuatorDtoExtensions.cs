﻿namespace Igor.Gateway.Dtos.Actuators
{
    public static class ActuatorDtoExtensions
    {
        public static UpdateActuatorDto ToUpdateDto(this ActuatorDto actuatorDto)
        {
            return new UpdateActuatorDto
            {
                Name = actuatorDto.Name,
                Properties = actuatorDto.Properties,
                SystemProperties = actuatorDto.SystemProperties,
                Commands = actuatorDto.Commands
            };
        }
    }
}
