﻿using System;
using System.Collections.Generic;
using System.Text;
using Igor.Gateway.Dtos.Common;
using Igor.Gateway.Dtos.Devices;
using Igor.Gateway.Dtos.Tags;

namespace Igor.Gateway.Dtos.Actuators
{
    public class ActuatorDto : IDeviceDto, IEntityDto, ITagDto
    {
        public ActuatorDto()
        {
            Commands = new List<CommandCollectionDto>();
        }

        public int Id { get; set; }

        
        public string Name { get; set; }

        public ActuatorSubtype Subtype { get; set; }
        public string Properties { get; set; }
        public string SystemProperties { get; set; }
        public string Protocol { get; set; }
        public string ExternalId { get; set; }
        public DateTime DiscoveredDate { get; set; }
        public bool IsOnline { get; set; }
        public DateTime? IsOnlineChanged { get; set; }
        public bool IsQuarantined { get; set; }
        public DateTime? IsQuarantinedChanged { get; set; }
        public int? NetworkNodeId { get; set; }
        public int? DeviceNodeId { get; set; }
        public int? SpaceId { get; set; }
        public IEnumerable<TagDto> Tags { get; set; }

        public IList<CommandCollectionDto> Commands { get; set; }

        public static implicit operator UpdateActuatorDto(ActuatorDto dto)
        {
            return dto?.ToUpdateDto();
        }

        public string ToCommandString()
        {
            var sb = new StringBuilder();

            foreach (var commandDto in Commands)
            {
                var key = $"{commandDto.Name}";

                foreach (var commandType in commandDto.Commands)
                {
                    var commandParameterCounter = 1;

                    var commandTypeKey = $"{key}:{commandType.CommandType}";

                    foreach (var commandTypeParameter in commandType.Parameters)
                    {
                        var parameterKey = $"{commandTypeKey}:{commandTypeParameter.ParameterType}";

                        if (commandType.CommandType == "Discrete" && commandType.Parameters.Count > 1)
                        {
                            parameterKey = $"{parameterKey}{commandParameterCounter}";
                            commandParameterCounter++;
                        }

                        sb.Append($"{parameterKey}:{commandTypeParameter.Value}");
                    }
                }
            }

            return sb.ToString();
        }
    }
}