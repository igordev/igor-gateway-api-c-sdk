﻿namespace Igor.Gateway.Dtos.Actuators
{
    public enum CommandType
    {
        Undefined = 0,
        Binary = 1,
        Percentage = 2,
        Discrete = 3,
        Range = 4,
        Passthrough8 = 5,
        Passthrough16 = 6,
        Passthrough32 = 7
    }
}