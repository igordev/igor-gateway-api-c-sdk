﻿namespace Igor.Gateway.Dtos.Actuators
{
    public class ActuatorStateChangedNotification
    {
        public int EntityId { get; set; }
        public string ExternalId { get; set; }
        public string CommandCollectionName { get; set; }
        public string CommandType { get; set; }
        public string Data { get; set; }
    }
}