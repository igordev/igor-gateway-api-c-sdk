﻿using System.Collections.Generic;

namespace Igor.Gateway.Dtos.Actuators
{
    /// <summary>
    /// Actuator
    /// </summary>
    public class CreateActuatorDto : CreateDeviceDto
    {
        public CreateActuatorDto()
        {
            Commands = new List<CommandCollectionDto>();
        }

        /// <summary>
        /// The external system's ID for this actuator
        /// </summary>
        
        public string ExternalId { get; set; }

        /// <summary>
        /// The type of actuator
        /// </summary>
        
        public ActuatorSubtype Subtype { get; set; }

        public IList<CommandCollectionDto> Commands { get; set; }
    }
}
