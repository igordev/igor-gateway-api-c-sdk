﻿using System.Collections.Generic;

namespace Igor.Gateway.Dtos.Actuators
{
    /// <summary>
    /// The actuator
    /// </summary>
    public class UpdateActuatorDto : UpdateDeviceDto
    {
        public UpdateActuatorDto()
        {
            Commands = new List<CommandCollectionDto>();
        }

        public IList<CommandCollectionDto> Commands { get; set; }
    }
}
