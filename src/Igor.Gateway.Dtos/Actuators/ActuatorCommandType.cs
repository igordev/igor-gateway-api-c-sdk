﻿namespace Igor.Gateway.Dtos.Actuators
{
    public enum ActuatorCommandType
    {
        Command = 0,
        String = 1
    }
}