﻿using System.Collections.Generic;

namespace Igor.Gateway.Dtos.Actuators
{
    public class ActuatorCommandCollectionDto
    {
        public ActuatorCommandCollectionDto()
        {
            Commands = new List<ActuatorCommandDto>();
        }

        public IList<ActuatorCommandDto> Commands { get; set; }
    }
}