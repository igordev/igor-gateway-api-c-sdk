﻿using System;

namespace Igor.Gateway.Dtos.Actuators
{
    public static class ActuatorSubtypeExtensions
    {
        public static bool IsAcCircuit(this ActuatorSubtype subtype)
        {
            return subtype.IsOfSubtypeGroup(ActuatorSubtype.AcCircuit);
        }
        
        public static bool IsDcCircuit(this ActuatorSubtype subtype)
        {
            return subtype.IsOfSubtypeGroup(ActuatorSubtype.DcCircuit);
        }
        
        public static bool IsFan(this ActuatorSubtype subtype)
        {
            return subtype.IsOfSubtypeGroup(ActuatorSubtype.Fan);
        }
        
        public static bool IsValve(this ActuatorSubtype subtype)
        {
            return subtype.IsOfSubtypeGroup(ActuatorSubtype.Valve);
        }

        public static bool IsShade(this ActuatorSubtype subtype)
        {
            return subtype.IsOfSubtypeGroup(ActuatorSubtype.Shade);
        }
        
        public static bool IsPump(this ActuatorSubtype subtype)
        {
            return subtype.IsOfSubtypeGroup(ActuatorSubtype.Pump);
        }
        
        public static bool IsMotor(this ActuatorSubtype subtype)
        {
            return subtype.IsOfSubtypeGroup(ActuatorSubtype.Motor);
        }
        
        public static bool IsLedBeacon(this ActuatorSubtype subtype)
        {
            return subtype.IsOfSubtypeGroup(ActuatorSubtype.LedBeacon);
        }
        
        public static bool IsLock(this ActuatorSubtype subtype)
        {
            return subtype.IsOfSubtypeGroup(ActuatorSubtype.Lock);
        }
        
        public static bool IsRelay(this ActuatorSubtype subtype)
        {
            return subtype.IsOfSubtypeGroup(ActuatorSubtype.Relay);
        }

        public static bool IsOfSubtypeGroup(this ActuatorSubtype input, ActuatorSubtype subtype)
        {
            var low = Math.Floor((int) subtype / 100d) * 100;
            var high = low + 100;

            return (int) input >= low && (int) input < high;
        }
    }
}