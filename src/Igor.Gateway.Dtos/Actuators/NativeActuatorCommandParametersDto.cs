﻿namespace Igor.Gateway.Dtos.Actuators
{
    public class NativeActuatorCommandParametersDto
    {
        public string CommandType { get; set; }

        public object Value { get; set; }
    }
}