﻿using Newtonsoft.Json.Linq;

namespace Igor.Gateway.Dtos.Actuators
{
    public class ActuatorCommandDto
    {
        public string Name { get; set; }

        public ActuatorCommandType Type { get; set; }

        public object Parameters { get; set; }

        public T GetParameters<T>()
        {
            if (Parameters is T variable)
                return variable;

            if (Parameters is JObject jObject)
                return jObject.ToObject<T>();

            return default(T);
        }
    }
}