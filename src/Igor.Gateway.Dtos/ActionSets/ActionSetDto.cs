﻿using Igor.Gateway.Dtos.Common;

namespace Igor.Gateway.Dtos.ActionSets
{
    public class ActionSetDto : IEntityDto
    {
        public int Id { get; set; }

         public string Name { get; set; }

        public bool IsSystem { get; set; }

        public int? ExecuteNumberOfTimes { get; set; }

        public ExecuteType? ExecuteType { get; set; }

        public ActionConcurrencyType? ActionConcurrencyType { get; set; }

        public ExecutionStatus Status { get; set; }
    }
}