﻿using Igor.Gateway.Dtos.Actions;
using Igor.Sdk.Common.Queries;

namespace Igor.Gateway.Dtos.ActionSets
{
    public class ActionSetsSearchParameters : PagingQueryParameters
    {
        private const string ActionTypesKey = "actionTypes";

        public string Term
        {
            get => this["term"];
            set => this["term"] = value;
        }

        public ActionType[] ActionTypes
        {
            get => GetEnumArray<ActionType>(ActionTypesKey);
            set => SetEnumArray(ActionTypesKey, value);
        }

        public bool? HasActions
        {
            get => GetBool("hasActions");
            set => SetBool("hasActions", value);
        }
        
        public QuerySortDirection? SortDirection
        {
            get
            {
                if (string.IsNullOrEmpty(this["sortDir"]))
                    return null;

                return this["sortDir"] == "asc" ? QuerySortDirection.Ascending : QuerySortDirection.Descending;
            }
            set
            {
                if (value.HasValue)
                    this["sortDir"] = value.Value == QuerySortDirection.Ascending ? "asc" : "desc";
            }
        }
        
        public string GetActionTypesAsString()
        {
            return this[ActionTypesKey];
        }

        public string SortBy
        {
            get => this["sortBy"];
            set => this["sortBy"] = value;
        }

        public string GetSortDirectionAsString()
        {
            if (SortDirection.HasValue)
                return SortDirection.Value == QuerySortDirection.Ascending ? "asc" : "desc";

            return null;
        }
    }
}
