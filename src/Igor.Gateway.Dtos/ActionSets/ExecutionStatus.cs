﻿namespace Igor.Gateway.Dtos.ActionSets
{
    public enum ExecutionStatus
    {
        Stopped = 0,
        Running = 1,
        Cancelling = 2
    }
}
