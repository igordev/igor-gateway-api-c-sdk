﻿namespace Igor.Gateway.Dtos.ActionSets
{
    public enum ActionConcurrencyType
    {
        Sequential = 0,
        Parallel = 1
    }
}
