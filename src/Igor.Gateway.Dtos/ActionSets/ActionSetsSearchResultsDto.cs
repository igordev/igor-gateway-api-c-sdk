﻿namespace Igor.Gateway.Dtos.ActionSets
{
    public class ActionSetsSearchResultsDto
    {
        /// <summary>
        /// The action set ID
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The action set name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Number of times the action set will execute
        /// </summary>
        public int? ExecuteNumberOfTimes { get; set; }

        /// <summary>
        /// The action set execution type
        /// </summary>
        public ExecuteType? ExecuteType { get; set; }

        /// <summary>
        /// The action set concurrency type
        /// </summary>
        public ActionConcurrencyType? ActionConcurrencyType { get; set; }

        /// <summary>
        /// The action set execution status
        /// </summary>
        public ExecutionStatus Status { get; set; }

        /// <summary>
        /// Number of actions on action set
        /// </summary>
        public int NumberOfActions { get; set; }
    }
}