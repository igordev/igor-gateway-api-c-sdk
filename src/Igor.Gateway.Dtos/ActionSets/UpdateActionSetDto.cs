﻿namespace Igor.Gateway.Dtos.ActionSets
{
    /// <summary>
    /// The action set
    /// </summary>
    public class UpdateActionSetDto
    {
        /// <summary>
        /// The action set name
        /// </summary>
        public string Name { get; set; }

        public int? ExecuteNumberOfTimes { get; set; }

        public ExecuteType? ExecuteType { get; set; }

        public ActionConcurrencyType? ActionConcurrencyType { get; set; }

        public ActionSetDto AsActionSetDto()
        {
            return new ActionSetDto
            {
                Name = Name,
                ExecuteNumberOfTimes = ExecuteNumberOfTimes,
                ExecuteType = ExecuteType,
                ActionConcurrencyType = ActionConcurrencyType
            };
        }

        public static implicit operator ActionSetDto(UpdateActionSetDto dto)
        {
            return dto?.AsActionSetDto();
        }
    }
}