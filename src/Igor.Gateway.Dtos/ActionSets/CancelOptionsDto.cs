﻿namespace Igor.Gateway.Dtos.ActionSets
{
    public class CancelOptionsDto
    {
        public CancelType CancelType { get; set; }
    }
}
