﻿namespace Igor.Gateway.Dtos.ActionSets
{
    /// <summary>
    /// The action set
    /// </summary>
    public class CreateActionSetDto : UpdateActionSetDto
    {
        public bool? IsSystem { get; set; }

        public new ActionSetDto AsActionSetDto()
        {
            return new ActionSetDto
            {
                Name = Name,
                ExecuteNumberOfTimes = ExecuteNumberOfTimes,
                ExecuteType = ExecuteType,
                ActionConcurrencyType = ActionConcurrencyType,
                IsSystem = IsSystem ?? false
            };
        }

        public static implicit operator ActionSetDto(CreateActionSetDto dto)
        {
            return dto?.AsActionSetDto();
        }
    }
}