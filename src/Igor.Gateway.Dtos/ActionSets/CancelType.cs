﻿namespace Igor.Gateway.Dtos.ActionSets
{
    public enum CancelType
    {
        Immediate = 0,
        EndOfCurrentIteration = 1
    }
}
