﻿namespace Igor.Gateway.Dtos.Sensors
{
    public static class SensorDtoExtensions
    {
        public static UpdateSensorDto ToUpdateDto(this SensorDto sensorDto)
        {
            return new UpdateSensorDto
            {
                Name = sensorDto.Name,
                Properties = sensorDto.Properties,
                SystemProperties = sensorDto.SystemProperties
            };
        }
    }
}