﻿namespace Igor.Gateway.Dtos.Sensors
{
    public enum SensorEventDataType
    {
        Unknown = 0,

        /// <summary>
        /// Signed integral, 8 bits, range from -128 to 127
        /// </summary>
        Int8 = 1,

        /// <summary>
        /// Signed integral, 16 bits, range from -32,768 to 32,767
        /// </summary>
        Int16 = 2,

        /// <summary>
        /// Signed integral, 32 bits, range from -2,147,483,648 to 2,147,483,647
        /// </summary>
        Int32 = 3,

        /// <summary>
        /// Signed integral, 64 bits, range from -9,223,372,036,854,775,808 to 9,223,372,036,854,775,807
        /// </summary>
        Int64 = 4,

        /// <summary>
        /// Unsigned integral, 8 bits, range from 0 to 255
        /// </summary>
        UInt8 = 5,

        /// <summary>
        /// Unsigned integral, 16 bits, range from 0 to 65,535
        /// </summary>
        UInt16 = 6,

        /// <summary>
        /// Unsigned integral, 32 bits, range from 0 to 4,294,967,295
        /// </summary>
        UInt32 = 7,

        /// <summary>
        /// Unsigned integral, 64 bits, range from 0 to 18,446,744,073,709,551,615
        /// </summary>
        UInt64 = 8,

        /// <summary>
        /// Decimal number, 128 bits, range is at least -7.9 × 10^-28 to 7.9 × 10^28, with at least 28-digit precision
        /// </summary>
        Decimal = 9,

        /// <summary>
        /// UTF-8 encoded sequence of characters.
        /// </summary>
        String = 10,

        /// <summary>
        /// A Base64 encoded string.
        /// </summary>
        Base64String = 11,
    }
}