﻿namespace Igor.Gateway.Dtos.Sensors
{
    /// <summary>
    /// The sensor
    /// </summary>
    public class UpdateSensorDto : UpdateDeviceDto
    {
        /// <summary>
        /// The sensor's unit of measure
        /// </summary>
        public string UnitOfMeasure { get; set; }
    }
}
