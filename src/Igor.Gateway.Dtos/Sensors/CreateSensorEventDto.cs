﻿namespace Igor.Gateway.Dtos.Sensors
{
    /// <summary>
    /// The sensor event
    /// </summary>
    public class CreateSensorEventDto
    {
        /// <summary>
        /// Sensor event type
        /// </summary>
        public string EventType { get; set; }

        /// <summary>
        /// Data type of the provided Data field value. e.g. Int8, UInt32, Decimal, String, etc.
        /// </summary>
        public string DataType { get; set; }

        /// <summary>
        /// The sensor event data
        /// </summary>
        public string Data { get; set; }
    }
}
