﻿using Igor.Gateway.Dtos.Devices;

namespace Igor.Gateway.Dtos.Sensors
{
    public enum SensorSubtype
    {
        [UnitOfMeasure(UnitOfMeasure.None)]
        Sensor = 0,
        
        [UnitOfMeasure(UnitOfMeasure.Voltage)]
        Voltage = 100,

        [UnitOfMeasure(UnitOfMeasure.Ampere)]
        Ampere = 200,

        [UnitOfMeasure(UnitOfMeasure.Temperature)]
        Temperature = 300,

        [UnitOfMeasure(UnitOfMeasure.Temperature)]
        AirTemperature = 301,

        [UnitOfMeasure(UnitOfMeasure.Temperature)]
        WaterTemperature = 302,

        [UnitOfMeasure(UnitOfMeasure.Pressure)]
        Pressure = 400,

        [UnitOfMeasure(UnitOfMeasure.Pressure)]
        BarometricPressure = 401,

        [UnitOfMeasure(UnitOfMeasure.Percentage)]
        RelativeHumidity = 500,

        [UnitOfMeasure(UnitOfMeasure.Bel)]
        SoundLevel = 600,
        
        [UnitOfMeasure(UnitOfMeasure.Percentage)]
        Co2Level = 700,

        [UnitOfMeasure(UnitOfMeasure.GasConcentration)]
        GasConcentration = 800,

        [UnitOfMeasure(UnitOfMeasure.GasConcentration)]
        Co2Concentration = 801,

        [UnitOfMeasure(UnitOfMeasure.GasConcentration)]
        O2Concentration = 802,

        [UnitOfMeasure(UnitOfMeasure.GasConcentration)]
        CoConcentration = 803,

        [UnitOfMeasure(UnitOfMeasure.Quantity)]
        PeopleCount = 900,

        Efficiency = 901,

        [UnitOfMeasure(UnitOfMeasure.Lux)]
        Photocell = 1000,

        [UnitOfMeasure(UnitOfMeasure.Lux)]
        IndoorPhotocell = 1001,

        [UnitOfMeasure(UnitOfMeasure.Lux)]
        OutdoorPhotocell = 1002,

        [UnitOfMeasure(UnitOfMeasure.Lux)]
        SkylightPhotocell = 1003,

        [UnitOfMeasure(UnitOfMeasure.Percentage)]
        Utilization = 1100,

        [UnitOfMeasure(UnitOfMeasure.Percentage)]
        SpaceUtilization = 1101,

        Tag = 1200,

        AssetTag = 1201,

        NfcTag = 1202,

        VitalSign = 1300,

        HeartRate = 1301,

        RespiratoryRate = 1302,

        FallDetector = 1303,

        TvocConcentration = 1400,

        ParticulateMatter = 1500,

        Pm1_0Concentration = 1501,

        Pm2_5Concentration = 1502,

        Pm4_0Concentration = 1503,

        Pm10_0Concentration = 1504,

        Nfc = 1600,

        NfcReader = 1601
    }
}