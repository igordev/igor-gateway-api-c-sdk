﻿using System;
using System.Collections.Generic;
using Igor.Gateway.Dtos.Common;
using Igor.Gateway.Dtos.Devices;
using Igor.Gateway.Dtos.Tags;

namespace Igor.Gateway.Dtos.Sensors
{
    public class SensorDto : IDeviceDto, IEntityDto, ITagDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public SensorSubtype Subtype { get; set; }
        public string Properties { get; set; }
        public string SystemProperties { get; set; }
        public string Protocol { get; set; }
        public string ExternalId { get; set; }
        public DateTime DiscoveredDate { get; set; }
        public bool IsOnline { get; set; }
        public DateTime? IsOnlineChanged { get; set; }
        public bool IsQuarantined { get; set; }
        public DateTime? IsQuarantinedChanged { get; set; }
        public int? NetworkNodeId { get; set; }
        public int? DeviceNodeId { get; set; }
        public int? SpaceId { get; set; }
        public string UnitOfMeasure { get; set; }
        public IEnumerable<TagDto> Tags { get; set; }

        public static implicit operator UpdateSensorDto(SensorDto dto)
        {
            return dto?.ToUpdateDto();
        }
    }
}