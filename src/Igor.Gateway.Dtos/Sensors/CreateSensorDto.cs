﻿namespace Igor.Gateway.Dtos.Sensors
{
    /// <summary>
    /// Sensor
    /// </summary>
    public class CreateSensorDto : CreateDeviceDto
    {
        /// <summary>
        /// The external system's ID for this sensor
        /// </summary>
        public string ExternalId { get; set; }

        /// <summary>
        /// The type of sensor
        /// </summary>
        public SensorSubtype Subtype { get; set; }

        /// <summary>
        /// The sensor's unit of measure
        /// </summary>
        public string UnitOfMeasure { get; set; }
    }
}
