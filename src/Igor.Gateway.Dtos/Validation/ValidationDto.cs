using System.Collections.Generic;
using System.Linq;

namespace Igor.Gateway.Dtos.Validation
{
    public class ValidationDto
    {
        public const string ValidationErrorsMessage = "Validation errors.";
        public const int ValidationErrorCode = 10258;
        public string Message => ValidationErrorsMessage;
        public int ErrorCode => ValidationErrorCode;

        public List<ValidationError> ValidationErrors { get; set; }

        public ValidationDto(IEnumerable<ValidationError> validationErrors)
        {
            ValidationErrors = GroupErrors(validationErrors).ToList();
        }

        private static IEnumerable<ValidationError> GroupErrors(IEnumerable<ValidationError> errors)
        {
            if (errors == null)
                return Enumerable.Empty<ValidationError>();

            var groups = errors.GroupBy(p => p.Property);
            return groups.Select(g =>
                new ValidationError(
                    g.Key,
                    g.SelectMany(p => p.Messages).ToArray(),
                    g.SelectMany(p => p.ErrorCode).ToArray()
                )
            );
        }
    }
}
