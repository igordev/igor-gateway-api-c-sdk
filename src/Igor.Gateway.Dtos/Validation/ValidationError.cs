﻿namespace Igor.Gateway.Dtos.Validation
{
    public class ValidationError
    {
        public string[] Messages { get; set; }
        public string Property { get; set; }
        public int[] ErrorCode { get; set; }

        public ValidationError()
            : this(null, new string[0], new int[0])
        {

        }

        public ValidationError(string message)
            : this(null, new[] { message }, null)
        {
        }

        public ValidationError(string message, int errorCode)
            : this(null, new[] { message }, new[] { errorCode })
        {
        }

        public ValidationError(string property, string message)
            : this(property, new [] { message}, null)
        {
        }

        public ValidationError(string property, string message, int errorCode)
            : this(property, new[] { message }, new[] { errorCode })
        {
        }

        public ValidationError(string property, string[] messages)
            : this(property, messages, null)
        {
        }

        public ValidationError(string property, string[] messages, int[] errorCode)
        {
            Property = property;
            Messages = messages;
            ErrorCode = errorCode;
        }
    }
}
