﻿namespace Igor.Gateway.Dtos.Tags
{
    public class TagsSearchResultsDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int NumberOfEntities { get; set; }
    }
}
