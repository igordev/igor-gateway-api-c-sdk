﻿namespace Igor.Gateway.Dtos.Tags
{
    public class UpdateTagDto
    {
        /// <summary>
        /// The tag name
        /// </summary>
        public string Name { get; set; }
    }
}
