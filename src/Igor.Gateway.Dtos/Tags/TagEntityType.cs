﻿namespace Igor.Gateway.Dtos.Tags
{
    public enum TagEntityType
    {
        SpaceGroup = 1,
        Space = 2,
        Light = 3,
        MotionSensor = 4,
        LightSensor = 5,
        TemperatureSensor = 6,
        Relay = 7,
        WallControl = 8,
        DryContact = 9,
        Actuator = 10,
        Sensor = 11,
        DeviceNode = 12,
        NetworkNode = 13
    }
}

