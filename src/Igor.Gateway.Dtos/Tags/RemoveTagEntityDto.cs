﻿namespace Igor.Gateway.Dtos.Tags
{
    public class RemoveTagEntityDto
    {
        public string TagName { get; set; }
        public TagEntityType EntityType { get; set; }
        public int EntityId { get; set; }
    }
}
