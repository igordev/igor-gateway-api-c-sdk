﻿namespace Igor.Gateway.Dtos.Tags
{
    public class AddTagsDto
    {
        /// <summary>
        /// The tag names
        /// </summary>
        public string[] Names { get; set; }
    }
}
