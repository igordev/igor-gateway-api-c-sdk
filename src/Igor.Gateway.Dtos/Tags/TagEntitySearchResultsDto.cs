﻿namespace Igor.Gateway.Dtos.Tags
{
    public class TagEntitySearchResultsDto
    {
        public int Id { get; set; }
        public TagEntityType Type { get; set; }
        public string Name { get; set; }
    }
}
