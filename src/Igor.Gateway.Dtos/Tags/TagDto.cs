﻿using System.Collections.Generic;
using Igor.Gateway.Dtos.Common;

namespace Igor.Gateway.Dtos.Tags
{
    public class TagDto : IEntityDto
    {
        /// <summary>
        /// The tag ID
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The tag name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The entity types using the tag
        /// </summary>
        public ICollection<TagEntityType> EntityTypes { get; set; }
    }
}
