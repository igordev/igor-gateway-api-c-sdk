﻿using System.Linq;
using Igor.Sdk.Common.Queries;

namespace Igor.Gateway.Dtos.Tags
{
    public class TagEntitySearchParameters : PagingQueryParameters
    {
        private const string TagIdsKey = "tagIds";
        private const string EntityTypesKey = "entityTypes";

        public string Term
        {
            get => this["term"];
            set => this["term"] = value;
        }

        public int[] TagIds
        {
            get => GetIntArray(TagIdsKey);
            set => SetIntArray(TagIdsKey, value);
        }

        public TagEntityType[] EntityTypes
        {
            get => GetEnumArray<TagEntityType>(EntityTypesKey);
            set => SetEnumArray(EntityTypesKey, value);
        }

        public string GetTagIdsAsString()
        {
            return this[TagIdsKey];
        }

        public string GetEntityTypesAsString()
        {
            return this[EntityTypesKey];
        }

        public string GetEntityTypesAsIntString()
        {
            if (EntityTypes?.Any() ?? false)
            {
                return string.Join(",", EntityTypes.Select(a => (int)a));
            }

            return null;
        }

        public QuerySortDirection? SortDirection
        {
            get
            {
                if (string.IsNullOrEmpty(this["sortDir"]))
                    return null;

                return this["sortDir"] == "asc" ? QuerySortDirection.Ascending : QuerySortDirection.Descending;
            }
            set
            {
                if (value.HasValue)
                    this["sortDir"] = value.Value == QuerySortDirection.Ascending ? "asc" : "desc";
            }
        }

        public string SortBy
        {
            get => this["sortBy"];
            set => this["sortBy"] = value;
        }

        public string GetSortDirectionAsString()
        {
            if (SortDirection.HasValue)
                return SortDirection.Value == QuerySortDirection.Ascending ? "asc" : "desc";

            return null;
        }
    }
}
