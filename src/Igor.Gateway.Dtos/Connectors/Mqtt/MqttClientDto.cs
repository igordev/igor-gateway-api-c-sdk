﻿using Igor.Gateway.Dtos.Common;

namespace Igor.Gateway.Dtos.Connectors.Mqtt
{
    public class MqttClientDto : CreateMqttClientDto, IEntityDto
    {
        public int Id { get; set; }
    }
}
