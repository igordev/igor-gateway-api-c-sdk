﻿using System.Collections.Generic;

namespace Igor.Gateway.Dtos.Connectors.Mqtt
{
    /// <summary>
    /// The MQTT client
    /// </summary>
    public class UpdateMqttClientDto
    {
        public string Name { get; set; }
        
        /// <summary>
        /// Whether the MQTT client is enabled
        /// </summary>
        public bool IsEnabled { get; set; }

        public bool IsPublishEnabled { get; set; }

        public bool IsSubscribeEnabled { get; set; }

        public MqttVersion Version { get; set; }

        public MqttProtocol Protocol { get; set; }

        public string Host { get; set; }

        public int Port { get; set; }

        public string WebSocketPath { get; set; }

        public string ClientId { get; set; }

        public string TopicPrefix { get; set; }

        public bool HasStoredCredentials { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public int? MaxPendingMessages { get; set; }

        public MqttQos Qos { get; set; }

        public bool CleanSession { get; set; }

        public int? CommunicationTimeout { get; set; }

        public int? ReconnectDelay { get; set; }

        public int? KeepAliveInterval { get; set; }

        //public bool IsTlsEnabled { get; set; }

        //public MqttTlsVersion? TlsVersion { get; set; }

        //public MqttTlsCertificateType? TlsCertificateType { get; set; }

        //public string ClientCAFile { get; set; }

        //public string ClientCertificateFile { get; set; }

        //public string ClientKeyFile { get; set; }

        //public string ClientKeyPassphrase { get; set; }

        public string WillTopic { get; set; }

        public string WillPayload { get; set; }

        public MqttQos? WillQos { get; set; }

        public bool? IsWillRetained { get; set; }
        
        public MqttClientType ClientType { get; set; }

        /// <summary>
        /// The domain events associated with this MQTT client
        /// </summary>
        public List<string> DomainEvents { get; set; }
    }
}
