﻿namespace Igor.Gateway.Dtos.Connectors.Mqtt
{
    public enum MqttQos
    {
        AtMostOnce,
        AtLeastOnce,
        ExactlyOnce
    }
}
