﻿namespace Igor.Gateway.Dtos.Connectors.Mqtt
{
    public enum MqttClientType
    {
        Standard,
        AzureIotEdge
    }
}
