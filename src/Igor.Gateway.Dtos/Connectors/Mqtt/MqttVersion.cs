﻿namespace Igor.Gateway.Dtos.Connectors.Mqtt
{
    public enum MqttVersion
    {
        v31,
        v311,
        v50
    }
}
