﻿
namespace Igor.Gateway.Dtos.Connectors.Mqtt
{
    /// <summary>
    /// The MQTT client
    /// </summary>
    public class CreateMqttClientDto : UpdateMqttClientDto
    {
        public int ApplicationKeyId { get; set; }
    }
}
