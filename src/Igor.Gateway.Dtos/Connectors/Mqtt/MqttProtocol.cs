﻿namespace Igor.Gateway.Dtos.Connectors.Mqtt
{
    public enum MqttProtocol
    {
        Tcp,
        WebSockets
    }
}
