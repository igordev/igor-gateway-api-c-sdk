$projectDirectories = Get-ChildItem tests 

$hasFailures = $false
foreach ($projectDirectory in $projectDirectories) {
    dotnet test "$($projectDirectory.FullName)\$($projectDirectory.BaseName).csproj"
    if ($LASTEXITCODE -ne 0) {
        $hasFailures = $true
    }
}

if ($hasFailures) {
    Write-Host "Some tests failed"
    exit 1
}