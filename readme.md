[![Build status](https://ci.appveyor.com/api/projects/status/1d5t43q057nfc745/branch/master?svg=true)](https://ci.appveyor.com/project/igor/igor-gateway-api-c-sdk/branch/master)

# Purpose

This is the C# SDK for the Igor Gateway Software API.

## Before you begin

You will need an Application Key created with your Igor Gateway Software before you can take advantage of the Igor Gateway Software API. Consult your Igor Gateway Software User Guide for more information or contact Igor Support.

In addition, projects contained within this repository target .NET Standard 2.0. You will need to ensure your projects are .NET Standard 2.0 compatible to use this SDK. Consult the Microsoft [documentation](https://docs.microsoft.com/en-us/dotnet/standard/net-standard) for .NET Standard for more information.

## Good to know
In addition to this SDK, there is an accompanying project within this repository or an additional NuGet package you can reference to help get you started using the SDK.

* **Igor.Gateway.Api.Sdk.Config**
      * .NET Framework 4.7.2 and .NET Core 2.0 configuration implementations for `Igor.Gateway.Api.Sdk.Core.Config.ISdkConfig`
      * Install using NuGet: `PM> Install-Package Igor.Gateway.Api.Sdk.Config`

# Getting started

You can get started with the SDK in a couple of ways:

1. [Download](https://bitbucket.org/igordev/igor-gateway-api-c-sdk/downloads/) the SDK source code from this repository and add a reference within your project or
2. Install the NuGet package using the package manager in Visual Studio
```
PM> Install-Package Igor.Gateway.Api.Sdk
```
After you have installed the package, add a reference to the Igor.Gateway.Api.Sdk to your project.

## Creating a service
Once you have a reference to the SDK in your project, you can easily get started by creating a new instance of any of the service classes in `Igor.Gateway.Api.Sdk.Apis`.

### Supplying your API key manually
```
class Program
{
    static void Main(string[] args)
    {
        var service = new SpaceService("[your API key here]");
    }
}
```
If you only supply the API key, the SDK assumes your application is running on the same machine as the Igor Gateway Software API and uses `localhost` for a location.

### Providing the API address
```
class Program
{
    static void Main(string[] args)
    {
        var service = new SpaceService("[your API key here]", "http://192.168.0.10");
    }
}
```
If your application is hosted on separate computer from the Igor Gateway Software API, you can provide the base URL of the gateway API as an additional constructor argument. `http://` is required.


## Supplying configuration values from your application configuration
You can also specify your API key and address of your gateway within your application configuration files. The `Igor.Gateway.Api.Sdk.Config` library contains default implementations for .NET Framework 4.6.1 and ASP.NET Core 2.0. Simply add two entries to either of your platform configuration files with the following keys:
* `igor:api-key`
* `igor:api-address`

### .NET Framework 4.7.2 web/app.config
```
<appSettings>
  <add key="igor:api-key" value="[your API key here]" />
  <add key="igor:api-address" value="http://192.168.0.10"/>
</appSettings>
```

### ASP.NET Core 2.0 appsettings.json
```
{
  "igor:api-key": "[your API key here]",
  "igor:api-address": "http://192.168.0.10",

  "subsection": {
    "suboption1": "subvalue1_from_json"
  },
  "wizards": [
    {
      "Name": "Gandalf",
      "Age": "1000"
    },
    {
      "Name": "Harry",
      "Age": "17"
    }
  ]
}
```

## Using the SDK with StructureMap
You can easily register the previously mentioned configuration implementations to your StructureMap registration (or IoC framework of choice) by specifying which type you would like to use based on your platform.
```
var container = new Container(_ =>
{
    For<ISdkConfig>().Use<IgorNetConfig>();  //net461
    For<ISdkConfig>().Use<IgorCoreConfig>(); //netcoreapp2.0

    _.Scan(x =>
    {
        x.AssembliesAndExecutablesFromApplicationBaseDirectory(
            assembly => assembly.FullName.ToLowerInvariant().Contains("igor"));
        x.WithDefaultConventions();
    });
});
```